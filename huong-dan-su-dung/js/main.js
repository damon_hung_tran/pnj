var current_slide = 0;
var total_slide = 0;
var _h = 0;
var scrollReady = true;
var _slide;
$(document).ready(function() {
	total_slide = $('.slide').length;
	_slide = $('.slide');
	$("body").css({
		overflow: 'hidden'
	});
	$('.slide img.slideimage').each(function() {
		$(this).css({
			height: $(window).innerHeight()
		});
	});
	_h = $(window).innerHeight();

	$("body").bind('mousewheel', function(delta, aS, aQ, deltaY) {
		delta.preventDefault();
		if (deltaY > 0) {
			scrollPrev();
		} else {
			if (deltaY < 0) {
				scrollNext();
			}
		}
		return false;
	});
	
	$('.slide a').click(function(e){
		e.preventDefault();
		$('.popup').css({
			width: $(window).width(),
			height: $(window).height()
		});
		$('.popup_contain').css({
			height: $(window).height() - 100,
			width: 1024,
			left: ($(window).width() - 1024)/2,
			top: 50
//			top: ($(window).height() - $('.popup_contain').height())/2 -100 + $("body").scrollTop(),
			
		});
		$('.popup_contain .contain').html($('.maincontain').html());
		$('.popup_contain .contain').css({
			height: $(window).height() - 100,
		});
		$('.popup_contain .contain img').css({
			height: 'auto',
			width: '100%'
		});
		$('.popup_contain .contain a').click(function(e){
			e.preventDefault();
		});
		scrollReady = false;
		$('.popup').fadeIn(500);
	});
	
	$('a.btclose').click(function(e){
		e.preventDefault();
		$('.popup').fadeOut(500);
		scrollReady = true;
	});
});

$(window).resize(function() {
	$('.slide img.slideimage').each(function() {
		$(this).css({
			height: $(window).innerHeight()
		});
	});
});

function scrollNext() {
	if (current_slide < total_slide - 1 && scrollReady == true) {
		current_slide++;
		scrollReady = false;
		$("html, body").animate(
				{scrollTop: $(_slide[current_slide]).position().top},
		750,
				'easeInOutExpo',
				function() {
					scrollReady = true;
				}

		);
	}
}

function scrollPrev() {
	if (current_slide > 0 && scrollReady == true) {
		current_slide--;
		scrollReady = false;
		$("html, body").animate(
				{scrollTop: $(_slide[current_slide]).position().top},
		750,
				'easeInOutExpo',
				function() {
					scrollReady = true;
				}

		);
	}
}