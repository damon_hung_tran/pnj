﻿(function (window) {

    if (typeof window.mntrSearchMngr !== "undefined") {
        return;
    }
    var $ = typeof mntrjQuery !== "undefined" ? mntrjQuery : jQuery;
    try {
        var secret = {
            "searchTermReg": /[#?&](?:q|p|w|ST|wd|search_query|field-keywords|search|keywords|kw|_nkw|searchtext|keywords|destCity|destination)=([^&#]+)/i,
            "domain": null,
            "searchFeed": "http://search.wizebar.com/srch/search.srf?prvdr=all",
            "lastSearchTerm": "",
            "userDataObject": {},
            "currentResults": [],
            "cachedScriptPromises":{},
            "busy":false,
            "currentProvider":"",
            "currentLogic":[],
            "dataFlashKey": "mpvDataSearch",
            "init": function () {
                var that = this;
                Logicsmngr.onModuleLoaded();
                $.cachedGetScript = function (url) {
                    secret.cachedScriptPromises[url] = secret.cachedScriptPromises[url] || $.ajax(url, {
                        dataType: 'jsonp',
                        error: function (jqXHR, textStatus) {
                            typeof callback === "function" ? callback() : false;
                            mpvInterface.onlineReport("searchMngr", "ajaxCallFailed: " + textStatus);
                        }
                    });
                    return secret.cachedScriptPromises[url];
                };
                $(document).on("click", "a", function () {
                    var term = that.searchTermReg.test(location.href);
                    if (term) {
                        secret.domain = that.getDomain(location.href);
                        secret.lastSearchTerm = that.searchTermReg.exec(location.href)[1];
                        //secret.lastSearchTerm = secret.lastSearchTerm.replace("+", " ");
                        secret.buildUserDataObject({ "searchTerm": that.lastSearchTerm });
                    }
                });
            },
            "buildUserDataObject": function (params) {
                try {
                    secret.userDataObject = secret.getFlashObject(secret.dataFlashKey) || {};//"mpvDataSearch"
                    secret.userDataObject[secret.domain] = secret.userDataObject[secret.domain] || [];
                    secret.userDataObject[secret.domain].push(params);
                    secret.setFlashObject(mpvJSON.stringify(secret.userDataObject), secret.dataFlashKey);
                }
                catch (e) {
                }
            },
            //#region tools
            "getDomain": function (url) {
                try {
                    var domain = /^(?:htt|ft)ps?:\/\/(?:www[.])?([^\/:?#]+)/.exec(url);
                    domain = domain && domain[1];
                    return domain;
                }
                catch (e) {
                }
            },
            "getQueryParam": function (name, queryString) {
                var match = new RegExp('(?:[?&]|^)' + name + '=([^&]*)', "i").exec(queryString);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            },
            //#endregion tools
            //#region flash
            "setFlashObject": function (value, key) {
                try {
                    mpvStorageMngr.setItem(key, value);
                    mpvInterface.onlineReport("searchMngr", "resultsSaved");
                }
                catch (e) {
                    mpvInterface.onlineReport("searchMngr", "resultsSavedError");
                }
            },
            "clean": function () {
                secret.lastSearchTerm = "";
                mpvStorageMngr.clearItem(secret.dataFlashKey);
            },
            "getFlashObject": function (key) {
                return mpvStorageMngr.getItem(key);
            }
            //#endregion

        },

        mngr = {
            "getResults": function () {
                mpvStorageMngr.clearItem(secret.dataFlashKey);
                return secret.currentResults;
            },
            "getLogic":function() {
                return secret.currentLogic;
            },
            "setResults": function (term, callback) {
                try {
                    
                    var url = secret.searchFeed + "&q=" + term + "&pblshrId=" + mpvInterface.getParam("prdct");
                    $.when($.cachedGetScript(url)).then(function (json) {
                        if (secret.currentResults.length === 0){

                            secret.currentResults = json && json.results && json.results["RESULTS"] || [];
                            secret.currentLogic.push(json && json.provider);
                            mpvInterface.onlineReport("searchMngr", "resultsCaptured");
                        }
                        
                        typeof callback === "function" && callback(json);
                    });
                  
                }
                catch (e) {
                    mpvInterface.onlineReport("searchMngr", "resultsCapturedError");
                }
            },
            "getLastSearchData": function () {
                try {
                    var userData = secret.getFlashObject(secret.dataFlashKey) || "{}";//"mpvDataSearch"
                    userData = mpvJSON.parse(userData);
                    var lastSearchObject = {};
                    $.each(userData, function (domain, val) {
                        lastSearchObject = val[val.length - 1] || {};
                    });
                    return lastSearchObject;
                }
                catch (e) {
                }
            }

        };

        if (typeof window.mntrSearchMngr === "undefined") {
            window.mntrSearchMngr = mngr;
            secret.init();
        }

    }

    catch (e) {
        Logicsmngr.onModuleLoaded();
    }

})(window);