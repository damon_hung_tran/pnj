﻿var lang = "";
var strMsg = "";
var obj = null;
var error = 0;
var validreg = /^([\w.]+)$/;
function refeshsess(obj) {
    $(".captchaImg").attr("src", Webroot +"captcha.php?v=" + Math.floor(Math.random() * 11));
    return false;
}
function ShowHide(showId, hidenID) {
    $(hidenID).hide();
    $(showId).show();
}
function checkformsearch(form) {
    if (($(form.key).val() == "") || ($(form.key).val() == $(form.key).attr("lang"))) {
        jAlert(languages.search_enter, languages.message_box_title, function (e) { $(form.key).focus(); });
        return false;
    }
    return true;
}
function showPersonalDetail(obj, groupname) {
   /* var innerID = $(obj).attr("href");
    $.post(Webroot + "doajax.aspx", { "op": "PersonalDetail", "id": $(obj).attr("rel"), "lang": lang }, function (respone) {
        if (respone.ID == "1") {
            $(innerID).html(respone.Content);
            $(groupname + ' .copy').hide();
            $(groupname + ' .detail').show();

            $(groupname + ' .detail').find('.btn_close').click(function () {
                $(groupname + ' .copy').show();
                $(groupname + ' .detail').hide();
                return false;
            });
            $(groupname + ' .scroll').tinyscrollbar();

        }
    }, "json");*/

    var innerID = $(obj).attr("href");
    $.post(Webroot + "doajax.aspx", { "op": "PersonalDetail", "id": $(obj).attr("rel"), "lang": lang }, function (respone) {
        if (respone.ID == "1") {
            $.colorbox({ html: respone.Content,
                onComplete: function () {
                    $.colorbox.resize();
                    $('.detailCopy').tinyscrollbar();
                }
            });
        }
    }, "json");
    return false;

}
function ResponeDetailShow(obj, id) {
    $.post(Webroot + "doajax.aspx", { "op": "ResponeDetail", "id": id, "lang": lang }, function (respone) {
        if (respone.ID == "1") {
            //$.colorbox({ html: respone.Content });
            $.colorbox({html: respone.Content, onComplete: function () {
                var obj = $('#cboxLoadedContent .scroll');
                if (obj != undefined && obj != null){
                    $('#cboxLoadedContent .scroll').tinyscrollbar();
                }
                //$.colorbox.resize();
            }
            });
        }
    }, "json");
}
function LoadNewsListHP(obj) {
    $.post(Webroot + "news.aspx", { "op": "LoadNewsList", "lang": lang }, function (respone) {
        if (respone != "") {
            $("#list-items-content").html(respone);
            $("#sec-news .breadcrumb a").removeClass("active");
            $(obj).addClass("active");
        }
    });
    return false;
}
function LoadPressListHP(obj) {
    $.post(Webroot + "news.aspx", { "op": "LoadPressList", "lang": lang }, function (respone) {
        if (respone != "") {
            $("#list-items-content").html(respone);
            $("#sec-news .breadcrumb a").removeClass("active");
            $(obj).addClass("active");
        }
    });
    return false;
}

function NewsPaging(obj, page, catid,tabid) {
    $.post(Webroot + "news.aspx", { "op": "loadnewspaging", "page": page, "lang": lang, "cid": catid }, function (respone) {
        if (respone.ID == 1) {
            $("#listnews-items-other").html(respone.Content);
        }
    }, "json");
    return false;
}
function showNewsDetail(obj) {
    $.post(Webroot + "news.aspx", { "op": "newsDetail", "id": $(obj).attr("rel"), "lang": lang }, function (respone) {
        if (respone != "") {
            $.colorbox({ html: respone,
                onComplete: function () {
                   // $.colorbox.resize();
                    //$('#cboxLoadedContent .scroll').tinyscrollbar();


                    var ginScroll = $('#cboxLoadedContent .scroll');
                    ginScroll.tinyscrollbar();

                    $('#cboxLoadedContent img').each(function(){
                        var getSrcImg = $(this).attr('src');
                        var Img = new Image();

                        Img.onerror = function(){
                            return false;
                        }
                        var src = getSrcImg;
                        Img.onload = function(){
                            var imgHeight = $(this).height();
                            $(this).height(imgHeight);

                            ginScroll.tinyscrollbar_update();

                            Img.onload=function(){

                            };
                        }
                        Img.src = src;
                    });


                    //$(window).load(function() {
                        //alert('loaded');
                    //});



                }
            });
        }
    });
    return false;
}
function ShowBranAttrProduct(obj, id) {
    $.post(Webroot + "doajax.aspx", { "op": "LoadProductInAtrr", "id": id, "lang": lang }, function (respone) {
        if (respone != "") {
            $("#allBrands #brandDetail").html(respone);
        }
    });
}
function ShowProductDetail(obj, catid) {
    $.post(Webroot + "doajax.aspx", { "op": "LoadProductBrand", "id": $(obj).attr("href"), "lang": lang }, function (respone) {
        if (respone != "") {
            $.colorbox({ html: respone, onComplete: function () {
                if (t) clearTimeout(t);
                tempRotateI = 0;
                tempRotateY = 0;
                $('.brand_detailpage .paging').html('')
                rotateImg();
                rotateImg_paging();

                var obj = $('#cboxLoadedContent .scroll');
                if (obj != undefined && obj != null) {
                    $('#cboxLoadedContent .scroll').tinyscrollbar();
                }
            }, width: 990, height: 490
            });
            /*$('.brand_detailpage').html('');
            $("#brandid_" + catid + " .brand_detailpage").html(respone);
            $('.secHome_bg').fullBg();

            if (t) clearTimeout(t);
            tempRotateI = 0;
            tempRotateY = 0;
            $('.brand_detailpage .paging').html('');
            rotateImg();
            rotateImg_paging();*/

            //Load video TVC
            /*$('.link_tvc').click(function (event) {
                event.preventDefault();
                $('#tvcLayer').show();
                var tvcmp4 = $(this).find("input[name='link_tvcmp4']").val();
                if (!detectDevice) {
                    var flashvars = { video_url: $(this).attr("rel"), auto_play: 1 };
                    var params = {};
                    var attributes = {};
                    swfobject.embedSWF("swf/videoplayer.swf", "tvcPlayer", "530", "330", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
                }
                else {
                    jwplayer("tvcPlayer").setup({
                        file: tvcmp4,
                        image: "video/solite.jpg"
                    });
                }
                return false;
            });
            $('#tvcLayer').click(function () {
                $('#tvcLayer').html('<div id="tvcPlayer"></div>').hide();
                return false;
            });*/

        }
    });
    return false;
}
function ShowProductItemsDetail(obj) {
    $.post(Webroot + "doajax.aspx", { "op": "LoadProductDetail", "id": $(obj).attr("href"), "lang": lang }, function (respone) {
        if (respone != "") {
            $("#productlist_items_detail").html(respone).show();
			$('.brand_detailpage').addClass('blur');

			var countChild = $('#productlist_items_detail li').length;
				countChild = countChild*310;
			$('#productlist_items_detail ul').width(countChild);

			$('.scroll').tinyscrollbar({ axis: 'x', scroll: false });
        }
    });
}
//User function
function forgetPassword(obj) {
    var email = $("#mem_emailforget").val();
    if (email == "") {
        jAlert(languages.enter_email, languages.message_box_title);
        $("#mem_emailforget").focus();
        return false;
    }
    else if (!isEmail(email)) {
        jAlert(languages.enter_email_invalid, languages.message_box_title);
    }
    else {
        var htmlold = $("#memberForget").html();
        $("#btnmemberForget").hide();
        $("#memberForget").html("<span class=\"sending_email\">" + languages.message_sending + "</span>");
        $.post(Webroot + "login.aspx", { "op": "forgetpass", "email": email }, function (result) {
            if (result.id==1) {
                $.colorbox.close();
                jAlert(result.error, languages.message_box_title);
            }
            else {
                $("#memberForget").html(htmlold);
                $("#btnmemberForget").show();
                jAlert(result.error, languages.message_box_title);
            }
            $("#btnmemberForget").removeAttr("disabled");
        },"json");
    }
    return false;
}
function popupLogin(p) {
    $.colorbox({ href: Webroot + "login.aspx?p=" + p,
        onComplete: function () {
            $.colorbox.resize();
        },
        onClosed: function () {
        }
    });
}
function loginform(form) {
    if ($(form.username).val() == "") {
        jAlert(languages.enter_username, languages.message_box_title, function (e) { $(form.username).focus(); });
        return false;
    }
    if ($(form.password).val() == "") {
        jAlert(languages.enter_password, languages.message_box_title, function (e) { $(form.password).focus(); });
        return false;
    }
    if ($(form.captcha).val() == "") {
        jAlert(languages.enter_security_code_invalid, languages.message_box_title, function (e) { $(form.captcha).focus(); });
        return false;
    }
    disableform(form, true);
    $.post(Webroot + "login.aspx", { "username": $(form.username).val(), "password": $(form.password).val(), "op": "login", "captcha": $(form.captcha).val() }, function (result) {
        if (result.ID == 1 && currModule == "myaccount") {
            window.location.href = Webroot + "my-account.html";
        }
        else {
            if (result.ID == 1) {
                $(form).get(0).reset();
                $.colorbox.close();
                $(".links .open_popup").remove();
                $(".userLogin").html(languages.welcome + ", " + result.Content + " - <a class=\"logout\" href=\"" + Webroot + "login.aspx?op=logout\">" + languages.logout + "</a>");
            }
            jAlert(result.Error, languages.message_box_title);
            disableform(form, false);
        }
    }, "json");
    return false;
}
//register
function registeruser(form) {
    if ($(form.username).val() == "") {
        strMsg += "<p>" + languages.enter_username_register + "</p>";
        obj = obj == null ? form.username : obj;
        error = error + 1;
    }
    else if (!validreg.test($(form.username).val())) {
        strMsg += "<p>" + languages.enter_username_invalid + "</p>";
        obj = obj == null ? $(form.username) : obj;
        error = error + 1;
    }
    if ($(form.firstName).val() == "") {
        strMsg += "<p>"+languages.enter_fullname+ "</p>";
        obj = obj == null ? form.firstName : obj;
        error = error + 1;
    }
    if ($(form.select_birth_day).val() == 0 || $(form.select_birth_month).val() == 0 || $(form.select_birth_year).val() == 0) {
        strMsg += "<p>" + languages.enter_birthday + "</p>";
        obj = obj == null ? form.select_birth_day : obj;
        error = error + 1;
    }
    if ($(form.email).val() == "") {
        strMsg += "<p>" + languages.enter_email + "</p>";
        obj = obj == null ? form.email : obj;
        error = error + 1;
    }
    else if ($(form.email).val() != "" && !isEmail($(form.email).val())) {
        strMsg += "<p>" + languages.enter_email_invalid + "</p>";
        obj = obj == null ? form.email : obj;
        error = error + 1;
    }
    if ($(form.password).val() == "") {
        strMsg += "<p>" + languages.enter_password + "</p>";
        obj = obj == null ? form.password : obj;
        error = error + 1;
    }
    else if ($(form.confirmpassword).val() == "") {
        strMsg += "<p>" + languages.password_again + "</p>";
        obj = obj == null ? form.confirmpassword : obj;
        error = error + 1;
    }
    else if ($(form.password).val() != $(form.confirmpassword).val()) {
        strMsg += "<p>" + languages.password_invaild + "</p>";
        obj = obj == null ? form.password : obj;
        error = error + 1;
    }
    if ($(form.address).val() == "") {
        strMsg += "<p>" + languages.enter_address + "</p>";
        obj = obj == null ? form.address : obj;
        error = error + 1;
    }
    if (strMsg != "") {
        strMsg = error > 1 ? '<div class="multi-msg">' + strMsg + '</div>' : strMsg;
        jAlert(strMsg, languages.message_box_title, function () {
            $(obj).focus();
        });
        strMsg = "";
        return false;
    }
    else {
        if (!$(form.acceptTerm).is(":checked")) {
            jAlert("<P>" + languages.register_accept + "</P>", '', function () {
                $(form.acceptTerm).focus();
            });
            return false;
        }
        var param = $(form).serialize();
        disableform(form, true);
        $.post(Webroot + "register.aspx", param, function (result) {
            if (result.ID!="") {
                $(form).get(0).reset();
                $.colorbox.close();
                $(".links .open_popup").remove();
                $(".userLogin").html("Xin chào, " + result.Content + " - <a class=\"logout\" href=\"" + Webroot + "login.aspx?op=logout\">Thoát</a>");
            }
            disableform(form, false);
            jAlert(result.Error, languages.message_box_title);
        }, "json");
    }
    return false;
}
//Candidate
function checkCandidates(form) {
    strMsg = "";
    if ($(form.name).val() == "") {
        strMsg += "<p>" + languages.enter_fullname + "</p>";
        obj = obj == null ? form.name : obj;
        error = error + 1;
    }
    if ($(form.select_birth_day).val() == 0 || $(form.select_birth_month).val() == 0 || $(form.select_birth_year).val() == 0) {
        strMsg += "<p>" + languages.select_birthday + "</p>";
        obj = obj == null ? form.select_birth_day : obj;
        error = error + 1;
    }
    if ($(form.email).val() == "") {
        strMsg += "<p>" + languages.enter_email + "</p>";
        obj = obj == null ? form.email : obj;
        error = error + 1;
    }
    else if (!isEmail($(form.email).val())) {
        strMsg += "<p>" + languages.enter_email_invalid + "</p>";
        obj = obj == null ? form.email : obj;
        error = error + 1;
    }
    if ($(form.phone).val() == "") {
        strMsg += "<p>" + languages.enter_phone + "</p>";
        obj = obj == null ? form.phone : obj;
        error = error + 1;
    }
    else if (isNaN($(form.phone).val()) || $(form.phone).val().length >= 15 || $(form.phone).val().length <= 5) {
        strMsg += "<p>" + languages.enter_phone_invalid + "</p>";
        obj = obj == null ? form.phone : obj;
        error = error + 1;
    }
    if ($(form.gender).val() == "") {
        strMsg += "<p>" + languages.select_sex + "</p>";
        obj = obj == null ? form.gender : obj;
        error = error + 1;
    }
    if ($(form.education).val() == "") {
        strMsg += "<p>" + languages.enter_education_level + "</p>";
        obj = obj == null ? form.education : obj;
        error = error + 1;
    }
    if ($(form.cv).val() == "") {
        strMsg += "<p>" + languages.enter_file_cv + "</p>";
        obj = obj == null ? form.cv : obj;
        error = error + 1;
    }
    else if (!checkfile($(form.cv))) {
        strMsg += "<p>" + languages.enter_cv_invalid + "</p>";
        obj = obj == null ? form.cv : obj;
        error = error + 1;
    }
    if ($(form.cv_letter).val() == "") {
        strMsg += "<p>" + languages.enter_file_letter + "</p>";
        obj = obj == null ? form.cv_letter : obj;
        error = error + 1;
    }
    else if (!checkfile($(form.cv_letter))) {
        strMsg += "<p>" + languages.enter_cv_letter_invalid + "</p>";
        obj = obj == null ? form.cv_letter : obj;
        error = error + 1;
    }
    if ($(form.captcha).val() == "") {
        strMsg += "<p>" + languages.enter_security_code + "</p>";
        obj = obj == null ? form.captcha : obj;
        error = error + 1;
    }
    if (strMsg != "") {
        strMsg = error > 1 ? '<div class="multi-msg">' + strMsg + '</div>' : strMsg;
        jAlert(strMsg, 'Thông báo', function () {
            $(obj).focus();
        });
        strMsg = "";
        return false;
    }
    else {
        //return true;
        $(form).submit();
    }
}
function loadPageJobs(obj, page) {
    var param = { "op": "loadjobslist", "lang": lang, "page": page, "sort_col": $("#columnSort").val(), "sort": $("#sortDirection").val() };
    $.post(Webroot + "doajax.aspx", param, function (respone) {
        if (respone.ID == 1) {
            $("#listjobs").html(respone.Content);
            if (respone.Page != null && respone.Page != "") {
                $(".paging").html(respone.Page);
            }
        }
        else {
            $("#listjobs").html(respone.Content);
        }
    }, "json");
}
function isEmail(s) {
    if (s.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}$/) != -1)
        return true;

    return false;
}

function checkfile(obj) {
    var filename = $(obj).val();
    var valid_extensions = /(\.doc|\.docx|\.odt|\.pdf)$/i;
    if(valid_extensions.test(filename)){
        return true;
    }
    return false;
}

function checkcontact(form) {
    strMsg = "";
    if ($(form.name).val() == "") {
        strMsg += "<p>" + languages.enter_fullname + "</p>";
        obj = obj == null ? form.name : obj;
        error = error + 1;
    }
    if ($(form.phone).val() == "") {
        strMsg += "<p>" + languages.enter_phone + "</p>";
        obj = obj == null ? form.phone : obj;
        error = error + 1;
    }
    else if (isNaN($(form.phone).val()) || $(form.phone).val().length >= 15 || $(form.phone).val().length <= 5) {
        strMsg += "<p>" + languages.enter_phone_invalid + "</p>";
        obj = obj == null ? form.phone : obj;
        error = error + 1;
    }
    if ($(form.email).val() == "") {
        strMsg += "<p>" + languages.enter_email + "</p>";
        obj = obj == null ? form.email : obj;
        error = error + 1;
    }
    else if (!isEmail($(form.email).val())) {
        strMsg += "<p>" + languages.enter_email_invalid + "</p>";
        obj = obj == null ? form.email : obj;
        error = error + 1;
    }
    if ($(form.sendcompany).val() == 0) {
        strMsg += "<p>" + languages.select_company_send + "</p>";
        obj = obj == null ? form.company : obj;
        error = error + 1;
    }
    if ($(form.content).val() == "") {
        strMsg += "<p>" + languages.enter_content_contact + "</p>";
        obj = obj == null ? form.content : obj;
        error = error + 1;
    }
    if ($(form.captcha).val() == "") {
        strMsg += "<p>" + languages.enter_security_code + "</p>";
        obj = obj == null ? form.captcha : obj;
        error = error + 1;
    }
    if (strMsg != "") {
        strMsg = error > 1 ? '<div class="multi-msg">' + strMsg + '</div>' : strMsg;
        jAlert(strMsg, languages.message_box_title, function () {
            $(obj).focus();
        });
        strMsg = "";
        return false;
    }
    else {
        //return true;
        var param = $(form).serialize();
        $.post(Webroot + "Contact.aspx", param, function (result) {
            if (result.ID == 1) {
                $(form).get(0).reset();
            }
            jAlert(result.Error, languages.message_box_title);
        }, "json");
    }
}

function checkfaqs(form) {
    strMsg = "";
    obj = null;
    if ($(form.fullname).val() == "") {
        strMsg += "<p>" + languages.enter_fullname + "</p>";
        obj = obj == null ? form.fullname : obj;
        error = error + 1;
    }
    if ($(form.phone_number).val() == "") {
        strMsg += "<p>" + languages.enter_phone + "</p>";
        obj = obj == null ? form.phone_number : obj;
        error = error + 1;
    }
    else if (isNaN($(form.phone_number).val()) || $(form.phone_number).val().length >= 15 || $(form.phone_number).val().length <= 5) {
        strMsg += "<p>" + languages.enter_phone_invalid + "</p>";
        obj = obj == null ? form.phone_number : obj;
        error = error + 1;
    }
    if ($(form.email_address).val() == "") {
        strMsg += "<p>" + languages.enter_email + "</p>";
        obj = obj == null ? form.email_address : obj;
        error = error + 1;
    }
    else if (!isEmail($(form.email_address).val())) {
        strMsg += "<p>" + languages.enter_email_invalid + "</p>";
        obj = obj == null ? form.email_address : obj;
        error = error + 1;
    }
    if ($(form.category).val() == "0" || $(form.category).val().length<=0) {
        strMsg += "<p>" + languages.enter_category + "</p>";
        obj = obj == null ? form.category : obj;
        error = error + 1;
    }
    if ($(form.content).val() == "") {
        strMsg += "<p>" + languages.enter_content_question + "</p>";
        obj = obj == null ? form.content : obj;
        error = error + 1;
    }
    if ($(form.captcha).val() == "") {
        strMsg += "<p>" + languages.enter_security_code + "</p>";
        obj = obj == null ? form.captcha : obj;
        error = error + 1;
    }
    if (strMsg != "") {
        strMsg = error > 1 ? '<div class="multi-msg">' + strMsg + '</div>' : strMsg;
        jAlert(strMsg, languages.message_box_title, function () {
            $(obj).focus();
        });
        strMsg = "";
        return false;
    }
    else {
        var param = $(form).serialize();
        $.post(Webroot + "faqs-post.aspx", param, function (result) {
            if (result.ID == 1) {
                $(form).get(0).reset();
            }
            jAlert(result.Error, languages.message_box_title);
        }, "json");
    }
}


function TimeLineYearDetail(obj) {
    $.post(Webroot + "doajax.aspx", { "op": "TimeLineYearDetail", "id": $(obj).attr("href"), "lang": lang }, function (respone) {
        $.colorbox({ html: respone, onComplete: function () {
            scroll();
            $.colorbox.resize();
        }
        });
    });
    return false;
}
//Report
function LoadReports(obj) {
    $.post(Webroot + "bao-cao-tai-chinh-ajax.aspx", { "op": "LoadReportsFinancial", "id": $(obj).val(), "lang": lang }, function (respone) {
        if (respone!="") {
            $("#listReports").html(respone);
        }
    });
    return false;
}
function LoadReportsPage(obj, page, cid) {
    $.post(Webroot + "bao-cao-tai-chinh-ajax.aspx", { "op": "LoadReportsPage", "year": $("#ListYearBC").val(), "page": page, "catid": cid }, function (respone) {
        if (respone != "") {
            $("#tablelist"+cid).html(respone);
        }
    });
    return false;
}
//Cổ đông
function LoadReportsCoDong(obj, page) {
    $.post(Webroot + "thong-tin-co-dong.aspx", { "op": "LoadReportYear", "year": $("#cbYearCD").val(), "page": page }, function (respone) {
        if (respone.Content != "") {
            $("#listReports").html(respone.Content);
        }
    },"json");
    return false;
}


//Event
function LoadEventForYear(obj) {
    $.post(Webroot + "doajax.aspx", { "op": "LoadEventsYear", "id": $(obj).val(), "lang": lang }, function (respone) {
        if (respone.ID == 1) {
            $("#listEvents").html(respone.Content);
        }
    }, "json");
    return false;
}
function UpdateUserInfo(obj, param) {
    $(obj).parent().hide().parent().find('span').show().next().show();
    param.push({
        name: "op",
        value: "UserInfoUpdate"
    });
    $.post(Webroot + "myaccount.aspx", param, function (respone) {
        if (respone.ID == 1) {
            if (respone.Content != "") {
                $(obj).parent().parent().find(".lblname").html(respone.Content);
            }
        }
        jAlert(respone.Error);
    }, "json");
    true
}
function clear_form_elements(ele) {
    $(ele).find(':input').each(function () {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}

function disableform(ele, isenable) {
    $(ele).find(':input').each(function () {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
            case 'button':
                $(this).attr("disabled", isenable);
                break;
            case 'checkbox':
            case 'radio':
                $(this).attr("disabled", isenable);
        }
    });
}
function toAbsURL(s) {
    var l = location, h, p, f, i;
    if (/^\w+:/.test(s)) {
        return s;
    }
    h = l.protocol + '//' + l.host + (l.port != '' ? (':' + l.port) : '');
    if (s.indexOf('/') == 0) {
        return h + s;
    }
    p = l.pathname.replace(/\/[^\/]*$/, '');
    f = s.match(/\.\.\//g);
    if (f) {
        s = s.substring(f.length * 3);
        for (i = f.length; i--; ) {
            p = p.substring(0, p.lastIndexOf('/'));
        }
    }
    return h + p + '/' + s;
}