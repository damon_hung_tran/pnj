
var languages = {
        "enter_username": "Xin vui lòng nhập tài khoản đăng nhập",            
        "enter_password": "Vui lòng nhập mật khẩu đăng nhập",            
        "enter_security_code_invalid": "Mã bảo mật bạn nhập không hợp lệ",            
        "welcome": "Xin chào",            
        "message_box_title": "Thông báo",      
        "enter_username_invalid" : "Tên đăng nhập chỉ chứa ký tự từ a-z và số và gạch dưới",
        "enter_fullname" : "Vui lòng nhập họ và tên",
        "enter_birthday" : "Vui lòng nhập ngày sinh của bạn",
        "enter_email" : "Vui lòng nhập địa chỉ email của bạn",
        "enter_email_invalid" : "Địa chỉ email của bạn không hợp lệ",
        "enter_address" : "Vui lòng nhập địa chỉ của bạn",
        "password_invaild" : "Mật khẩu bạn nhập không đúng",
        "password_again" : "Xin vui lòng nhập lại mật khẩu",
        "register_accept" : "Tôi đã đọc và đồng ý với điều khoản sử dụng",
        "enter_content_upload_photo":"Xin vui lòng nhập nội dung cần gửi",
        "enter_title_upload_photo":"Xin vui lòng nhập tiêu đề",
        "enter_phone_invalid":"Số điện thoại bạn không hợp lệ",
        "enter_phone":"Vui lòng nhập số điện thoại của bạn",
        "enter_company":"Vui lòng chọn công ty",
        "select_company_send":"Vui lòng chọn công ty cần gửi",
        "enter_content_contact":"Vui lòng nhập nội dung cần liên hệ",
        "enter_security_code":"Vui lòng nhập mã xác nhận",
        "select_sex":"Vui lòng chọn giới tính",
        "enter_education_level":"Vui lòng nhập học vấn của bạn",
        "enter_file_cv":"Xin vui lòng đính kèm hồ sơ xin việc của bạn",
        "enter_cv_invalid":"File đính kèm bạn gửi không hợp lệ. Vui lòng chọn file khác",
        "enter_file_letter":"Xin vui lòng đính kèm thư xin việc của bạn",
        "enter_cv_letter_invalid":"File đính kèm bạn gửi không hợp lệ. Vui lòng chọn file khác",
        "select_birthday":"Vui lòng chọn ngày sinh của bạn",
        "enter_username_register":"Vui lòng nhập tài khoản cần đăng ký",
        "message_sending":"Thông tin của bạn đang được gửi. Vui lòng chờ trong giây lát",
         "search_enter":"Xin vui lòng nhập từ khóa cần tìm kiếm",
         "no_found":"Không tìm thấy thông tin bạn yêu cần xem",
         "month":"Tháng",
         "story":"Truyện",
         "picture":"Hình",
         "logout":"Thoát",
         "enter_category":"Vui lòng chọn danh mục",
         "enter_content_question":"Vui lòng nhập nội dung câu hỏi"                  
        };