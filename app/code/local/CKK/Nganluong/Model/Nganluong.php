<?php
class CKK_Nganluong_Model_Nganluong extends Mage_Payment_Model_Method_Abstract
{

    protected $_code  = 'nganluong';
    protected $_formBlockType = 'nganluong/form_nganluong';
    protected $_infoBlockType = 'nganluong/info_nganluong';
	

    public function assignData($data)
    {
		parent::assignData($data);
		$session = Mage::getSingleton('core/session');
		$session->setData('payment_additional', $data);
		
        $details = array();
		
        if ($this->getVi()) {
            $details['use_vi'] = $this->getVi();
        }
        if ($this->getMailingAddress()) {
            $details['mailing_address'] = $this->getMailingAddress();
        }
        if ($this->getReturnUrl()){
        	$details['return_url']=$this->getReturnUrl();
        }
        if($this->getCheckout()){
        	$details['checkout']=$this->getCheckout();
        }
		if($this->getVisa()){
        	$details['use_visa']=$this->getVisa();
        }
		if($this->getAtm()){
        	$details['use_atm']=$this->getAtm();
        }
        if($this->getMerchantSite()){
        	$details['merchantID']=$this->getMerchantSite();
        }
        if($this->getSecureCode()){
        	$details['secure_code']=$this->getSecureCode();
        }
		if($this->getOptPayment()){
        	$details['option_payment']=$this->getOptPayment();
        }
		if($this->getBankCode()){
        	$details['bankcode']=$this->getBankCode();
        }
		
		if (!empty($details)) {
            $this->getInfoInstance()->setAdditionalData(serialize($details));
        }
        return $this;
      

    }

  
	public function getVi()
    {
        return $this->getConfigData('use_vi');
    }
	public function getVisa()
    {
        return $this->getConfigData('use_visa');
    }
	public function getAtm()
    {
        return $this->getConfigData('use_atm');
    }
    public function getMailingAddress()
    {
        return $this->getConfigData('mailing_address');
    }
    public function getCheckout()
    {
    	return $this->getConfigData('checkout');
    }
    public function getMerchantSite()
    {
    	return $this->getConfigData('merchantID');
    }
    public function getSecureCode()
    {
    	return $this->getConfigData('secure_code');
    }
    public function getReturnUrl()
    {
       return $this->getConfigData('return_url');
    }
	public function getOptPayment(){
		$post = Mage::app()->getRequest()->getPost();
		$option_payment = '';
		$option_payment = $post['payment']['option_payment'];
		return $option_payment;
		
	}
	public function getBankCode(){
		$post = Mage::app()->getRequest()->getPost();
		$bankcode = '';
		$bankcode = $post['payment']['bankcode'];
		return $bankcode;
		
	}
	public function validate()
    {
        parent::validate();
 		$post = Mage::app()->getRequest()->getPost();
		$option_payment = $bankcode  = '';
		$option_payment = $this->getOptPayment();
		$bankcode = $this->getBankCode();
		$_SESSION['test'] = $this->getOptPayment();
        if(empty($option_payment)){
            $errorCode = 'invalid_data';
            $errorMsg = $this->_getHelper()->__('Bạn chưa chọn phương thức thanh toán qua NgânLượng.vn');
        }else {
			if($option_payment=='ATM_ONLINE'&&$bankcode == ''){
				$errorCode = 'invalid_data';
            	$errorMsg = $this->_getHelper()->__('Bạn chưa chọn ngân hàng');	
			}	
		}
 		
        if($errorMsg){
            Mage::throwException($errorMsg);
        }
        return $this;
    }
	public function callNganLuong(){
		return new NL_CheckOutV3($this->getMerchantSite(),$this->getSecureCode(),$this->getMailingAddress());
	}
	public function MarkOrderPaid($order,$comment,$trans) {
	
		$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
	
		if (!count($order->getInvoiceCollection())) {
		  $invoice = $order->prepareInvoice()->setTransactionId($trans)->addComment($comment)->register()->pay();
		  $shipment = $order->prepareShipment();
			if ($shipment) {
				$shipment->register();
		 		$order->setIsInProcess(true);
		  		Mage::getModel('core/resource_transaction')->addObject($shipment)->addObject($shipment->getOrder())->save();
			}
		   $shipment->save();
	
		  Mage::getModel('core/resource_transaction')->addObject($invoice)->addObject($invoice->getOrder())->save();
		  
		  $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE);
		 /* 
		  try {
			$order->sendNewOrderEmail();
		  } catch (Exception $e) {
			Mage::logException($e);
		  }*/
		} else {
		  Mage::log('Lỗi.', null, 'nganluong.log');
		}
  }
  	public function MarkOrderCancelled($order) {
   	 $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
	}
  
	public function visaCheckout($order_id,$bank_code){
			$order    = Mage::getModel('sales/order')->loadByIncrementId($order_id);
			$customer =  $order->getShippingAddress()->getData();
			$buyer_fullname = $order->getBillingAddress()->getName();
			$buyer_email    = $order->getBillingAddress()->getEmail();
			$buyer_mobile   = $customer['telephone'];
			$buyer_address  = $customer['street'];
			$total_amount   = $order->getTotalDue();
			$cancel_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'nganluong/standard/cancel/?order_id='.$order_id;
			$payment_type ='';
			$discount_amount =0;
			$order_description='';
			$tax_amount=0;
			$fee_shipping=0;
			$array_items[0]= array( 'item_name1' => $order_id,
									 'item_quantity1' => 1,
									 'item_amount1' => $total_amount,
									 'item_url1' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB));
			$return_url = $this->getReturnUrl();
			$nganluong  =  $this->callNganLuong();
			$nl_result= $nganluong->VisaCheckout($order_id,$total_amount,$bank_code,$payment_type,$order_description,$tax_amount,$fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile, $buyer_address,$array_items);
			if ($nl_result->error_code =='00'){ 
				return (string)$nl_result->checkout_url;
			}else{
				echo $nl_result->error_message;
				die();
			}
		
	}
	public function viCheckout($order_id){
			$order    = Mage::getModel('sales/order')->loadByIncrementId($order_id);
			$customer =  $order->getShippingAddress()->getData();
			$buyer_fullname = $order->getBillingAddress()->getName();
			$buyer_email    = $order->getBillingAddress()->getEmail();
			$buyer_mobile   = $customer['telephone'];
			$buyer_address  = $customer['street'];
			$total_amount   = $order->getTotalDue();
			$cancel_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'nganluong/standard/cancel/?order_id='.$order_id;
			$payment_type ='';
			$discount_amount =0;
			$order_description='';
			$tax_amount=0;
			$fee_shipping=0;
			$array_items[0]= array( 'item_name1' => $order_id,
									 'item_quantity1' => 1,
									 'item_amount1' => $total_amount,
									 'item_url1' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB));
			$return_url = $this->getReturnUrl();
			$nganluong  =  $this->callNganLuong();
			$nl_result= $nganluong->NLCheckout($order_id,$total_amount,$payment_type,$order_description,$tax_amount,$fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile, $buyer_address,$array_items);
			if ($nl_result->error_code =='00'){ 
				return (string)$nl_result->checkout_url;
			}else{
				echo $nl_result->error_message;
				die();
			}
		
	}
	public function atmCheckout($order_id,$bank_code){
			$order          = Mage::getModel('sales/order')->loadByIncrementId($order_id);
			$customer       = $order->getShippingAddress()->getData();
			$buyer_fullname = $order->getBillingAddress()->getName();
			$buyer_email    = $order->getBillingAddress()->getEmail();
			$buyer_mobile   = $customer['telephone'];
			$buyer_address  = $customer['street'];
			$total_amount   = $order->getTotalDue();
			$cancel_url     = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'nganluong/standard/cancel';
			$payment_type   = '';
			$discount_amount   = 0;
			$order_description = '';
			$tax_amount        = 0;
			$fee_shipping      = 0;
			$array_items[0]= array( 'item_name1' => $order_id,
									 'item_quantity1' => 1,
									 'item_amount1' => $total_amount,
									 'item_url1' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB));
			$return_url = $this->getReturnUrl();
			$nganluong  =  $this->callNganLuong();
			$nl_result = $nganluong->BankCheckout($order_id,$total_amount,$bank_code,$payment_type,$order_description,$tax_amount, $fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile,  $buyer_address,$array_items) ;
			if ($nl_result->error_code =='00'){ 
				unset($_SESSION['bank_code']);
				return (string)$nl_result->checkout_url;
			}else{
				unset($_SESSION['bank_code']);
				echo $nl_result->error_message;
				die();
			}
		
	}
	public function returnUrl($request){
		$nganluong  =  $this->callNganLuong();
		$nl_result = $nganluong->GetTransactionDetail($request['token']);
		if($nl_result){
			$nl_errorcode           = (string)$nl_result->error_code;
			$nl_transaction_status  = (string)$nl_result->transaction_status;
			if($nl_errorcode == '00') {
				if($nl_transaction_status == '00' || $nl_transaction_status == '01') {
					//$nl_result->order_code
					 $order   = Mage::getModel('sales/order')->loadByIncrementId($nl_result->order_code);
					 
					 $comment = 'Đã thanh toán '.($nl_result->payment_type=='2'?' tạm giữ':' ngay').' '.$nl_result->total_amount.' VNĐ - hóa đơn '.$nl_result->order_code.' transaction_id: '.$nl_result->transaction_id;
					//  var_dump($comment);
					//trạng thái thanh toán thành công
					if($order->getState()!='complete'){
						$this->MarkOrderPaid($order,$comment,$nl_result->transaction_id) ;
						return 'Thanh toán thành công';
					}else { return  'Giao dịch được cập nhật'; 
						
					}
				}
		
			}else{
				return $nganluong->GetErrorMessage($nl_errorcode);
			}
		}

	}
	
	public function getOrderPlaceRedirectUrl()
	{		
			$option_payment = $this->getOptPayment();
			$bankcode = $this->getBankCode();
			if($option_payment=="NL")
				return Mage::getUrl('nganluong/standard/vi', array('_secure' => true));
			else if($option_payment=="ATM_ONLINE"&& $bankcode !=""){
				$_SESSION['bank_code'] = $bankcode;
				return Mage::getUrl('nganluong/standard/atm', array('_secure' => true));
			}
                        else if($option_payment=="VISA"){
                            $_SESSION['bank_code'] = $bankcode;
                            return Mage::getUrl('nganluong/standard/visa', array('_secure' => true));
                        }	
			else {
				$errorCode = 'invalid_data';
            	$errorMsg = $this->_getHelper()->__('Lỗi không xác định');	
				Mage::throwException($errorMsg);	
			}
	}
	
}
class Config
{  
	public static $_VERSION="3.1";
	public static $_FUNCTION ="SetExpressCheckout";
	public static $_URL_SERVICE ="https://www.nganluong.vn/checkout.api.nganluong.post.php";
}


class NL_CheckOutV3
{
		  
		public $merchant_id = '';
		public $merchant_password = '';
		public $receiver_email = '';
		public $cur_code = 'vnd';


		function __construct($merchant_id, $merchant_password, $receiver_email)
		{
			$this->merchant_id = $merchant_id;
			$this->merchant_password = $merchant_password;
			$this->receiver_email = $receiver_email;				
		}	
		
	  function GetTransactionDetail($token){	
			###################### BEGIN #####################
					$params = array(
						'merchant_id'       => $this->merchant_id ,
						'merchant_password' => MD5($this->merchant_password),
						'version'           => Config::$_VERSION,
						'function'          => 'GetTransactionDetail',
						'token'             => $token
					);						
					$api_url = Config::$_URL_SERVICE;
					$post_field = '';
					foreach ($params as $key => $value){
						if ($post_field != '') $post_field .= '&';
						$post_field .= $key."=".$value;
					}
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$api_url);
					curl_setopt($ch, CURLOPT_ENCODING , 'UTF-8');
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field);
					$result = curl_exec($ch);
					$status = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					$error = curl_error($ch);
					if ($result != '' && $status==200){
						$nl_result  = simplexml_load_string($result);						
						return $nl_result;
					}
					
					return false;
			###################### END #####################
	  
	  }	
	  
	  
	/*

	Hàm lấy link thanh toán bằng thẻ visa
	===============================
	Tham số truyền vào bắt buộc phải có
				order_code
				total_amount
				payment_method

				buyer_fullname
				buyer_email
				buyer_mobile
	===============================			
		$array_items mảng danh sách các item name theo quy tắc 
		item_name1
		item_quantity1
		item_amount1
		item_url1
		.....
		payment_type Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn
	 */			
	function VisaCheckout($order_code,$total_amount,$bank_code,$payment_type,$order_description,$tax_amount,$fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile, $buyer_address,$array_items) 
			{
			 $params = array(
					'cur_code'			=>	$this->cur_code,
					'function'				=> Config::$_FUNCTION,
					'version'				=> Config::$_VERSION,
					'merchant_id'			=> $this->merchant_id, //Mã merchant khai báo tại NganLuong.vn
					'receiver_email'		=> $this->receiver_email,
					'merchant_password'		=> MD5($this->merchant_password), //MD5(Mật khẩu kết nối giữa merchant và NganLuong.vn)						
					'order_code'			=> $order_code, //Mã hóa đơn do website bán hàng sinh ra
					'total_amount'			=> $total_amount, //Tổng số tiền của hóa đơn
					'payment_method'		=> 'VISA', //Phương thức thanh toán, nhận một trong các giá trị 'VISA','ATM_ONLINE', 'ATM_OFFLINE' hoặc 'NH_OFFLINE'												
                             'bank_code'				=> $bank_code, //Mã Ngân hàng
					'payment_type'			=> $payment_type, //Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn
					'order_description'		=> $order_description, //Mô tả đơn hàng
					'tax_amount'			=> $tax_amount, //Tổng số tiền thuế
					'fee_shipping'			=> $fee_shipping, //Phí vận chuyển
					'discount_amount'		=> $discount_amount, //Số tiền giảm giá
					'return_url'			=> $return_url, //Địa chỉ website nhận thông báo giao dịch thành công
					'cancel_url'			=> $cancel_url, //Địa chỉ website nhận "Hủy giao dịch"
					'buyer_fullname'		=> $buyer_fullname, //Tên người mua hàng
					'buyer_email'			=> $buyer_email, //Địa chỉ Email người mua
					'buyer_mobile'			=> $buyer_mobile, //Điện thoại người mua
					'buyer_address'			=> $buyer_address, //Địa chỉ người mua hàng
					'total_item'			=> count($array_items)
				);
				$post_field = '';
				foreach ($params as $key => $value){
					if ($post_field != '') $post_field .= '&';
					$post_field .= $key."=".$value;
				}
				if(count($array_items)>0){
				 foreach($array_items as $array_item){
					foreach ($array_item as $key => $value){
						if ($post_field != '') $post_field .= '&';
						$post_field .= $key."=".$value;
					}
				}
				}
				//die($post_field);
				
			$nl_result=$this->CheckoutCall($post_field);
			return $nl_result;
		}

	/*
	Hàm lấy link thanh toán qua ngân hàng
	===============================
	Tham số truyền vào bắt buộc phải có
				order_code
				total_amount			
				bank_code // Theo bảng mã ngân hàng
				
				buyer_fullname
				buyer_email
				buyer_mobile
	===============================	
		
		$array_items mảng danh sách các item name theo quy tắc 
		item_name1
		item_quantity1
		item_amount1
		item_url1
		.....			
		payment_type Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn

	*/			  
	function BankCheckout($order_code,$total_amount,$bank_code,$payment_type,$order_description,$tax_amount,$fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile, $buyer_address,$array_items) 
	   {
			 $params = array(
					'cur_code'			=>	$this->cur_code,
					'function'				=> Config::$_FUNCTION,
					'version'				=> Config::$_VERSION,
					'merchant_id'			=> $this->merchant_id, //Mã merchant khai báo tại NganLuong.vn
					'receiver_email'		=> $this->receiver_email,
					'merchant_password'		=> MD5($this->merchant_password), //MD5(Mật khẩu kết nối giữa merchant và NganLuong.vn)						
					'order_code'			=> $order_code, //Mã hóa đơn do website bán hàng sinh ra
					'total_amount'			=> $total_amount, //Tổng số tiền của hóa đơn						
					'payment_method'		=> 'ATM_ONLINE', //Phương thức thanh toán, nhận một trong các giá trị 'ATM_ONLINE', 'ATM_OFFLINE' hoặc 'NH_OFFLINE'
					'bank_code'				=> $bank_code, //Mã Ngân hàng
					'payment_type'			=> $payment_type, //Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn
					'order_description'		=> $order_description, //Mô tả đơn hàng
					'tax_amount'			=> $tax_amount, //Tổng số tiền thuế
					'fee_shipping'			=> $fee_shipping, //Phí vận chuyển
					'discount_amount'		=> $discount_amount, //Số tiền giảm giá
					'return_url'			=> $return_url, //Địa chỉ website nhận thông báo giao dịch thành công
					'cancel_url'			=> $cancel_url, //Địa chỉ website nhận "Hủy giao dịch"
					'buyer_fullname'		=> $buyer_fullname, //Tên người mua hàng
					'buyer_email'			=> $buyer_email, //Địa chỉ Email người mua
					'buyer_mobile'			=> $buyer_mobile, //Điện thoại người mua
					'buyer_address'			=> $buyer_address, //Địa chỉ người mua hàng
					'total_item'			=> count($array_items)
				);
				
				$post_field = '';
				foreach ($params as $key => $value){
					if ($post_field != '') $post_field .= '&';
					$post_field .= $key."=".$value;
				}
				if(count($array_items)>0){
				 foreach($array_items as $array_item){
					foreach ($array_item as $key => $value){
						if ($post_field != '') $post_field .= '&';
						$post_field .= $key."=".$value;
					}
				}
				}
			//die($post_field);
			$nl_result=$this->CheckoutCall($post_field);
			return $nl_result;
		}
		
	

		/*

		Hàm lấy link thanh toán tại văn phòng ngân lượng

		===============================
		Tham số truyền vào bắt buộc phải có
					order_code
					total_amount			
					bank_code // HN hoặc HCM
					
					buyer_fullname
					buyer_email
					buyer_mobile
		===============================	
			
			$array_items mảng danh sách các item name theo quy tắc 
			item_name1
			item_quantity1
			item_amount1
			item_url1
			.....			
			payment_type Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn

		*/			  
	 function TTVPCheckout($order_code,$total_amount,$bank_code,$payment_type,$order_description,$tax_amount,
								$fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile, 
								$buyer_address,$array_items) 
	   {
			 $params = array(
					'cur_code'			=>	$this->cur_code,
					'function'				=> Config::$_FUNCTION,
					'version'				=> Config::$_VERSION,
					'merchant_id'			=> $this->merchant_id, //Mã merchant khai báo tại NganLuong.vn
					'receiver_email'		=> $this->receiver_email,
					'merchant_password'		=> MD5($this->merchant_password), //MD5(Mật khẩu kết nối giữa merchant và NganLuong.vn)						
					'order_code'			=> $order_code, //Mã hóa đơn do website bán hàng sinh ra
					'total_amount'			=> $total_amount, //Tổng số tiền của hóa đơn						
					'payment_method'		=> 'ATM_ONLINE', //Phương thức thanh toán, nhận một trong các giá trị 'ATM_ONLINE', 'ATM_OFFLINE' hoặc 'NH_OFFLINE'
					'bank_code'				=> $bank_code, //Mã Ngân hàng
					'payment_type'			=> $payment_type, //Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn
					'order_description'		=> $order_description, //Mô tả đơn hàng
					'tax_amount'			=> $tax_amount, //Tổng số tiền thuế
					'fee_shipping'			=> $fee_shipping, //Phí vận chuyển
					'discount_amount'		=> $discount_amount, //Số tiền giảm giá
					'return_url'			=> $return_url, //Địa chỉ website nhận thông báo giao dịch thành công
					'cancel_url'			=> $cancel_url, //Địa chỉ website nhận "Hủy giao dịch"
					'buyer_fullname'		=> $buyer_fullname, //Tên người mua hàng
					'buyer_email'			=> $buyer_email, //Địa chỉ Email người mua
					'buyer_mobile'			=> $buyer_mobile, //Điện thoại người mua
					'buyer_address'			=> $buyer_address, //Địa chỉ người mua hàng
					'total_item'			=> count($array_items)
				);
				
				$post_field = '';
				foreach ($params as $key => $value){
					if ($post_field != '') $post_field .= '&';
					$post_field .= $key."=".$value;
				}
				if(count($array_items)>0){
				 foreach($array_items as $array_item){
					foreach ($array_item as $key => $value){
						if ($post_field != '') $post_field .= '&';
						$post_field .= $key."=".$value;
					}
				}
				}
				
			$nl_result=$this->CheckoutCall($post_field);
			return $nl_result;
		}
		
		/*

		Hàm lấy link thanh toán dùng số dư ví ngân lượng
		===============================
		Tham số truyền vào bắt buộc phải có
					order_code
					total_amount
					payment_method

					buyer_fullname
					buyer_email
					buyer_mobile
		===============================			
			$array_items mảng danh sách các item name theo quy tắc 
			item_name1
			item_quantity1
			item_amount1
			item_url1
			.....

			payment_type Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn
		 */			
	function NLCheckout($order_code,$total_amount,$payment_type,$order_description,$tax_amount,
								$fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile, 
								$buyer_address,$array_items) 
			{
			 $params = array(
					'cur_code'			=>$this->cur_code,
					'function'				=> Config::$_FUNCTION,
					'version'				=> Config::$_VERSION,
					'merchant_id'			=> $this->merchant_id, //Mã merchant khai báo tại NganLuong.vn
					'receiver_email'		=> $this->receiver_email,
					'merchant_password'		=> MD5($this->merchant_password), //MD5(Mật khẩu kết nối giữa merchant và NganLuong.vn)						
					'order_code'			=> $order_code, //Mã hóa đơn do website bán hàng sinh ra
					'total_amount'			=> $total_amount, //Tổng số tiền của hóa đơn						
					'payment_method'		=> 'NL', //Phương thức thanh toán
					'payment_type'			=> $payment_type, //Kiểu giao dịch: 1 - Ngay; 2 - Tạm giữ; Nếu không truyền hoặc bằng rỗng thì lấy theo chính sách của NganLuong.vn
					'order_description'		=> $order_description, //Mô tả đơn hàng
					'tax_amount'			=> $tax_amount, //Tổng số tiền thuế
					'fee_shipping'			=> $fee_shipping, //Phí vận chuyển
					'discount_amount'		=> $discount_amount, //Số tiền giảm giá
					'return_url'			=> $return_url, //Địa chỉ website nhận thông báo giao dịch thành công
					'cancel_url'			=> $cancel_url, //Địa chỉ website nhận "Hủy giao dịch"
					'buyer_fullname'		=> $buyer_fullname, //Tên người mua hàng
					'buyer_email'			=> $buyer_email, //Địa chỉ Email người mua
					'buyer_mobile'			=> $buyer_mobile, //Điện thoại người mua
					'buyer_address'			=> $buyer_address, //Địa chỉ người mua hàng
					'total_item'			=> count($array_items) //Tổng số sản phẩm trong đơn hàng
				);
				$post_field = '';
				foreach ($params as $key => $value){
					if ($post_field != '') $post_field .= '&';
					$post_field .= $key."=".$value;
				}
				if(count($array_items)>0){
				 foreach($array_items as $array_item){
					foreach ($array_item as $key => $value){
						if ($post_field != '') $post_field .= '&';
						$post_field .= $key."=".$value;
					}
				}
				}
				
			//die($post_field);
			$nl_result=$this->CheckoutCall($post_field);
			return $nl_result;
		}
			
			
		
function CheckoutCall($post_field){
		
			$api_url = Config::$_URL_SERVICE;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$api_url);
			curl_setopt($ch, CURLOPT_ENCODING , 'UTF-8');
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field);
			$result = curl_exec($ch);
			$status = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
			$error = curl_error($ch);
			
			if ($result != '' && $status==200){						
				$xml_result = str_replace('&','&amp;',(string)$result);
				$nl_result  = simplexml_load_string($xml_result);					
				$nl_result->error_message = $this->GetErrorMessage($nl_result->error_code);										
			}
			else $nl_result->error_message = $error;
			return $nl_result;
		
		}
		
function GetErrorMessage($error_code) {
			$arrCode = array(
			'00'=>  'Không có lỗi',
			'99'=>  'Lỗi không được định nghĩa hoặc không rõ nguyên nhân',
			'01'=>  'Lỗi tại NgânLượng.vn nên không sinh được phiếu thu hoặc giao dịch',
			'02'=>  'Địa chỉ IP của merchant gọi tới NganLuong.vn không được chấp nhận',
			'03'=>  'Sai tham số gửi tới NganLuong.vn (có tham số sai tên hoặc kiểu dữ liệu)',
			'04'=>  'Tên hàm API do merchant gọi tới không hợp lệ (không tồn tại)',
			'05'=>  'Sai version của API',
			'06'=>  'Mã merchant không tồn tại hoặc chưa được kích hoạt',
			'07'=>  'Sai mật khẩu của merchant',
			'08'=>  'Tài khoản người bán hàng không tồn tại',
			'09'=>  'Tài khoản người nhận tiền đang bị phong tỏa',
			'10'=>  'Hóa đơn thanh toán không hợp lệ',
			'11'=>  'Số tiền thanh toán không hợp lệ',
			'12'=>  'Đơn vị tiền tệ không hợp lệ',
			'13'=>  'Sai số lượng sản phẩm',
			'14'=>  'Tên sản phẩm không hợp lệ',
			'15'=>  'Sai số lượng sản phẩm/hàng hóa trong chi tiết đơn hàng',
			'16'=>  'Số tiền trong chi tiết đơn hàng không hợp lệ',
			'17'=>  'Phương thức thanh toán không được hỗ trợ',
			'18'=>  'Tài khoản hoặc mật khẩu NL của người thanh toán không chính xác',
			'19'=>  'Tài khoản người thanh toán đang bị phong tỏa, không thể thực hiện giao dịch',
			'20'=>  'Số dư khả dụng của người thanh toán không đủ thực hiện giao dịch',
			'21'=>  'Giao dịch NL đã được thanh toán trước đó, không thể thực hiện lại',
			'22'=>  'Ngân hàng từ chối thanh toán (do thẻ/tài khoản ngân hàng bị khóa hoặc chưa đăng ký sử dụng dịch vụ IB)',
			'23'=>  'Lỗi kết nối tới hệ thống Ngân hàng (NH không trả lời yêu cầu thanh toán)',
			'24'=>  'Thẻ/tài khoản hết hạn sử dụng',
			'25'=>  'Thẻ/Tài khoản không đủ số dư để thanh toán',
			'26'=>  'Nhập sai tài khoản truy cập Internet-Banking',
			'27'=>  'Nhập sai OTP quá số lần quy định',
			'28'=>  'Lỗi phía Ngân hàng xử lý giao dịch thanh toán nhưng chưa rõ nguyên nhân hoặc lỗi này chưa được mô tả',
			'29'=>  'Mã token không tồn tại',
			'30'=>  'Giao dịch không tồn tại ');

			   return $arrCode[(string)$error_code];
		}
		
		
		
}