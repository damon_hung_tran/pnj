<?php

class CKK_Nganluong_StandardController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {    
		 $order   = Mage::getModel('sales/order')->loadByIncrementId(100000031);
		 $method  = Mage::getModel('nganluong/nganluong');
		 $comment = '';
         $method->MarkOrderPaid($order,$comment);
    }
 	public function cancelAction() {    
		 $order = Mage::getModel('sales/order')->loadByIncrementId($_GET['order_id']);
		 $method = Mage::getModel('nganluong/nganluong');
         $method->MarkOrderCancelled($order);
         $this->_redirectUrl(Mage::getUrl('checkout/cart'));
    }
    public function visaAction() {
        $session = Mage::getSingleton('checkout/session');
        $order_id = $session->getLastRealOrderId();
        if ($order_id == '') {
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        } else {
            $url = Mage::getModel('nganluong/nganluong')->visaCheckout($order_id,$_SESSION['bank_code']);
        }
        //echo "<pre>"; var_dump($url);echo "</pre>";
        unset($_SESSION['bank_code']);
        $this->_redirectUrl($url);
    }

    public function viAction() {
        $session = Mage::getSingleton('checkout/session');
        $order_id = $session->getLastRealOrderId();

        if ($order_id == '') {
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        } else {
            $url = Mage::getModel('nganluong/nganluong')->viCheckout($order_id);
        }
        //echo "<pre>"; var_dump($url);echo "</pre>";
        $this->_redirectUrl($url);
    }

    public function atmAction() {

        $session = Mage::getSingleton('checkout/session');
        $order_id = $session->getLastRealOrderId();
        if ($order_id == '') {
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        } else {
            $url = Mage::getModel('nganluong/nganluong')->atmCheckout($order_id, $_SESSION['bank_code']);
        }
        //echo "<pre>"; var_dump($url);echo "</pre>";
		unset($_SESSION['bank_code']);
        $this->_redirectUrl($url);
    }

    public function successAction() {
		 if(isset($_GET)) $request = $_GET;
		 $method  = Mage::getModel('nganluong/nganluong');
         $mess = $method->returnUrl($request);
		 echo '<script>alert("'.$mess.'");document.location.href="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'";</script>';
		 $this->_redirectUrl(Mage::getUrl('checkout/onepage/success'));
        	//returnUrl
    }

    

}
