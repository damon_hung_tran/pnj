<?php
class CKK_Nganluong_Block_Info_Nganluong extends Mage_Payment_Block_Info
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payment/info/nganluong.phtml');
    }
	public function getInfo(){
		$info = $this->getData('info');
        if (!($info instanceof Mage_Payment_Model_Info)) {
            Mage::throwException($this->__('Cannot retrieve the payment info model object.'));
        }
        return $info;
	}
	public function getMethod()  {
		 $details = @unserialize($this->getInfo()->getAdditionalData());
        if($details['option_payment'] == 'NL'){return 'Thanh toán trực tuyến qua ví NgânLượng.vn';}
		else if($details['option_payment']== 'ATM_ONLINE'){return 'Thanh toán online bằng thẻ ngân hàng nội địa - '.$details['bankcode'];}
		else if($details['option_payment'] == 'VISA'){return 'Thanh toán bằng thẻ Visa hoặc MasterCard';}
		else return 'Thanh toán trực tuyến qua NgânLượng.vn';
    }
	
}
