<?php

class DBG_PromotionProducts_Model_Observer
{
    public function afterSaveProduct($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getData('dbg_promotion_product_position'))
        {
            $product->setData('dbg_promotion_product_position', 10000);
        }

        Mage::getResourceSingleton('promotionproducts/product_position')->saveProductPosition($product);

    }
}



