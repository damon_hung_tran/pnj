<?php
/**
 * @category    DBG
 * @package     DBG Featured Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */
class DBG_NewProducts_Model_Resource_Product_Position extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('newproducts/product_position', null);
    }

    public function saveProductPosition($product)
    {
        if(!$product->getId()){
            return $this;
        }

        $deleteCondition = array('product_id=?' => $product->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        $data = array(
            'product_id' => $product->getId(),
            'position'  => $product->getDbgNewProductPosition()
        );

        $this->_getWriteAdapter()->insert($this->getMainTable(), $data);

        return $this;
    }
}