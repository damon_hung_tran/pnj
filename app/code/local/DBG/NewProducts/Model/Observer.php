<?php

class DBG_NewProducts_Model_Observer
{
    public function afterSaveProduct($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getData('dbg_new_product_position'))
        {
            $product->setData('dbg_new_product_position', 10000);
        }

        Mage::getResourceSingleton('newproducts/product_position')->saveProductPosition($product);

    }
}



