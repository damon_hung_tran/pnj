<?php
/**
 * @category    DBG
 * @package     DBG Promotion2 Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */
class DBG_Promotion2Products_Model_System_Config_Source_Sort
{
	/*
	 * Prepare data for System->Configuration dropdown
	 * */
	public function toOptionArray()
	{
		return array(
			0 => Mage::helper('adminhtml')->__('Random'),
			1 => Mage::helper('adminhtml')->__('Last Added'),
			2 => Mage::helper('adminhtml')->__('Position'),
		);
	}
}
