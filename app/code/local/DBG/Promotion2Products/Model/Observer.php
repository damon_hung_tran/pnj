<?php

class DBG_Promotion2Products_Model_Observer
{
    public function afterSaveProduct($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getData('dbg_promotion2_product_position'))
        {
            $product->setData('dbg_promotion2_product_position', 10000);
        }

        Mage::getResourceSingleton('promotion2products/product_position')->saveProductPosition($product);

    }
}



