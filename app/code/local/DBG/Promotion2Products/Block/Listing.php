<?php
/**
 * @category    DBG
 * @package     DBG Promotion2 Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */
class DBG_Promotion2Products_Block_Listing extends Mage_Catalog_Block_Product_Abstract {
    /*
     * Check sort option and limits set in System->Configuration and apply them
     * Additionally, set template to block so call from CMS will look like {{block type="promotion2products/listing"}}
     */

    public function __construct() {

        $this->setTemplate('dbg/promotion2products/block_promotion2_products.phtml');

        $this->setLimit((int) Mage::getStoreConfig("promotion2products/cmspage/number_of_items"));
        $sort_by = Mage::getStoreConfig("promotion2products/cmspage/product_sort_by");
        $this->setItemsPerRow((int) Mage::getStoreConfig("promotion2products/cmspage/number_of_items_per_row"));

        switch ($sort_by) {
            case 0:
                $this->setSortBy("rand()");
                break;
            case 1:
                $this->setSortBy("created_at desc");
                break;
			case 2:
                $this->setSortBy("position asc");
                break;
            default:
                $this->setSortBy("rand()");
        }
    }

    /*
     * Load promotion2 products collection
     * */

    protected function _beforeToHtml() {
        $collection = Mage::getResourceModel('catalog/product_collection');

        $attributes = Mage::getSingleton('catalog/config')
                ->getProductAttributes();

        $collection->addAttributeToSelect($attributes)
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToFilter('dbg_promotion2_product', 1, 'left')
                ->addStoreFilter()
                ->getSelect()->order($this->getSortBy())->limit($this->getLimit());
		
		$collection
            ->joinField(
                'position',
                'promotion2products/product_position',
                'position',
                'product_id=entity_id'
            );

        $collection->getSelect()
            ->order($this->getSortBy())
            ->limit($this->getLimit());
		
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $this->_productCollection = $collection;

        $this->setProductCollection($collection);
        return parent::_beforeToHtml();
    }

    protected function _toHtml() {

        if (!$this->helper('promotion2products')->getIsActive()) {
            return '';
        }

        return parent::_toHtml();
    }

    /*
     * Return label for CMS block output
     * */

    protected function getBlockLabel() {
        return $this->helper('promotion2products')->getCmsBlockLabel();
    }

}