<?php

class DBG_ExclusiveProducts_Model_Observer
{
    public function afterSaveProduct($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getData('dbg_exclusive_product_position'))
        {
            $product->setData('dbg_exclusive_product_position', 10000);
        }

        Mage::getResourceSingleton('exclusiveproducts/product_position')->saveProductPosition($product);

    }
}



