<?php

/**
 * @category    DBG
 * @package     DBG Exclusive Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */
class DBG_ExclusiveProducts_IndexController extends Mage_Core_Controller_Front_Action {
    /*
     * Check settings set in System->Configuration and apply them for exclusive-products page
     * */

    public function indexAction() {

        if (!Mage::helper('exclusiveproducts')->getIsActive()) {
            $this->_forward('noRoute');
            return;
        }

        $template = Mage::getConfig()->getNode('global/page/layouts/' . Mage::getStoreConfig("exclusiveproducts/standalone/layout") . '/template');

        $this->loadLayout();

        $this->getLayout()->getBlock('root')->setTemplate($template);
        $this->getLayout()->getBlock('head')->setTitle($this->__(Mage::getStoreConfig("exclusiveproducts/standalone/meta_title")));
        $this->getLayout()->getBlock('head')->setDescription($this->__(Mage::getStoreConfig("exclusiveproducts/standalone/meta_description")));
        $this->getLayout()->getBlock('head')->setKeywords($this->__(Mage::getStoreConfig("exclusiveproducts/standalone/meta_keywords")));

        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbsBlock->addCrumb('exclusive_products', array(
            'label' => Mage::helper('exclusiveproducts')->__(Mage::helper('exclusiveproducts')->getPageLabel()),
            'title' => Mage::helper('exclusiveproducts')->__(Mage::helper('exclusiveproducts')->getPageLabel()),
        ));

        $this->renderLayout();
    }

}