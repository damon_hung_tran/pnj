<?php
/**
 * @category    DBG
 * @package     DBG Exclusive Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */

$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */

$installer->startSetup();
$sql = "DROP TABLE IF EXISTS dbg_exclusiveproducts_position";
$installer->run($sql);
$table = $installer->getConnection()
    ->newTable($installer->getTable('exclusiveproducts/product_position'))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
    ))
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 4);

$installer->getConnection()
    ->createTable($table);

$installer->endSetup();