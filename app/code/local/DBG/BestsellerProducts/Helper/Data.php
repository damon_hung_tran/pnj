<?php

/**
 * @category    DBG
 * @package     DBG Bestseller Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */
class DBG_BestsellerProducts_Helper_Data extends Mage_Core_Helper_Abstract {

    const PATH_PAGE_HEADING = 'bestsellerproducts/standalone/heading';
    const PATH_CMS_HEADING = 'bestsellerproducts/cmspage/heading_block';
    const DEFAULT_LABEL = 'Bestseller Products';

    public function getCmsBlockLabel() {
        $configValue = Mage::getStoreConfig(self::PATH_CMS_HEADING);
        return strlen($configValue) > 0 ? $configValue : self::DEFAULT_LABEL;
    }

    public function getPageLabel() {
        $configValue = Mage::getStoreConfig(self::PATH_PAGE_HEADING);
        return strlen($configValue) > 0 ? $configValue : self::DEFAULT_LABEL;
    }

    public function getIsActive() {
        return (bool) Mage::getStoreConfig('bestsellerproducts/general/active');
    }

}