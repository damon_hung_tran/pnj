<?php

class DBG_BestsellerProducts_Model_Observer
{
    public function afterSaveProduct($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getData('dbg_bestseller_product_position'))
        {
            $product->setData('dbg_bestseller_product_position', 10000);
        }

        Mage::getResourceSingleton('bestsellerproducts/product_position')->saveProductPosition($product);

    }
}



