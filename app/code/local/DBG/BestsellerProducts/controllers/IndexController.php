<?php

/**
 * @category    DBG
 * @package     DBG Bestseller Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */
class DBG_BestsellerProducts_IndexController extends Mage_Core_Controller_Front_Action {
    /*
     * Check settings set in System->Configuration and apply them for bestseller-products page
     * */

    public function indexAction() {

        if (!Mage::helper('bestsellerproducts')->getIsActive()) {
            $this->_forward('noRoute');
            return;
        }

        $template = Mage::getConfig()->getNode('global/page/layouts/' . Mage::getStoreConfig("bestsellerproducts/standalone/layout") . '/template');

        $this->loadLayout();

        $this->getLayout()->getBlock('root')->setTemplate($template);
        $this->getLayout()->getBlock('head')->setTitle($this->__(Mage::getStoreConfig("bestsellerproducts/standalone/meta_title")));
        $this->getLayout()->getBlock('head')->setDescription($this->__(Mage::getStoreConfig("bestsellerproducts/standalone/meta_description")));
        $this->getLayout()->getBlock('head')->setKeywords($this->__(Mage::getStoreConfig("bestsellerproducts/standalone/meta_keywords")));

        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbsBlock->addCrumb('bestseller_products', array(
            'label' => Mage::helper('bestsellerproducts')->__(Mage::helper('bestsellerproducts')->getPageLabel()),
            'title' => Mage::helper('bestsellerproducts')->__(Mage::helper('bestsellerproducts')->getPageLabel()),
        ));

        $this->renderLayout();
    }

}