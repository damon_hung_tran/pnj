<?php

class DBG_FeaturedProducts_Model_Observer
{
    public function afterSaveProduct($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getData('dbg_featured_product_position'))
        {
            $product->setData('dbg_featured_product_position', 10000);
        }

        Mage::getResourceSingleton('featuredproducts/product_position')->saveProductPosition($product);

    }
}



