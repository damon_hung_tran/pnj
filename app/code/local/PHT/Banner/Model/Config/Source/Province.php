<?php

class PHT_Banner_Model_Config_Source_Province {

     public function toOptionArray() {
        $_collection = Mage::getSingleton('directory/region')->getCollection()
                        ->addFieldToFilter('country_id', 'VN')
                        ->addOrder('name', 'ASC');
        $_result = array();
        foreach ($_collection as $item) {
            $data = array(
                'value' => $item->getRegionId(),
                'label' => $item->getName()
                );
            $_result[] = $data;
        }
        return $_result;
    }
     
}