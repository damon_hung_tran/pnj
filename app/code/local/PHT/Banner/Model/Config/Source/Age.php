<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    PHT
 * @package     PHT_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Position config model
 *
 * @category   PHT
 * @package    PHT_Banner
 * @author     PHT
 */
class PHT_Banner_Model_Config_Source_Age
{
    const UNKNOWN			= '0';
    const AGE_UNDER_24		= '1';
    const AGE_25_TO_AGE_45	= '2';
    const AGE_OVER_45   	= '3';
    
    

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return array(
			array('value' => self::UNKNOWN, 'label'=>Mage::helper('adminhtml')->__('Default')),
            array('value' => self::AGE_UNDER_24, 'label'=>Mage::helper('adminhtml')->__('Student (Under 25 age)')),
            array('value' => self::AGE_25_TO_AGE_45, 'label'=>Mage::helper('adminhtml')->__('Ages 25 -> 45')),
            array('value' => self::AGE_OVER_45, 'label'=>Mage::helper('adminhtml')->__('Ages Over 45'))
        );
    }
    
    public function toGridOptionArray() {
        return array(
			self::UNKNOWN => Mage::helper('adminhtml')->__('Default'),
            self::AGE_UNDER_24 => Mage::helper('adminhtml')->__('Student (Under 25 age)'),
            self::AGE_25_TO_AGE_45 => Mage::helper('adminhtml')->__('Ages 25 -> 45'),
            self::AGE_OVER_45 => Mage::helper('adminhtml')->__('Ages Over 45')
        );
    }
}
