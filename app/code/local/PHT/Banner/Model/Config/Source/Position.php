<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    PHT
 * @package     PHT_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Position config model
 *
 * @category   PHT
 * @package    PHT_Banner
 * @author     PHT
 */
class PHT_Banner_Model_Config_Source_Position
{
	const CONTENT_BANNER    = 'CONTENT_BANNER';
    const CONTENT_TOP       = 'CONTENT_TOP';
    const CONTENT_CATEGORY  = 'CONTENT_CATEGORY';
    const CONTENT_CAT_DETAIL  = 'CONTENT_CAT_DETAIL';   
    const CONTENT_POP_UP    = 'CONTENT_POP_UP';
	const CONTENT_BRANDS    = 'CONTENT_BRANDS';
	const CONTENT_SI_CATEGORY = 'CONTENT_SI_CATEGORY';
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return array(
			array('value' => self::CONTENT_BANNER, 'label'=>Mage::helper('adminhtml')->__('Banner Slide')),
            array('value' => self::CONTENT_TOP, 'label'=>Mage::helper('adminhtml')->__('Banner Top')),
            array('value' => self::CONTENT_CATEGORY, 'label'=>Mage::helper('adminhtml')->__('Banner Category')),
            array('value' => self::CONTENT_CAT_DETAIL, 'label'=>Mage::helper('adminhtml')->__('Banner Category Detail')),
            array('value' => self::CONTENT_POP_UP, 'label'=>Mage::helper('adminhtml')->__('Banner Pop-up')),
            array('value' => self::CONTENT_SI_CATEGORY, 'label' => Mage::helper('adminhtml')->__('Banner Sidebar Category')),
			//array('value' => self::CONTENT_BRANDS, 'label'=>Mage::helper('adminhtml')->__('Banner Brands'))
			
        );
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toGridOptionArray() {
        return array(
			self::CONTENT_BANNER => Mage::helper('adminhtml')->__('Banner Slide'),
            self::CONTENT_TOP => Mage::helper('adminhtml')->__('Banner Top'),
            self::CONTENT_CATEGORY => Mage::helper('adminhtml')->__('Banner Category'),
            self::CONTENT_CAT_DETAIL => Mage::helper('adminhtml')->__('Banner Category Detail'),
            self::CONTENT_POP_UP => Mage::helper('adminhtml')->__('Banner Pop-up'),
            self::CONTENT_SI_CATEGORY => Mage::helper('adminhtml')->__('Banner Sidebar Category'),
			//self::CONTENT_BRANDS => Mage::helper('adminhtml')->__('Brands')	
        );
    }
}
