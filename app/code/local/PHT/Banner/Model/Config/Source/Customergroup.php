<?php

class PHT_Banner_Model_Config_Source_Customergroup {

	 public function toOptionArray() {
        $_collection = Mage::getSingleton('customer/group')->getCollection();

        $_result = array();
        foreach ($_collection as $item) {
            $data = array(
                'value' => $item->getCustomerGroupId(),
                'label' => $item->getCustomerGroupCode()
                );
            $_result[] = $data;
        }
        return $_result;
    }
     
}