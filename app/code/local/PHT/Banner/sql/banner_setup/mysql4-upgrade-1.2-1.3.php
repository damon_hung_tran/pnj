<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
$installer->run("
-- DROP TABLE IF EXISTS `{$this->getTable('banner/banner_province')}`;
    CREATE TABLE `{$this->getTable('banner/banner_province')}` (
      `banner_id` smallint(6) NOT NULL,
      `province_id` smallint(10) NULL DEFAULT '0',
      PRIMARY KEY (`banner_id`,`province_id`),
      KEY `province_id` (`province_id`)
    ) 
    ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='PHT Banner Province' ;
");

$installer->endSetup();