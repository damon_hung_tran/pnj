<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
$installer->run("
CREATE TABLE `{$this->getTable('banner/banner_customer_group')}` (
  `banner_id` smallint(6) NOT NULL,
  `customer_group_id` smallint(6) NOT NULL,
  PRIMARY KEY (`banner_id`,`customer_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='PHT Banner Customer Group' ;

");

$installer->getConnection()->addColumn(
    $this->getTable('banner/banner'), //table name
    'gender',      //column name
    'smallint(6) NOT NULL'  //datatype definition
);

$installer->getConnection()->addColumn(
    $this->getTable('banner/banner'), //table name
    'age',      //column name
    'smallint(6) NOT NULL'  //datatype definition
);

$installer->endSetup();