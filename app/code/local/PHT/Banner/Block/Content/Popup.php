<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    PHT
 * @package     PHT_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base Banner Block Content Pop-up
 *
 * @category   PHT
 * @package    PHT_Banner
 * @author     Pham Hoang Tan
 */
class PHT_Banner_Block_Content_Popup extends PHT_Banner_Block_Banner {
    protected $_position = 'CONTENT_POP_UP';
    
    public function _getPopupCollection(){
			$cookies = Mage::getModel('core/cookie')->get($this->_position);
			if($cookies != ''){
				return array();
			}else{
				$collection = $this->_getCollection($this->_position); 
				$cookieExpires = Mage::getStoreConfig('banner/banner_popup/cookies_lifetime');
				Mage::getModel('core/cookie')->set($this->_position,time(),$cookieExpires);
				
				return $collection;
			}
		}
}