<?php
class PHT_Banner_Block_Adminhtml_Banner_Renderer_StartDate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		if (!empty($value)) {
			$value = date('d/m/Y H:i:s', strtotime($value));
		}
		return '<span style="color:blue;">'.$value.'</span>';
	}
}