<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    PHT
 * @package     PHT_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * General form
 *
 * @category   PHT
 * @package    PHT_Banner
 * @author     PHT
 */

class PHT_Banner_Block_Adminhtml_Banner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $_model = Mage::registry('banner_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('banner_form', array('legend'=>Mage::helper('banner')->__('General Information')));
        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('banner')->__('Title'),
			'title'  	=> Mage::helper('banner')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
            'value'     => $_model->getName()
        ));

        $fieldset->addField('position', 'select', array(
            'label'     => Mage::helper('banner')->__('Position'),
			'title'  	=> Mage::helper('banner')->__('Position'),
            'name'      => 'position',
            'values'    => Mage::getSingleton('banner/config_source_position')->toOptionArray(),
            'value'     => $_model->getPosition()
        ));
		
		$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);        
		$fieldset->addField('start_date', 'date', array(
		  'label'  		=> Mage::helper('banner')->__('Start Date'),
		  'title'  		=> Mage::helper('banner')->__('Start Date'),
		  'class'     	=> 'validate-date',
          'required'  	=> true,
		  'image' 		=> $this->getSkinUrl('images/grid-cal.gif'),
		  'input_format'=> Varien_Date::DATETIME_INTERNAL_FORMAT,
		  'format'      => $dateFormatIso,
		  'time' 		=> true,
		  'name'   		=> 'start_date',
		  'value'		=> $_model->getStartDate()
		));
		
		$fieldset->addField('end_date', 'date', array(
		  'label'		=> Mage::helper('banner')->__('End Date'),
		  'title'		=> Mage::helper('banner')->__('End Date'),
		  'class'     	=> 'validate-date',
          'required'  	=> false,
		  'image'		=> $this->getSkinUrl('images/grid-cal.gif'),
		  'input_format'=> Varien_Date::DATETIME_INTERNAL_FORMAT,
		  'format'      => $dateFormatIso,
		  'time' 		=> true,
		  'name'   		=> 'end_date',
		  'value'		=> $_model->getEndDate()
		));
		
		$fieldset->addField('customer_groups', 'multiselect', array(
            'label'     => Mage::helper('banner')->__('Customer Group'),
            'title'     => Mage::helper('banner')->__('Customer Group'),
            'required'  => true,
            'name'      => 'customer_groups[]',
            'values'    => Mage::getSingleton('banner/config_source_customergroup')->toOptionArray(),
            'value'     => $_model->getCustomerGroupId()
        ));
        
        $provinceList = array(array('value' => 0, 'label' => $this->__('All Province')));
        $fieldset->addField('province', 'multiselect', array(
            'label'     => Mage::helper('banner')->__('Province'),
            'title'     => Mage::helper('banner')->__('Province'),
            'required'  => false,
            'name'      => 'province[]',
            'values'    => array_merge($provinceList, Mage::getSingleton('banner/config_source_province')->toOptionArray()),
            'value'     => $_model->getProvinceId()
        ));
        
        $fieldset->addField('gender', 'select', array(
            'label'     => Mage::helper('banner')->__('Gender'),
            'title'     => Mage::helper('banner')->__('Gender'),
            'required'  => false,
            'name'      => 'gender',
            'values'    => Mage::getSingleton('banner/config_source_gender')->toOptionArray(),
            'value'     => $_model->getGender()
        ));
        
        $fieldset->addField('age', 'select', array(
            'label'     => Mage::helper('banner')->__('Age'),
            'title'     => Mage::helper('banner')->__('Age'),
            'required'  => false,
            'name'      => 'age',
            'values'    => Mage::getSingleton('banner/config_source_age')->toOptionArray(),
            'value'     => $_model->getAge()
        ));

        $fieldset->addField('sort_order', 'text', array(
            'label'     => Mage::helper('banner')->__('Sort By'),
			'title'		=> Mage::helper('banner')->__('Sort By'),
            'required'  => false,
            'name'      => 'sort_order',
            'value'     => $_model->getSortOrder()
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('banner')->__('Is Active'),
            'title'     => Mage::helper('banner')->__('Is Active'),
            'name'      => 'is_active',
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => $_model->getIsActive()
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('stores', 'multiselect', array(
                'label'     => Mage::helper('banner')->__('Visible In'),
                'title'     => Mage::helper('banner')->__('Visible In'),
                'required'  => true,
                'name'      => 'stores[]',
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
                'value'     => $_model->getStoreId()
            ));
        }
        else {
            $fieldset->addField('stores', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
        }


        if( Mage::getSingleton('adminhtml/session')->getBannerData() ) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getBannerData());
            Mage::getSingleton('adminhtml/session')->setBannerData(null);
        }
        
        return parent::_prepareForm();
    }
}
