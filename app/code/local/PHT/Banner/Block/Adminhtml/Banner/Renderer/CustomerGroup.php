<?php
	class PHT_Banner_Block_Adminhtml_Banner_Renderer_CustomerGroup extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
	{
		public function render(Varien_Object $row){
			$_customergroups = array();
			$bannerId = Mage::getModel('banner/banner')->load($row->getId());
			$groups = $bannerId->getCustomerGroupId();
			foreach($groups as $group){
				$item = Mage::getModel('customer/group')->load($group);
				$_customergroups[] = $item->getCustomerGroupCode();
			}
			$result = implode('<br/>',$_customergroups);
			return $result;
		}
	}
?>