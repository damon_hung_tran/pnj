<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    PHT
 * @package     PHT_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base Banner Block
 *
 * @category   PHT
 * @package    PHT_Banner
 * @author     PHT
 */
class PHT_Banner_Block_Banner extends Mage_Core_Block_Template {
    protected $_position = null;
    protected $_isActive = 1;
    protected $_groupId  = 0;
    protected $_province = 0;
    protected $_age1     = 24;
    protected $_age2     = 45;
    protected $_collection;

	protected function getAgeCustomer($customer) {		
		$cus_dob = $customer->getDob();
		if(!$cus_dob){
			return false;
		}
    	$_currentdate = Mage::app()->getLocale()->storeDate();
    	$cus_dob = new Zend_Date($cus_dob);
		$age= $_currentdate->subDate($cus_dob)->toString('y');	
		return (int)$age;
		
    }
    
    protected function getAgeOption($customer) {
		$age = $this->getAgeCustomer($customer);
		if (!$age) {
			return 0;
		}
		if($age <= $this->_age1){
			return 1;
		}
		elseif($age > $this->_age1 && $age <= $this->_age2){
			return 2;
		}
		elseif($age > $this->_age2){
			return 3;
		}	
    }
    
    protected function _getCollection($position = null) {
        if ($this->_collection) {
            return $this->_collection;
        }

        $storeId = Mage::app()->getStore()->getId();
        $this->_collection = Mage::getModel('banner/banner')->getCollection()
                ->addEnableFilter($this->_isActive);
        if (!Mage::app()->isSingleStoreMode()) {
            $this->_collection->addStoreFilter($storeId);
        }
        
        $login = Mage::getSingleton('customer/session')->isLoggedIn();        
        if($login){
        	$groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
       		$customer = Mage::getSingleton('customer/session')->getCustomer();
            $customerAddressId = $customer->getDefaultBilling();
            $address = Mage::getModel('customer/address')->load($customerAddressId);
            $province =$address->getRegionId();
            
			$this->_collection->addEnableCustomerFilter($groupId)
				->addFieldToFilter('gender', array('in' => array(0, (int)$customer->getGender())))
				->addFieldToFilter('age', array('in' => array(0,$this->getAgeOption($customer))));
				
            if(!is_array($province)){
				$province=array($province);
				$this->_collection->addEnableProvinceFilter(array_merge(array('0'), $province));
			}
		} else {
			$this->_collection->addEnableCustomerFilter($this->_groupId);
            $this->_collection->addEnableProvinceFilter($this->_province);
       	}
		
        if (Mage::registry('current_category')) {
            $_categoryId = Mage::registry('current_category')->getId();
            $this->_collection->addCategoryFilter($_categoryId);
        } elseif (Mage::app()->getFrontController()->getRequest()->getRouteName() == 'cms') {
            $_pageId = Mage::getBlockSingleton('cms/page')->getPage()->getPageId();
            $this->_collection->addPageFilter($_pageId);
        }

        if ($position) {
            $this->_collection->addPositionFilter($position);
        } elseif ($this->_position) {
            $this->_collection->addPositionFilter($this->_position);
        }
        
        return $this->_collection;      
    }
}