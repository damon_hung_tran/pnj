<?php
/**
 * Atwix
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Atwix
 * @package     Atwix_ExtendedGrid
 * @author      Atwix Core Team
 * @copyright   Copyright (c) 2014 Atwix (http://www.atwix.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Atwix_ExtendedGrid_Model_Observer
{
    /**
     * Joins extra tables for adding custom columns to Mage_Adminhtml_Block_Sales_Order_Grid
     * @param Varien_Object $observer
     * @return Atwix_Exgrid_Model_Observer
     */
    public function salesOrderGridCollectionLoadBefore($observer)
    {
        $collectionLoaded = Mage::registry('collection_loaded');
        if(!$collectionLoaded){
            $collection = $observer->getOrderGridCollection();
            $select = $collection->getSelect();
            
            //  $select->joinLeft(array('payment' => $collection->getTable('sales/order_payment')), 'payment.parent_id=main_table.entity_id', array('payment_method' => 'method'));
            $select->join('sales_flat_order_item', '`sales_flat_order_item`.order_id=`main_table`.entity_id', array('attribute_set' => new Zend_Db_Expr('group_concat(`sales_flat_order_item`.attribute_set SEPARATOR ", ")'),
                    'sku' => new Zend_Db_Expr('group_concat(`sales_flat_order_item`.sku SEPARATOR ", ")')
            ));
            //         $select->join(array('billing_address' => 'sales_flat_order_address'), '`main_table`.entity_id = `billing_address_a`.parent_id AND `billing_address`.address_type="billing"', array('region','city'));
            $select->join(array('shipping_address' => 'sales_flat_order_address'), '`main_table`.entity_id = `shipping_address`.parent_id AND `shipping_address`.address_type="shipping"', array('shipping_address.telephone','shipping_address.region'));
            $select->group('main_table.entity_id');
            Mage::register('collection_loaded', true);
        }
    }
    
    public function saleOrderPlaceAfter(Varien_Event_Observer $observer) {
        $order = $observer->getOrder();
        $itemCollection = $order->getItemsCollection();
        foreach($itemCollection as $_item){
            $_product = Mage::getModel('catalog/product')->load($_item->getProductId());
            $prodAttributeSet = Mage::getModel('eav/entity_attribute_set')->load($_product->getAttributeSetId())->getAttributeSetName();
            $_item->setData('attribute_set', $prodAttributeSet);
        }
    }
	
	public function romExtendQuoteItem($observer)
    {
        $event = $observer->getEvent();
        $quote_item = $event->getQuoteItem();
        $quote_item->setRealOriginalPrice($quote_item->getProduct()->getPrice());
    }

    public function romExtendOrderItem($observer)
    {	
        $event = $observer->getEvent();
        $order_item = $event->getOrderItem();
        $order_item->setRealOriginalPrice($event->getItem()->getRealOriginalPrice());
    }
}