<?php
class Atwix_ExtendedGrid_Model_Sales_Order_Pdf_Invoice extends Mage_Sales_Model_Order_Pdf_Invoice
{
	/**
     * Return PDF document
     *
     * @param  array $invoices
     * @return Zend_Pdf
     */
    public function getPdf($invoices = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->emulate($invoice->getStoreId());
                Mage::app()->setCurrentStore($invoice->getStoreId());
            }
            $page  = $this->newPage();
            $order = $invoice->getOrder();
            /* Add image */
            $this->insertLogo($page, $invoice->getStore());
            /* Add address */
            $this->insertAddress($page, $invoice->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $order,
                Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $order->getStoreId())
            );
            /* Add document text and number */
            $this->insertDocumentNumber(
                $page,
                Mage::helper('sales')->__('Invoice # ') . $invoice->getIncrementId()
            );
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($invoice->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
			
            /* Add totals */
            $this->insertTotals($page, $invoice);
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->revert();
            }
			$this->_drawFooter($page);
        }
        $this->_afterGetPdf();
        return $pdf;
    }
	protected function insertLogo(&$page, $store = null)
    {
        $this->y = $this->y ? $this->y : 815;
        $image = Mage::getStoreConfig('sales/identity/logo', $store);
        if ($image) {
            $image = Mage::getBaseDir('media') . '/sales/store/logo/' . $image;
            if (is_file($image)) {
                $image       = Zend_Pdf_Image::imageWithPath($image);
                $top         = 830; //top border of the page
                $widthLimit  = 270; //half of the page width
                $heightLimit = 270; //assuming the image is not a "skyscraper"
                $width       = $image->getPixelWidth();
                $height      = $image->getPixelHeight();

                //preserving aspect ratio (proportions)
                $ratio = $width / $height;
                if ($ratio > 1 && $width > $widthLimit) {
                    $width  = $widthLimit;
                    $height = $width / $ratio;
                } elseif ($ratio < 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width  = $height * $ratio;
                } elseif ($ratio == 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width  = $widthLimit;
                }

                $y1 = $top - $height;
                $y2 = $top;
                $x1 = 25;
                $x2 = $x1 + $width;

                //coordinates after transformation are rounded by Zend
                $page->drawImage($image, $x1, $y1, $x2, $y2);
				$font = $this->_setFontBold($page, 16);
				$_value = 'Phiếu giao hàng';
				$page->drawText(trim(strip_tags($_value)),
                        $this->getAlignCenter($_value, 30, 440, $font, 10),
                        $top-30,
                        'UTF-8');
				
                $this->y = $y1 - 10;
            }
        }
    }
	protected function _drawFooter(Zend_Pdf_Page $page)
	{
		/* Add table head */
		$this->_setFontRegular($page, 14);
	//	$page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
	//	$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
	//	$page->setLineWidth(0.5);
	//	$page->drawRectangle(25, $this->y, 570, $this->y-15);
		$this->y = 100;
	//	$page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
		$lines[0][] = array(
				'text' => Mage::helper('sales')->__('Ngày..........tháng.........năm %s', date('Y')),
				'font' => 'italic',
				'feed' => 460,
		);
		$lineBlock = array(
				'lines'  => $lines,
				'height' => 10
		);
		$this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
		$page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
		$this->y -= 3;
		unset($lines[0]);
		
		//columns headers
		$lines[0][] = array(
				'text' => Mage::helper('sales')->__('Khách hàng'),
				'font' => 'bold',
				'feed' => 50,
		);
			
		$lines[0][] = array(
				'text'  => Mage::helper('sales')->__('Đơn vị vận chuyển'),
				'font' => 'bold',
				'feed'  => 250
		);
	
		$lines[0][] = array(
				'text'  => Mage::helper('sales')->__('Thủ kho'),
				'font' => 'bold',
				'feed'  => 480
		);
	
		
		$lineBlock = array(
				'lines'  => $lines,
				'height' => 10
		);
	
		$this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
		$page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
		unset($lines[0]);
		//columns headers
		$lines[0][] = array(
				'text' => Mage::helper('sales')->__('(Ký và ghi rõ họ tên)'),
				'font' => 'italic',
				'feed' => 50,
		);
		$lines[0][] = array(
				'text' => Mage::helper('sales')->__('(Ký và ghi rõ họ tên)'),
				'font' => 'italic',
				'feed' => 250,
		);
		$lines[0][] = array(
				'text'  => Mage::helper('sales')->__('(Ký và ghi rõ họ tên)'),
				'font' => 'italic',
				'feed'  => 480
		);
		
		
		$lineBlock = array(
				'lines'  => $lines,
				'height' => 10
		);
		$this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
		$page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
		$this->y -= 20;
	}
	
	protected function insertTotals($page, $source){
        $order = $source->getOrder();
        $totals = $this->_getTotalsList($source);
        $lineBlock = array(
            'lines'  => array(),
            'height' => 15
        );
        foreach ($totals as $total) {
            $total->setOrder($order)
                ->setSource($source);

            if ($total->canDisplay()) {
                $total->setFontSize(10);
                foreach ($total->getTotalsForDisplay() as $totalData) {
                    $lineBlock['lines'][] = array(
                        array(
                            'text'      => $totalData['label'],
                            'feed'      => 475,
                            'align'     => 'right',
                            'font_size' => $totalData['font_size'],
                            'font'      => 'bold'
                        ),
                        array(
                            'text'      => $totalData['amount'],
                            'feed'      => 565,
                            'align'     => 'right',
                            'font_size' => $totalData['font_size'],
                            'font'      => 'bold'
                        ),
                    );
                }
            }
        }

        $this->y -= 20;
        $page = $this->drawLineBlocks($page, array($lineBlock));
        return $page;
    }
}