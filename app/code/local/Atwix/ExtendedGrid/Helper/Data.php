<?php
/**
 * Atwix
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 * @category    Atwix
 * @package     Atwix_ExtendedGrid
 * @author      Atwix Core Team
 * @copyright   Copyright (c) 2014 Atwix (http://www.atwix.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Atwix_ExtendedGrid_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * parameters for addColumnAfter method
     * @return array
     */
    public function getAttributeSetColumnParams()
    {
        $attr_sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
					->setOrder('attribute_set_name','ASC')
                    ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
                    ->load()
                    ->toOptionHash();
        return array(
            'header' => 'Attribute set',
            'index' => 'attribute_set',
            'type' => 'options',
            'options' => $attr_sets,
            'filter' => 'Atwix_ExtendedGrid_Block_Adminhtml_Widget_Grid_Column_Filter_SelectComplex',
            'renderer' => 'Atwix_ExtendedGrid_Block_Adminhtml_Widget_Grid_Column_Renderer_OptionsComplex',
        );
    }
    
    public function getPaymentMethodColumnParams(){
        $methods = array();
        $allAvailablePaymentMethods = Mage::getModel('payment/config')->getActiveMethods();
		
        foreach($allAvailablePaymentMethods as $method => $arr){
            $methods[$method] = $arr->getTitle();
        }
        return array(
                'header' => 'Payment method',
                'index' => 'payment_method',
                'type' => 'options',
                'options' => $methods,
                'filter_index' => 'payment.method',
        );
		
		
    }
	public function getVietCitiesColumnParams(){ 
        $citiesTop = array("Hồ Chí Minh" => "Hồ Chí Minh", "Hà Nội" => "Hà Nội" , "Đà Nẵng" => "Đà Nẵng", "Cần Thơ" => "Cần Thơ");
		
        $citesList = Mage::getResourceModel('directory/region_collection')
							->addCountryFilter('VN')
							->toOptionArray();
				
		$topCities = array('514' , '508', '512' , '499', '497');
			
        foreach($citesList as $city => $arr){
			if(in_array($arr['value'], $topCities)){
				unset($citesList[$city]);
			}elseif(!empty($city)){
				$cities[$arr['title']] = $arr['title'];
			}
        }
		$cities = array_merge((array)$citiesTop, (array)$cities);
		
        return array(
                'header' => 'Cities',
                'index' => 'region',
                'type' => 'options',
                'options' => $cities,
                'filter_index' =>'shipping_address.region',
        );
    }
    public function getVietCitiesOptions(){
        $citiesTop = array("Hồ Chí Minh" => "Hồ Chí Minh", "Hà Nội" => "Hà Nội" , "Đà Nẵng" => "Đà Nẵng", "Cần Thơ" => "Cần Thơ");
    
        $citesList = Mage::getResourceModel('directory/region_collection')
        ->addCountryFilter('VN')
        ->toOptionArray();
    
        $topCities = array('514' , '508', '512' , '499', '497');
        	
        foreach($citesList as $city => $arr){
            if(in_array($arr['value'], $topCities)){
                unset($citesList[$city]);
            }elseif(!empty($city)){
                $cities[$arr['title']] = $arr['title'];
            }
        }
        $cities = array_merge((array)$citiesTop, (array)$cities);
        return $cities;
    }
}
