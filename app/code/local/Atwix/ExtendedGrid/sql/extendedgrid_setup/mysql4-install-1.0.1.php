<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute(
        'order_item',
        'attribute_set',
        array(
                'type' => 'varchar', /* varchar, text, decimal, datetime */
                'grid' => true /* or true if you wan't use this attribute on orders grid page */
        )
);

$orders = Mage::getModel('sales/order')->getCollection();
foreach($orders as $order){
    $itemCollection = $order->getItemsCollection();
    foreach($itemCollection as $_item){
        $_product = Mage::getModel('catalog/product')->load($_item->getProductId());
        $prodAttributeSet = Mage::getModel('eav/entity_attribute_set')->load($_product->getAttributeSetId())->getAttributeSetName();
        $_item->setData('attribute_set', $prodAttributeSet);
        $_item->save();
    }
}
$installer->endSetup();

	  