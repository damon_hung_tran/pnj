<?php

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'visa_promotion', array(
    'label' => 'Visa Promotion',
    'type' => 'varchar',
    'input' => 'select',
    'backend' => '',
    'frontend' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'is_searchable' => true,
    'filterable' => true,
    'comparable' => false,
    'option' => array(
        'value' => array('optionone' => array('0%'),
            'optiontwo' => array('5%'),
            'optionthree' => array('10%'),
        )
    ),
    'is_visible_on_front' => true,
    'is_visible_in_advanced_search' => true,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
    'used_in_product_listing' => true,
    'used_for_sort_by' => true,
    'unique' => false
));

$installer->addAttribute("quote_address", "visa_discount", array("type"=>"varchar"));
$installer->addAttribute("order", "visa_discount", array("type"=>"varchar"));

$installer->endSetup();
