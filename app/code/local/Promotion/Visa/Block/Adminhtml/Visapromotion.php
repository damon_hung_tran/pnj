<?php

/**
 * @category    DBG
 * @package     DBG Bestseller Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net> (PHT Extended and develop more)
 * @modified    Mladen Lotar <mladen.lotar@surgeworks.com>, Vedran Subotic <vedran.subotic@surgeworks.com>
 */
class Promotion_Visa_Block_Adminhtml_Visapromotion extends Mage_Adminhtml_Block_Widget_Grid_Container {

    /**
     * Set template
     */
    public function __construct() {
        $this->_controller = 'adminhtml_visapromotion';
        $this->_blockGroup = 'visapromotion';
        $this->_headerText = Mage::helper('visapromotion')->__('Products Visa Promotion Manager');
        parent::__construct();
    }

    protected function _prepareLayout() {
        $this->setChild('store_switcher', $this->getLayout()->createBlock('adminhtml/store_switcher', 'store_switcher')->setUseConfirm(false)
        );
        return parent::_prepareLayout();
    }
     public function getGridHtml() {

        return $this->getChildHtml('store_switcher') . $this->getChildHtml('grid');
    }

}
