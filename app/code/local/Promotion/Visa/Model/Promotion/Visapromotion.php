<?php

/**
 * @name         :  Apptha One Step Checkout
 * @version      :  1.7
 * @since        :  Magento 1.4
 * @author       :  Apptha - http://www.apptha.com
 * @copyright    :  Copyright (C) 2011 Powered by Apptha
 * @license      :  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @Creation Date:  June 20 2011
 * @Modified By  :  Bala G
 * @Modified Date:  August 1 2013
 *
 * */
class Promotion_Visa_Model_Promotion_Visapromotion extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    var $_percent = null;
    public function __construct() {
        $this->setCode('visa_discount');
    }
    protected function _getPaymentMethod(){
       $post = Mage::app()->getRequest()->getPost();
		$bankcode = '';
		$bankcode = $post['payment']['bankcode'];
		return $bankcode;
    }
    public function collect(Mage_Sales_Model_Quote_Address $address) {
        parent::collect($address);
        $isVisa = $this->_getPaymentMethod();
        if($isVisa !== 'VISA'){
            return $this;
        }
         $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }
        $quote = $address->getQuote();
        $quoteItems = $quote->getItemsCollection();
        foreach ($quoteItems as $qItem) {
            $visa_promotion = Mage::getModel('catalog/product')->load($qItem->getProduct_id())->getAttributeText('visa_promotion');
            if ($visa_promotion) {
                $base_row_total = (double) $qItem->getRealOriginalPrice();
                $discountAmount += $base_row_total * (double) $visa_promotion / 100;
                $total_promotion += $base_row_total;
            }
        }
        $discount_visa = $discountAmount ? $discountAmount : 0; 
        $discount_visa_convert = Mage::app()->getStore()->convertPrice($discount_visa);
        $address->setData('visa_discount', -$discount_visa_convert );
//        $this->_addAmount($address->getVisaDiscount())->_addBaseAmount($address->setBaseVisaDiscount());
//        $address->setBaseVisaDiscount(-$discount_visa_convert);
//        $address->setVisaDiscount(-$discount_visa_convert);
        $this->_percent = round(($discount_visa_convert * 100) /$total_promotion);
        $address->setGrandTotal($address->getGrandTotal() + $address->getVisaDiscount());
        $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->setBaseVisaDiscount());
        return $this;
    }
    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        parent::fetch($address);
        $amount = $address->getVisaDiscount();
        $isVisa = $this->_getPaymentMethod();
        if($isVisa !== 'VISA'){
            return $this;
        }
        if ($amount != 0) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $this->getLabel(),
                'value' => $amount
            ));
        }
        return $this;
    }
    public function getLabel() {
        return Mage::helper('onestepcheckout')->__('GIẢM THÊM '.$this->_percent.'% TRÊN GIÁ GỐC KHI THANH TOÁN VISA') . ' (' . $this->_percent . '%):';
    }
}
