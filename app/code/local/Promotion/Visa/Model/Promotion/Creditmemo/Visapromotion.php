<?php

/**
 * @name         :  Apptha One Step Checkout
 * @version      :  1.7
 * @since        :  Magento 1.4
 * @author       :  Apptha - http://www.apptha.com
 * @copyright    :  Copyright (C) 2011 Powered by Apptha
 * @license      :  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @Creation Date:  June 20 2011
 * @Modified By  :  Bala G
 * @Modified Date:  August 1 2013
 *
 * */
class Promotion_Visa_Model_Promotion_Creditmemo_Visapromotion extends Mage_Sales_Model_Order_Invoice_Total_Abstract {

    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        $order = $creditmemo->getOrder();
        $orderVisaDiscount = $order->getVisaDiscount();
        if ($orderVisaDiscount) {
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $orderVisaDiscount);
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $orderVisaDiscount);
        }
        return $this;
    }

}
