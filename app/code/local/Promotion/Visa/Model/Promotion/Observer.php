<?php

class Promotion_Visa_Model_Promotion_Observer {

    public function saveVisaDiscount(Varien_Event_Observer $observer) {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress && $shippingAddress->getData('visa_discount')) {
            $order->setData('visa_discount', $shippingAddress->getData('visa_discount'));
        } else {
            $billingAddress = $quote->getBillingAddress();
            $order->setData('visa_discount', $billingAddress->getData('visa_discount'));
        }
        $order->save();
    }

}
