<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Promotion_Visa_Model_Visapromotion extends Mage_Core_Model_Abstract {

    public function getAttributeSetId() {
        return $this->_getAttributeSetIdUseVisaPromotion();
    }

    protected function _getAttributeSetIdUseVisaPromotion() {
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $attribute_id = $eavAttribute->getIdByCode('catalog_product', 'visa_promotion');
        $connection = Mage::getSingleton('core/resource')
                ->getConnection('core_read');
        $select = $connection->select()
                ->from('eav_entity_attribute', array('attribute_set_id'))
                ->where('attribute_id=?', $attribute_id);
        return $connection->fetchAll($select);
    }

}
