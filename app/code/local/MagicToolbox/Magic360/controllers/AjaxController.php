<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Masoi
 * Date: 1/8/14
 * Time: 12:09 PM
 * To change this template use File | Settings | File Templates.
 */
class MagicToolbox_Magic360_AjaxController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->_title($this->__('Magic 360&#8482; Settings'));
        $this->loadLayout()->_setActiveMenu('magictoolbox/magic360')->renderLayout();
    }
}