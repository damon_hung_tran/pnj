<?php
class Dbiz_Preorder_Model_Resource_Product_Customer extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('preorder/product_customer', 'product_id');
        $this->_isPkAutoIncrement = false;
    }
}
