<?php
class Dbiz_Preorder_Model_Product_Customer extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('preorder/product_customer');
    }
}