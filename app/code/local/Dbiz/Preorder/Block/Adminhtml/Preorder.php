<?php

class Dbiz_Preorder_Block_Adminhtml_Preorder extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {
        $this->_controller = 'adminhtml_preorder';
        $this->_blockGroup = 'preorder';
        $this->_headerText = Mage::helper('preorder')->__('Pre-order Manager');
        parent::__construct();
        $this->_removeButton('add');
    }
}