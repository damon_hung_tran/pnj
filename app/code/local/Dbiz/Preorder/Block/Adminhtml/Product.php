<?php

class Dbiz_Preorder_Block_Adminhtml_Product extends Mage_Adminhtml_Block_Widget_Grid_Container {

    protected $_saveButtonLabel = 'Save Dbiz Preorder Products';

    public function __construct() 
    {
        $this->_blockGroup = 'preorder';
        $this->_controller = 'adminhtml_product';

        $this->_headerText = Mage::helper('adminhtml')->__('Dbiz preorder products');

        parent::__construct();

        $this->_removeButton('add');

        $this->_addButton('save', array(
            'label' => $this->_saveButtonLabel,
            'onclick' => 'categorySubmit(\'' . $this->getSaveUrl() . '\')',
            'class' => 'Save',
        ));
    }

    public function getSaveUrl() {
        return $this->getUrl('*/*/save', array('store' => $this->getRequest()->getParam('store')));
    }

    protected function _afterToHtml($html) {
        return $this->_prependHtml() . parent::_afterToHtml($html);
    }

    private function _prependHtml() {
        $html = '
        
        <form id="preorder_product_form" action="' . $this->getSaveUrl() . '" method="post" enctype="multipart/form-data">
        <input name="form_key" type="hidden" value="' . $this->getFormKey() . '" />
            <div class="no-display">
                <input type="hidden" name="preorder_products" id="in_preorder_products" value="" />
            </div>
        </form>
        ';

        return $html;
    }


    public function getHeaderHtml() {
        return '<h3 style="background-image: url(' . $this->getSkinUrl('images/product_rating_full_star.gif') . ');" class="' . $this->getHeaderCssClass() . '">' . $this->getHeaderText() . '</h3>';
    }

    protected function _prepareLayout() {
        $this->setChild('store_switcher', $this->getLayout()->createBlock('adminhtml/store_switcher', 'store_switcher')->setUseConfirm(false)
        );
        return parent::_prepareLayout();
    }

    public function getGridHtml() {

        return $this->getChildHtml('store_switcher') . $this->getChildHtml('grid');
    }

}