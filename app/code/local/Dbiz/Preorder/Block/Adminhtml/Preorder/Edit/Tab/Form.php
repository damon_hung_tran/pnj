<?php

    class Dbiz_Preorder_Block_Adminhtml_Preorder_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
    {
        protected function _prepareForm()
        {
            $form = new Varien_Data_Form();
            $this->setForm($form);
            $fieldset = $form->addFieldset('preorder_form', array('legend'=>Mage::helper('preorder')->__('information')));

            $fieldset->addField('name', 'label', array(
                'label'     => Mage::helper('preorder')->__('Name'),
                'name'      => 'name',
            ));
            $fieldset->addField('email', 'label', array(
                'label'     => Mage::helper('preorder')->__('Email'),
                'name'      => 'email',
            ));
            $fieldset->addField('phone', 'label', array(
                'label'     => Mage::helper('preorder')->__('Phone'),
                'name'      => 'phone',
            ));
            $fieldset->addField('address', 'label', array(
                'label'     => Mage::helper('preorder')->__('Address'),
                'name'      => 'address',
            ));
            $fieldset->addField('product_sku', 'label', array(
                'label'     => Mage::helper('preorder')->__('Product SKU'),
                'name'      => 'product_sku',
            ));
            $fieldset->addField('product_name', 'label', array(
                'label'     => Mage::helper('preorder')->__('Product Name'),
                'name'      => 'product_name',
            ));
            
            $data = Mage::registry('preorder_data')->getData();
            if( !empty($data['handle']) ){
                $fieldset->addField('handle', 'label', array(
                    'label'     => Mage::helper('preorder')->__('Processed By'),
                    'name'      => 'handle',
                )); 
            }
            $fieldset->addField('note', 'textarea', array(
                'label'     => Mage::helper('preorder')->__('Note'),
                'name'      => 'note',
            ));

            $status_options = array();
            $status = Mage::getModel('preorder/status')->getCollection();
            foreach ($status as $s) $status_options[] = array('value'=>$s->getStatusId(), 'label'=>$s->getName());
            $fieldset->addField('status', 'select', array(
                'label'     => Mage::helper('preorder')->__('Status'),
                'name'      => 'status',
                'values'    => $status_options,
            ));

            if ( Mage::getSingleton('adminhtml/session')->getPreorderData() )
            {
                $form->setValues(Mage::getSingleton('adminhtml/session')->getPreorderData());
                Mage::getSingleton('adminhtml/session')->setPreorderData(null);
            }elseif ( Mage::registry('preorder_data') ) {
                $form->setValues(Mage::registry('preorder_data')->getData());
            }

            return parent::_prepareForm();
        }
    }
