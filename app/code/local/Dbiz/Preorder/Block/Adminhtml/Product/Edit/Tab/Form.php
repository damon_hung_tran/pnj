<?php

    class Dbiz_Preorder_Block_Adminhtml_Product_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
    {
        protected function _prepareForm()
        {
            $form = new Varien_Data_Form();
            $this->setForm($form);
            $fieldset = $form->addFieldset('product_form', array('legend'=>Mage::helper('preorder')->__('information')));

            $fieldset->addField('product_sku', 'label', array(
                'label'     => Mage::helper('preorder')->__('Product SKU'),
                'name'      => 'product_sku',
            ));
            $fieldset->addField('product_name', 'label', array(
                'label'     => Mage::helper('preorder')->__('Product Name'),
                'name'      => 'product_name',
            ));
            $fieldset->addField('customer_count', 'text', array(
                'label'     => Mage::helper('preorder')->__('Customer count'),
                'name'      => 'customer_count',
            ));
            $fieldset->addField('customer_text', 'textarea', array(
                'label'     => Mage::helper('preorder')->__('Customer List'),
                'name'      => 'customer_text',
            ));
            
            if ( Mage::getSingleton('adminhtml/session')->getProductData() )
            {
                $form->setValues(Mage::getSingleton('adminhtml/session')->getProductData());
                Mage::getSingleton('adminhtml/session')->setProductData(null);
            }elseif ( Mage::registry('product_data') ) {
                $form->setValues(Mage::registry('product_data')->getData());
            }

            return parent::_prepareForm();
        }
    }
