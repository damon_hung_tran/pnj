<?php

    class Dbiz_Preorder_Adminhtml_ProductController extends Mage_Adminhtml_Controller_Action
    {
        protected function _initProduct() {

            $product = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));


            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }

            $product->setData('_edit_mode', true);

            Mage::register('product', $product);

            return $product;
        }

        public function indexAction()
        {
            $this->_initProduct();

            $this->loadLayout()->_setActiveMenu('dbiz_solutions');
            $this->_addContent($this->getLayout()->createBlock('preorder/adminhtml_product'));
            $this->renderLayout();
        }

        public function gridAction() 
        {
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('preorder/adminhtml_product_grid')->toHtml()
            );
        }
        
        public function editAction() 
        {
            $id           = $this->getRequest()->getParam('id');
            $model        = Mage::getModel('preorder/product_customer')->load($id);
            $productInfo  = Mage::getModel('preorder/preorder')->getProduct($id);
            $model->productName = $productInfo['product_name'];
            $model->productSku  = $productInfo['product_sku'];

            if ($model->getId() || $id == 0) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if (!empty($data)) {
                    $model->setData($data);
                }
                
                Mage::register('product_data', $model);

                $this->loadLayout();
                $this->_setActiveMenu('dbiz_solutions');
                $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
                $this->_addContent($this->getLayout()->createBlock('preorder/adminhtml_product_edit'))->_addLeft($this->getLayout()->createBlock('preorder/adminhtml_product_edit_tabs'));
                $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);

                $this->renderLayout();
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('preorder')->__('Item does not exist'));
                $this->_redirect('*/*/');
            }
        }

        public function saveAction() 
        {
            $data = $this->getRequest()->getPost();
            $collection = Mage::getModel('catalog/product')->getCollection();
            $storeId = $this->getRequest()->getParam('store', 0);

            parse_str($data['preorder_products'], $preorder_products);

            $collection->addIdFilter(array_keys($preorder_products));

            try {
                $productCustomer = Mage::getModel('preorder/product_customer');
                // Update Product and insert to dbiz_preorder_customers table
                foreach ($collection->getItems() as $product)
                {
                    $data = explode('_', $preorder_products[$product->getEntityId()]);
                    
                    // Save product
                    $product->setData('dbiz_preorder_product', $data[0]);
                    $product->setStoreId($storeId);
                    $product->save();
                    
                    // Insert OR delete product_customer
                    if( $data[0] == 0 ){
                        $productCustomer->load($product->getEntityId())->delete();
                    }else{
                        $productCustomer->setProductId($product->getEntityId());
                        $productCustomer->save();    
                    }
                }
                $this->_getSession()->addSuccess($this->__('Dbiz preorder product was successfully saved.'));
                $this->_redirect('*/*/index', array('store' => $this->getRequest()->getParam('store')));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/index', array('store' => $this->getRequest()->getParam('store')));
            }
        }
        
        public function saveEditAction()
        {
            if ($data = $this->getRequest()->getPost()) 
            {
                $model = Mage::getModel('preorder/product_customer');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                
                try {
                    $model->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('preorder')->__('successfully saved'));
                    Mage::getSingleton('adminhtml/session')->setFormData(false);

                    if ($this->getRequest()->getParam('back')) {
                        $this->_redirect('*/*/edit', array('id' => $model->getId()));
                        return;
                    }
                    $this->_redirect('*/*/');
                    return;
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
            }
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('preorder')->__('Unable to find item to save'));
            $this->_redirect('*/*/');
        }
        
        protected function _validateSecretKey() 
        {
            return true;
        }

        protected function _isAllowed() 
        {
            return Mage::getSingleton('admin/session')->isAllowed('admin/catalog/preorder');
        }

        private function _getExportFileName($extension='csv') 
        {

            $store_id = $this->getRequest()->getParam('store');

            $name = 'preorder_products_';

            if ($store_id) {
                $store = Mage::getModel('core/store')->load($store_id);

                if ($store && $store->getId()) {
                    return $name . $store->getName() . '.' . $extension;
                }
            }

            return $name . 'AllStores.' . $extension;
        }

        /**
        * Export stylist grid to CSV format
        */
        public function exportCsvAction() {

            $fileName = $this->_getExportFileName('csv');

            $content = $this->getLayout()->createBlock('preorder/adminhtml_product_grid')
            ->getCsvFile();

            $this->_prepareDownloadResponse($fileName, $content);
        }

        /**
        * Export stylist grid to XML format
        */
        public function exportXmlAction() {

            $fileName = $this->_getExportFileName('xml');

            $content = $this->getLayout()->createBlock('preorder/adminhtml_product_grid')
            ->getExcelFile();

            $this->_prepareDownloadResponse($fileName, $content);
        }
}