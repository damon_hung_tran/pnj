<?php
class Dbiz_Preorder_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if ($data = $this->getRequest()->getPost()) 
        {
            // Insert to dbiz_preorders table
            $model = Mage::getModel('preorder/preorder');
            $model->setData($data)->setCreatedTime(now())->setUpdateTime(now())->setStoreId(Mage::app()->getStore()->getId())->save();
            
            // Update to dbiz_preorder_customers
            $model = Mage::getModel('preorder/product_customer');
            $customerProduct = $model->load($data['product_id']);
            $customerProduct->setCustomerCount((int)$customerProduct->getCustomerCount() + 1);
            $customerProduct->save();
            echo 'inserted';
        }else{
            echo 'not insert';
        }
    }
}