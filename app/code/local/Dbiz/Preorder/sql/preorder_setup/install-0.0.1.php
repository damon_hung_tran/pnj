<?php

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'dbiz_preorder_product', array(
    'group'             => 'General',
    'type'              => 'int',
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Dbiz preorder product',
    'input'             => 'boolean',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '0',
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
    'is_configurable'   => false
));

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('preorder/preorder')};
CREATE TABLE {$this->getTable('preorder/preorder')} (
    `preorder_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
    `product_id` int( 11 ) unsigned NOT NULL ,
    `name` varchar( 32 ) NOT NULL default '',
    `email` varchar( 32 ) default NULL,
    `phone` varchar( 32 ) NOT NULL default '',
    `address` varchar( 255 ) default NULL,
    `status` tinyint( 1 ) NOT NULL default 1,
    `note` varchar( 255 ) default NULL,
    `processed_by` varchar( 20 ) default NULL,
    `created_time` datetime default NULL ,
    `update_time` datetime default NULL ,
    `store_id` int( 11 ) unsigned NOT NULL ,
    PRIMARY KEY ( `preorder_id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS {$this->getTable('preorder/product_customer')};
CREATE TABLE {$this->getTable('preorder/product_customer')} (
    `product_id` int( 11 ) unsigned NOT NULL,
    `customer_count` int( 11 ) unsigned default NULL,
    `customer_text` TEXT default NULL,
    PRIMARY KEY ( `product_id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS {$this->getTable('preorder/status')};
CREATE TABLE {$this->getTable('preorder/status')} (
    `status_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
    `name` varchar( 20 ) NOT NULL default '',
    `created_time` datetime default NULL ,
    `update_time` datetime default NULL ,
    `status` tinyint( 1 ) NOT NULL default 1,
    PRIMARY KEY ( `status_id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO {$this->getTable('preorder/status')}
VALUES (NULL , 'Mới', NOW( ), NOW( ), 1), (NULL , 'Đang xử lý', NOW( ), NOW( ), 1), (NULL , 'Đã xử lý', NOW( ), NOW( ), 1), (NULL , 'Hủy', NOW( ), NOW( ), 1);

");

$installer->endSetup(); 
