<?php

    class Dbiz_Quickview_Block_Quickview extends Mage_Catalog_Block_Product_Abstract
    {
        public function _prepareLayout()
        {
            return parent::_prepareLayout();
        }

        public function getQuickview($productId)
        {
            $_product = Mage::getModel('catalog/product')->load($productId);

            $this->setTemplate('dbiz_quickview/index.phtml');
            $this->setProduct($_product);
            return $this->toHtml();
        }
}