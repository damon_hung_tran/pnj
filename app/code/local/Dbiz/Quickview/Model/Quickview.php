<?php

class Dbiz_Quickview_Model_Quickview extends Mage_Core_Model_Abstract {
    
    protected function _construct()
    {
        parent::_construct();
        $this->_init('quickview/quickview');
    }
}