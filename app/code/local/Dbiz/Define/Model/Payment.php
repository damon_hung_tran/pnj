<?php

class Dbiz_Define_Model_Payment extends Mage_Adminhtml_Model_System_Config_Source_Payment_Allmethods {
	
	public function toOptionArray() {
		$methods = array();
        $payments = Mage::getSingleton('payment/config')->getActiveMethods();
        foreach ($payments as $paymentCode=>$paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = array(
                'label'   => $paymentTitle,
                'value' => $paymentCode,
            );
        }
        return $methods;
	}
	
}