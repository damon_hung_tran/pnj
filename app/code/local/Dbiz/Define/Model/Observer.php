<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dbiz_Define_Model_Observer {
    const GIFT_CARD_INFO = 1;
    public function checkGiftCard($observer){
        $result = new Varien_Object();
        $result->setHasError(false);
        $quoteItem = $observer->getEvent()->getItem();
        $attSetId = $quoteItem->getProduct()->getAttributeSetId();
        if($attSetId == 4){
            $quoteItem->addMessage(
                    Mage::helper('define')->__('GIFT_CARD_INFO')
                );
            
        }
        
        return $this;
    }
    
}