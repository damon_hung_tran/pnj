<?php
/**
 * Dbiz
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE_EULA.html.
 * It is also available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Dbiz
 * @package    Dbiz_Catalog
 * @author     Pham Tan
 */

class Dbiz_Catalog_Helper_Observer extends Mage_Catalog_Helper_Data
{
	
	function limitProductCompare(Varien_Event_Observer $event) {
		if (Mage::helper('catalog/product_compare')->getItemCount()< Mage::getStoreConfig('catalog/recently_products/compared_count') ) return;

		$session = Mage::getSingleton('catalog/session');
		Mage::getSingleton('catalog/product_compare_list')->removeProduct($event->getProduct());

		$session->getMessages()->clear();
		$session->addError($this->__('You have reached the limit of products to compare. Remove one and try again.'));
		
	} 
	
}
