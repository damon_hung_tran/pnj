<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dbiz_Catalog_Adminhtml_Import_OrderController extends Mage_Adminhtml_Controller_Action{
    
    public function indexAction(){
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function getSubCategoryAction(){
        $_cid = $this->getRequest()->getPost('cat');
        $html = '';
        if($_cid != ''){
            $category_model = Mage::getModel('catalog/category');
            $cat = $category_model->load($_cid);
            $subCat = explode(',',$cat->getChildren());
            $sub_collection  = $category_model
                    ->getCollection()
                    ->addAttributeToSelect(array('name', 'entity_id'))
                    ->addFieldToFilter("entity_id", array("in", $subCat) );
            $cat_data = array();
            foreach($sub_collection as $catname){
                $cat_data[$catname->getId()] = $catname->getName();
            } 
            $html = $this->getLayout()->createBlock('dbiz_catalog/import_ajaxCate')->setData('categories',$cat_data)->toHtml();
        }
        $this->getResponse()->setBody($html);
    }
    
    public function updateAction(){
        $request = $this->getRequest();
        $cid = $request->getPost('cid');
        $category = Mage::getModel('catalog/category')->load($cid);
        $positions = $category->getProductsPosition();
        $data = file_get_contents($_FILES['orders']['tmp_name']);
        $datas = explode("\n", $data);
        $black_list = array();
        $utf8bom = "\xef\xbb\xbf"; 
        $stt = 1;
        $is_update = false;
        foreach($datas as $_row){
            if(trim($_row) == ''){
                continue;
            }
            $tmp = explode(',', $_row);
            foreach($tmp as &$_tmp){
                $_tmp = trim($_tmp);
            }
            if($stt == 1){
                str_replace($utf8bom, '', $tmp[0]);
            }
            $product = Mage::getSingleton('catalog/product');
            $productId = $product->getIdBySku($tmp[0]);
            if(!$productId) {
                $black_list[$tmp[0]] = 'Sku không tồn tại';
            }else if(!isset($positions[$productId])){
                $black_list[$tmp[0]] = 'Sku không có trong danh mục sản phẩm cập nhật';
            }else if(!isset($positions[$productId])){
                $black_list[$tmp[0]] = 'Sku có thứ tự sai('.$tmp[1].')';
            }else{
                $positions[$productId] = $tmp[1];
                $is_update = true;
            }
            $stt ++;   
        }
        if($is_update){
            $category->setPostedProducts($positions);
            $category->save();
        }
        if(count($black_list) > 0){
            foreach($black_list as $pid => $info){
                Mage::getSingleton('core/session')->addError('Sku:['.$pid.']: '.$info);
            }
        }else{
            Mage::getSingleton('core/session')->addSuccess('Import sucessful');
        }
        $this->_redirect('/adminhtml_import_order/result/');
    }
    
    public function resultAction(){
        $this->loadLayout();
        $this->renderLayout();
    }
}