<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Dbiz_Catalog_Blocks_Import_Order extends Mage_Core_Block_Template {
    
    public function getFirstCategory(){
        $categories = Mage::getModel('catalog/category') ->getCollection() 
                ->addAttributeToSelect('*') 
                ->addAttributeToFilter('level',2);
        
        return $categories;
    }
    
}