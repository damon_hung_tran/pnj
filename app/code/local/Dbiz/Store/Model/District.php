<?php
class Dbiz_Store_Model_District extends Mage_Core_Model_Abstract {

	protected function _construct() {
		parent::_construct();
		$this->_init('store/district');
	}
	
	public function getDistrictList($region_id) {
		//Load product model collecttion filtered by attribute set id
		$items = array();
        if(!empty($region_id)){
            $items = Mage::getModel('store/district')
            ->getCollection()
            ->addFieldToFilter('region_id', $region_id);
        }
        
        //process your product collection as per your bussiness logic
        $values = array();
        if (count($items) > 0)
        {
			foreach($items as $item)
			{
	            $values[] = array('value'=>$item->getDistrictId(),'label'=>$item->getDefaultName());
	        }
        }
        
        //return all products name with attribute set 'my_custom_attribute'
        return $values;
	}
	
}
?>