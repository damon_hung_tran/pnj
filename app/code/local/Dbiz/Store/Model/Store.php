<?php

class Dbiz_Store_Model_Store extends Mage_Core_Model_Abstract {

	protected function _construct() {
		parent::_construct();
		$this->_init('store/store');
	}

	function getListRegionOption() {
		$_collection = Mage::getModel('directory/region')
		->getCollection()
		->addFieldToFilter('country_id', 'VN');
		$option = array();
		foreach($_collection as $item){
			$options[$item->getId()] = $item->getDefaultName();
		}
		return $options;
	}
	
	function getListDistrictOption($region_id) {
		$region_id = empty($region_id) ? 485  : $region_id;
		$_collection = Mage::getModel('store/district')
		->getCollection()
		->addFieldToFilter('region_id', $region_id);
		$option = array();
		foreach($_collection as $item){
			$options[$item->getId()] = $item->getDefaultName();
		}
		return $options;
	}

}
