<?php
class Dbiz_Store_Model_Mysql4_District_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	/**
	 * Constructor method
	 */
	protected function _construct() {
		$this->_init('store/district');
		$this->_map['fields']['id'] = 'main_table.district_id';
		$this->_map['fields']['region_id'] = 'region.region_id';
	}

	protected function _renderFiltersBefore() {
		$this->getSelect()->join(
			array('region' => $this->getTable('store/region')), 'main_table.region_id = region.region_id', array()
		);
		return parent::_renderFiltersBefore();
	}

}