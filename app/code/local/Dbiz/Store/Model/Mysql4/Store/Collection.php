<?php

/**
 * Booking Resource Collection
 *
 * @category   PHT
 * @package    PHT_Bookings
 * @author     
 */
class Dbiz_Store_Model_Mysql4_Store_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	/**
	 * Constructor method
	 */
	protected function _construct() {
		$this->_init('store/store');
		$this->_map['fields']['id'] = 'main_table.id';
		$this->_map['fields']['region_id'] = 'region.region_id';
		$this->_map['fields']['district_id'] = 'district.district_id';
	}

	protected function _renderFiltersBefore() {
		$this->getSelect()->join(
			array('region' => $this->getTable('store/region')), 'main_table.region_id = region.region_id', array()
		);
		$this->getSelect()->join(
			array('district' => $this->getTable('store/district')), 'main_table.district_id = district.district_id', array()
		);
		return parent::_renderFiltersBefore();
	}

}
