<?php
class Dbiz_Store_Model_Mysql4_District extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * Initialize resource model
	 */
	protected function _construct() {
		$this->_init('store/district', 'district_id');
	}

}
?>