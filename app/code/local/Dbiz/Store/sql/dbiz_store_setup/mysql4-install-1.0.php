<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS `{$this->getTable('dbiz_stores')}`;
	
CREATE TABLE `dbiz_stores` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`region_id` INT(11) NOT NULL,
	`district_id` INT(11) NOT NULL,
	`address` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`tel` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`published` TINYINT(1) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `region_id` (`region_id`),
	INDEX `district_id` (`district_id`)
)
COLLATE='utf8_unicode_ci'");

$installer->endSetup();