<?php
class Dbiz_Store_Adminhtml_AjaxController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction ()
    {
        $this->loadLayout();
        $req  = Mage::app()->getRequest();
        $this->renderLayout();
    }
    
    public function getdistrictAction() {
		$region_id = $this->getRequest()->getParam('region_id');
        $districts = Mage::getModel('store/district')->getDistrictList($region_id);
        $json = json_encode($districts);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($json);
    }
}