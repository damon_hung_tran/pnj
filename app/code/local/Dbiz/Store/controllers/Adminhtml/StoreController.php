<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 */

/**
 * Booking controller
 */
class Dbiz_Store_Adminhtml_StoreController extends Mage_Adminhtml_Controller_Action {

	protected function _initAction() {
		$this->loadLayout()
				->_setActiveMenu('store');
		return $this;
	}

	public function indexAction() {
		$this->_title($this->__('Manage Store'));
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('store/adminhtml_store'));
		$this->renderLayout();
	}

	public function addAction() {
		$this->_forward('edit');
	}

	public function editAction() {
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('store/store')->load($id);
			
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('store_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('store/items');

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('store/adminhtml_store_edit'))
					->_addLeft($this->getLayout()->createBlock('store/adminhtml_store_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('store')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	
	public function saveAction()
	{
		 if ($data = $this->getRequest()->getPost()) 
		 {
            $_model = Mage::getModel('store/store');
            
            $_model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));
            $_model->save();
            
            if ($this->getRequest()->getParam('back')) 
            {
            	$this->_redirect('*/*/edit', array('id' => $_model->getId()));
                return;
            }
            
            $this->_redirect('*/*/');
                return;
		 }
		 
		 Mage::getSingleton('adminhtml/session')->addError(Mage::helper('store')->__('Unable to find store to save'));
         $this->_redirect('*/*/');
	}
	
	public function deleteAction()
	{
		if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('store/store');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Store was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
	}
}
