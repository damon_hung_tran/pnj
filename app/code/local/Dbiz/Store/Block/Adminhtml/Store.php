<?php

class Dbiz_Store_Block_Adminhtml_Store extends Mage_Adminhtml_Block_Widget_Grid_Container {
	
    public function __construct() {
        $this->_controller = 'adminhtml_store';
        $this->_blockGroup = 'store';
        $this->_headerText = Mage::helper('store')->__('Manage Store');
        parent::__construct();
		$this->setTemplate('dbiz_store/store.phtml');
    }
	
	protected function _prepareLayout() {	
        $this->setChild('add_new_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'label'     => Mage::helper('store')->__('Add Store'),
                'onclick'   => "setLocation('".$this->getUrl('*/*/add')."')",
                'class'   => 'add'
                ))
        );
        /**
         * Display store switcher if system has more one store
         */
        $this->setChild('grid', $this->getLayout()->createBlock('store/adminhtml_store_grid', 'store.grid'));
        return parent::_prepareLayout();
    
	}
	
	public function getAddNewButtonHtml() {
        return $this->getChildHtml('add_new_button');
    }

    public function getGridHtml() {
        return $this->getChildHtml('grid');
    }

    public function getStoreSwitcherHtml() {
        return $this->getChildHtml('store_switcher');
    }
	
}