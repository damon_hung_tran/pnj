<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 */

/**
 * Store grid
 */
class Dbiz_Store_Block_Adminhtml_Store_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    
	public function __construct() {
        parent::__construct();
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('store/store')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('id', array(
                'header'    => 	Mage::helper('store')->__('ID'),
                'align'     =>	'right',
                'width'     => 	'50px',
                'index'     => 	'id',
        ));
		$this->addColumn('region_id', array(
                'header'    => 	Mage::helper('store')->__('Region'),
                'index'     => 	'region_id',
        ));
		$this->addColumn('district_id', array(
                'header'    => 	Mage::helper('store')->__('District'),
                'index'     => 	'district_id',
        ));
		$this->addColumn('default_name', array(
                'header'    => 	Mage::helper('store')->__('District'),
                'index'     => 	'default_name',
        ));
		
		$this->addColumn('address', array(
                'header'    => 	Mage::helper('store')->__('Address'),
                'index'     => 	'address',
        ));
		$this->addColumn('tel', array(
                'header'    => 	Mage::helper('store')->__('Tel'),
                'index'     => 	'tel',
        ));
		$this->addColumn('email', array(
                'header'    => 	Mage::helper('store')->__('Email'),
                'index'     => 	'email',
        ));
		$this->addColumn('published', array(
                'header'    => 	Mage::helper('store')->__('Published'),
                'index'     => 	'published',
				'type'      => 'options',
                'options'   => array(
                        1 => Mage::helper('store')->__('Enabled'),
                        0 => Mage::helper('store')->__('Disabled'),
                )
        ));
        $this->addColumn('action',
                array(
                'header'    =>  Mage::helper('store')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                        array(
                                'caption'   => Mage::helper('store')->__('Edit'),
                                'url'       => array('base'=> '*/*/edit'),
                                'field'     => 'id'
                        ),
                        array(
                                'caption'   => Mage::helper('store')->__('Delete'),
                                'url'       => array('base'=> '*/*/delete'),
                                'field'     => 'id'
                        )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
        return parent::_prepareColumns();
    }
    
     public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
	
}