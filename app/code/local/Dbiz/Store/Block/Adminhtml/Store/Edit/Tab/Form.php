<?php

class Dbiz_Store_Block_Adminhtml_Store_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $_model = Mage::registry('store_data');
        
        $form = new Varien_Data_Form();
        $this->setForm($form);
		
        $fieldset = $form->addFieldset('store_form', array('legend'=>Mage::helper('store')->__('General Information')));
        $event = $fieldset->addField('region_id', 'select', array(
            'label'     => Mage::helper('store')->__('Region'),
			'title'  	=> Mage::helper('store')->__('Region'),
            'name'      => 'region_id',
			'value'		=> $_model->getRegionId(),
            'options'     => Mage::getSingleton('store/store')->getListRegionOption(),
            'onchange'	=> 'reload_district(this.value)'
        ));
        $event->setAfterElementHtml("<script type=\"text/javascript\">
        	function reload_district(region_id){
//        		alert(region_id);
        		var _url = \"" . $this->getUrl('store/adminhtml_ajax/getdistrict') . "region_id/\" + region_id;
//        		alert(_url);
        		new Ajax.Request(_url, {
                	method: 'get',
                    onLoading: function (transport) {
                                   
                    },
                    onComplete: function(transport) {
                    	$('district_id').innerHTML = '';
                        var json = transport.responseText.evalJSON();
                        json.each(function(element){
                        	$('district_id').options.add(new Option(element.label, element.value));
                    	});
                    }
                });
        	}
        </script>");
		$fieldset->addField('district_id', 'select', array(
            'label'     => Mage::helper('store')->__('District'),
			'title'  	=> Mage::helper('store')->__('District'),
            'name'      => 'district_id',
			'value'		=> $_model->getDistrictId(),
            'options'     => Mage::getSingleton('store/store')->getListDistrictOption($_model->getRegionId())
        ));
        $fieldset->addField('address', 'text', array(
            'label'     => Mage::helper('store')->__('Address'),
			'title'  	=> Mage::helper('store')->__('Address'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'address',
            'value'     => $_model->getAddress()
        ));
		$fieldset->addField('tel', 'text', array(
            'label'     => Mage::helper('store')->__('Tel'),
			'title'  	=> Mage::helper('store')->__('Tel'),
            'name'      => 'tel',
            'value'     => $_model->getTel()
        ));
		
		$fieldset->addField('email', 'text', array(
            'label'     => Mage::helper('store')->__('Email'),
			'title'  	=> Mage::helper('store')->__('Email'),
            'name'      => 'email',
            'value'     => $_model->getEmail()
        ));
		$fieldset->addField('published', 'select', array(
            'label'     => Mage::helper('store')->__('Publsihed'),
			'title'  	=> Mage::helper('store')->__('Publsihed'),
            'name'      => 'published',
            'value'     => $_model->getPublished(),
			'options'	=> array(
				1 => 'Enable',
				0	=> 'Disable'
			)
        ));
        
        return parent::_prepareForm();
    }
}
