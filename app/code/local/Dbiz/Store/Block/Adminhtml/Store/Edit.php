<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dbiz
 * @package     Dbiz_Store
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Store edit block
 *
 * @category   Dbiz
 * @package    Dbiz_Store
 * @author     Dbiz
 */
class Dbiz_Store_Block_Adminhtml_Store_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {
		parent::__construct();

		$this->_objectId = 'id';
		$this->_blockGroup = 'store';
		$this->_controller = 'adminhtml_store';

		$this->_updateButton('save', 'label', Mage::helper('store')->__('Save Store'));
		$this->_updateButton('delete', 'label', Mage::helper('store')->__('Delete Store'));

		$this->_addButton('saveandcontinue', array(
			'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick' => 'saveAndContinueEdit()',
			'class' => 'save',
				), -100);

		$this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
	}

	public function getHeaderText() {
		$id = $this->getRequest()->getParam('id');
		if (isset($i) AND $id > 0) {
			return Mage::helper('store')->__("Edit Store '%s'", $this->htmlEscape(Mage::registry('store_data')->getName()));
		} else {
			return Mage::helper('store')->__("Insert Store");
		}
	}

}
