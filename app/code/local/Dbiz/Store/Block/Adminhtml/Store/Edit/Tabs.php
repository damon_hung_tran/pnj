<?php

class Dbiz_Store_Block_Adminhtml_Store_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

	public function __construct() {
		parent::__construct();
		$this->setId('store_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('store')->__('Store Information'));
	}

	protected function _beforeToHtml() {
		$this->addTab('general_section', array(
			'label' => Mage::helper('store')->__('General Information'),
			'title' => Mage::helper('store')->__('General Information'),
			'content' => $this->getLayout()->createBlock('store/adminhtml_store_edit_tab_form')->toHtml(),
		));

		return parent::_beforeToHtml();
	}

}
