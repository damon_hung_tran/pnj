<?php

	class Dbiz_Store_Block_Store extends Mage_Core_Block_Template {

		public function _prepareLayout() {
			return parent::_prepareLayout();
		}

		public function getRegion()
		{
			$regions = Mage::getModel('store/store')->getListRegionOption();
			return $regions;
		}

		public function getDistrict($region_id)
		{
			$districts = Mage::getModel('store/store')->getListDistrictOption($region_id);
			return $districts;
		}

		public function getItems($region_id, $district_id)
		{
			$_collection = Mage::getModel('store/store')
			->getCollection()
			->addFieldToFilter('region_id', $region_id)
			->addFieldToFilter('district_id', $district_id);
			$option = array();
			return $_collection;
		}
	}
