<?php

/**
 * Customer Master Report
 *
 * @category   local	
 * @package    Dbiz_Dbizreport
 * @author      Hung Tran
 */
class Dbiz_Masterreport_Adminhtml_Customer_MasterreportController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Customer Master Report'), Mage::helper('adminhtml')->__('Customer Master Report'));
        return $this;
    }

    public function indexAction() {

        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('masterreport/adminhtml_customer_grid'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('masterreport/adminhtml_customer_grid')->toHtml()
        );
    }

    public function exportCsvAction() {
        $fileName = 'customermasterreport.csv';
        $content = $this->getLayout()->createBlock('masterreport/adminhtml_customer_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportXmlAction() {
        $fileName = 'customermasterreport.xml';
        $content = $this->getLayout()->createBlock('masterreport/adminhtml_customer_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

}
