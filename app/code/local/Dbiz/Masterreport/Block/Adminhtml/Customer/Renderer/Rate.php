<?php
/**
 * Customer Master Report
 *
 * @category   local	
 * @package    Dbiz_Dbizreport
 * @author      Hung Tran
 */
class Dbiz_Masterreport_Block_Adminhtml_Customer_Renderer_Rate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
     public function render(Varien_Object $row)
    {
        $rate= $row->getData($this->getColumn()->getIndex());
        $point = $row->getData('point');
        $rate_currency = $rate * $point;
       
        return $rate_currency;
    }
}