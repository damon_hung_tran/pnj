<?php
/**
 * Customer Master Report
 *
 * @category   local	
 * @package    Dbiz_Dbizreport
 * @author      Hung Tran
 */
class Dbiz_Masterreport_Block_Adminhtml_Customer_Renderer_Used extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
     public function render(Varien_Object $row)
    {
        $point_used= $row->getData($this->getColumn()->getIndex());
        $point_rate = $row->getData('point_rate');
        $rate_currency = $point_used * $point_rate;
        return $rate_currency;
    }
}