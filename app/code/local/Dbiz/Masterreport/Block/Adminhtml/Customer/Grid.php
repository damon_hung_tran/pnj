<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer Master Report
 *
 * @category   local	
 * @package    Dbiz_Dbizreport
 * @author      Hung Tran
 */
class Dbiz_Masterreport_Block_Adminhtml_Customer_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('customerReportGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('real_order_id');
	 $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('sales/order_collection');
          $collection->getSelect()
                ->joinLeft(
                array('order_address'=>'sales_flat_order_address'), 
                'main_table.entity_id = order_address.parent_id AND order_address.address_type="billing"',array('billing_name'=>'firstname','report_country_id' => 'country_id','report_city' => 'city','report_email' => 'email','report_tell' => 'telephone','report_add' => 'street','report_region' =>'region'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
  $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));
      
        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));
        $this->addColumn('email', array(
            'header' => Mage::helper('masterreport')->__('Email'),
            'width' => '150',
            'index' => 'report_email'
        ));
        $this->addColumn('Telephone', array(
            'header' => Mage::helper('masterreport')->__('Telephone'),
            'width' => '100',
            'index' => 'report_tell'
        ));
        $this->addColumn('billing_street', array(
            'header' => Mage::helper('masterreport')->__('Address'),
            'width' => '100',
            'index' => 'report_add',
        ));
       
        $this->addColumn('billing_city', array(
            'header' => Mage::helper('masterreport')->__('District'),
            'width' => '100',
            'index' => 'report_city',
        ));
        $this->addColumn('billing_region', array(
            'header' => Mage::helper('masterreport')->__('State/Province'),
            'width' => '100',
            'index' => 'report_region',
        ));
        $this->addColumn('billing_country_id', array(
            'header' => Mage::helper('masterreport')->__('Country'),
            'width' => '100',
            'type' => 'country',
            'index' => 'report_country_id',
        ));
        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));
        
//        $this->addColumn('status', array(
//            'header' => Mage::helper('sales')->__('Status'),
//            'index' => 'status',
//            'type'  => 'options',
//            'width' => '70px',
//            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
//        ));
          $this->addExportType('*/*/exportCsv', Mage::helper('masterreport')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('masterreport')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

}
