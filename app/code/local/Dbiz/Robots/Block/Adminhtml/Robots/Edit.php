<?php
/**
 *
 * Package : Robots
 * Edition : local
 * Developed By : Dbiz Group
 * 
 */
class Dbiz_Robots_Block_Adminhtml_Robots_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		$this->setTemplate('robots/robotsadd.phtml');
	}
	
}
