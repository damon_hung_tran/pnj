<?php
class Dbiz_District_Model_Mysql4_District_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
	/**
	 * Constructor method
	 */
	protected function _construct() {
		$this -> _init('district/district');
	}
	protected function _renderFiltersBefore() {
		return parent::_renderFiltersBefore();
	}

	public function joinRegion() {
		$table_name = Mage::getSingleton('core/resource') -> getConnection('core_read') -> getTableName('directory_country_region');
		$this->getSelect() -> joinLeft(array('region' => $table_name), 'main_table.region_id = region.region_id', array('region_name' => 'region.default_name'));
		return $this;
	}
	public function toOptionArray()
    {
        $options = $this->_toOptionArray('district_id', 'default_name', array('title' => 'default_name'));
        return $options;
    }

}
