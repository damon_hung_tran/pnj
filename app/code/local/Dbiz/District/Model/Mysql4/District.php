<?php

class Dbiz_District_Model_Mysql4_District extends Mage_Core_Model_Mysql4_Abstract {
    /**
     * Initialize resource model
     */
    protected function _construct() {
        $this->_init('district/district', 'district_id');
    }

}