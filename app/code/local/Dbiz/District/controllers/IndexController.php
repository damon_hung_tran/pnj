<?php
class Dbiz_District_IndexController extends Mage_Core_Controller_Front_Action
{
    public function IndexAction($hasEmptyOption=true)
    {
        $province = Mage::app()->getRequest()->getPost('province');
        
        $query    = "SELECT * FROM directory_country_district WHERE region_id=$province";
        $districts = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($query); 
        
        $options = '';
        if( $hasEmptyOption ) $options = '<option value="">Vui lòng chọn</option>';
        
        if( !empty($districts) )
        {
            foreach( $districts as $district )
            {
                $options .= '<option value="'. $district['default_name'] .'">'. $district['default_name'] .'</option>';
            }    
        }
        
        echo $options;       
        return;
    }
}