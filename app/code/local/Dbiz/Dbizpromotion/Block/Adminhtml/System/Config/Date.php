<?php

class Dbiz_Dbizpromotion_Block_Adminhtml_System_Config_Date extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $date = new Varien_Data_Form_Element_Date();
        $format = 'dd-MM-yyyy';

        $data = array(
            'name'      => $element->getName(),
            'html_id'   => $element->getId(),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
        );
        $date->setData($data);
        $date->setValue($element->getValue(), $format);
        $date->setFormat($format);
        $date->setForm($element->getForm());

        return $date->getElementHtml();
        
        /*$res = '';
        $divId = $element->getId();
        $res .= <<<EOD
        <input name="{$element->getName()}" id="{$divId}_date" value="{$element->getValue()}" type="text" style="width:110px !important;" /> <img src="{$this->getSkinUrl('images/grid-cal.gif')}" alt="" id="{$divId}_date_trig" title="{$this->__('Select Date')}" style="" />
        <script type="text/javascript">
        //<![CDATA[
            //this example uses dd.MM.yyyy hh:mm format.
            Calendar.setup({
                inputField: "{$divId}_date",
                ifFormat: "%d-%m-%Y %H:%M",
                showsTime: true,
                firstDay: 1,
                timeFormat: "24",
                button: "{$divId}_date_trig",
                align: "Bl",
                singleClick : true
            });
        //]]>
        </script>
EOD;
        return $res;*/
    }
}