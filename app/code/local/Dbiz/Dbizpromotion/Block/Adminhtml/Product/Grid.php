<?php

class Dbiz_Dbizpromotion_Block_Adminhtml_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();

        $this->setId('dbiz_dbizpromotion_products');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);

        $this->setRowClickCallback('productRowClick');
        $this->setCheckboxCheckCallback('registerProduct');
    }

    public function getProduct() {
        return Mage::registry('product');
    }

    protected function _getStore() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _addColumnFilterToCollection($column) {

        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField('websites', 'catalog/product_website', 'website_id', 'product_id=entity_id', null, 'left');
            }
        }

        if ($column->getId() == "dbizpromotion") {
            
            $productIds = $this->_getSelectedProducts();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
            }
        } else {

            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    protected function _prepareCollection() {

        $store = $this->_getStore();

        $collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect('name')
			->addAttributeToSelect('sku')
            ->addAttributeToSelect('attribute_set_id')
			->addAttributeToSelect('dbiz_promotion_product')
			->addAttributeToSelect('type_id')
			->addAttributeToFilter('visibility', array('nin' => array(1, 3)));

        if ($store->getId()) {
            $collection->addStoreFilter($store);
            $collection->joinAttribute('custom_name', 'catalog_product/name', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', 1, 'inner', $store->getId());
            $collection->joinAttribute('price', 'catalog_product/price', 'entity_id', null, 'left', $store->getId());
        } else {
            $collection->addAttributeToSelect('price');
            $collection->addAttributeToSelect('status');
            $collection->addAttributeToSelect('visibility');
        }

        $action_name = $this->getRequest()->getActionName();

        if ($action_name == 'exportCsv' || $action_name == 'exportXml') {
            $collection->addAttributeToFilter('dbiz_promotion_product', array('eq' => true));
        }
		
        $this->setCollection($collection);

        parent::_prepareCollection();

        $this->getCollection()->addWebsiteNamesToResult();

        return $this;
    }

    protected function _prepareColumns() {

        $action_name = $this->getRequest()->getActionName();

        if ($action_name != 'exportCsv' && $action_name != 'exportXml') {

            $this->addColumn('dbizpromotion', array(
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'dbizpromotion',
                'values' => $this->_getSelectedProducts(),
                'align' => 'center',
                'index' => 'entity_id'
            ));
        }

        $this->addColumn('entity_id', array(
            'header' => Mage::helper('catalog')->__('ID'),
            'sortable' => true,
            'width' => '60',
            'index' => 'entity_id',
            'type' => 'number'
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'index' => 'name',
        ));
        
        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setOrder('attribute_set_name','ASC')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();
        $this->addColumn('set_name',
            array(
                'header'=> Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '100px',
                'index' => 'attribute_set_id',
                'type'  => 'options',
                'options' => $sets,
        ));

        $this->addColumn('type', array(
            'header' => Mage::helper('catalog')->__('Type'),
            'width' => '60px',
            'index' => 'type_id',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

        $this->addColumn('sku', array(
            'header' => Mage::helper('catalog')->__('SKU'),
            'width' => '140',
            'index' => 'sku'
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('websites', array(
                'header' => Mage::helper('catalog')->__('Websites'),
                'width' => '100px',
                'sortable' => false,
                'index' => 'websites',
                'type' => 'options',
                'options' => Mage::getModel('core/website')->getCollection()->toOptionHash(),
            ));
        }

        $store = $this->_getStore();
        $this->addColumn('price', array(
            'header' => Mage::helper('catalog')->__('Price'),
            'type' => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
            'index' => 'price',
        ));
		
        $this->addExportType('*/*/exportCsv', Mage::helper('dbizpromotion')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('dbizpromotion')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _getSelectedProducts($json = false) 
    {
        $temp = $this->getRequest()->getPost('dbizpromotion_ids');
        $store = $this->_getStore();

        if ($temp) {
            parse_str($temp, $dbizpromotion_ids);
        }

        $_prod = Mage::getModel('catalog/product')->getCollection()
                ->joinAttribute('dbiz_promotion_product', 'catalog_product/dbiz_promotion_product', 'entity_id', null, 'left', $store->getId())
                ->addAttributeToFilter('dbiz_promotion_product', '1');

        $products = $_prod->getColumnValues('entity_id');
        $selected_products = array();


        if ($json == true) {
            foreach ($products as $key => $value) {
                $selected_products[$value] = '1';
            }
            return Zend_Json::encode($selected_products);
        } else {

            foreach ($products as $key => $value) {
                if ((isset($dbizpromotion_ids[$value])) && ($dbizpromotion_ids[$value] == 0)) {
                    
                }else
                    $selected_products[$value] = '0';
            }

            if (isset($dbizpromotion_ids))
                foreach ($dbizpromotion_ids as $key => $value) {
                    if ($value == 1)
                        $selected_products[$key] = '0';
                }

            return array_keys($selected_products);
        }

        return $products;
    }

    //add javascript before/after grid html
    protected function _afterToHtml($html) {
        return $this->_prependHtml() . parent::_afterToHtml($html) . $this->_appendHtml();
    }

    private function _prependHtml()
    {
        $html = '<script type="text/javascript">
//<![CDATA[
    categoryForm = new varienForm("dbizpromotion_product_form");
	categoryForm.submit= function (url) {
	    this._submit();
        return true;
    };

    function getParamValue(element, checked){
        var value = "0_0";
        if(checked){
		    value = "1" + "_" + "0";
		}
		return value;
    }

    function registerProduct(grid, element, checked){
                
		checkBoxes.set(element.value, getParamValue(element, checked));
		$("in_dbizpromotion_products").value = checkBoxes.toQueryString();
	   	grid.reloadParams = {"dbizpromotion_ids":checkBoxes.toQueryString()};
    }
    
    function categorySubmit(url) {
    	var params = {};
        var fields = $("dbizpromotion_product_form").getElementsBySelector("input", "select");
        categoryForm.submit();
    }
    
    function productRowClick(grid, event){
        var trElement = Event.findElement(event, "tr");
        var isInput   = Event.element(event).tagName == "INPUT";
        
        if(trElement){
            var checkbox = Element.getElementsBySelector(trElement, "input");
            if(checkbox[0]){
                var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                grid.setCheckboxChecked(checkbox[0], checked);
            }
        }
    }
//]]>
</script>';

        return $html;
    }

    private function _appendHtml() {
        $html = '<script type="text/javascript">	
			var checkBoxes = $H();
        </script>';

        return $html;
    }

}