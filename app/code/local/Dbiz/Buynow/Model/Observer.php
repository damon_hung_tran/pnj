<?php
/**
* @category   Dbiz
* @package    Buynow button
* @copyright  Copyright (c) 2013 Wimbolt Ltd (http://www.giaiphapso.com)
* @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
* @author     Tran Ngoc Bao <bao.tran@giaiphapso.com>
* */

class Dbiz_Buynow_Model_Observer
{
    public function addToCartComplete(Varien_Event_Observer $observer) {
        // Send the user to the Item added page
		if( Mage::getStoreConfig("buynow/buynow/active")){
			$response = $observer->getResponse();
			$request = $observer->getRequest();
			if($request->getParam('buynow')){
				$response->setRedirect(Mage::getUrl('onestepcheckout/'));
				Mage::getSingleton('checkout/session')->setNoCartRedirect(true);
			}
		}
        
    }
}