<?php
class Dbiz_Buynow_Block_Buynow extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    public function getTitle()
    {
    	return $this->__("Buynow Button");
    }
}