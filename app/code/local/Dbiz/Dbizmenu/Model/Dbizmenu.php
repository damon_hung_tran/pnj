<?php

class Dbiz_Dbizmenu_Model_Dbizmenu extends Mage_Core_Model_Abstract {

	protected function _construct() {
		parent::_construct();
		$this->_init('dbizmenu/dbizmenu');
	}
	
	function getMenuParentOption($exp_id = null, $parent_id = 0) {
		
		$_collection =  $this->getCollection();
		
		$_collection->getSelect()->where('parent_id = ?', $parent_id)
				->where('menu_id <> ?', $exp_id);
		
		$option = array();
		foreach($_collection as $item){
			$options[$item->getMenuId()] = $item->getName();
		}
		
		return $options;
	}
	
	function getMenus($parent_id = 0) {
		$_collections =  $this->getCollection();
		
		$_collections->getSelect()->where('parent_id = ?', $parent_id)
				->where('published = ?', 1)
				->order('order ASC')
				;
		
		return $_collections;
	}
	
}