<?php

class Dbiz_Dbizmenu_Model_Mysql4_Dbizmenu extends Mage_Core_Model_Mysql4_Abstract {
    /**
     * Initialize resource model
     */
    protected function _construct() {
        $this->_init('dbizmenu/dbizmenu', 'menu_id');
    }

}
