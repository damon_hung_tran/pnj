<?php

/**
 * Booking Resource Collection
 *
 * @category   PHT
 * @package    PHT_Bookings
 * @author     
 */
class Dbiz_Dbizmenu_Model_Mysql4_Dbizmenu_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	/**
	 * Constructor method
	 */
	protected function _construct() {
		$this->_init('dbizmenu/dbizmenu');
	}

	protected function _renderFiltersBefore() {
		return parent::_renderFiltersBefore();
	}

}
