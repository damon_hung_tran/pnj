<?php

class Dbiz_Dbizmenu_Adminhtml_DbizmenuController extends Mage_Adminhtml_Controller_Action {

	protected function _initAction() {
		$this->loadLayout()
				->_setActiveMenu('dbizmenu');
		return $this;
	}

	public function indexAction() {
		$this->_title($this->__('Dbiz Menu Manager'));
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('dbizmenu/adminhtml_dbizmenu'));
		$this->renderLayout();
	}

	public function addAction() {
		$this->_forward('edit');
	}

	public function newAction() {
		$this->_forward('edit');
	}

	public function editAction() {
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('dbizmenu/dbizmenu')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('dbizmenu_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('dbizmenu/items');

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('dbizmenu/adminhtml_dbizmenu_edit'))
					->_addLeft($this->getLayout()->createBlock('dbizmenu/adminhtml_dbizmenu_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('dbizmenu')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function saveAction() {
		$parent_id = $id = $this->getRequest()->getParam('parent_id');
		if (!isset($parent_id)) $parent_id = 0; 
		
		if ($data = $this->getRequest()->getPost()) {
			//Upload image
			if (isset($_FILES['image']['name']) and (file_exists($_FILES['image']['tmp_name']))) {
				try {
					$uploader = new Varien_File_Uploader('image');
					$uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png')); // or pdf or anything
					$uploader->setAllowRenameFiles(false);

					// setAllowRenameFiles(true) -> move your file in a folder the magento way
					// setAllowRenameFiles(true) -> move your file directly in the $path folder
					$uploader->setFilesDispersion(false);
					$path = Mage::getBaseDir('media') . DS . 'menu_icon' .DS ;
					$uploader->save($path, $_FILES['image']['name']);
					$data['image'] = $_FILES['image']['name'];
				} catch (Exception $e) {
					
				}
			} else {

				if (isset($data['image']['delete']) && $data['image']['delete'] == 1)
					$data['image'] = null;
				else
					unset($data['image']);
			}
			
			//Upload banner
			if (isset($_FILES['banner']['name']) and (file_exists($_FILES['banner']['tmp_name']))) {
				try {
					$uploader = new Varien_File_Uploader('banner');
					$uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png')); // or pdf or anything
					$uploader->setAllowRenameFiles(false);

					// setAllowRenameFiles(true) -> move your file in a folder the magento way
					// setAllowRenameFiles(true) -> move your file directly in the $path folder
					$uploader->setFilesDispersion(false);
					$path = Mage::getBaseDir('media') . DS . 'menu_banner' .DS ;
					$uploader->save($path, $_FILES['banner']['name']);
					$data['banner'] = $_FILES['banner']['name'];
				} catch (Exception $e) {
					//
				}
			} else {
				if (isset($data['banner']['delete']) && $data['banner']['delete'] == 1) {
					@unlink( Mage::getBaseDir('media') . DS . 'menu_banner'.DS.$data['banner']);
					$data['banner'] = null;
				} else {
					unset($data['banner']);
				}
					
			}

			$_model = Mage::getModel('dbizmenu/dbizmenu');
			$_model->setData($data)
					->setId($this->getRequest()->getParam('id'));
			$_model->save();

			if ($this->getRequest()->getParam('back')) {
				$this->_redirect('*/*/edit/parent_id/'.$parent_id.'/', array('id' => $_model->getId()));
				return;
			} else {
				$this->_redirect('*/*/index/id/'.$parent_id);
				return;
			}

			
		}

		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('dbizmenu')->__('Unable to find menu to save'));
		$this->_redirect('*/*/index/id/'.$parent_id);
	}

	public function deleteAction() {
		if ($this->getRequest()->getParam('id') > 0) {
			try {
				$model = Mage::getModel('dbizmenu/dbizmenu');

				$model->setId($this->getRequest()->getParam('id'))
						->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Menu was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function getParentId() {
		$parent_id = $this->getRequest()->getParam('id');
		if (!isset($parent_id))
			$parent_id = 0;
		return $parent_id;
	}

	public function backtoparentAction() {
		$current_id = $this->getParentId();
		$parent_id = 0;
		if ($current_id > 0) {
			$item = Mage::getModel('dbizmenu/dbizmenu')->load($current_id);
			$parent_id = $item->getParentId();
		}
		$this->_redirect('*/*/index/id/' . $parent_id);
	}

}
