<?php

class Dbiz_Dbizmenu_Block_Dbizmenu extends Mage_Core_Block_Template {

	public function getMenus($parent_id = 0) {
		$model = Mage::getModel('dbizmenu/dbizmenu');
		$items = $model->getMenus($parent_id);
		return $items;
	}

	public function formatItemUrl($url) {
		$url = trim($url);
		if (substr($url, 0, strlen('http://')) != 'http://' AND substr($url, 0, strlen('https://')) != 'https://') {
			if (substr($url, 0, 1) == '/') {
				$url = substr($url, 1);
			}

			$url = $this->getBaseUrl() . $url;
		}

		return $url;
	}

}
