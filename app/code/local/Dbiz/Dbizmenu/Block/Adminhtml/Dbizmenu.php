<?php

class Dbiz_Dbizmenu_Block_Adminhtml_Dbizmenu extends Mage_Adminhtml_Block_Widget_Grid_Container {

	public function __construct() {
		$this->_controller = 'adminhtml_dbizmenu';
		$this->_blockGroup = 'dbizmenu';
		$this->_headerText = Mage::helper('dbizmenu')->__('Manage Menu');
		parent::__construct();
		$this->setTemplate('dbiz/dbizmenu/dbizmenu.phtml');
	}

	protected function _prepareLayout() {
		$this->setChild('add_new_button', $this->getLayout()->createBlock('adminhtml/widget_button')
						->setData(array(
							'label' => Mage::helper('dbizmenu')->__('Add Menu'),
							'onclick' => "setLocation('" . $this->getUrl('*/*/add') . "')",
							'class' => 'add'
						))
		);

		/**
		 * Display Dbizmenu switcher if system has more one Dbizmenu
		 */
		$this->setChild('grid', $this->getLayout()->createBlock('dbiz_dbizmenu/adminhtml_dbizmenu_grid', 'dbizmenu.grid'));
		return parent::_prepareLayout();
	}

	public function getAddNewButtonHtml() {
		$parent_id = (int) $this->getRequest()->getParam('id');
		$result =  '<button type="submit" class="add" value="add" onclick="setLocation(\''.$this->getUrl('*/*/add/parent_id/' . $parent_id) .'\');"><span><span><span></span>'.$this->__('Add Menu').'</span></span></button>';
		return $result;
	}

	public function getAddBackButtonHtml() {
		$result = null;
		$parent_id = (int) $this->getRequest()->getParam('id');
		if (!isset($parent_id)) {
			$parent_id = 0;
		}
		
		if ($parent_id > 0) {
			$result =  '<button type="submit" class="back" value="back_to_parent" onclick="setLocation(\''.$this->getUrl('*/*/backtoparent/id/' . $parent_id) .'\');"><span><span><span></span>'.$this->__('Back').'</span></span></button>';
		}

		return $result;
	}

	public function getGridHtml() {
		return $this->getChildHtml('grid');
	}

	public function getDbizmenuSwitcherHtml() {
		return $this->getChildHtml('dbizmenu_switcher');
	}

}
