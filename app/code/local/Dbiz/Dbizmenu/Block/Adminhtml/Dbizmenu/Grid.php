<?php

class Dbiz_Dbizmenu_Block_Adminhtml_Dbizmenu_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
	}

	protected function _prepareCollection() {
		
		$parent_id = $this->getParentId();
		
		$collection = Mage::getModel('dbizmenu/dbizmenu')->getCollection()
				->addFieldToFilter('parent_id', $parent_id);
		$this->setCollection($collection);
		
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('menu_id', array(
			'header' => Mage::helper('dbizmenu')->__('ID'),
			'align' => 'center',
			'width' => '50px',
			'index' => 'menu_id',
		));
		$this->addColumn('name', array(
			'header' => Mage::helper('dbizmenu')->__('Title'),
			'index' => 'name',
		));
		$this->addColumn('url', array(
			'header' => Mage::helper('dbizmenu')->__('URL'),
			'index' => 'url',
		));
		$this->addColumn('image', array(
			'header' => Mage::helper('dbizmenu')->__('Icon'),
			'index' => 'image',
			'type'	=> 'image',
			'value'	=> 'image/url',
			'renderer' => 'Dbiz_Dbizmenu_Block_Adminhtml_Dbizmenu_Renderer_Image'
		));
		
		$this->addColumn('order', array(
			'header' => Mage::helper('dbizmenu')->__('Sort Order'),
			'index' => 'order',
			'width' => '50px',
			'align' => 'center'
		));
		
		$this->addColumn('published', array(
			'header' => Mage::helper('dbizmenu')->__('Published'),
			'index' => 'published',
			'type' => 'options',
			'options' => array(
				1 => Mage::helper('dbizmenu')->__('Enabled'),
				0 => Mage::helper('dbizmenu')->__('Disabled'),
			)
		));
		$this->addColumn('action', array(
			'header' => Mage::helper('dbizmenu')->__('Action'),
			'width' => '100',
			'type' => 'action',
			'getter' => 'getId',
			'actions' => array(
				array(
					'caption' => Mage::helper('dbizmenu')->__('Detail'),
					'url' => array('base' => '*/*/'),
					'field' => 'id'
				),
				array(
					'caption' => Mage::helper('dbizmenu')->__('Edit'),
					'url' => array('base' => '*/*/edit'),
					'field' => 'id'
				),
				array(
					'caption' => Mage::helper('dbizmenu')->__('Delete'),
					'url' => array('base' => '*/*/delete'),
					'field' => 'id',
					'confirm'  => Mage::helper('dbizmenu')->__('Are you sure to delete the selected item ?')
				)
			),
			'filter' => false,
			'sortable' => false,
			'index' => 'stores',
			'is_system' => true,
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit/parent_id/'.$this->getParentId(), array('id' => $row->getId()));
	}
	
	public function getParentId() {
		$parent_id = $this->getRequest()->getParam('id');
		if (!isset($parent_id)) $parent_id = 0;
		return $parent_id;
	}

}
