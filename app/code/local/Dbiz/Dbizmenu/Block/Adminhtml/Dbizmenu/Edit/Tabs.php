<?php

class Dbiz_Dbizmenu_Block_Adminhtml_Dbizmenu_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

	public function __construct() {
		parent::__construct();
		$this->setId('dbizmenu_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('dbizmenu')->__('Menu Information'));
	}

	protected function _beforeToHtml() {
		$this->addTab('general_section', array(
			'label' => Mage::helper('dbizmenu')->__('General Information'),
			'title' => Mage::helper('dbizmenu')->__('General Information'),
			'content' => $this->getLayout()->createBlock('dbizmenu/adminhtml_dbizmenu_edit_tab_form')->toHtml(),
		));

		return parent::_beforeToHtml();
	}

}
