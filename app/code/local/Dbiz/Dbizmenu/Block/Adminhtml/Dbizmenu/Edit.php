<?php

class Dbiz_Dbizmenu_Block_Adminhtml_Dbizmenu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {
		parent::__construct();

		$this->_objectId = 'menu_id';
		$this->_blockGroup = 'dbizmenu';
		$this->_controller = 'adminhtml_dbizmenu';
		$parent_id = $id = $this->getRequest()->getParam('parent_id');
		if (!isset($parent_id)) $parent_id = 0; 
		
		$this->_updateButton('save', 'label', Mage::helper('dbizmenu')->__('Save Menu'));		
		$this->_updateButton('delete', 'label', Mage::helper('dbizmenu')->__('Delete Menu'));
		$this->_updateButton('back', 'onclick', 'setLocation(\''.$this->getUrl('*/*/index/id/'.$parent_id) . '\')');
		
		
		$this->_addButton('saveandcontinue', array(
			'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick' => 'saveAndContinueEdit()',
			'class' => 'save',
				), -100);

		$this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
	}

	public function getHeaderText() {
		$id = $this->getRequest()->getParam('menu_id');
		if (isset($i) AND $id > 0) {
			return Mage::helper('dbizmenu')->__("Edit Menu '%s'", $this->htmlEscape(Mage::registry('dbizmenu_data')->getName()));
		} else {
			return Mage::helper('dbizmenu')->__("Insert Menu");
		}
	}

}
