<?php

class Dbiz_Dbizmenu_Block_Adminhtml_Dbizmenu_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $_model = Mage::registry('dbizmenu_data');
        $parent_id = $id = $this->getRequest()->getParam('parent_id');
		if (!isset($parent_id)) $parent_id = 0;
		
        $form = new Varien_Data_Form(array(
			'enctype' => 'multipart/form-data'
		));
        $this->setForm($form);
		
        $fieldset = $form->addFieldset('dbizmenu_form', array('legend'=>Mage::helper('dbizmenu')->__('General Information')));
        
		$fieldset->addField('parent_id', 'hidden', array(
            'class'     => 'required-entry',
            'name'      => 'parent_id',
            'value'     => $parent_id
        ));
		
        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('dbizmenu')->__('Title'),
			'title'  	=> Mage::helper('dbizmenu')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
            'value'     => $_model->getName()
        ));
		$fieldset->addField('url', 'text', array(
            'label'     => Mage::helper('dbizmenu')->__('URL'),
			'title'  	=> Mage::helper('dbizmenu')->__('URL'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'url',
            'value'     => $_model->getUrl()
        ));
		$fieldset->addField('image', 'image', array(
            'label'     => Mage::helper('dbizmenu')->__('Icon'),
			'title'  	=> Mage::helper('dbizmenu')->__('Icon'),
            'name'      => 'image',
            'value'     => $_model->getImage()
        ));
		$fieldset->addField('banner', 'image', array(
            'label'     => Mage::helper('dbizmenu')->__('Banner'),
			'title'  	=> Mage::helper('dbizmenu')->__('Banner'),
            'name'      => 'banner',
            'value'     => $_model->getBanner()
        ));
		 $fieldset->addField('banner_url', 'text', array(
            'label'     => Mage::helper('dbizmenu')->__('Banner URL'),
			'title'  	=> Mage::helper('dbizmenu')->__('Banner URL'),
            'name'      => 'banner_url',
            'value'     => $_model->getBannerUrl()
        ));
		$fieldset->addField('published', 'select', array(
            'label'     => Mage::helper('dbizmenu')->__('Publsihed'),
			'title'  	=> Mage::helper('dbizmenu')->__('Publsihed'),
            'name'      => 'published',
            'value'     => $_model->getPublished(),
			'options'	=> array(
				1 => 'Enable',
				0	=> 'Disable'
			)
        ));
        $fieldset->addField('order', 'text', array(
            'label'     => Mage::helper('dbizmenu')->__('Order'),
			'title'  	=> Mage::helper('dbizmenu')->__('Order'),
            'name'      => 'order',
            'value'     => $_model->getOrder()
        ));
		$fieldset->addField('style', 'text', array(
            'label'     => Mage::helper('dbizmenu')->__('Style sheet'),
			'title'  	=> Mage::helper('dbizmenu')->__('Style sheet'),
            'name'      => 'style',
            'value'     => $_model->getStyle()
        ));
        return parent::_prepareForm();
	
    }
}
