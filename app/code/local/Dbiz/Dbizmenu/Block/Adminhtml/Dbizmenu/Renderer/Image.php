<?php
class Dbiz_Dbizmenu_Block_Adminhtml_Dbizmenu_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
	
	public function render(Varien_Object $row)
	{
		$img_src =  $row->getData($this->getColumn()->getIndex());
		$result = '';
		if (!empty($img_src)) {
			$result = '<img src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'menu_icon/' . $img_src . '"/>';
		}
		return $result;
	}
	
}