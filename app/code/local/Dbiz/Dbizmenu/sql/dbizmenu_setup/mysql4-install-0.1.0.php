<?php

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE `dbiz_menus` (
	`menu_id` INT(10) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`image` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`banner` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`banner_url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`published` TINYINT(1) NULL DEFAULT NULL,
	`parent_id` INT(1) NULL DEFAULT NULL,
	`order` INT(10) NULL DEFAULT NULL,
	`params` TEXT NULL COLLATE 'utf8_unicode_ci',
	`style` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`menu_id`)
)
COLLATE='utf8_unicode_ci';
");
$installer->endSetup();