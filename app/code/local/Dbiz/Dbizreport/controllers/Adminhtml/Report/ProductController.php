<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require('Mage/Adminhtml/controllers/Report/ProductController.php');
class Dbiz_Dbizreport_Adminhtml_Report_ProductController extends Mage_Adminhtml_Report_ProductController{
    
    public function viewedAction()
    {
        $this->_title($this->__('Reports'))->_title($this->__('Products'))->_title($this->__('Most Viewed'));

        $this->_showLastExecutionTime(Mage_Reports_Model_Flag::REPORT_PRODUCT_VIEWED_FLAG_CODE, 'viewed');

        $this->_initAction()
            ->_setActiveMenu('report/products/viewed')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Products Most Viewed Report'), Mage::helper('adminhtml')->__('Products Most Viewed Report'));

        $gridBlock = $this->getLayout()->getBlock('report_product_viewed.grid');
        $update = Mage::app()->getLayout()->getUpdate();
    $removeInstruction = "<remove name=\"grid.filter.form\"/>";
    $update->addUpdate($removeInstruction);
        $filterFormBlock = $this->getLayout()->createBlock('dbizreport/report_filter_formViewed');
        $this->_initReportAction(array(
            $gridBlock,
            $filterFormBlock
        ));

        $this->renderLayout();
    }
}