<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dbiz_Dbizreport_Model_Resource_Report_Product_Viewed_Collection extends Mage_Reports_Model_Resource_Report_Product_Viewed_Collection{
    
    public function __construct() {
        $filter = Mage::app()->getRequest()->getParam('filter');
        $filter_data = Mage::helper('adminhtml')->prepareFilterString($filter);
        if(isset($filter_data['num_of_product']) && (int)$filter_data['num_of_product'] > 0){
            $this->_ratingLimit = (int)$filter_data['num_of_product'];
        }
        parent::__construct();
    }
    
}