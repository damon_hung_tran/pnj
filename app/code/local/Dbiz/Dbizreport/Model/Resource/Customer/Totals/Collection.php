<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dbiz_Dbizreport_Model_Resource_Customer_Totals_Collection extends Mage_Reports_Model_Resource_Customer_Orders_Collection
{
    protected function _joinFields($from = '', $to = '')
    {
        $this->joinCustomerName()
            ->groupByCustomer()
            ->addOrdersCount()
            ->addAttributeToFilter('created_at', array('from' => $from, 'to' => $to, 'datetime' => true))
            ->appendTelephone();
        return $this;
    }
    public function appendTelephone(){
        $select = $this->getSelect();
        $select->joinLeft(
                    array('order_address' => 'sales_flat_order_address'),
                    'main_table.entity_id = order_address.parent_id and order_address.address_type = "billing"',
                    array('telephone'));
        return $this;
    }
    
}