<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Dbiz_Dbizreport_Block_Report_Customer_Orders_Grid extends Mage_Adminhtml_Block_Report_Customer_Orders_Grid{
    
    public function __construct() {
        parent::__construct();
        $this->_subReportSize = 0;
    }


    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        $this->getCollection()->setPageSize(false)->initReport('reports/customer_orders_collection');
    }
    protected function _prepareColumns(){
        $this->addColumn('name', array(
            'header'    => $this->__('Customer Name'),
            'sortable'  => false,
            'index'     => 'name'
        ));
        $this->addColumn('telephone', array(
            'header'    => $this->__('Telephone'),
            'sortable'  => false,
            'index'     => 'telephone',
            'align' => 'right'
        ));
        $this->addColumn('orders_count', array(
            'header'    => $this->__('Number of Orders'),
            'width'     => '100px',
            'sortable'  => false,
            'index'     => 'orders_count',
            'total'     => 'sum',
            'type'      => 'number'
        ));

        $baseCurrencyCode = $this->getCurrentCurrencyCode();

        $this->addColumn('orders_avg_amount', array(
            'header'    => $this->__('Average Order Amount'),
            'width'     => '200px',
            'align'     => 'right',
            'sortable'  => false,
            'type'      => 'number',
            'currency_code'  => $baseCurrencyCode,
            'index'     => 'orders_avg_amount',
            'total'     => 'orders_sum_amount/orders_count',
            //'renderer'  =>'adminhtml/report_grid_column_renderer_currency'
        ));

        $this->addColumn('orders_sum_amount', array(
            'header'    => $this->__('Total Order Amount'),
            'width'     => '200px',
            'align'     => 'right',
            'sortable'  => false,
            'type'      => 'number',
            'currency_code'  => $baseCurrencyCode,
            'index'     => 'orders_sum_amount',
            'total'     => 'sum',
            //'renderer'  => 'adminhtml/report_grid_column_renderer_currency',
        ));

        $this->addExportType('*/*/exportOrdersCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportOrdersExcel', Mage::helper('reports')->__('Excel XML'));
        
        return $this;
        
    }
}
