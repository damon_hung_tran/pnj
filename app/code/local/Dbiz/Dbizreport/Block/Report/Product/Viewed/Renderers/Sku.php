<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dbiz_Dbizreport_Block_Report_Product_Viewed_Renderers_Sku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
    
    public function render(Varien_Object $row){
        $value =  $row->getData($this->getColumn()->getIndex());
        $product = Mage::getSingleton('catalog/product')->load($value);
        return $product->getSku();
    }
}