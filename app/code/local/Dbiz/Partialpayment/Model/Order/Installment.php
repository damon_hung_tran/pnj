<?php 
class Dbiz_Partialpayment_Model_Order_Installment extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('partialpayment/order_installment');
    }
    /**
     * Clear order object data
     *
     * @param string $key data key
     * @return Mage_Sales_Model_Order
     */
    public function unsetData($key=null)
    {
        parent::unsetData($key);
        if (is_null($key)) { 
            $this->_items = null;
        }
        return $this;
    }
}