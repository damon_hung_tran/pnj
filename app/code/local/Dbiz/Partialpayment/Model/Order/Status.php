<?php
class Dbiz_Partialpayment_Model_Order_Status
{
    /**
     * Options getter
     *
     * @return array
     */
    static public function toOptionArray(){
        return array(
			array('value' => 'processing', 'label'=>Mage::helper('partialpayment')->__('Processing')),
            array('value' => 'complete', 'label'=>Mage::helper('partialpayment')->__('Complete')),
            array('value' => 'pending', 'label'=>Mage::helper('partialpayment')->__('Pending')),
            array('value' => 'canceled', 'label'=>Mage::helper('partialpayment')->__('Canceled')),
        );
    }
    
    static public function getOptionHash()
    {
        foreach (self::toOptionArray() as $value => $label) {
            $options[$label['value']] = $label['label'];
        }
        return $options;
    }
}