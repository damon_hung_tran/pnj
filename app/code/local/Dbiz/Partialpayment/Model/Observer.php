<?php
class Dbiz_Partialpayment_Model_Observer {
    
    protected function savePartialPaymentOrder($order){
        $partialOrder = Mage::getModel('partialpayment/order');
        $orderId = $order->getId();
        if(!empty($orderId)){
            $totalInstallment  = 0;
            switch(Mage::helper('partialpayment')->getPartialPaymentOptions()){
                case '2_installments':
                    $totalInstallment = 2;
                    break;
                case 'fixed_installments':
                case 'flexy_payments':
                    $totalInstallment = Mage::helper('partialpayment')->getTotalInstallment();
                    break;
            }
            $paymentPlanDate = Mage::helper('partialpayment')->getCalculateDatePlan();
            
            $transaction = Mage::getSingleton('core/resource')->getConnection('core_write');
            try {
                $transaction->beginTransaction();
            
                $paidInstallment = 1;
                $partialOrder->setRealOrderId($orderId);
                $partialOrder->setIncrementId($order->getRealOrderId());
                $partialOrder->setCustomerId($order->getCustomerId());
                $partialOrder->setCustomerFirstname($order->getCustomerFirstname());
                $partialOrder->setCustomerLastname($order->getCustomerLastname());
                $partialOrder->setCustomerEmail($order->getCustomerEmail());
                $partialOrder->setGrandTotal($order->getGrandTotal());
                $partialOrder->setPartialPaidAmount($order->getPartialPaidAmount());
                $partialOrder->setPartialLaterAmount($order->getPartialLaterAmount());
                $partialOrder->setPaidInstallment($paidInstallment);
                $partialOrder->setTotalInstallment($totalInstallment);
                $partialOrder->setStatus($order->getStatus());
                $partialOrder->setStoreId($order->getStoreId());
                $partialOrder->setCreatedAt(Mage::getSingleton('core/date')->gmtTimestamp());
                $partialOrder->setUpdatedAt(Mage::getSingleton('core/date')->gmtTimestamp());
                $partialOrder->save();
                //
                $paymentMethod = $order->getPayment()->getMethodInstance()->getCode();
                //force status to paid in installment pay
                $partialInstallmentStatus = 'paid';
                if($order->getStatus() == 'processing' || $order->getStatus() == 'complete'){
                    $partialInstallmentStatus = 'paid';
                }
                $partialInstallment = Mage::getModel('partialpayment/order_installment');
                $partialInstallment->setPartialorderId($partialOrder->getId());
                $partialInstallment->setStoreId($order->getStoreId());
                $partialInstallment->setInstallmentAmount($partialOrder->getPartialPaidAmount());
                $partialInstallment->setPaymentMethod($paymentMethod);
                $partialInstallment->setStatus($partialInstallmentStatus);
                $partialInstallment->setPaidAt(Mage::getSingleton('core/date')->gmtTimestamp());
                $partialInstallment->setDueDate(Mage::getSingleton('core/date')->gmtTimestamp());
                $partialInstallment->setCreatedAt(Mage::getSingleton('core/date')->gmtTimestamp());
                $partialInstallment->setUpdatedAt(Mage::getSingleton('core/date')->gmtTimestamp());
                $partialInstallment->save();
                
                if($totalInstallment > 2){
                    $partialInstallmentAmount = $order->getPartialLaterAmount()/($totalInstallment - 1);
                }else{
                    $partialInstallmentAmount = $order->getPartialLaterAmount();
                }
                $partialInstallmentStatus = 'remaining';
                $paymentMethod = '';
                for($i = 1; $i < $totalInstallment; $i++){
                    $paymentPlanDate = $paymentPlanDate*$i;
                    $date = new Zend_Date();
                    $partialInstallment = Mage::getModel('partialpayment/order_installment');
                    $partialInstallment->setPartialorderId($partialOrder->getId());
                    $partialInstallment->setStoreId($order->getStoreId());
                    $partialInstallment->setInstallmentAmount($partialInstallmentAmount);
                    $partialInstallment->setPaymentMethod($paymentMethod);
                    $partialInstallment->setStatus($partialInstallmentStatus);
                    $partialInstallment->setDueDate($date->add($paymentPlanDate, Zend_Date::DAY)->toString());
                    $partialInstallment->setCreatedAt(Mage::getSingleton('core/date')->gmtTimestamp());
                    $partialInstallment->setUpdatedAt(Mage::getSingleton('core/date')->gmtTimestamp());
                    $partialInstallment->save();
                }
                //
                $transaction->commit();
            } catch (Exception $e) {
                var_dump($e);exit;
                $transaction->rollback();
            }
            
        }
    }
	public function savePartialInstallmentTotal(Varien_Event_Observer $observer) {
		$order = $observer->getEvent ()->getOrder ();
		$quote = $observer->getEvent ()->getQuote ();
		if(Mage::getStoreConfig('partialpayment/general/active')){
		    $shippingAddress = $quote->getShippingAddress();
		    $partialPaidAmount = $shippingAddress->getData('partial_paid_amount');
		    $partialLaterAmount = $shippingAddress->getData('partial_later_amount');
		    if($partialPaidAmount){
	            if ($shippingAddress && $shippingAddress->getData ( 'partial_paid_amount' )) {
	                $order->setData ( 'partial_paid_amount', $shippingAddress->getData ( 'partial_paid_amount' ) );
	                $order->setData ( 'partial_later_amount', $shippingAddress->getData ( 'partial_later_amount' ) );
	            } else {
	                $billingAddress = $quote->getBillingAddress ();
	                $order->setData ( 'partial_paid_amount', $billingAddress->getData ( 'partial_paid_amount' ) );
	                $order->setData ( 'partial_later_amount', $shippingAddress->getData ( 'partial_later_amount' ) );
	            }
	            $order->save ();
	            $this->savePartialPaymentOrder($order);
		    }
		}
		
	}
	
	//check product has partial payment before add to cart
	public function checkoutCartProductAddBefore(Varien_Event_Observer $observer){
	    $product = $observer->getEvent ()->getProduct ();
	    $controllerAction = $observer->getEvent()->getControllerAction();
	    $allow_partial_payment = $controllerAction->getRequest()->getParam('allow_partial_payment');
	    $productId = $controllerAction->getRequest()->getParam('product');
	    $applyPartialPaymentTo = Mage::getStoreConfig('partialpayment/functionalities_group/apply_partial_payment_to');
	    if(Mage::getStoreConfig('partialpayment/general/active')){
	        if(!$allow_partial_payment){
	            //apply partial payment to all product
	            if($applyPartialPaymentTo == 1){
	                $isPartialPayment = true;
	            //apply partial payment to selected product only
	            }elseif($applyPartialPaymentTo == 2){
	                if(empty($product)){
	                    $product = Mage::getModel('catalog/product')
	                    ->setStoreId(Mage::app()->getStore()->getId())
	                    ->load($productId);
	                
	                    if($product->getPartialPayment()){
	                        $isPartialPayment = true;
	                    }
	                }
                //apply partial payment to pre order product only( out of stock)
	            }elseif($applyPartialPaymentTo == 3){
	                if(empty($product)){
	                    $product = Mage::getModel('catalog/product')
	                    ->setStoreId(Mage::app()->getStore()->getId())
	                    ->load($productId);
	                
	                    if(!$product->getIsInStock()){
	                        $isPartialPayment = true;
	                    }
	                }
	            }
	            //appy partial payement to whole of cart
	            elseif($applyPartialPaymentTo == 4){
	               //@TODO later
	            }
	            if($isPartialPayment){
	                if(empty($product)){
	                    $product = Mage::getModel('catalog/product')
	                    ->setStoreId(Mage::app()->getStore()->getId())
	                    ->load($productId);
	                }
	                $controllerAction->getRequest()->setParam('return_url', $product->getProductUrl());
	                $controllerAction->getRequest()->setParam('product', 0);
	            }
	        }
	    }
	}
}