<?php
class Dbiz_Partialpayment_Model_Total_Quote_Partial_Later
	extends Mage_Sales_Model_Quote_Address_Total_Abstract {

	var $_percent = null;
	
    public function __construct(){ 
        $this->setCode('partial_later');
    }
    
	public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);
        if(!Mage::getStoreConfig('partialpayment/general/active')){
            return $this;
        }
        $partialPaidAmount = $address->getPartialPaidAmount();
        $grandTotal = $address->getGrandTotal();
        $address->setPartialLaterAmount(0);
        $address->setBasePartialLaterAmount(0);
        $partialLaterAmount = 0;
        $store = $address->getQuote()->getStore();
        if($partialPaidAmount){
            $partialLaterAmount = $grandTotal - $partialPaidAmount;
        }
        
        $partialLaterAmount = $store->roundPrice ( $partialLaterAmount );
        $this->_addAmount($partialLaterAmount);
        $this->_addBaseAmount($partialLaterAmount);
        Mage::log('GrandTotal Dbiz_Partialpayment_Model_Total_Quote_Partial_Later'. $address->getGrandTotal());
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount     = $address->getPartialLaterAmount();
        if (($amount != 0) ) {
            $address->addTotal(array(
                'code'      => $this->getCode(),
                'title'     => Mage::helper('partialpayment')->__(' Amount to be Paid Later :'),
                'value'     => $amount,
                'area'      => 'footer',
//                 'style'      => 'text-align:left'
            ));
        }
        return $this;
    }
}