<?php
class Dbiz_Partialpayment_Model_Total_Quote_Partial_Paid
	extends Mage_Sales_Model_Quote_Address_Total_Abstract {

	var $_percent = null;
	
    public function __construct(){ 
        $this->setCode('partial_paid');
    }
    
	public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);
        if(!Mage::getStoreConfig('partialpayment/general/active')){
            return $this;
        }
        $partialCalculationType = Mage::getStoreConfig('partialpayment/calculation_settings/installment_calculation_type');
        $partialInstallmentAmount = Mage::getStoreConfig('partialpayment/calculation_settings/first_installment_amount');
        
        $address->setPartialPaidAmount(0);
        $address->setBasePartialPaidAmount(0);
        $partialAmount = 0;
        
        $store = $address->getQuote()->getStore();

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }
        foreach ($items as $item) {
            if ($item->getParentItem()) {
                continue;
            }
        
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $taxRateRequest->setProductClassId($child->getProduct()->getTaxClassId());
                    $rate = $this->_calculator->getRate($taxRateRequest);
                    $this->_calcUnitTaxAmount($child, $rate);
                    $this->_addAmount($child->getTaxAmount());
                    $this->_addBaseAmount($child->getBaseTaxAmount());
                    $applied = $this->_calculator->getAppliedRates($taxRateRequest);
                    if ($rate > 0) {
                        $itemTaxGroups[$child->getId()] = $applied;
                    }
                    $this->_saveAppliedTaxes(
                        $address,
                        $applied,
                        $child->getTaxAmount(),
                        $child->getBaseTaxAmount(),
                        $rate
                    );
                    $child->setTaxRates($applied);
                }
                $this->_recalculateParent($item);
            }
            else { 
                if($item->getProduct()->getPartialPayment()){
                    $qty = $item->getQty();
                    $price = $item->getPrice();
                    $rowTotal = $qty*$price;
                    if($partialCalculationType == 2){
                        $partialAmount += $rowTotal*$partialInstallmentAmount/100;
                    }else{
                        $partialAmount += $partialInstallmentAmount*$qty;
                    }
                }
                
            }
        }
        $partialAmount = $store->roundPrice ( $partialAmount );
        $this->_addAmount($partialAmount);
        $this->_addBaseAmount($partialAmount);
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount     = $address->getPartialPaidAmount();
        if (($amount != 0) ) {
            $address->addTotal(array(
                'code'      => $this->getCode(),
                'title'     => Mage::helper('partialpayment')->__('Paying Now:'),
                'value'     => $amount,
                'area'      => 'footer',
            ));
        }
        return $this;
    }
}