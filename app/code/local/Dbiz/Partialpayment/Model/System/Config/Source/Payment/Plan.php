<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Used in creating options for Yes|No config value selection
 *
 */
class Dbiz_Partialpayment_Model_System_Config_Source_Payment_Plan
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'monthly', 'label'=>Mage::helper('partialpayment')->__('Monthly')),
            array('value' => 'weekly', 'label'=>Mage::helper('partialpayment')->__('Weekly')),
        	array('value' => 'days', 'label'=>Mage::helper('partialpayment')->__('Days')),
        );
    } 

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'monthly' => Mage::helper('adminhtml')->__('Monthly'),
            'weekly' => Mage::helper('adminhtml')->__('Weekly'),
        	'days' => Mage::helper('adminhtml')->__('Days'),
        );
    }

}
