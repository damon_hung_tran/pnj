<?php 
class Dbiz_Partialpayment_Model_Order extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('partialpayment/order');
    }
    /**
     * Clear order object data
     *
     * @param string $key data key
     * @return Mage_Sales_Model_Order
     */
    public function unsetData($key=null)
    {
        parent::unsetData($key);
        if (is_null($key)) {
            $this->_items = null;
        }
        return $this;
    }
    public function getOrderInstallments(){
        $collection = Mage::getModel('partialpayment/order_installment')
                      ->getCollection()
                      ->addFieldToFilter('partialorder_id',$this->getId());
        return $collection;
    }
}