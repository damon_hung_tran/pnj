<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute(
    'order',
    'partial_paid_amount',
    array(
            'type' => 'decimal', /* varchar, text, decimal, datetime */
            'grid' => false /* or true if you wan't use this attribute on orders grid page */
    )
);
$installer->addAttribute(
    'order',
    'partial_later_amount',
    array(
        'type' => 'decimal', /* varchar, text, decimal, datetime */
        'grid' => false /* or true if you wan't use this attribute on orders grid page */
    )
);

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('partialpayment_sales_order')};
    
CREATE TABLE {$this->getTable('partialpayment_sales_order')} (
  `partialorder_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `real_order_id` int(11) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `customer_firstname` varchar(255) DEFAULT NULL,
  `customer_lastname` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `grand_total` decimal(15,4)  DEFAULT NULL,
  `partial_paid_amount` decimal(15,4) DEFAULT NULL,
  `partial_later_amount` decimal(15,4) DEFAULT NULL,
  `total_installment` smallint(5) NOT NULL default '0',
  `paid_installment` smallint(5) NOT NULL default '0',
  `order_currency_code` VARCHAR( 50 ) DEFAULT NULL,
  `status` varchar(32) NOT NULL default '',
  `store_id` smallint(5) NOT NULL default '0',
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL,
  PRIMARY KEY (`partialorder_id`),
  UNIQUE KEY `UNQ_PARTIALPAYMENT_ORDER_INCREMENT_ID` (`increment_id`),
  UNIQUE KEY `IDX_PARTIALPAYMENT_ORDER_REAL_ID` (`real_order_id`),
  KEY `IDX_PARTIALPAYMENT_ORDER_STATUS` (`status`),
  KEY `IDX_PARTIALPAYMENT_ORDER_STORE_ID` (`store_id`),
  KEY `IDX_PARTIALPAYMENT_ORDER_CREATED_AT` (`created_at`),
  KEY `IDX_PARTIALPAYMENT_ORDER_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_PARTIALPAYMENT_ORDER_UPDATED_AT` (`updated_at`),
  CONSTRAINT `IDX_PARTIALPAYMENT_ORDER_REAL_ID` FOREIGN KEY (`real_order_id`) REFERENCES `sales_flat_order` (`entity_id`)
  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('partialpayment_sales_order_installment')};

CREATE TABLE {$this->getTable('partialpayment_sales_order_installment')} (
  `item_id` INT( 11 ) unsigned NOT NULL auto_increment,
  `partialorder_id` int(11) unsigned NOT NULL,
  `store_id` smallint(5) NOT NULL default '0',
  `installment_amount` decimal(15,4)  NOT NULL default '0',
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL,
  `paid_at` timestamp NULL,
  `due_date` timestamp NULL,
  `status` varchar(32) NOT NULL default '',
  `payment_method` varchar(32) NOT NULL default '', 
  PRIMARY KEY (`item_id`),
  KEY `IDX_PARTIALPAYMENT_ITEM_ORDER_ID` (`partialorder_id`),
  KEY `IDX_PARTIALPAYMENT_ITEM_STATUS` (`status`),
  CONSTRAINT `IDX_PARTIALPAYMENT_ITEM_ORDER_ID` FOREIGN KEY (`partialorder_id`) REFERENCES `{$this->getTable('partialpayment_sales_order')}` (`partialorder_id`)
  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");
$installer->endSetup();

	  