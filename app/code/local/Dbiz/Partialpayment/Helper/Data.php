<?php
class Dbiz_Partialpayment_Helper_Data extends Mage_Core_Helper_Abstract
{
    CONST XML_PATH_ACTIVE = 'partialpayment/general/active';
    CONST XML_PATH_PARTIAL_PAYMENT_OPTIONS = 'partialpayment/functionalities_group/partial_payment_options';
    CONST XML_PATH_PARTIAL_PAYMENT_OPTIONAL = 'partialpayment/functionalities_group/partial_payment_optional';
    CONST XML_PATH_APPLY_PARTIAL_PAYMENT_TO = 'partialpayment/functionalities_group/apply_partial_payment_to';
    CONST XML_PATH_PARTIAL_TOTAL_INSTALLMENT = 'partialpayment/functionalities_group/total_installments';
    CONST XML_PATH_PARTIAL_PAYMENT_PLAN = 'partialpayment/calculation_settings/payment_plan';
    CONST XML_PATH_PARTIAL_PAYMENT_TOTAL_NO_DAYS = 'partialpayment/calculation_settings/total_no_days';
    
    public function checkIfModuleIsEnabled() {
        return Mage::getStoreConfig(self::XML_PATH_ACTIVE);
    }
    
    public function getPartialPaymentOptions() {
        return Mage::getStoreConfig(self::XML_PATH_PARTIAL_PAYMENT_OPTIONS);
    }
    
    public function getPartialPaymentOptional() {
        return Mage::getStoreConfig(self::XML_PATH_PARTIAL_PAYMENT_OPTIONAL);
    }
    
    public function getApplyPartialPaymentTo() {
        return Mage::getStoreConfig(self::XML_PATH_APPLY_PARTIAL_PAYMENT_TO);
    }
    
    public function getTotalInstallment() {
        return Mage::getStoreConfig(self::XML_PATH_PARTIAL_TOTAL_INSTALLMENT);
    }
    
    public function getPaymentPlan() {
        return Mage::getStoreConfig(self::XML_PATH_PARTIAL_PAYMENT_PLAN);
    }
    
    public function getCalculateDatePlan(){
        
        $paymentPlan = $this->getPaymentPlan();
        $dates = 0;
        switch($paymentPlan){
            case 'monthly':
                $dates = 30;
                break;
            case 'weekly':
                $dates = 7;
                break;
            case 'days':
                $dates =  Mage::getStoreConfig(self::XML_PATH_PARTIAL_PAYMENT_TOTAL_NO_DAYS);
                break;
        }
        return $dates;
    }
    
}
	 