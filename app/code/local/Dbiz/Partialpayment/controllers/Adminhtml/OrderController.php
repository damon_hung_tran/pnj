<?php
class Dbiz_Partialpayment_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        if (!Mage::helper('partialpayment')->checkIfModuleIsEnabled()) {return;}
        $this->_initAction();
		$this->loadLayout();
		$this->_addContent($this->getLayout()->createBlock('partialpayment/adminhtml_order'));
		$this->renderLayout();
	}	
	/**
	 * Init layout, menu and breadcrumb
	 *
	 * @return Mage_Adminhtml_Sales_OrderController
	 */
	protected function _initAction()
	{
	    $this->loadLayout()
	    ->_setActiveMenu('dbiz_solutions/partialpayment')
	    ->_addBreadcrumb($this->__('Partial Payment'), $this->__('Orders'))
	    ->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));
	    
	    return $this;
	}
	public function dashboardAction()
	{
        if (!Mage::helper('partialpayment')->checkIfModuleIsEnabled()) {return;}
		
		$this->loadLayout();
		$this->_addContent($this->getLayout()->createBlock('partialpayment/adminhtml_order_dashboard'));
		$this->renderLayout();
	}
	/**
	 * Initialize order model instance
	 *
	 * @return Mage_Sales_Model_Order || false
	 */
	protected function _initOrder()
	{
	    $id = $this->getRequest()->getParam('partialorder_id');
	    $order = Mage::getModel('partialpayment/order')->load($id);
	   
	    if (!$order->getId()) {
	        $this->_getSession()->addError($this->__('This order no longer exists.'));
	        $this->_redirect('*/*/');
	        $this->setFlag('', self::FLAG_NO_DISPATCH, true);
	        return false;
	    }
	    Mage::register('partial_order', $order);
	    Mage::register('current_partialorder', $order);
	    return $order;
	}
	public function editAction()
	{   
	    $this->_title($this->__('Partial payment'))->_title($this->__('Orders'));
	    
	    if ($order = $this->_initOrder()) {
	        $this->_initAction();
	        $this->_title(sprintf("#%s", $order->getIncrementId()));
	        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
	        $this->getLayout()->getBlock('head')->addJs('jquery/jquery-1.7.2.js');
	        $this->getLayout()->getBlock('head')->addJs('onestepcheckout/admin/payment.js');
	        $this->_addContent($this->getLayout()->createBlock("partialpayment/adminhtml_order_edit"));
	        $this->renderLayout();
	    }
	}
	public function saveAction()
	{
	    $params = $this->getRequest()->getPost();
	    
	    if ($order = $this->_initOrder()) {
	        $order->setStatus($params['status']);
	        
	        if($params['item_id']){
	            $collectionInstallment = $order->getOrderInstallments();
	            foreach($collectionInstallment as $installment){
	                
	                if($installment->getId() == $params['item_id']){
	                    
	                    $transaction = Mage::getSingleton('core/resource')->getConnection('core_write');
	                    $postInstallment = $params['installment'][$params['item_id']];
	                    //check paid install ment
	                    try {
                            $transaction->beginTransaction();
    	                    if($postInstallment['installment_status'] != 'paid' ){
    	                        
    	                        $paidAmount = $order->getPartialPaidAmount() - $installment->getInstallmentAmount();
    	                        $paidInstallment = $order->getPaidInstallment() - 1;
    	                        //set installment order to paid if last status of it is not paid
    	                        if($installment->getStatus() == 'paid'){
    	                            $order->setPartialPaidAmount($paidAmount);
    	                            $order->setPaidInstallment($paidInstallment);
    	                            $order->save();
    	                        }
    	                        $installment->setPaidAt(null);
    	                        $installment->setPaymentMethod('');
    	                    }else{
    	                        
    	                        $orderMain = Mage::getModel('sales/order')->load($order->getRealOrderId());
    	                        $paidAmount = $order->getPartialPaidAmount() + $installment->getInstallmentAmount();
    	                        $paidInstallment = $order->getPaidInstallment() + 1;
    	                        if($installment->getStatus() != 'paid'){
    	                            $order->setPartialPaidAmount($paidAmount);
    	                            $order->setPaidInstallment($paidInstallment);
    	                            $order->save();
    	                        }
    	                        
    	                        $installment->setPaidAt(Mage::getSingleton('core/date')->gmtTimestamp());
    	                        $installment->setPaymentMethod($postInstallment['payment']['method']);
    	                        
    	                        if($order->getPaidInstallment() == $order->getTotalInstallment()){
    	                            $orderMain->setPartialPaidAmount($orderMain->getGrandTotal());
    	                            $orderMain->setPartialPaidLater(0);
    	                            $orderMain->save();
    	                        }
    	                    }
    	                    
    	                    $installment->setDueDate($postInstallment['installment_due_date']);
    	                    $installment->setStatus($postInstallment['installment_status']);
    	                    
    	                    $installment->save();
    	                    $transaction->commit();
	                    }catch (Exception $e) {
                            $transaction->rollback();
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                            Mage::getSingleton('adminhtml/session')->setFormData($data);
                            $this->_redirect('*/*/edit', array(
                            	'partialorder_id' 	=> $this->getRequest()->getParam('partialorder_id'),
                            ));
                            return;
                        }
	                }
	                break;
	            }
	        }
	    }
	    $this->_redirect('*/*/');
	    return;
	} 
    public function ajaxBlockAction(){
        if (!Mage::helper('partialpayment')->checkIfModuleIsEnabled()) {return;}
    	$output = '';
    	$output = $this->getLayout()->createBlock("partialpayment/adminhtml_order_installment")->toHtml();
    	$this->getResponse()->setBody($output);
    }
}
