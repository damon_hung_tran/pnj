<?php
class Dbiz_Partialpayment_Block_PartialPayment extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    public function getTitle()
    {
    	return $this->__("Partial Payment");
    }
}