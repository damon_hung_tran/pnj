<?php
class Dbiz_Partialpayment_Block_Catalog_Product_Info extends Mage_Catalog_Block_Product_Abstract
{
    CONST XML_PATH_ACTIVE = 'partialpayment/general/active';
    CONST XML_PATH_PARTIAL_PAYMENT_OPTIONS = 'partialpayment/functionalities_group/partial_payment_options';
    CONST XML_PATH_PARTIAL_PAYMENT_OPTIONAL = 'partialpayment/functionalities_group/partial_payment_optional';
    CONST XML_PATH_APPLY_PARTIAL_PAYMENT_TO = 'partialpayment/functionalities_group/apply_partial_payment_to';
    
    public function _construct()
	{
        parent::_construct();
		$this->setTemplate('partialpayment/catalog/product/view/info.phtml');
	}
	public function _isPartialPayment(){
	    if($this->checkIfModuleIsEnabled()) {
    	    if($this->getApplyPartialPaymentTo() == 1) {
    	        return true;
    	    }elseif($this->getApplyPartialPaymentTo() == 2){
    	        if($this->getProduct()->getPartialPayment()){
    	            return true;
    	        }
    	    }
	    }
	    return false;
	}
	protected function _toHtml() {
	    if(!$this->_isPartialPayment()) {
	        return "";
	    }
	    return parent::_toHtml();
	}
	
	public function checkIfModuleIsEnabled() {
        return Mage::getStoreConfig(self::XML_PATH_ACTIVE);
	}
	
	public function getPartialPaymentOptions() {
	    return Mage::getStoreConfig(self::XML_PATH_PARTIAL_PAYMENT_OPTIONS);
	}
	
	public function getPartialPaymentOptional() {
	    return Mage::getStoreConfig(self::XML_PATH_PARTIAL_PAYMENT_OPTIONAL);
	}
	
	public function getApplyPartialPaymentTo() {
	    return Mage::getStoreConfig(self::XML_PATH_APPLY_PARTIAL_PAYMENT_TO);
	}
	
}