<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base block for rendering category and product forms
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dbiz_Partialpayment_Block_Adminhtml_Order_Info extends Dbiz_Partialpayment_Block_Adminhtml_Order_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('partialpayment/order/view/tab/info.phtml');
    }
    protected function _prepareLayout()
    {   
        $this->_objectId    = 'partialorder_id';
        $this->_controller  = 'adminhtml_order';
        $this->_blockGroup = 'partialpayment';
        $this->_mode = 'edit';
        if ($this->_blockGroup && $this->_controller && $this->_mode) { 
            $this->setChild('form', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_' . $this->_mode ));
        }
        return parent::_prepareLayout();
    }
    /**
     * Retrieve source model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getSource()
    {
        return $this->getOrder();
    }

}
