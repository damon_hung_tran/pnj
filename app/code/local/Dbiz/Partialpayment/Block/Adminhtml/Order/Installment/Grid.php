<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Promotionalgift Grid Block
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @author      Magestore Developer
 */
class Dbiz_Partialpayment_Block_Adminhtml_Order_Installment_Grid extends Mage_Adminhtml_Block_Template
{
    public function _prepareLayout() {
        $controller = Mage::app()->getRequest()->getControllerName();
        $module = Mage::app()->getRequest()->getModuleName();
        if ($module == 'onestepcheckout') {
            $this->setTemplate('promotionalgift/multiplecheckoutbanner.phtml');
        } else {
            if ($controller == 'onepage') {
                $this->setTemplate('promotionalgift/multiplecheckoutbanner.phtml');
            } else {
                $this->setTemplate('promotionalgift/banner.phtml');
            }
        }
        return parent::_prepareLayout();
    }
    /**
     * prepare collection for block to display
     *
     * @return Magestore_Promotionalgift_Block_Adminhtml_Promotionalgift_Grid
     */
    protected function prepareCollection()
    {
        $collection = Mage::getModel('partialpayment/order_installment')->getCollection();
        foreach($collection as $item){
            var_dump($item->getData());exit;
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
}