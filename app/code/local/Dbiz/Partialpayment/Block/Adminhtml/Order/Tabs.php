<?php
class Dbiz_Partialpayment_Block_Adminhtml_Order_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('partial_order_view_tabs');
        $this->setDestElementId('partial_order_view');
        $this->setTitle(Mage::helper("partialpayment")->__("Order"));
    }
    
	protected function _beforeToHtml()
	{
		$this->addTab("partial_order_tab_info", array(
			"label" => Mage::helper("partialpayment")->__("Information"),
			"title" => Mage::helper("partialpayment")->__("Information"),
			"content" => $this->getLayout()->createBlock("partialpayment/adminhtml_sales_order_view_tab_info")->toHtml(),
		));
		return parent::_beforeToHtml();
	}
	public function getOrder()
	{
	    if ($this->hasOrder()) {
	        return $this->getData('order');
	    }
	    if (Mage::registry('current_partialorder')) {
	        return Mage::registry('current_partialorder');
	    }
	    if (Mage::registry('partial_order')) {
	        return Mage::registry('partial_order');
	    }
	    Mage::throwException(Mage::helper('partialpayment')->__('Cannot get the order instance.'));
	}
	
	
}
