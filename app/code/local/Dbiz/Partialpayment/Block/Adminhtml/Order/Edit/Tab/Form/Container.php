<?php 
class Dbiz_Partialpayment_Block_Adminhtml_Order_Edit_Tab_Form_Container extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('partialpayment/widget/form/container.phtml');
    }
}
