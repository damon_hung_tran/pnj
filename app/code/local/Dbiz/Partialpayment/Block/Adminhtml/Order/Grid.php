<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Promotionalgift Grid Block
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @author      Magestore Developer
 */
class Dbiz_Partialpayment_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('partialpaymentGrid');
        $this->setDefaultSort('partialorder_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * prepare collection for block to display
     *
     * @return Magestore_Promotionalgift_Block_Adminhtml_Promotionalgift_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('partialpayment/order')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     * prepare columns for this grid
     *
     * @return Magestore_Promotionalgift_Block_Adminhtml_Promotionalgift_Grid
     */
    protected function _prepareColumns()
    {
        $currencyCode = Mage::app()->getStore()->getBaseCurrency()->getCode();
        $this->addColumn('partialorder_id', array(
            'header'    => Mage::helper('partialpayment')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'type'  	=> 'text',
            'index'     => 'partialorder_id',
        ));

        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('partialpayment')->__('Order ID'),
            'align'     =>'left',
			'width'		=> '50px',
			'type' 		=> 'text',
            'index'     => 'increment_id',
        ));
		//$store = $this->_getStore();
        $this->addColumn('customer_id', array(
            'header'    => Mage::helper('partialpayment')->__('Customer ID'),
            'width'     => '50px',
            'type' 		=> 'int',
            'index'     => 'customer_id',
        ));
		 $this->addColumn('customer_firstname', array(
            'header'    => Mage::helper('partialpayment')->__('Customer First Name'),
            'type'  	=> 'text',
		    'width'     => '50px',
		    'align'     => 'left',
            'index'     => 'customer_firstname',
        ));
		 $this->addColumn('customer_lastname', array(
            'header'    => Mage::helper('partialpayment')->__('Customer Last Name'),
            'width'     => '50px',
			'type'  	=> 'text',
            'index'     => 'customer_lastname',
        ));
		$this->addColumn('customer_email', array(
		     'header'    => Mage::helper('partialpayment')->__('Customer Email'),
		     'width'     => '50px',
		     'type'		 => 'text',
		     'index'     => 'customer_email',
		));
		$this->addColumn('grand_total', array(
		     'header'    => Mage::helper('partialpayment')->__('Total Amount'),
		     'width'     => '50px',
		     'type'		 => 'currency',
		     'currency'  => 'order_currency_code',
		     'index'     => 'grand_total',
		));
		$this->addColumn('partial_paid_amount', array(
		    'header'    => Mage::helper('partialpayment')->__('Paid Amount'),
		    'width'     => '50px',
		    'type'		=> 'currency',
		    'currency'  => 'order_currency_code',
		    'index'     => 'partial_paid_amount',
		));
		$this->addColumn('partial_later_amount', array(
		    'header'    => Mage::helper('partialpayment')->__('Due Amount'),
		    'width'     => '50px',
		    'type'		=> 'currency',
		    'currency'  => 'order_currency_code',
		    'index'     => 'partial_later_amount',
		));
		
		$this->addColumn('total_installment', array(
		    'header'    => Mage::helper('partialpayment')->__('Total Installment'),
		    'width'     => '50px',
		    'type'		=> 'int',
		    'index'     => 'total_installment',
		));
		$this->addColumn('paid_installment', array(
		    'header'    => Mage::helper('partialpayment')->__('Paid Installment'),
		    'width'     => '50px',
		    'type'		=> 'int',
		    'index'     => 'paid_installment',
		));
		
		$this->addColumn('created_at', array(
            'header'    => Mage::helper('partialpayment')->__('Created Date'),
            'width'     => '100px',
			'type'		=> 'datetime',
            'index'     => 'created_at',
        ));
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('partialpayment')->__('Updated Date'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'updated_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('partialpayment')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('partialpayment/order_status')->getOptionHash(),
        ));
        if (Mage::getSingleton('admin/session')->isAllowed('partialpayment/order/actions/edit')) {
            $this->addColumn('action',
                array(
                    'header'    => Mage::helper('partialpayment')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getPartialOrderId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('partialpayment')->__('Edit'),
                            'url'     => array('base'=>'partialpayment/adminhtml_order/edit'),
                            'field'   => 'partialorder_id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
                ));
        }
       // $this->addExportType('*/*/exportCsv', Mage::helper('promotionalgift')->__('CSV'));
       // $this->addExportType('*/*/exportXml', Mage::helper('promotionalgift')->__('XML'));

        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('partialorder_id' => $row->getId()));
    }
}