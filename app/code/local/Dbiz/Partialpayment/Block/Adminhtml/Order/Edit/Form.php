<?php
class Dbiz_Partialpayment_Block_Adminhtml_Order_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
	/**
     * Init Form properties
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('partial_order_form');
        $this->setTitle(Mage::helper('catalog')->__('Search Information'));
//         $this->setTemplate('partialpayment/order/view/tab/info.phtml');
    }

    /**
     * Prepare form fields
     *
     * @return Mage_Adminhtml_Block_Catalog_Search_Edit_Form
     */
    protected function _prepareForm()
    {  
        $model = Mage::registry('partial_order');
        /* @var $model Mage_CatalogSearch_Model_Query */
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array(
                'partialorder_id'    => $this->getRequest()->getParam('partialorder_id'),
            )),
            'method'    => 'post',
        )); 

        $fieldset = $form->addFieldset('partialpayment_form', array('legend'=>Mage::helper('partialpayment')->__('General Information')));
        
        $fieldset->addField('partialorder_id', 'label', array(
            'name'      => 'customer_firstname',
            'label'     => Mage::helper('partialpayment')->__('Partial Payment Id'),
            'title'     => Mage::helper('partialpayment')->__('Partial Payment Id'),
        ));
        
        $fieldset->addField('increment_id', 'label', array(
            'name'      => 'increment_id',
            'label'     => Mage::helper('partialpayment')->__('Order Id'),
            'title'     => Mage::helper('partialpayment')->__('Order Id'),
        ));
        
        $fieldset->addField('customer_id', 'label', array(
            'name'      => 'customer_id',
            'label'     => Mage::helper('partialpayment')->__('Customer ID'),
            'title'     => Mage::helper('partialpayment')->__('Customer ID'),
        ));
        
        $fieldset->addField('customer_firstname', 'label', array(
            'name'      => 'customer_firstname',
            'label'     => Mage::helper('partialpayment')->__('First Name'),
            'title'     => Mage::helper('partialpayment')->__('First Name'),
        ));
        
        $fieldset->addField('customer_lastname', 'label', array(
            'name'      => 'customer_lastname',
            'label'     => Mage::helper('partialpayment')->__('Last Name'),
            'title'     => Mage::helper('partialpayment')->__('Last Name'),
        ));
        
        $fieldset->addField('customer_email', 'label', array(
            'name'      => 'customer_email',
            'label'     => Mage::helper('partialpayment')->__('Email'),
            'title'     => Mage::helper('partialpayment')->__('Email'),
        ));
        $fieldset->addType('price','Dbiz_Partialpayment_Varien_Data_Form_Element_Price');
        
        $fieldset->addField('grand_total', 'price', array(
            'name'      => 'grand_total',
            'label'     => Mage::helper('partialpayment')->__('Total Amount'),
            'title'     => Mage::helper('partialpayment')->__('Total Amount'),
        ));
        
        $fieldset->addField('partial_paid_amount', 'price', array(
            'name'      => 'partial_paid_amount',
            'label'     => Mage::helper('partialpayment')->__('Paid Amount'),
            'title'     => Mage::helper('partialpayment')->__('Paid Amount'),
        ));
        
        $fieldset->addField('partial_later_amount', 'price', array(
            'name'      => 'partial_later_amount',
            'label'     => Mage::helper('partialpayment')->__('Due Amount'),
            'title'     => Mage::helper('partialpayment')->__('Due Amount'),
        ));
        
        $fieldset->addField('total_installment', 'label', array(
            'name'      => 'total_installment',
            'label'     => Mage::helper('partialpayment')->__('Total Installment'),
            'title'     => Mage::helper('partialpayment')->__('Total Installment'),
        ));
        
        $fieldset->addField('paid_installment', 'label', array(
            'name'      => 'paid_installment',
            'label'     => Mage::helper('partialpayment')->__('Paid Installment'),
            'title'     => Mage::helper('partialpayment')->__('Paid Installment'),
        ));
        
        $remainInstallment = $model->getTotalInstallment() - $model->getPaidInstallment();
        $model->setData('remaining_installment', $remainInstallment);
        $fieldset->addField('remaining_installment', 'label', array(
            'name'      => 'remaining_installment',
            'label'     => Mage::helper('partialpayment')->__('Remaining Installment'),
            'title'     => Mage::helper('partialpayment')->__('Remaining Installment'),
        ));
        
        $fieldset->addField('created_at', 'label', array(
            'name'      => 'remaining_installment',
            'label'     => Mage::helper('partialpayment')->__('Created At'),
            'title'     => Mage::helper('partialpayment')->__('Created A'),
        ));
        
        $fieldset->addField('updated_at', 'label', array(
            'name'      => 'remaining_installment',
            'label'     => Mage::helper('partialpayment')->__('Updated At'),
            'title'     => Mage::helper('partialpayment')->__('Updated At'),
        ));
        
        $fieldset->addField('status', 'select', array(
            'name'      => 'status',
            'label'     => Mage::helper('partialpayment')->__('Status'),
            'title'     => Mage::helper('partialpayment')->__('Status'),
            'options'   => Mage::getSingleton('partialpayment/order_status')->getOptionHash(),
        ));
        
        $field = $fieldset->addField('installment_collection', 'label', array(
            'name'      => 'installment_collection',
            'label'     => Mage::helper('partialpayment')->__('Installment'),
            'title'     => Mage::helper('partialpayment')->__('Installment'),
        ));
        $renderer = $this->getLayout()->createBlock('partialpayment/adminhtml_form_renderer_fieldset_installment');
        $field->setRenderer($renderer);
        
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
