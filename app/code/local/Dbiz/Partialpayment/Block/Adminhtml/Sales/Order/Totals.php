<?php
class Dbiz_Partialpayment_Block_Adminhtml_Sales_Order_Totals extends Dave_Vietship_Block_Adminhtml_Sales_Order_Totals
{

    protected function _initTotals()
    {
		parent::_initTotals();
		
    	$partialPaid = $this->getSource()->getPartialPaidAmount();
    	$partialLater = $this->getSource()->getPartialLaterAmount();
    	
    	if($partialPaid != $partialLater && !$partialLater){
    	    $this->_totals['partial_paid'] = new Varien_Object(array(
    	        'code'      => 'partial_paid',
    	        'value'     => $partialPaid,
    	        'base_value'=> $partialPaid,
    	        'label'     => Mage::helper ( 'partialpayment' )->__ ( 'Paid Amount' ),
    	        'area'      => 'footer'
    	    ));
    	    $this->_totals['partial_later'] = new Varien_Object(array(
    	        'code'      => 'partial_later',
    	        'value'     => $partialLater,
    	        'base_value'=> $partialLater,
    	        'label'     => Mage::helper ( 'partialpayment' )->__ ( 'Amount to be Paid Later' ),
    	        'area'      => 'footer'
    	    ));
    	}
        
        return $this;
    }
}
