<?php
class Dbiz_Callforme_Model_Mysql4_Callforme_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('callforme/callforme');
    }
}
