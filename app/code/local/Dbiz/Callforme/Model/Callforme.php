<?php
class Dbiz_Callforme_Model_Callforme extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('callforme/callforme');
    }
    
    public function getProduct($productId)
    {    
        $_product = Mage::getModel('catalog/product')->load($productId);
        
        return array('product_name'=>$_product->getName(), 'product_sku'=>$_product->getSKU());
    }
}
