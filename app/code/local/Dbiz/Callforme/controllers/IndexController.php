<?php
class Dbiz_Callforme_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if ($data = $this->getRequest()->getPost()) 
        {
            $model = Mage::getModel('callforme/callforme');
            $model->setData($data)->setCreatedTime(now())->setUpdateTime(now())->setStoreId(Mage::app()->getStore()->getId())->save();
            echo 'inserted';
        }else{
            echo 'not insert';
        }
    }
}