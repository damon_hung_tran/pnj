<?php

    class Dbiz_Callforme_Adminhtml_CallformeController extends Mage_Adminhtml_Controller_Action
    {
        public function indexAction()
        {
            $this->loadLayout()->_setActiveMenu('dbiz_solutions');
            $this->_addContent($this->getLayout()->createBlock('callforme/adminhtml_list'));
            $this->renderLayout();
        }
        
        public function editAction() 
        {
            $id           = $this->getRequest()->getParam('id');
            $model        = Mage::getModel('callforme/callforme')->load($id);
            $product_info = $model->getProduct( $model->getProductId() );
            $model->productName = $product_info['product_name'];
            $model->productSku  = $product_info['product_sku'];
            
            if ($model->getId() || $id == 0) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if (!empty($data)) {
                    $model->setData($data);
                }

                Mage::register('callforme_data', $model);

                $this->loadLayout();
                $this->_setActiveMenu('dbiz_solutions');
                $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
                $this->_addContent($this->getLayout()->createBlock('callforme/adminhtml_list_edit'))->_addLeft($this->getLayout()->createBlock('callforme/adminhtml_list_edit_tabs'));
                $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
                
                $this->renderLayout();
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('preorder')->__('Item does not exist'));
                $this->_redirect('*/*/');
            }
        }

        public function massStatusAction()
        {
            $callforme_ids = $this->getRequest()->getParam('callforme');
            if(!is_array($callforme_ids)) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
            } else {
                try {
                    foreach ($callforme_ids as $id) {
                        $item = Mage::getModel('callforme/callforme')
                        ->load($id)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();

                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($callforme_ids))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
            $this->_redirect('*/*/index');
        }
        
        public function saveAction() 
        {
            if ($data = $this->getRequest()->getPost()) 
            {
                $model = Mage::getModel('callforme/callforme');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));

                try {
                    $model->setUpdateTime(now())->setHandle(Mage::getSingleton('admin/session')->getUser()->getUsername());
                    $model->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('preorder')->__('successfully saved'));
                    Mage::getSingleton('adminhtml/session')->setFormData(false);

                    if ($this->getRequest()->getParam('back')) {
                        $this->_redirect('*/*/edit', array('id' => $model->getId()));
                        return;
                    }
                    $this->_redirect('*/*/');
                    return;
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
            }
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('callforme')->__('Unable to find item to save'));
            $this->_redirect('*/*/');
        }

        public function massDeleteAction() 
        {
            $ids = $this->getRequest ()->getParam ( 'callforme' );
            if (! is_array ( $ids )) {
                Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
            } else {
                try {
                    $callforme = Mage::getModel ( 'callforme/callforme' );
                    foreach ( $ids as $id ) {
                        $callforme->load ( $id );
                        $callforme->delete ();
                    }
                    Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully deleted', count ( $ids ) ) );
                } catch ( Exception $e ) {
                    Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                }
            }
            $this->_redirect ( '*/*/index' );
        }
        
        public function exportCsvAction() 
        {
            $fileName = 'callforme.csv';
            $content = $this->getLayout ()->createBlock ( 'callforme/adminhtml_list_grid' )->getCsv();

            $this->_sendUploadResponse ( $fileName, $content );
        }

        public function exportXmlAction() 
        {
            $fileName = 'callforme.xml';
            $content = $this->getLayout ()->createBlock ( 'callforme/adminhtml_list_grid' )->getXml();

            $this->_sendUploadResponse ( $fileName, $content );
        }

        protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') 
        {
            $response = $this->getResponse ();
            $response->setHeader ( 'HTTP/1.1 200 OK', '' );
            $response->setHeader ( 'Pragma', 'public', true );
            $response->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true );
            $response->setHeader ( 'Content-Disposition', 'attachment; filename=' . $fileName );
            $response->setHeader ( 'Last-Modified', date ( 'r' ) );
            $response->setHeader ( 'Accept-Ranges', 'bytes' );
            $response->setHeader ( 'Content-Length', strlen ( $content ) );
            $response->setHeader ( 'Content-type', $contentType );
            $response->setBody ( $content );
            $response->sendResponse ();
            die ();
        }
}