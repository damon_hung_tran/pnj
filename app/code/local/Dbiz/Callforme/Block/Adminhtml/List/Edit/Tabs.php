<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-L.txt
 *
 * @category   AW
 * @package    AW_Blog
 * @copyright  Copyright (c) 2009-2010 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-L.txt
 */

class Dbiz_Callforme_Block_Adminhtml_List_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('list_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('callforme')->__('Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('callforme')->__('Information'),
          'title'     => Mage::helper('callforme')->__('Information'),
          'content'   => $this->getLayout()->createBlock('callforme/adminhtml_list_edit_tab_form')->toHtml(),
      ));
	  
      return parent::_beforeToHtml();
  }
}
