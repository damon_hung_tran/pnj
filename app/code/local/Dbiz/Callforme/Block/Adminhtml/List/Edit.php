<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-L.txt
 *
 * @category   AW
 * @package    AW_Blog
 * @copyright  Copyright (c) 2009-2010 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-L.txt
 */

class Dbiz_Callforme_Block_Adminhtml_List_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'callforme';
        $this->_controller = 'adminhtml_list';
        
        $this->_updateButton('save', 'label', Mage::helper('callforme')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('callforme')->__('Delete'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('callforme_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'note');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'note');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('callforme_data') && Mage::registry('callforme_data')->getId() ) {
            return Mage::helper('callforme')->__("Edit '%s'", $this->htmlEscape(Mage::registry('callforme_data')->getName()));
        } else {
            return Mage::helper('callforme')->__('Add');
        }
    }
}
