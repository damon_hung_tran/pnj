<?php
class Dbiz_Callforme_Block_Adminhtml_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('CallformeGrid');
        $this->setDefaultSort('callforme_id'); // This is the primary key of the database
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('callforme/callforme')->getCollection();
        $collection->getSelect()->join(array('p' => "catalog_product_entity"), "main_table.product_id = p.entity_id", array('p.sku'));
        $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', 'name');
        $collection->getSelect()->join(array('ev' => "catalog_product_entity_varchar"), "p.entity_id = ev.entity_id AND ev.attribute_id=$attributeId", array('ev.value AS product_name'));
        //echo '<pre>'; print_r($collection->getSelect()->__toString() ); die();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('callforme_id', array(
            'header'    => Mage::helper('callforme')->__('ID'),
            'align'     =>'center',
            'width'     => '20px',
            'index'     => 'callforme_id',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('callforme')->__('Name'),
            'align'     => 'left',
            'index'     => 'name',
        ));
        
        $this->addColumn('email', array(
            'header'    => Mage::helper('callforme')->__('Email'),
            'align'     => 'left',
            'index'     => 'email',
        ));
        
        $this->addColumn('phone', array(
            'header'    => Mage::helper('callforme')->__('Phone'),
            'align'     => 'left',
            'index'     => 'phone',
        ));
        
        $this->addColumn('address', array(
            'header'    => Mage::helper('callforme')->__('Address'),
            'align'     => 'left',
            'index'     => 'address',
        ));
        
        $this->addColumn('sku', array(
            'header'    => Mage::helper('callforme')->__('Product SKU'),
            'align'     => 'left',
            'index'     => 'sku',
        ));
        
        $this->addColumn('product_name', array(
            'header'    => Mage::helper('callforme')->__('Product name'),
            'align'     => 'left',
            'index'     => 'product_name',
            'filter_condition_callback' => array($this, '_filterProductNameCondition'),
        ));
        
        $status_options = array();
        $status = Mage::getModel('callforme/status')->getCollection();
        foreach ($status as $s) $status_options[$s->getStatusId()] = $s->getName();
        
        $this->addColumn('status', array(
            'header'    => Mage::helper('callforme')->__('Status'),
            'align'     => 'center',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => $status_options,
        ));
        
        $this->addColumn('created_time', array(
            'header'    => Mage::helper('callforme')->__('Created At'),
            'align'     => 'center',
            'index'     => 'created_time',
        ));
        
        $this->addColumn('note', array(
            'header'    => Mage::helper('preorder')->__('Note'),
            'align'     => 'center',
            'index'     => 'note',
        ));
        
        $this->addColumn('handle', array(
            'header'    => Mage::helper('preorder')->__('Processed By'),
            'align'     => 'center',
            'index'     => 'handle',
        ));
        
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('preorder')->__('Action'),
                'width'     => '50',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('preorder')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

        /*$this->addColumn('store_id', array(
            'header'    => Mage::helper('callforme')->__('Store'),
            'align'     => 'center',
            'width'     => '160px',
            'index'     => 'store_id',
            'type'        => 'store',
            'store_all'     => true,
            'store_view'    => true,
            'sortable'      => false,
            'filter_condition_callback'
            => array($this, '_filterStoresCondition'),
        ));*/

        $this->addExportType('*/*/exportCsv', Mage::helper('callforme')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('callforme')->__('XML'));

        return parent::_prepareColumns();
    }
    
    protected function _filterProductNameCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->getSelect()->where("ev.value LIKE '%$value%'");
    }
    
    /*protected function _filterStoresCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addFieldToFilter('store_id', array('finset' => $value));
    }*/

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('c.callforme_id');
        $this->getMassactionBlock()->setFormFieldName('callforme');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('callforme')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('callforme')->__('Are you sure?')
        ));
        
        $status_options = array();
        $status = Mage::getModel('callforme/status')->getCollection();
        foreach ($status as $s) $status_options[$s->getStatusId()] = $s->getName();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('callforme')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('callforme')->__('Status'),
                         'values' => $status_options
                     )
             )
        ));

        return $this;
    }
}