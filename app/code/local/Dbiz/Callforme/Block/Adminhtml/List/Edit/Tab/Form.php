<?php

    class Dbiz_Callforme_Block_Adminhtml_List_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
    {
        protected function _prepareForm()
        {
            $form = new Varien_Data_Form();
            $this->setForm($form);
            $fieldset = $form->addFieldset('callforme_form', array('legend'=>Mage::helper('callforme')->__('information')));

            $fieldset->addField('name', 'label', array(
                'label'     => Mage::helper('callforme')->__('Name'),
                'name'      => 'name',
            ));
            $fieldset->addField('email', 'label', array(
                'label'     => Mage::helper('callforme')->__('Email'),
                'name'      => 'email',
            ));
            $fieldset->addField('phone', 'label', array(
                'label'     => Mage::helper('callforme')->__('Phone'),
                'name'      => 'phone',
            ));
            $fieldset->addField('address', 'label', array(
                'label'     => Mage::helper('callforme')->__('Address'),
                'name'      => 'address',
            ));
            $fieldset->addField('product_sku', 'label', array(
                'label'     => Mage::helper('callforme')->__('Product SKU'),
                'name'      => 'product_sku',
            ));
            $fieldset->addField('product_name', 'label', array(
                'label'     => Mage::helper('callforme')->__('Product Name'),
                'name'      => 'product_name',
            ));
            
            $data = Mage::registry('callforme_data')->getData();
            if( !empty($data['handle']) ){
                $fieldset->addField('handle', 'label', array(
                    'label'     => Mage::helper('callforme')->__('Processed By'),
                    'name'      => 'handle',
                )); 
            }
            $fieldset->addField('note', 'textarea', array(
                'label'     => Mage::helper('callforme')->__('Note'),
                'name'      => 'note',
            ));

            $status_options = array();
            $status = Mage::getModel('callforme/status')->getCollection();
            foreach ($status as $s) $status_options[] = array('value'=>$s->getStatusId(), 'label'=>$s->getName());
            $fieldset->addField('status', 'select', array(
                'label'     => Mage::helper('callforme')->__('Status'),
                'name'      => 'status',
                'values'    => $status_options,
            ));

            if ( Mage::getSingleton('adminhtml/session')->getCallformeData() )
            {
                $form->setValues(Mage::getSingleton('adminhtml/session')->getCallformeData());
                Mage::getSingleton('adminhtml/session')->setCallformeData(null);
            }elseif ( Mage::registry('callforme_data') ) {
                $form->setValues(Mage::registry('callforme_data')->getData());
            }

            return parent::_prepareForm();
        }
    }