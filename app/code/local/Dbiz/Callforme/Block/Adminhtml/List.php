<?php

class Dbiz_Callforme_Block_Adminhtml_List extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {
        $this->_controller = 'adminhtml_list';
        $this->_blockGroup = 'callforme';
        $this->_headerText = Mage::helper('callforme')->__('Call for me Manager');
        parent::__construct();
        $this->_removeButton('add');
    }
}