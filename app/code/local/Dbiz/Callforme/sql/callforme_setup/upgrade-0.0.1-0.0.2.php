<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('callforme/callforme')} ADD `note` varchar( 255 ) default NULL, ADD `processed_by` varchar( 20 ) default NULL;

");

$installer->endSetup();
