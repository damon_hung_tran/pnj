<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('callforme/callforme')};
CREATE TABLE {$this->getTable('callforme/callforme')} (
    `callforme_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
    `product_id` int( 11 ) unsigned NOT NULL ,
    `name` varchar( 32 ) NOT NULL default '',
    `email` varchar( 32 ) default NULL,
    `phone` varchar( 32 ) NOT NULL default '',
    `address` varchar( 255 ) default NULL,
    `status` tinyint( 2 ) NOT NULL default 1,
    `created_time` datetime default NULL ,
    `update_time` datetime default NULL ,
    `store_id` int( 11 ) unsigned NOT NULL ,
    PRIMARY KEY ( `callforme_id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS {$this->getTable('callforme/status')};
CREATE TABLE {$this->getTable('callforme/status')} (
    `status_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
    `name` varchar( 20 ) NOT NULL default '',
    `created_time` datetime default NULL ,
    `update_time` datetime default NULL ,
    `status` tinyint( 2 ) NOT NULL default 1,
    PRIMARY KEY ( `status_id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO {$this->getTable('callforme/status')}
VALUES (NULL , 'Mới', NOW( ), NOW( ), 1), (NULL , 'Đang xử lý', NOW( ), NOW( ), 1), (NULL , 'Đã xử lý', NOW( ), NOW( ), 1), (NULL , 'Hủy', NOW( ), NOW( ), 1);

");

$installer->endSetup(); 
