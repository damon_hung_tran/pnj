<?php

    class Dbiz_Petty_Adminhtml_ExportProductImageController extends Mage_Adminhtml_Controller_Action
    {
        protected function _initProduct() {

            $product = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));


            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }

            $product->setData('_edit_mode', true);

            Mage::register('product', $product);

            return $product;
        }

        public function indexAction()
        {
            $this->_initProduct();

            $this->loadLayout()->_setActiveMenu('reports');
            $this->_addContent($this->getLayout()->createBlock('petty/adminhtml_exportproductimage'));
            $this->renderLayout();
        }

        public function gridAction() 
        {
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('petty/adminhtml_exportproductimage_grid')->toHtml()
            );
        }

        protected function _validateSecretKey() 
        {
            return true;
        }

        protected function _isAllowed() 
        {
            return Mage::getSingleton('admin/session')->isAllowed('admin/catalog/petty');
        }

        private function _getExportFileName($extension='csv') 
        {
            return 'product_image.'.$extension;
        }

        /**
        * Export stylist grid to CSV format
        */
        public function exportCsvAction() {

            $fileName = $this->_getExportFileName('csv');
            $content  = $this->getLayout()->createBlock('petty/adminhtml_exportproductimage_grid')->getCsvFile();
            $this->_prepareDownloadResponse($fileName, $content);
        }

        /**
        * Export stylist grid to XML format
        */
        public function exportXmlAction() {

            $fileName = $this->_getExportFileName('xml');

            $content = $this->getLayout()->createBlock('petty/adminhtml_exportproductimage_grid')
            ->getExcelFile();
            
            $this->_prepareDownloadResponse($fileName, $content);
        }

        protected function _prepareDownloadResponse(
            $fileName,
            $content,
            $contentType = 'application/octet-stream',
            $contentLength = null)
        {
            $session = Mage::getSingleton('admin/session');
            if ($session->isFirstPageAfterLogin()) {
                $this->_redirect($session->getUser()->getStartupPageUrl());
                return $this;
            }

            $isFile = false;
            $file   = null;
            if (is_array($content)) {
                if (!isset($content['type']) || !isset($content['value'])) {
                    return $this;
                }
                if ($content['type'] == 'filename') {
                    $isFile         = true;
                    $file           = $content['value'];
                    $contentLength  = filesize($file);
                }
            }
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Encoding: UTF-8');
            header('Content-type: text/csv; charset=UTF-8');
            header("Content-Disposition: attachment;filename={$fileName}");
            echo "\xEF\xBB\xBF"; // UTF-8 BOM

            if (!is_null($content)) {
                if ($isFile) {
                    $this->getResponse()->clearBody();
                    $this->getResponse()->sendHeaders();

                    $ioAdapter = new Varien_Io_File();
                    $ioAdapter->open(array('path' => $ioAdapter->dirname($file)));
                    $ioAdapter->streamOpen($file, 'r');
                    while ($buffer = $ioAdapter->streamRead()) {
                        print $buffer;
                    }
                    $ioAdapter->streamClose();
                    if (!empty($content['rm'])) {
                        $ioAdapter->rm($file);
                    }

                    exit(0);
                } else {
                    $this->getResponse()->setBody($content);
                }
            }
            return $this;
        }
}