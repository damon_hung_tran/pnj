<?php

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'dbiz_created_at', array(
    'group'             => 'General',
    'type'              => 'varchar',
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Dbiz Created At',
    'input'             => 'label',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
    'is_configurable'   => false
));

$installer->addAttribute('catalog_product', 'dbiz_updated_at', array(
    'group'             => 'General',
    'type'              => 'varchar',
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Dbiz Updated At',
    'input'             => 'label',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
    'is_configurable'   => false
));

$installer->addAttribute('catalog_product', 'dbiz_inactive_at', array(
    'group'             => 'General',
    'type'              => 'datetime',
    'backend'           => 'eav/entity_attribute_backend_time_created',
    'frontend'          => '',
    'label'             => 'Dbiz Inactive At',
    'input'             => 'date',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
    'is_configurable'   => false
));

$installer->endSetup();
