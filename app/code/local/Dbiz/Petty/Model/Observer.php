<?php
class Dbiz_Petty_Model_Observer {

    public function checkoutSubmitAllAfter($observer) 
    {
        $order    = $observer->getOrder();
        
        // Get old order
        $oldOrder = Mage::getModel('sales/order')->load($order->getRelationParentId());
        
        // Getting username
        $user = Mage::getSingleton('admin/session');
        $username = $user->getUser()->getUsername();
        $append = " [name]".$username;
        
        //appending username with markup to comment
        $comments = $order->getStatusHistoryCollection(true);
        foreach ($comments as $c) {
            $c->setData('comment', $c->getComment() . $append)->save();
        }
        
        if( $oldOrder ){
            $order->setCreatedAt($oldOrder->getCreatedAt());
        }
        
        // Save attribute set
        $itemCollection = $order->getItemsCollection();
        foreach($itemCollection as $_item){
            $_product = Mage::getModel('catalog/product')->load($_item->getProductId());
            $prodAttributeSet = Mage::getModel('eav/entity_attribute_set')->load($_product->getAttributeSetId())->getAttributeSetName();
            $_item->setData('attribute_set', $prodAttributeSet);
        }
        
        $order->save();
    }
    
    public function catalogProductNewAction($observer)
    {
        $product = $observer->getProduct();
        $product->setDbizCreatedAt( date('Y-m-d') );
        $product->setDbizUpdatedAt( date('Y-m-d') );
    }
    
    public function catalogProductEditAction($observer)
    {
        $product = $observer->getProduct();       
        $product->setDbizCreatedAt( $product->getCreatedAt() );
        $product->setDbizUpdatedAt( $product->getUpdatedAt() );
    }
}