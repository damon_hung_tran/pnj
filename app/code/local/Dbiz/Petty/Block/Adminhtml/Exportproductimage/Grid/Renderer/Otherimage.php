<?php
class Dbiz_Petty_Block_Adminhtml_Exportproductimage_Grid_Renderer_Otherimage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $lineBreak = '<br/>';
        $out = '';
        
        $action_name = $this->getRequest()->getActionName();
        if ($action_name == 'exportCsv' || $action_name == 'exportXml') {
            $lineBreak = ", ";
        }
        
        $_product = Mage::getModel('catalog/product')->load($row->getEntityId());
        
        foreach ($_product->getMediaGalleryImages() as $image){
            if( $image->getFile() != $_product->getThumbnail() && $image->getFile() != $_product->getSmallImage() && $image->getFile() != $_product->getImage()  )
                $out .= $image->getFile() . $lineBreak;
        }
        
        /*$val = Mage::helper('catalog/image')->init($row, 'thumbnail')->resize(97);
        $out = "<img src=". $val ." width='97px'/>";*/
        return $out;
    }
}