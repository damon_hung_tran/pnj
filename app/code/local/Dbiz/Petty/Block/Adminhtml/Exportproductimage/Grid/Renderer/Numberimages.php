<?php
class Dbiz_Petty_Block_Adminhtml_Exportproductimage_Grid_Renderer_Numberimages extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $_product = Mage::getModel('catalog/product')->load($row->getEntityId());
        
        return count($_product->getMediaGalleryImages());
    }
}