<?php

class Dbiz_Petty_Block_Adminhtml_ExportProductImage extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() 
    {
        $this->_blockGroup = 'petty';
        $this->_controller = 'adminhtml_exportproductimage';

        $this->_headerText = Mage::helper('adminhtml')->__('Dbiz product image');

        parent::__construct();

        $this->_removeButton('add');
    }

    public function getSaveUrl() {
        return $this->getUrl('*/*/save', array('store' => $this->getRequest()->getParam('store')));
    }

    protected function _afterToHtml($html) {
        return $this->_prependHtml() . parent::_afterToHtml($html);
    }

    private function _prependHtml() {
        $html = '
        
        <form id="product_image_form" action="' . $this->getSaveUrl() . '" method="post" enctype="multipart/form-data">
        <input name="form_key" type="hidden" value="' . $this->getFormKey() . '" />
            <div class="no-display">
                <input type="hidden" name="export_product_image" id="in_export_product_image" value="" />
            </div>
        </form>
        ';

        return $html;
    }


    public function getHeaderHtml() {
        return '<h3 style="background-image: url(' . $this->getSkinUrl('images/product_rating_full_star.gif') . ');" class="' . $this->getHeaderCssClass() . '">' . $this->getHeaderText() . '</h3>';
    }

    protected function _prepareLayout() {
        $this->setChild('store_switcher', $this->getLayout()->createBlock('adminhtml/store_switcher', 'store_switcher')->setUseConfirm(false)
        );
        return parent::_prepareLayout();
    }

    public function getGridHtml() {

        return $this->getChildHtml('store_switcher') . $this->getChildHtml('grid');
    }

}