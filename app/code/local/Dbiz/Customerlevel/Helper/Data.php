<?php

class Dbiz_Customerlevel_Helper_Data extends Mage_Core_Helper_Abstract {

	public function isEnabled() {
		return Mage::getStoreConfig('rewardpoints/customer_level/enable');
	}

	public function getAvailableCustomerGroup($customer_id) {
		$result = array();

		$collection = Mage::getModel('levelrule/levelrule')->getCollection()
				->addFieldToFilter('customer_group_from', $customer_id);

		$collection->getSelect()->join(
				array('customergroup' => 'customer_group'), 
				'main_table.customer_group_to=customergroup.customer_group_id', 
				array('customergroup.customer_group_id',
					'customergroup.customer_group_code'
				)
		);

		$result = &$collection;

		return $result;
	}

}
