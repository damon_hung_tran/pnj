<?php 
class Dbiz_Promotime_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
	/**
	 * Get product final price
	 *
	 * @param double $qty
	 * @return double
	 */
	public function getFinalPrice($qty=null)
	{	
		return $this->getPriceModel()->getFinalPrice($qty, $this);
	}
}
