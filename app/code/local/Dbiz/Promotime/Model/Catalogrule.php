<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Promotionalgift Model
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @author      Magestore Developer
 */
class Dbiz_Promotime_Model_Catalogrule extends Mage_Core_Model_Abstract
{	
    public function _construct()
    {
        parent::_construct();
        $this->_init('promotime/catalogrule');
    }
    /**
     * Fix bug when save website ids and customer group id in magento v1.7
     *
     **/
    protected function _beforeSave()
    {
    	parent::_beforeSave();
    	
    	if ($this->hasCatalogruleIds()) {
    		$ruleIds = $this->getCatalogruleIds();
    		if (is_array($ruleIds) && !empty($ruleIds)) {
    			$this->setCatalogruleIds(implode(',', $ruleIds));
    		}
    	} 
    	return $this;
    }
	
}