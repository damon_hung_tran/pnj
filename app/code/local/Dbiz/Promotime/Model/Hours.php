<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Promotionalgift Status Model
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @author      Magestore Developer
 */
class Dbiz_Promotime_Model_Hours extends Varien_Object
{
    
    /** 
     * get model option as array
     *
     * @return array
     */
    static public function getOptionArray()
    {
    	$hoursArray = array();
    	for($i = 0; $i <= 24; $i++){
    		$hoursArray[] = array($i, $i);
    	}
        return $hoursArray;
    }
    
    /**
     * get model option hash as array
     *
     * @return array
     */
    static public function getOptionHash()
    {
        $options = array();
        for($i = 0; $i <= 24; $i++){
        	$options[] = array(
                'value'    => $i,
                'label'    => $i
            );
        }
        return $options;
    }
}