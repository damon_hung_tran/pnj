<?php

class Dbiz_Promotime_Model_Layer extends Mage_Catalog_Model_Layer
{
    public function getProductCollection()
    {	
		if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
            $collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
        } else {
			if (is_null($this->_productCollection)) {
				
				$promotionProductIds = array(); 
		
				$rules = Mage::getResourceModel('catalogrule/rule_collection')
				->addWebsiteFilter(Mage::app()->getWebsite()->getId())
				->load();
				
				// read: if there are active rules
				if($rules->getData()) {
					$rule_ids = array(); // i used this down below to style the products according to which rule affected
					$productIds[] = array(); // this will hold the ids of the products
					//load promotimes rule
					$promoTimes = Mage::getSingleton('promotime/catalogrule')
								->getCollection()
								->getRuningRules();
					
					foreach($promoTimes as $promotime){
						if(strpos($promotime['catalogrule_ids'],",") !== false){
							$rule_ids = explode(",", $promotime['catalogrule_ids']);
						}else{
							$rule_ids = array($promotime["catalogrule_ids"]);
						}
					}
					/* if($_SERVER["REMOTE_ADDR"] == "180.93.171.162" ){
						print_r($rule_ids);exit;  
					} */
					if(in_array(0,$rule_ids)){
						$allCatalogRule = true;
					}else{
						$allCatalogRule = false;
					}
					
					foreach($rules as $rule) {
						$rule->setWebsiteIds(Mage::app()->getWebsite()->getId());
						if($rule->getIsActive()  && (in_array($rule->getId(),$rule_ids) || $allCatalogRule == true) && ( $rule->getFromDate()  < $rule->getNow()  &&  $rule->getNow() < $rule->getToDate())
								||   $rule->getToDate() == '' &&  $rule->getNow() > $rule->getFromDate()  && (in_array($rule->getId(),$rule_ids) || $allCatalogRule == true)){
							$rule_ids[] = $rule->getId();
							$productIds[] = $rule->getMatchingProductIds(); // affected products come in here */
							/* if($_SERVER["REMOTE_ADDR"] == "180.93.171.162" ){
								print_r($productIds);exit;  
							} */
						}
					}
					foreach($productIds as $item => $value){
						foreach($value as $productId => $data){
							if($data[Mage::app()->getWebsite()->getId()] == 1){
								$promotionProductIds[] = $productId;
							}
						}
					}
					
					// we initialize a new collection and filter solely by the product IDs we got before, read: select all products with their entity_id in $arr
					if(empty($promotionProductIds)){
						$promotionProductIds = array(0);
					}			
					$collection = Mage::getModel('catalog/product')
								->getCollection()
								->addAttributeToSelect('*')     
								->addAttributeToFilter('entity_id', array('in'=>$promotionProductIds));
								
					$this->prepareProductCollection($collection);
					$this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
					$this->setCollection($collection);
				}
			}
        }
		// Zend_Debug::dump($collection->getSelect()->__toString());die();
		return $collection;
    }
	
	public function getProductIds()
	{
		$products = Mage::getResourceModel('catalog/product_collection');
		$productIds = array();
		foreach($products as $product){
			$availableRule = Mage::getModel('promotionalgift/catalogrule')->validateItem($product->getId());
			if($availableRule){
				$productIds[] = $product->getId();
			}
		}
		return $productIds;
	}
	
	public function _getProductCollection()
    {	
			$collection  = Mage::getResourceModel('catalog/product_collection')
								->addAttributeToSelect('*')
								->addFieldToFilter('entity_id', array('in'=>$this->getProductIds()));								
		return $collection;	
    }
	
}
