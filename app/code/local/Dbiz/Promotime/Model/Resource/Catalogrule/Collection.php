<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Promotionalgift Resource Collection Model
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @author      Magestore Developer
 */
class Dbiz_Promotime_Model_Resource_Catalogrule_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('promotime/catalogrule');
    }
    /**
     * Return array of unique product type ids in collection
     *
     * @return array
     */
    public function getPromotimeRuleActiveIds()
    {
    	$select = $this->getSelect();
    	/** @var $select Varien_Db_Select */
    	$select->distinct(true);
    	$select->columns('catalogrule_ids'); 
    	$select->columns('from_time');
    	$select->columns('to_time');
//     	$select->where("from_time <= {$nowHour} AND to_time >= {$nowHour} "); 
    	$select->where("status = 1");
    	return $this->getConnection()->fetchAll($select);
    }	/**     * Return array of unique product type ids in collection     *     * @return array     */    
	public function getRuningRules()    {
		$nowHour = Mage::app()->getLocale()->storeDate(Mage::app()->getStore(), null, true)->toString('H');    
		$nowMin = Mage::app()->getLocale()->storeDate(Mage::app()->getStore(), null, true)->toString('m');    	
		$now = $nowHour + floatval($nowMin)/60;        	
		$select = $this->getSelect();    	
		/** @var $select Varien_Db_Select */    	
		$select->distinct(true);    	
		$select->columns('catalogrule_ids');    
		$select->columns('from_time');    	
		$select->columns('to_time');    
		$select->where("from_time <= {$now} AND to_time >= {$now} ");  
		$select->where("status = 1");  
		return $this->getConnection()->fetchAll($select);    
	}
}