<?php

/**
 * 
 * @author working
 *
 */
class Dbiz_Promotime_Model_Observer {

    public function processFrontFinalPrice($observer) {
        
        if (!Mage::helper('promotime')->enablePromotime())
            return $this;
        $product = $observer->getEvent()->getProduct();
        $pId = $product->getId();
        $storeId = $product->getStoreId();

        if ($observer->hasDate()) {
            $date = $observer->getEvent()->getDate();
        } else {
            $date = Mage::app()->getLocale()->storeTimeStamp($storeId);
        }

        if ($observer->hasWebsiteId()) {
            $wId = $observer->getEvent()->getWebsiteId();
        } else {
            $wId = Mage::app()->getStore($storeId)->getWebsiteId();
        }

        if ($observer->hasCustomerGroupId()) {
            $gId = $observer->getEvent()->getCustomerGroupId();
        } elseif ($product->hasCustomerGroupId()) {
            $gId = $product->getCustomerGroupId();
        } else {
            $gId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        }
// time now
        $nowHour = Mage::app()->getLocale()->storeDate(Mage::app()->getStore(), null, true)->toString('H');
        $nowMin = Mage::app()->getLocale()->storeDate(Mage::app()->getStore(), null, true)->toString('m');
        $nowSecond = Mage::app()->getLocale()->storeDate(Mage::app()->getStore(), null, true)->toString('s');
        $now = $nowHour + floatval($nowMin) / 60;
        $key = "$date|$wId|$gId|$pId";
        if (!isset($this->_rulePrices[$key])) {
            /* $rulePrice = Mage::getResourceModel('catalogrule/rule')
              ->getRulePrice($date, $wId, $gId, $pId);
              $this->_rulePrices[$key] = $rulePrice; */
            $rules = Mage::getResourceModel('catalogrule/rule')
                    ->getRulesFromProduct($date, $wId, $gId, $pId);

            $promoTimes = Mage::getSingleton('promotime/catalogrule')
                    ->getCollection()
                    ->getPromotimeRuleActiveIds();
$ruleActive = null;
 $promotimeRule =array();
 $ruleIds = array();
            foreach ($rules as $rule) {
                foreach ($promoTimes as $promo) {
                    // validate promotime rule
                   
                    if (in_array($rule["rule_id"], explode(",", $promo['catalogrule_ids']))) {
                        $promotimeRule[] = $rule["rule_id"];
                        if ($nowHour < $promo["from_time"] || $now > $promo["to_time"]) {
                            continue;
                        }
                    }
                    // run promotime rule
                    if (strpos($promo['catalogrule_ids'], ",") !== false) {
                        if (in_array($rule["rule_id"], explode(",", $promo['catalogrule_ids'])) || in_array(0, explode(",", $promo['catalogrule_ids']))) {
                            $ruleActive = $promo;
                            $found = true;
                            break;
                        }
                    } elseif ($promo['catalogrule_ids'] == $rule["rule_id"] || $promo['catalogrule_ids'] == 0) {
                        $ruleActive = $promo;
                        $found = true;
                        break;
                    }
                }
                
                $ruleIds[] = $rule["rule_id"];
            }
           $foundOther = FALSE;
            $catalogRule = array_diff($ruleIds, $promotimeRule);
            if (is_array($catalogRule)) {
                $foundOther = true;
            }
        }
        if (isset($found) && $found === true || $foundOther == true) {


            if ($nowHour < $ruleActive["from_time"] || $now > $ruleActive["to_time"]) {
                $basicPrice = $product->getData('price');
                $discountAmount = null;
                foreach ($catalogRule as $perRule => $rule_id) {
                    $catalogRule = Mage::getModel('catalogrule/rule')->load($rule_id);
                    $catalogRule->setWebsiteIds(Mage::app()->getStore());
                    $discountAmount += $catalogRule->getDiscountAmount();
                }
                $promoPrice = $basicPrice - (($basicPrice * $discountAmount) / 100);
                $product->setFinalPrice($promoPrice);
                $product->setMinPrice($promoPrice);
                $product->setMaxPrice($promoPrice);
                $product->setMinimalPrice($promoPrice);
                Mage::getSingleton('core/session')->setPromotime(false);
            } else {
                /* $hourRuned = $nowHour - $ruleActive["from_time"];
                  $secondRuned = intval($hourRuned) + intval($nowMin*60) + intval($nowSecond);
                  $seconds_time =  intval($ruleActive["to_time"] - $ruleActive["from_time"]) * 3600;
                  $time_down = $seconds_time - $secondRuned; */
                $product->setData('time_down', $ruleActive["to_time"]);
            }
        }
    }

}
