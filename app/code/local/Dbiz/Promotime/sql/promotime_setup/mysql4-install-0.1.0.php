<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('promotime_catalog_rule')};

CREATE TABLE {$this->getTable('promotime_catalog_rule')} (
  `rule_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) default '',
  `description` text default '',
  `status` smallint(6) NOT NULL default '0',
  `website_ids` text default '',
  `catalogrule_ids` text default '',
  `from_time` smallint(2) NOT NULL default '0',
  `to_time` smallint(2) NOT NULL default '0',
  `priority` int(11) unsigned default '0',
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();

