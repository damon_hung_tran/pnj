<?php
class Dbiz_Promotime_Block_Product_View extends Mage_Catalog_Block_Product_View
{	

	public function _construct()
	{
		parent::_construct();
		$this->setTemplate('dbiz_promotime/product/view.phtml');
	}
	
	public function validateProductHasPromotime($product_id)
	{
		return Mage::getModel('promotime/catalogrule')->validateItem($product_id);
	}
	
	
	public function getCurrentProduct()
	{
		return Mage::registry('current_product');
	}
	
}

