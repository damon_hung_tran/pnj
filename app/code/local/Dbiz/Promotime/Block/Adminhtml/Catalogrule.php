<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Promotionalgift Adminhtml Block
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @author      Magestore Developer
 */
class Dbiz_Promotime_Block_Adminhtml_Catalogrule extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {	
        $this->_controller = 'adminhtml_catalogrule';
        $this->_blockGroup = 'promotime';
        $this->_headerText = Mage::helper('promotime')->__('Catalog Rule Manager');
        $this->_addButtonLabel = Mage::helper('promotime')->__('Add Rule');
        parent::__construct();
    }
}