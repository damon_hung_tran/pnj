<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Promotionalgift Edit Form Content Tab Block
 * 
 * @category    Magestore
 * @package     Magestore_Promotionalgift
 * @author      Magestore Developer
 */
class Dbiz_Promotime_Block_Adminhtml_Catalogrule_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Magestore_Promotionalgift_Block_Adminhtml_Promotionalgift_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        if (Mage::getSingleton('adminhtml/session')->getCatalogruleData()) {
            $data = Mage::getSingleton('adminhtml/session')->getCatalogruleData();
            Mage::getSingleton('adminhtml/session')->setCatalogruleData(null);
        } elseif (Mage::registry('catalogrule_data')) {
            $data = Mage::registry('catalogrule_data')->getData();
        }
        $fieldset = $form->addFieldset('catalogrule_form', array(
            'legend'=>Mage::helper('promotime')->__('Rule information')
        ));
		
		$inStore = $this->getRequest()->getParam('store');
		$defaultLabel = Mage::helper('promotime')->__('Use Default');
		$defaultTitle = Mage::helper('promotime')->__('-- Please Select --');
		$scopeLabel = Mage::helper('promotime')->__('STORE VIEW');
		
        $fieldset->addField('name', 'text', array(
            'label'        => Mage::helper('promotime')->__('Rule Name'),
            'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'name',
        ));

        $fieldset->addField('description', 'textarea', array(
            'name'       => 'description',
            'label'      => Mage::helper('promotime')->__('Description'),
            'title'      => Mage::helper('promotime')->__('Description'),
            'style'		 => 'width: 98%; height: 100px;',
            'wysiwyg'    => false,
            'required'   => true,
        ));
		
        $fieldset->addField('status', 'select', array(
            'label'        => Mage::helper('promotime')->__('Status'),
            'name'        => 'status',
            'values'    => Mage::getSingleton('promotime/status')->getOptionHash(),
        ));
		

		$collection = Mage::getModel('catalogrule/rule')->getCollection();
		$collection ->getSelect()
					->reset(Zend_Db_Select::COLUMNS)
					->columns('rule_id')
					->columns('name');
		$temp = $collection->getData();
        $found = false; 
        $catalogrules = array();
        foreach ($temp as $rule) {
            if ($rule['rule_id']==0) {
                $found = true;
            }
            $catalogrules[] = array('label' => $rule['name'], 'value' => $rule['rule_id']) ;
        }
        
        if (!$found) {
            array_unshift($catalogrules, array('value'=>0, 'label'=>Mage::helper('promotime')->__('All catalog rules')));
        }
        
        $fieldset->addField('catalogrule_ids', 'multiselect', array(
            'name'      => 'catalogrule_ids[]',
            'label'     => Mage::helper('promotime')->__('Catalog Price rules'),
            'title'     => Mage::helper('promotime')->__('Catalog Price rules'),
            'required'  => true,
            'values'    => $catalogrules,
        ));
		
		
        $fieldset->addField('from_time', 'select', array(
            'name'   => 'from_time',
            'label'  => Mage::helper('promotime')->__('Start Time'),
            'title'  => Mage::helper('promotime')->__('Start Time'),
        	'values' => Mage::getSingleton('promotime/hours')->getOptionHash(),
        ));
        
        $fieldset->addField('to_time', 'select', array(
            'name'   => 'to_time',
            'label'  => Mage::helper('promotime')->__('End Time'),
            'title'  => Mage::helper('promotime')->__('End Time'),
        	'values' => Mage::getSingleton('promotime/hours')->getOptionHash(),
        ));

        $fieldset->addField('priority', 'text', array(
            'name' => 'priority',
            'label' => Mage::helper('promotime')->__('Priority'),
			'note'  => Mage::helper('promotime')->__('The smaller the value, the higher the priority.'),
        ));
		
        $form->setValues($data);
        return parent::_prepareForm();
    }
}