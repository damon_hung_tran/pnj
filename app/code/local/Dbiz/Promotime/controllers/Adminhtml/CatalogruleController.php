<?php
class Dbiz_Promotime_Adminhtml_CatalogruleController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout();
        return $this;
    }

    public function indexAction()
    {	
        $this->_title($this->__('Catalog Rules'))->_title($this->__('Catalog Rules'));
        $this->_initAction()
             ->renderLayout();
    }
	
	public function editAction() 
	{
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('promotime/catalogrule')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('catalogrule_data', $model);
			
			$this->_title($this->__('Promotional Timer'))
				->_title($this->__('Manage rule'));
			if ($model->getId()){
				$this->_title($model->getName());
			}else{
				$this->_title($this->__('New rule'));
			}

			$this->loadLayout();
			$this->_setActiveMenu('promotime/catalogrule');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Rule Manager'), Mage::helper('adminhtml')->__('Rule Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Rule News'), Mage::helper('adminhtml')->__('Rule News'));
			

			$this->_addContent($this->getLayout()->createBlock('promotime/adminhtml_catalogrule_edit'))
				->_addLeft($this->getLayout()->createBlock('promotime/adminhtml_catalogrule_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('promotime')->__('Rule does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() 
	{
		$this->_forward('edit');
	}
	

	
	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost()) {
			if (isset($data['from_time']) && $data['from_time'] == '') $data['from_time'] = null;
			if (isset($data['to_time']) && $data['to_time'] == '') $data['to_time'] = null;
			$model = Mage::getModel('promotime/catalogrule');
			
			// add data to model
			$model->addData($data)
				->setId($this->getRequest()->getParam('id'));
			try {
// 				$model->loadPost($data);
				//save date
				$model->setData('from_time',$data['from_time']);
				$model->setData('to_time',$data['to_time']);
			
				$model->save();
				// var_dump($data);die();
				//save list of gift items
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('promotime')->__('Catalog rule was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array(
						'id' => $model->getId(),
					));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                	'id' 	=> $this->getRequest()->getParam('id'),
                ));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('promotime')->__('Unable to find catalog rule to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() 
	{
        if (!Mage::helper('magenotification')->checkLicenseKeyAdminController($this)) {return;}
		if( $this->getRequest()->getParam('id') > 0 ){
			try {
				$model = Mage::getModel('promotime/catalogrule');
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Rule was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array(
					'id' 	=> $this->getRequest()->getParam('id'),
					'store' => $this->getRequest()->getParam('store'),
				));
			}
		}
		$this->_redirect('*/*/',array('store' => $this->getRequest()->getParam('store')));
	}
}
