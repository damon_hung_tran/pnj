<?php

/**
 * Booking Resource Collection
 *
 * @category   PHT
 * @package    PHT_Bookings
 * @author     
 */
class Dbiz_Levelrule_Model_Mysql4_Levelrule_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	/**
	 * Constructor method
	 */
	protected function _construct() {
		$this->_init('levelrule/levelrule');
		$this->_map['fields']['id'] = 'main_table.id';
	}

	protected function _renderFiltersBefore() {
		return parent::_renderFiltersBefore();
	}

}
