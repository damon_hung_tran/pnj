<?php

class Dbiz_Levelrule_Model_Levelrule extends Mage_Core_Model_Abstract {

	protected function _construct() {
		parent::_construct();
		$this->_init('levelrule/levelrule');
	}
	
	public function getCustomerGroupList() {
		//Load product model collecttion filtered by attribute set id
		 $items = Mage::getModel('customer/group')
            ->getCollection();
        //process your product collection as per your bussiness logic
        $values = array();
        if (count($items) > 0)
        {
			foreach($items as $item)
			{
	            $values[] = array('value'=>$item->getCustomerGroupId(),'label'=>$item->getCustomerGroupCode());
	        }
        }
        //return all products name with attribute set 'my_custom_attribute'
        return $values;
	}

}