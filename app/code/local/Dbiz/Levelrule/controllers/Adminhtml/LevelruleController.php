<?php
class Dbiz_Levelrule_Adminhtml_LevelruleController extends Mage_Adminhtml_Controller_Action {
	
	protected function _initAction() {
		$this->loadLayout()
				->_setActiveMenu('levelrule');
		return $this;
	}
	
	public function indexAction() {
		$this->_title($this->__('Member level rules'));
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('levelrule/adminhtml_levelrule'));
		$this->renderLayout();
	}
	
	public function addAction() {
		$this->_forward('edit');
	}

	public function editAction() {
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('levelrule/levelrule')->load($id);
			
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('levelrule_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('levelrule/items');

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('levelrule/adminhtml_levelrule_edit'))
					->_addLeft($this->getLayout()->createBlock('levelrule/adminhtml_levelrule_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('levelrule')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	
	public function saveAction()
	{
		 if ($data = $this->getRequest()->getPost()) 
		 {
            $_model = Mage::getModel('levelrule/levelrule');
            
            $_model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));
            $_model->save();
            
            if ($this->getRequest()->getParam('back')) 
            {
            	$this->_redirect('*/*/edit', array('id' => $_model->getId()));
                return;
            }
            
            $this->_redirect('*/*/');
                return;
		 }
		 
		 Mage::getSingleton('adminhtml/session')->addError(Mage::helper('levelrule')->__('Unable to find store to save'));
         $this->_redirect('*/*/');
	}
	
	public function deleteAction()
	{
		if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('levelrule/levelrule');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The Rule was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
	}
	
}