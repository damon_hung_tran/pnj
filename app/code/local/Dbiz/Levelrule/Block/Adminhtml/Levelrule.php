<?php

class Dbiz_Levelrule_Block_Adminhtml_Levelrule extends Mage_Adminhtml_Block_Widget_Grid_Container {
	
    public function __construct() {
        $this->_controller = 'adminhtml_levelrule';
        $this->_blockGroup = 'levelrule';
        $this->_headerText = Mage::helper('levelrule')->__('Manage Levelrule');
        parent::__construct();
		$this->setTemplate('dbiz/levelrule/levelrule.phtml');
    }
	
	protected function _prepareLayout() {	
        $this->setChild('add_new_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'label'     => Mage::helper('levelrule')->__('Add Levelrule'),
                'onclick'   => "setLocation('".$this->getUrl('*/*/add')."')",
                'class'   => 'add'
                ))
        );
        /**
         * Display levelrule switcher if system has more one levelrule
         */
        $this->setChild('grid', $this->getLayout()->createBlock('levelrule/adminhtml_levelrule_grid', 'levelrule.grid'));
        return parent::_prepareLayout();
    
	}
	
	public function getAddNewButtonHtml() {
        return $this->getChildHtml('add_new_button');
    }

    public function getGridHtml() {
        return $this->getChildHtml('grid');
    }

    public function getLevelruleSwitcherHtml() {
        return $this->getChildHtml('levelrule_switcher');
    }
	
}