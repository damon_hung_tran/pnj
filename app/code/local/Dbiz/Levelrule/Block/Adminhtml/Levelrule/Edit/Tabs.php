<?php

class Dbiz_Levelrule_Block_Adminhtml_Levelrule_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

	public function __construct() {
		parent::__construct();
		$this->setId('levelrule_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('levelrule')->__('Rule Information'));
	}

	protected function _beforeToHtml() {
		$this->addTab('general_section', array(
			'label' => Mage::helper('levelrule')->__('General Information'),
			'title' => Mage::helper('levelrule')->__('General Information'),
			'content' => $this->getLayout()->createBlock('levelrule/adminhtml_levelrule_edit_tab_form')->toHtml(),
		));

		return parent::_beforeToHtml();
	}

}
