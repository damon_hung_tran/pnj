<?php
class Dbiz_Levelrule_Block_Adminhtml_Levelrule_Renderer_CustomerGroupFromName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$groupId =  $row->getData($this->getColumn()->getIndex());
		$group = Mage::getModel('customer/group')->load($groupId);
		return $group->getCustomerGroupCode();
	}
}
