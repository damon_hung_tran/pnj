<?php

class Dbiz_Levelrule_Block_Adminhtml_Levelrule_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
		$_model = Mage::registry('levelrule_data');

		$form = new Varien_Data_Form();
		$this->setForm($form);

		$fieldset = $form->addFieldset('levelrule_form', array('legend' => Mage::helper('levelrule')->__('General Information')));

		$fieldset->addField('customer_group_from', 'select', array(
			'label' => Mage::helper('levelrule')->__('From'),
			'title' => Mage::helper('levelrule')->__('From'),
			'name' => 'customer_group_from',
			'value' => $_model->getCustomerGroupFrom(),
			'values' => Mage::getSingleton('levelrule/levelrule')->getCustomerGroupList(),
			'required'	=> true
		));

		$fieldset->addField('customer_group_to', 'select', array(
			'label' => Mage::helper('levelrule')->__('To'),
			'title' => Mage::helper('levelrule')->__('To'),
			'name' => 'customer_group_to',
			'value' => $_model->getCustomerGroupTo(),
			'values' => Mage::getSingleton('levelrule/levelrule')->getCustomerGroupList(),
			'required'	=> true
		));
		
		$fieldset->addField('points', 'text', array(
			'label' => Mage::helper('levelrule')->__('Points'),
			'title' => Mage::helper('levelrule')->__('Points'),
			'name' => 'points',
			'value' => $_model->getPoints(),
			'required'	=> true
		));

		$fieldset->addField('published', 'select', array(
			'label' => Mage::helper('levelrule')->__('Publsihed'),
			'title' => Mage::helper('levelrule')->__('Publsihed'),
			'name' => 'published',
			'value' => $_model->getPublished(),
			'options' => array(
				1 => 'Enable',
				0 => 'Disable'
			)
		));

		return parent::_prepareForm();
	}

}
