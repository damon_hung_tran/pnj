<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 */

/**
 * Levelrule grid
 */
class Dbiz_Levelrule_Block_Adminhtml_Levelrule_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
	}

	protected function _prepareCollection() {
		$collection = Mage::getModel('levelrule/levelrule')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('id', array(
			'header' => Mage::helper('levelrule')->__('ID'),
			'align' => 'right',
			'width' => '50px',
			'index' => 'id',
		));
		$this->addColumn('customer_group_from', array(
			'header' => Mage::helper('levelrule')->__('From'),
			'index' => 'customer_group_from',
			'renderer'	=> 'Dbiz_Levelrule_Block_Adminhtml_Levelrule_Renderer_CustomerGroupFromName'
		));
		$this->addColumn('customer_group_to', array(
			'header' => Mage::helper('levelrule')->__('To'),
			'index' => 'customer_group_to',
			'renderer'	=> 'Dbiz_Levelrule_Block_Adminhtml_Levelrule_Renderer_CustomerGroupFromName'
		));
		$this->addColumn('points', array(
			'header' => Mage::helper('levelrule')->__('Points'),
			'index' => 'points',
		));
		$this->addColumn('published', array(
			'header' => Mage::helper('levelrule')->__('Published'),
			'index' => 'published',
			'type' => 'options',
			'options' => array(
				1 => Mage::helper('levelrule')->__('Enabled'),
				0 => Mage::helper('levelrule')->__('Disabled'),
			)
		));
		$this->addColumn('action', array(
			'header' => Mage::helper('levelrule')->__('Action'),
			'width' => '100',
			'type' => 'action',
			'getter' => 'getId',
			'actions' => array(
				array(
					'caption' => Mage::helper('levelrule')->__('Edit'),
					'url' => array('base' => '*/*/edit'),
					'field' => 'id'
				),
				array(
					'caption' => Mage::helper('levelrule')->__('Delete'),
					'url' => array('base' => '*/*/delete'),
                    'confirm'  => Mage::helper('levelrule')->__('Are you sure?'),
					'field' => 'id'
				)
			),
			'filter' => false,
			'sortable' => false,
			'index' => 'levelrules',
			'is_system' => true,
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

}
