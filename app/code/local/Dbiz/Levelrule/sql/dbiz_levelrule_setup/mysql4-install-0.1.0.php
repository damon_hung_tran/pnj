<?php

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE `dbiz_levelrules` (
  `id` int(11) NOT NULL auto_increment,
  `customer_group_from` int(11) default NULL,
  `customer_group_to` int(11) default NULL,
  `points` int(11) default NULL,
  `published` tinyint(4) default NULL,
  PRIMARY KEY  (`id`),
  KEY `customer_group_from` (`customer_group_from`),
  KEY `published` (`published`)
)
COLLATE='utf8_unicode_ci';
");

$installer->endSetup();