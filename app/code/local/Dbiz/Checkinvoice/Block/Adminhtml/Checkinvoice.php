<?php

class Dbiz_Checkinvoice_Block_Adminhtml_Checkinvoice extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        parent::__construct();
       $this->setTemplate('checkinvoice/invoice.phtml');
    }
    
    public function getOrder(){
        return Mage::registry('current_order');
    }
    
    public function getIsInvoice(){
        return $this->getOrder()->getCheckinvoice();
    }
    
    public function getCompanyAddress(){
        return $this->getOrder()->getCompanyaddress();
    }
    
    public function getCustomertaxvat(){
        return $this->getOrder()->getCustomer_taxvat();
    }
    
    public function getCompanyname(){
         return $this->getOrder()->getBillingAddress()->getData('company');
    }
}