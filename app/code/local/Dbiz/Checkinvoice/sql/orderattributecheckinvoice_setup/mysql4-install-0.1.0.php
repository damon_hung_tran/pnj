<?php
$this->startSetup();
$this->addAttribute('order', 'checkinvoice', array(
    'type'          => 'int',
    'label'         => 'Check Invoice',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'user_defined'  =>  true,
));
$this->addAttribute('order', 'companyaddress', array(
    'type'          => 'varchar',
    'label'         => 'Company address',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'user_defined'  =>  true,
));
 
$this->endSetup();

/*$installer = $this;
$installer->startSetup();
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->addAttribute('order', 'checkinvoice', array(
    'position'      => 1,
    'input'         => 'text',
    'type'          => 'int',
    'label'         => 'Invoice',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => 1,
    'visible_on_front'  => 1,
    'grid'  => true,
));

$installer->endSetup();*/