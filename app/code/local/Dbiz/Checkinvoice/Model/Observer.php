<?php
    class Dbiz_Checkinvoice_Model_Observer
    {

        public function Checkinvoice(Varien_Event_Observer $observer)
        {
            $checkInvoice   = Mage::app()->getRequest()->getPost('checkinvoice');
            $companyAddress = Mage::app()->getRequest()->getPost('companyaddress');
            
            if($checkInvoice){
                $checkInvoice = 1;    
            }else{
                $checkInvoice = 0;
            }
            
            //get event data
            $event = $observer->getEvent();
            //get order
            $order = $event->getOrder();   
            //set the company_address here
            $order->setCompanyaddress($companyAddress);
            //set the checkinvoice here
            $order->setCheckinvoice($checkInvoice);
        }
    }
