<?php
$installer = $this;

$installer->startSetup();

$folders_needed=array('var/export'=>0777, 'var/import'=>0777, 'media/import'=>0777);
foreach($folders_needed as $path => $permission){
	if (!is_dir($path)) {//check if the folder exists and create it if it's needed
		mkdir($path);
		chmod($path,$permission);
	}
}

$installer->run("
INSERT INTO dataflow_profile SET
	name='Export all categories',
	actions_xml='<action type=\"catalog/convert_adapter_category\" method=\"export\">\n<var name=\"root_category_id\"><![CDATA[1]]></var>\n</action>',
	created_at=NOW(),
	updated_at=NOW();

INSERT INTO dataflow_profile SET
	name='Import all categories',
	actions_xml='<action type=\"catalog/convert_adapter_category\" method=\"import\">\n<var name=\"import_file\"><![CDATA[var/import/categories.csv]]></var></action>',
	created_at=NOW(),
	updated_at=NOW();");
$installer->endSetup(); ?>
