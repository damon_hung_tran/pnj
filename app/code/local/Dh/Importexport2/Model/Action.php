<?php 
class Dh_Importexport2_Model_Action extends Mage_Dataflow_Model_Convert_Action_Abstract {
	protected $SKUS;
    protected $SKUS_to_check;
    protected $attributes;
    protected $values;
    protected $warnings;
    
    public function validate(){
    	//----------- Config -----------//
    	$required_fields=array('short_description', 'attribute_set', 'type', 'category_path', 'name', 'sku', 'status', 'price', 'weight', 'description');
    	$integer_fields=array('capacity', 'number', 'qty', 'link_category_id');
    	$float_fields=array('price', 'special_price', 'cost', 'weight', 'quantity');
    	$option_fields=array('cartridge_type', 'color');
    	$default_website=array();
    	foreach(explode(',', Mage::getStoreConfig('import/default_values/import_websites_default')) as $website_id){
    		$default_website[]=Mage::app()->getWebsite($website_id)->getCode();
    	}
    	$default_website=implode(' , ', $default_website);
    	//------------------------------//
    	echo '<li id="validation"><p><img src="/skin/adminhtml/default/default/images/note_msg_icon.gif" alt="note" />Wait during datas validation</p>';
    	try{
	    	$filename='var/import/'.$this->getRequest()->getParam("files");
	    	$file=fopen($filename, 'r');
	    	$write_file=fopen($filename.'.generated.csv', 'w');
	    	$this->getRequest()->setParam("files", $this->getRequest()->getParam("files").'.generated.csv');
	    	if($file && $write_file){
	    		$this->attributes=fgetcsv($file);
	    		if(count($this->attributes)){
	    			if(count(array_diff($required_fields, $this->attributes))==0){
	    				$row_count=2;
	    				try {
	    					$this->SKUS=array();
	    					$this->SKUS_to_check=array();
	    					$attribute_tax=Mage::getModel('catalog/product')->getResource()->getAttribute('tax_class_id')->getSource()->getOptionText(Mage::getStoreConfig('import/default_values/import_tax_default'));
	    					$attribute_layout=Mage::getModel('catalog/product')->getResource()->getAttribute('page_layout')->getSource()->getOptionText(Mage::getStoreConfig('import/default_values/import_page_layout_default'));
			    			while($this->values=fgetcsv($file)){
			    				$this->setAttr('tax_class_id', $attribute_tax);
			    				$this->setAttr('page_layout', $attribute_layout);
			    				if($this->getAttributeByName('websites')==NULL){
			    					$this->setAttr('websites', $default_website);
			    				}
			    				if($this->getAttributeByName('cartridge_type')=='Original' && $this->getAttributeByName('visibility')!='Search'){
    								$this->setAttr('visibility', 'Search');
    							} else {
    								$this->setAttr('visibility', 'Catalog, Search');
    							}
			    				
			    				for($i=0;$i<count($this->attributes);$i++){
			    					try{
			    						$this->values[$i]=trim($this->values[$i]);
				    					if(in_array($this->attributes[$i], $required_fields)){
				    						if($this->values[$i]!=0 && empty($this->values[$i])){
				    							throw new Exception('is empty.');
				    						}
				    					}
				    					if(!empty($this->values[$i]) && (in_array($this->attributes[$i], $integer_fields) || in_array($this->attributes[$i], $float_fields))){
				    						if(is_numeric($this->values[$i])){
				    							if(in_array($this->attributes[$i], $integer_fields) && $this->values[$i]!=0  && ($this->values[$i]-intval($this->values[$i]))!=0){
				    								throw new Exception('is not integer.');
				    							}
				    						} else {
				    							throw new Exception('is not a number.');
				    						}
				    					} else if(in_array($this->attributes[$i], $option_fields)){
				    						$attribute=Mage::getModel('catalog/product')->getResource()->getAttribute($this->attributes[$i]);
	                          				$options = $attribute->getSource()->getAllOptions(true, true);
	                          				$option_validated=false;
	                          				$allowedValues=array();
	                          				$allowedValuesDone=false;
	                          				foreach(explode(' , ', $this->values[$i]) as $value){
		                          				foreach($options as $option){
		                          					if(!$allowedValuesDone){
		                          						$allowedValues[]=$option['label'];
		                          					}
		                          					if($value==$option['label']){
		                          						$option_validated=true;
		                          						break;
		                          					}
		                          				}
		                          				$allowedValuesDone=true;
		                          				if(!$option_validated){
		                          					throw new Exception ('contains an incorrect value. (Should be one of those values : '.implode(',', $allowedValues).')');
		                          				}
	                          				}
				    					}
				    					switch ($this->attributes[$i]) {
				    						case 'websites':
				    							$validate=false;
				    							$allowedValues=array();
				    							foreach(Mage::app()->getWebsites()as $website){
				    								$allowedValues[]=$website->getCode();
				    							}
				    							if(!empty($this->values[$i])){
					    							foreach(explode(' , ', $this->values[$i]) as $_website){
						    							if(in_array($_website, $allowedValues)){
						    								$validate=true;break;
						    							}
					    							}
				    							}
				    							if(!$validate){
				    								$this->values[$i]=$default_website;
				    								throw new Exception('Warning : is empty or not valid. (Should be one or several of those values : '.implode(',', $allowedValues).'). It will be replaced by default value : '.$default_website);
				    							}
				    						
				    						break;
				    						
				    						case 'attribute_set':
					    						$attributesets=Mage::getModel('catalog/product_attribute_set_api')->items();
				    							$validate=false;
				    							$allowedValues=array();
				    							foreach($attributesets as $attributeset){
				    								if($this->values[$i]==$attributeset['name']){
				    									$validate=true;break;
				    								}
				    								$allowedValues[]=$attributeset['name'];
				    							}
				    							if(!$validate){
				    								throw new Exception('is not valid. (Should be one of those values : '.implode(',', $allowedValues).')');
				    							}
				    						break;
				    						
				    						case 'type':
				    							$types=Mage::getModel('catalog/product_type_api')->items();
				    							$validate=false;
				    							$allowedValues=array();
				    							foreach($types as $type){
				    								
				    								if($this->values[$i]==$type['type']){
				    									$validate=true;break;
				    								}
				    								$allowedValues[]=$type['type'];
				    							}
				    							if(!$validate){
				    								throw new Exception('is not valid. (Should be one of those values : '.implode(',', $allowedValues).')');
				    							}
				    						break;
				    						
				    						case 'status':
				    							$allowedValues=array('Enabled', 'Disabled');
				    							if(!in_array($this->values[$i], $allowedValues)){
				    								throw new Exception('is not valid. (Should be one of those values : '.implode(',', $allowedValues).')');
				    							}
				    						break;
				    						
				    						case 'category_path':
				    							$validate=true;
				    							$wrong_paths=array();
				    							if(in_array($this->getAttributeByName('attribute_set'), array('Cartridge', 'Toner'))){
				    								$paths=explode(',', $this->values[$i]);
				    								if(count($paths)){
				    									foreach($paths as $path){
							    							if(count(explode('|', $path))!= 5){
							    								$validate=false;
							    								$wrong_paths[]='("'.$path.'" should be level 5)';
							    							}
				    									}
				    								} else {
				    									throw new Exception('Warning : is empty.');
				    								}
				    							} else {//Printer and Paper
				    								$paths=explode(',', $this->values[$i]);
				    								if(count($paths)){
				    									foreach($paths as $path){
						    								if(count(explode('|', $path))!= 2){
						    									$validate=false;
							    								$wrong_paths[]='("'.$path.'" should be level 2)';
							    							}
				    									}
				    								} else {
				    									throw new Exception('Warning : is empty.');
				    								}
				    							}
				    							if(!$validate){
				    								throw new Exception('Warning : is not valid. '.implode(' ', $wrong_paths));
				    							}
				    						break;
				    						
				    						case 'sku':
				    							if(in_array($this->values[$i], $this->SKUS)){
				    								throw new Exception('is duplicated.');
				    							} else {
				    								$this->SKUS[]=$this->values[$i];
				    							}
				    						break;
				    						
				    						case 'image':
				    						case 'thumbnail':
				    						case 'small_image':
				    							if(!empty($this->values[$i]) && !file_exists('media/import/'.$this->values[$i])){
				    								throw new Exception('Warning : doesn\'t exists.');
				    							}
				    						break;
				    						
				    						case 'related_compatible_sku':
				    							if($this->getAttributeByName('cartridge_type')=='Original' && empty($this->values[$i])){
				    								throw new Exception('Warning : is empty.');
				    							}
				    						case 'related_products_sku':
				    						case 'upsell_products_sku':
				    						case 'crosssell_products_sku':
				    							if(!empty($this->values[$i])){
					    							if(count(explode(',', $this->values[$i]))>1){
					    								foreach(explode(',', $this->values[$i]) as $_sku){
					    									$this->checkSkuExists($_sku);
					    								}
					    							} else {
					    								$this->checkSkuExists($this->values[$i]);
					    							}
				    							}
				    						break;
				    						
				    						case 'link_category_id':
				    							if(!empty($this->values[$i]) && $this->getAttributeByName('attribute_set')=='Printer'){
				    								if(!Mage::getModel('catalog/category')->load($this->values[$i])){
				    									throw new Exception('Warning : doesn\'t exist');
				    								}
				    							}
				    						break;
				    						
				    						//---------- Attributes required for cartridges/toners only ----------//
				    						case 'short_name':
				    						case 'cartridge_type':
				    							if(in_array($this->getAttributeByName('attribute_set'), array('Cartridge', 'Toner')) && empty($this->values[$i])){
				    								throw new Exception('is empty.');
				    							}
				    						break;
				    					}
			    					} catch(Exception $e){
					    				if(strpos($e->getMessage(), 'Warning : ')===0){
				    						$this->warning('Warning ! Row '.$row_count.' : field "'.$this->attributes[$i].'" '.substr($e->getMessage(), 10));
				    					} else {
				    						$this->error('Row '.$row_count.' : field "'.$this->attributes[$i].'" '.$e->getMessage());
				    						continue;
				    					}
				    				}
			    				}
			    				
			    				if($row_count==2){
			    					fputcsv($write_file, $this->attributes);
			    				}
			    				
			    				$row_count++;
			    				fputcsv($write_file, $this->values);
			    			}
	    				} catch (Exception $e){
			    			$this->error('Row '.$row_count.' : field "'.$this->attributes[$i].'" '.$e->getMessage());
			    		}
			    		
			    		foreach(Mage::getModel('catalog/product')->getCollection() as $product){
			    			$this->SKUS[]=$product->getSku();
			    		}

	    				if(count(array_diff($this->SKUS_to_check, $this->SKUS))!=0){
		    				$this->warning('Warning ! Some linked SKUS doesn\'t exists : '.implode(', ', array_diff($this->SKUS_to_check, $this->SKUS)));
		    			}
		    			
	    			} else {
	    				throw new Exception('Missing required fields in '.$filename.' ('.implode(',', array_diff($required_fields, $this->attributes)).')');
	    			}
	    		} else {
	    			throw new Exception('No datas found in '.$filename);
	    		}
	    	} else {
	    		throw new Exception($filename.' file is not readable.');
	    	}

	    	if(count($this->warnings)){
	    		echo '<img src="/skin/adminhtml/default/default/images/fam_bullet_error.gif" alt="warning" />'.implode('<br/><img src="/skin/adminhtml/default/default/images/fam_bullet_error.gif" alt="warning" />', $this->warnings);
	    		echo '<br/>';
	    	} 
	    	if(count($this->errors)){
	    		echo '<img src="/skin/adminhtml/default/default/images/error_msg_icon.gif" alt="error" /><span style="color:red;">';
	    		echo implode('</span><br/><img src="/skin/adminhtml/default/default/images/error_msg_icon.gif" alt="error" /><span style="color:red;">', $this->errors);
	    		echo '</span>';
	    	}
	    	else {
	    		if(count($this->warnings)){
	    			echo '<p><img src="/skin/adminhtml/default/default/images/fam_bullet_error.gif" alt="warning" /><img src="/skin/adminhtml/default/default/images/fam_bullet_error.gif" alt="warning" /><img src="/skin/adminhtml/default/default/images/fam_bullet_error.gif" alt="warning" />Some warnings have been generated. You should try to fix it but you can ignore it if you want : <input type="button" id="validateButton" value="Yes, import datas anyway" /></p>';
	    		} else {
    				echo '<p id="validated"><img src="/skin/adminhtml/default/default/images/fam_bullet_success.gif" alt="success" />Validated !</p>';
	    		}
	    	}
    	} catch(Exception $e){
    		echo '<img src="/skin/adminhtml/default/default/images/error_msg_icon.gif" alt="error" /><span style="color:red;">Fatal error : ';
    		echo $e->getMessage();
    		echo '</span></li>';
    		exit();
    	}
    	echo '</li>';
    }
    public function setAction($action=null){
    	
    }
	public function addException($action=null){
    	
    }
    public function getRequest(){
    	return Mage::app()->getRequest();
    }
    protected function setAttr($code, $value){
    	$key=array_search($code, $this->attributes);
    	if($key!==false){
    		$this->values[$key]=$value;
    	} else {
    		$this->attributes[]=$code;
    		$this->values[]=$value;
    	}
    }
    protected function getAttributeByName($attr){
    	$key=array_search($attr, $this->attributes);
    	if($key!==false){
    		return $this->values[$key];
    	} else {
    		return null;
    	}
    }
    
    protected function checkSkuExists($sku){
    	if(in_array($sku, $this->SKUS)){
    		return true;
    	} else {
    		$this->SKUS_to_check[]=$sku;
    		return false;
    	}
    }
    
    protected function warning($msg){
    	$this->warnings[]=$msg;
    }
    protected $errors;
	protected function error($msg){
    	$this->errors[]=$msg;
    }
}
?>
