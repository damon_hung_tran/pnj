<?php
define('ARRAY_MARK', '#!ARRAY!#');
class Dh_Importexport2_Model_Adapter_Category
    extends Mage_Eav_Model_Convert_Adapter_Entity
{
	/**
     *
     * "Error report" related vars
     * @var mixed
     */
	protected $_date;
	protected $_report_error_file=null;
	protected $_report_error_file_name;
	protected $_id_error=0;
	/**
	 *
	 * Constructor
	 */
	public function __construct(){
		$this->_date=date('Y-m-d-H-i-s');
		$this->_report_error_file_name='var/report/report_categoryimportexport2_'.$this->_date;
	}
	/**
	 *
	 * Error handler (log into a report file AND send it to client using $this->addException method)
	 * @param string $msg error message
	 * @param string $status error status (use Mage_Dataflow_Model_Convert_Exception const)
	 */
    protected function error_report($msg, $status=Mage_Dataflow_Model_Convert_Exception::WARNING){
    	if($this->_report_error_file===null){

    		$this->_report_error_file=fopen($this->_report_error_file_name, 'a');
    		chmod($this->_report_error_file_name,0777);
    	}
    	if($this->_report_error_file){
    		$this->addException('Error '.$this->_id_error.' : '.$msg, $status);
    		fwrite($this->_report_error_file, 'Error '.$this->_id_error.' '.$status.' : '.$msg."\n");
    		$this->_id_error++;
    	}
    }
    /**
     *
     * Send the error count to client using $this->addException method
     */
    protected function error_notice(){
    	if($this->_id_error > 0){
    		$this->addException($this->_id_error.' error(s) reported in '.$this->_report_error_file_name,
    		Mage_Dataflow_Model_Convert_Exception::FATAL);
    	} else {
    		$this->addException('0 error reported !');
    	}
    }
    /**
     *
     * Parse the import file, create a temp files with all the parsed datas and send the javascript to client for the other part of the import (see controllers/GetController.php indexAction())
     */
    public function import()
    {
    	$import_file=fopen($this->getVar('import_file'),"r");
    	if($import_file){
    		$this->addException($this->getVar('import_file').' loaded.');
    		$attributes=fgetcsv($import_file);
    		$line_data=fgetcsv($import_file);
    		$cats_datas=array();
    		$count=0;
    		
    		$attributesNot2Process= array('entity_id', 'path', 'parent_id', 'increment_id');
    		
    		while($line_data!==FALSE && count($line_data)>1){
    			$attribute_index=0;
	    			$cat_data=array();
	    			unset($_parent_id);
	    			foreach($attributes as $attribute){
	    				if(strlen(trim($line_data[$attribute_index])) && !in_array($attribute, $attributesNot2Process)){
	    					if($attribute=='image' || $attribute=='thumbnail'){

	    						$media_filename_src='media/import/'.$line_data[$attribute_index];
	    						$media_filename_dest='media/catalog/category/'.$line_data[$attribute_index];

	    						if(file_exists($media_filename_src) && !file_exists($media_filename_dest)){
	    							rename($media_filename_src, $media_filename_dest);
	    						}

	    						if (!file_exists($media_filename_dest)) {
	    							$this->error_report($media_filename_src.' and '.$media_filename_dest.' don\'t exist. The image can\'t be imported.');
	    						} else {
	    							$cat_data[$attribute]= $line_data[$attribute_index];
	    						}

	    					} else {
	    						if(strpos($line_data[$attribute_index], ARRAY_MARK)===0){
    								$cat_data[$attribute]= explode(',', substr($line_data[$attribute_index], strlen(ARRAY_MARK)));
    							} else {
    								$cat_data[$attribute]= $line_data[$attribute_index];
    							}
	    					}
	    				}
	    				$attribute_index++;
	    			}
	    			$cats_datas[]=$cat_data;
	    			$line_data=fgetcsv($import_file);
	    			$count++;
    		}
    		$inf = fopen('var/import/tmp.data',"w");
	        fwrite($inf,serialize($cats_datas));
	        chmod($file,0777);

			?>
			<script type="text/javascript" src="/skin/adminhtml/default/default/js/jquery-1.6.2.min.js"></script>
			<script type="text/javascript">
			function number_format (number, decimals, dec_point, thousands_sep) {
			    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
			    var n = !isFinite(+number) ? 0 : +number,
			        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			        s = '',
			        toFixedFix = function (n, prec) {
			            var k = Math.pow(10, prec);
			            return '' + Math.round(n * k) / k;
			        };
			    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
			    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
			    if (s[0].length > 3) {
			        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
			    }
			    if ((s[1] || '').length < prec) {
			        s[1] = s[1] || '';
			        s[1] += new Array(prec - s[1].length + 1).join('0');
			    }
			    return s.join(dec);
			}
			$jQuery(document).ready(function(){
				function progressMessage(msg){
					$jQuery('#liFinished').html('<img src="/skin/adminhtml/default/default/images/small-ajax-loader.gif" class="v-middle" style="margin-right:5px">'+msg);
				}
				var errorCount=<?php echo $this->_id_error;?>;
				function processDataCall(count){
					$jQuery.post('/importexport2/get/index', {id: count, importId:<?php echo $this->_date;?>}, function(data){
						progressMessage((count+1)+'/<?php echo $count?> categories processed. ('+number_format(100*(count+1)/<?php echo $count?>, 1)+'%).');
						if(data.state=='fatal_error'){
							for(var i=0;i<data.errors.length;i++){
								errorCount++;
								$jQuery('<li style="background-color:#FBB; "><img src="/skin/adminhtml/default/default/images/error_msg_icon.gif" class="v-middle">'+data.errors[i]+'</li>').insertBefore('#liFinished');
							}
						}
						count++;
						if(count<<?php echo $count;?>){
							processDataCall(count);
						} else {
							$jQuery('#liFinished').html('<img src="/skin/adminhtml/default/default/images/note_msg_icon.gif" class="v-middle" style="margin-right:5px"><?php echo $count?> categories processed. Import complete.');
							if(errorCount>0){
								$jQuery('<li style="background-color:#FBB; "><img src="/skin/adminhtml/default/default/images/error_msg_icon.gif" class="v-middle">'+errorCount+' error(s) reported in <?php echo $this->_report_error_file_name;?></li>').insertAfter('#liFinished');
							}
							alert('Import complete!');
						}
					}, 'json');
				}
				progressMessage('Starting import...');
				processDataCall(0);
			});

			</script>
			<?php
	        $this->addException($this->getVar('import_file').' parsed.');
    	} else {
    		$this->addException($this->getVar('import_file').' or root category can\'t be loaded. Import aborted.',
    		 Mage_Dataflow_Model_Convert_Exception::FATAL);
    	}
    }



    /**
     *
     * Do the whole export process
     */
    public function export()
    {
    	if($this->getVar('root_category_id')){
			$rootCategory=$this->getVar('root_category_id');//get the root category from xml profile
		} else {
			$rootCategory=1;//default value
		}
		$this->addException('Loading root category (ID : '.$rootCategory.')');
	 	$rootCategory=Mage::getModel('catalog/category')->load($rootCategory);
		if ($rootCategory) {
			try{
				$mode = 777;
				$np = "0".$mode;
				if (!is_dir("var/export/")) {//check if the destination folder exists and create it if it's needed
					mkdir("var/export");
					chmod("var/export",eval("return({$np});"));
				}
			} catch(Exception $e){
				$this->error_report('Error during the destination folder creation : '.$e->getMessage(), Mage_Dataflow_Model_Convert_Exception::FATAL);
				break;
			}
			try{
				$file = 'var/export/categories-'.$this->_date.'.csv';
				$file_content='"Path"';
				$attributes=array();
				foreach ($rootCategory->getAttributes() as $attribute) {//get all the attributes code for category and write the first line of the csv file
					$attributes[]=$attribute->getAttributeCode();
					$file_content.=',"'.$attribute->getAttributeCode().'"';
				}
				$file_content.="\n";
				$collection=Mage::getModel('catalog/category')->getCollection()->addAttributeToFilter('path', array('like'=> $rootCategory->getPath().'/%'));
				//echo $rootCategory->getPath().'% ; '.$collection->count();
				$categories_to_process_ids=$collection->getAllIds();
				array_unshift($categories_to_process_ids,$rootCategory->getId());
				$i=0;
			} catch(Exception $e){
				$this->error_report('Error during the loading of the root category\'s children : '.$e->getMessage(), Mage_Dataflow_Model_Convert_Exception::FATAL);
				break;
			}
			foreach($categories_to_process_ids as $idCat){
				try{
					// initialisation of some vars
					$var=array();
					unset($pathIds);
					unset($path);
					unset($category);
					// loading category
					$category=Mage::getModel('catalog/category')->load($idCat);

					$pathIds=$category->getPathIds();//getting the path ids and converting it to path names
					unset($pathIds[0]);//we don't want the level 0 in the path
					//unset($pathIds[1]);
					if(!count($pathIds)){
						continue;
					}
					foreach($pathIds as $pathId){
						$path[]=Mage::getModel('catalog/category')->load($pathId)->getName();
					}
					$path=implode('|', $path);

					$file_content.='"'.$path.'"';

					foreach($attributes as $attribute){//writing the content of the file
						$data=$category->getData($attribute);
						$file_content.=',"';
						if(is_array($data)){//array datas are marked and imploded
							$file_content.=ARRAY_MARK.implode(',', $data);
						} else {
							$file_content.=$data;
						}
						$file_content.='"';
					}
					$file_content.="\n";
					$i++;
				} catch (Exception $e){
					$this->error_report('Category '.$idCat.' : '.$e->getMessage(), Mage_Dataflow_Model_Convert_Exception::FATAL);
				}
			}
			$inf = fopen($file,"w");
	        fwrite($inf,$file_content);
	        chmod($file,0777);
	        fclose($inf);
	        if($i>0){
	        	$this->addException('Export complete. '.$i.' categories exported in <a href="/'.$file.'">'.$file.'</a>');
	        } else {
	        	$this->error_report('Export failed.', Mage_Dataflow_Model_Convert_Exception::FATAL) ;
	        }
		} else {
			exit('Fatal error : No root cateogry found.');
		}
		$this->error_notice();
        return $this;
    }
}

?>
