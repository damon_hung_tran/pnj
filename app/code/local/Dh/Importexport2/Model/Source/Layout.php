<?php 
class Dh_Importexport2_Model_Source_Layout {
	
	protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options = Mage::getModel('catalog/product_attribute_source_layout')->getAllOptions();
        }
        return $this->_options;
    }
    
    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value)
    {
        $options = $this->toOptionArray();

        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }
}
?>