<?php
class Dh_Importexport2_GetController extends Mage_Core_Controller_Front_Action {
	protected $_report_error_file=null;
	protected $_report_error_file_name;
	protected $_return;
	protected function error_report($msg){
    	if($this->_report_error_file===null){
    		$this->_report_error_file_name='var/report/report_categoryimportexport2_'.$this->getRequest()->getParam("importId");
    		$this->_report_error_file=fopen($this->_report_error_file_name, 'a');
    		chmod($this->_report_error_file_name,0777);
    	}
    	if($this->_report_error_file){
    		$this->_return['state']='fatal_error';
	        $this->_return['errors'][]=$msg;
    		fwrite($this->_report_error_file, 'Error : '.$msg."\n");
    	}
    }
    /**
     *
     * Category import action
     */
    public function indexAction () {
    	Mage::app()->setCurrentStore(0);
        if($this->getRequest()->getParam("id")!==NULL){
        	$id=$this->getRequest()->getParam("id");
	        $file_content = file_get_contents('var/import/tmp.data');
	        if($file_content){
	        	$datas=unserialize($file_content);
	        	if(is_array($datas) && isset($datas[$id])){
	        		try {
	        			$new_cat=NULL;
	        			if(isset($datas[$id]['Path'])){
	        				$cat_names=explode('|', $datas[$id]['Path']);
	        				unset($datas[$id]['Path']);
	        				$level=1;
							$parent_id=null;
							$current_path_ids='1';
							foreach($cat_names as $cat_name){
								if($parent_id){$current_path_ids.='/'.$parent_id;}//we keep the current path_ids in order to be able to create the missing categories if needed (see below)
								
								//selection of the category by its name, level, and parent_id (if it's not the first one (level 1))
							    $categories=Mage::getModel('catalog/category')->getCollection();
							    if($cat_name!='Default Category' || $level>1){
							    	$categories->addAttributeToFilter('name', $cat_name);
							    }
							    $categories->addAttributeToFilter('level', $level);
							    if($parent_id){
							    	$categories->addAttributeToFilter('path', array('like' => $current_path_ids.'%'));
							    }
							    if($categories->count()>=1){//we found our category
							    	if($level>=count($cat_names)){
							    		$new_cat=$categories->getFirstItem();
							    	} else {
							    		$parent_id=$categories->getFirstItem()->getId();
							    	}
							    } else {
							    	//creation of the missing category
							    	$cat= Mage::getModel('catalog/category');
							    	$datas2=array('name'=>$cat_name, 'attribute_set_id'=> 3, 'is_active'=>1, 'include_in_menu' => 0, 'parent_id' => $parent_id);
							    	$cat->setData($datas2);
							    	$cat->save();
							    	//we set the path with the current path_ids
							    	$cat->setPath($current_path_ids.'/'.$cat->getId());
							    	$cat->setLevel($level);
							    	$cat->save();
							    	if($level>=count($cat_names)){
							    		$new_cat=$cat;
							    	} else {
							    		$parent_id=$cat->getId();
							    	}
							    }
							    $level++;
							}
	        			}
	        			if($new_cat==NULL){
	        				$new_cat= Mage::getModel('catalog/category');
	        				$new_cat->setData($datas[$id]);
		        			$this->_return['state']='success';
		        			$new_cat->save();
		        			if($current_path_ids){
		        				$new_cat->setPath($current_path_ids.'/'.$new_cat->getId());
		        				$new_cat->save();
		        			}
	        			} else {
	        				$datas[$id]['entity_id']=$new_cat->getEntityId();
	        				$new_cat->setData($datas[$id]);
		        			$this->_return['state']='success';
		        			$new_cat->save();
	        			}
	        			
	        			$this->_return['saved_id']=$new_cat->getId();
	        			
	        		} catch (Exception $e) {
	        			$this->error_report('Category '.$id.' : '.$e->getMessage());
	        		}
	        	} else {
	        		$this->error_report('Temp file corrupted.');
	        	}
	        } else {
	        	$this->error_report('Temp file can\'t be loaded.');
	        }
        } else {
        	$this->error_report('No id received.');
        }

        echo json_encode($this->_return);
    }
    /**
     *
     * arrays of saved products indexed by sku
     * @var array(string=>Mage_Catalog_Model_Product)
     */
    protected $_productsToSave=array();
    protected $_productsLoaded=array();
    /**
     *
     * Load a product by its sku. If the product has already been loaded, it is not reloaded.
     * @param string $sku product's sku
     * @return Mage_Catalog_Model_Product $product
     */
    protected function loadBySku($sku){
    	if(!array_key_exists($sku, $this->_productsLoaded)){
    		$this->_productsLoaded[$sku]=Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
    	}
    	return $this->_productsLoaded[$sku];
    }
    /**
     *
     * Action for linking products to their related, upsell, and crosssell products using the temp files created in the first part of the products import
     */
    public function linkAction(){
    	$errors=array();
    	//related products
    	$links=explode("\n", file_get_contents('var/import/links_related.data'));
    	unset($links[count($links)-1]);
    	foreach($links as $link){
    		$link_array=array();
    		$skus=explode(',', $link);//we get every skus
    		$product=$this->loadBySku($skus[0]);//we load the first product which is the source part of the link
    		if($product){
				unset($skus[0]);//we remove this product from the array
				foreach($skus as $sku){//we load the destination products of the link and get their ids
					if($_product=$this->loadBySku($sku)){
						$link_array[$_product->getId()]=array('position'=> 0);
					} else {
						$errors[]='Product "'.$sku.'" is missing.';
					}
				}
				$product->setRelatedLinkData($link_array);//we set the link
				$this->_productsToSave[$product->getSku()]=$product;//we will save the products at the end, so we won't save two times the same product
			} else {
				$errors[]='"Related products" linking for product '.$skus[0].' failed. Product is missing.';
			}
    	}
    	unlink('var/import/links_related.data');//delete the file

    	//cross-sell products (same thing as related products)
    	$links=explode("\n", file_get_contents('var/import/links_crosssell.data'));
    	unset($links[count($links)-1]);
    	foreach($links as $link){
    		$link_array=array();
    		$skus=explode(',', $link);
    		$product=$this->loadBySku($skus[0]);
    		if($product){
				unset($skus[0]);
				foreach($skus as $sku){
					if($_product=$this->loadBySku($sku)){
						$link_array[$_product->getId()]=array('position'=> 0);
					} else {
						$errors[]='Product "'.$sku.'" is missing.';
					}
				}
				$product->setCrossSellLinkData($link_array);
				$this->_productsToSave[$product->getSku()]=$product;
			} else {
				$errors[]='"Cross-sell products" linking for product '.$skus[0].' failed. Product is missing.';
			}
    	}
    	unlink('var/import/links_crosssell.data');//delete the file


    	//upsell products (same thing as related products)
    	$links=explode("\n", file_get_contents('var/import/links_upsell.data'));
    	unset($links[count($links)-1]);
    	foreach($links as $link){
    		$link_array=array();
    		$skus=explode(',', $link);
    		$product=$this->loadBySku($skus[0]);
    		if($product){
				unset($skus[0]);
				foreach($skus as $sku){
					if($_product=$this->loadBySku($sku)){
						$link_array[$_product->getId()]=array('position'=> 0);
					} else {
						$errors[]='Product "'.$sku.'" is missing.';
					}
				}
				$product->setUpSellLinkData($link_array);
				$this->_productsToSave[$product->getSku()]=$product;
			} else {
				$errors[]='"Up-sell products" linking for product '.$skus[0].' failed. Product is missing.';
			}
    	}
    	unlink('var/import/links_upsell.data');//delete the file


    	foreach($this->_productsToSave as $product){//we save all the processed products
    		$product->save();
    	}
    	if(count($errors)){
    		echo implode('<br />'."\n", $errors);
    		$reported=false;
    		if(Mage::app()->getRequest()->getParam('starttime')){
    			$error_filename='var/report/product-report-'.Mage::app()->getRequest()->getParam('starttime');
        		$error_file=fopen($error_filename, 'a');
        		if($error_file){
	        		fwrite($error_file, implode("\n", $errors));
	        		$reported=true;
        			fclose($error_file);
        		}
    		}
    		echo '<br/>';
    		echo $reported ? 'Errors have been logged in '.$error_filename.' .' : 'Impossible to log the errors.';
    	} else {
    		echo '1';
    	}
    }
    
    public function batchrunAction(){
    	if ($this->getRequest()->isPost()) {
    		Mage::app()->setCurrentStore(0);
            $batchId = $this->getRequest()->getPost('batch_id',0);
            $rowIds  = explode(',', $this->getRequest()->getPost('rows'));

            /* @var $batchModel Mage_Dataflow_Model_Batch */
            $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

            if (!$batchModel->getId()) {
                //exit
				return ;
            }
            
            if (!is_array($rowIds) || count($rowIds) < 1) {
                //exit
				return ;
            }
            if (!$batchModel->getAdapter()) {
                //exit
                return ;
            }

            $batchImportModel = $batchModel->getBatchImportModel();
            //var_dump($batchImportModel);
            $importIds = $batchImportModel->getIdCollection();

            $adapter = Mage::getModel($batchModel->getAdapter());
            $adapter->setBatchParams($batchModel->getParams());

            $errors = array();
            $saved  = 0;

            foreach ($rowIds as $importId) {
                $batchImportModel->load($importId);
                if (!$batchImportModel->getId()) {
                    $errors[] = Mage::helper('dataflow')->__('Skip undefined row.');
                    continue;
                }

                try {
                    $importData = $batchImportModel->getBatchData();
                    //var_dump($batchImportModel->getBatchData());
                    $adapter->saveRow($importData);
                }
                catch (Exception $e) {
                    $errors[] = $e->getMessage();
                    continue;
                }
                $saved ++;
            }

            $result = array(
                'savedRows' => $saved,
                'errors'    => $errors
            );
            echo Mage::helper('core')->jsonEncode($result);
        }
    }
    protected $SKUS;
    protected $SKUS_to_check;
    protected $attributes;
    protected $values;
    protected $warnings;
    public function validateAction(){
    	//----------- Config -----------//
    	$required_fields=array('short_description', 'attribute_set', 'type', 'category_path', 'name', 'sku', 'status', 'visibility', 'cartridge_type', 'price', 'weight', 'description');
    	$integer_fields=array('capacity', 'number', 'qty', 'link_category_id');
    	$float_fields=array('price', 'special_price', 'cost', 'weight', 'quantity');
    	$option_fields=array('cartridge_type', 'color');
    	$default_website='ink004';
    	//------------------------------//
    	try{
	    	$filename=$this->getRequest()->getParam("file");
	    	$file=fopen($filename, 'r');
	    	$write_file=fopen($filename.'.generated.csv', 'w');
	    	if($file && $write_file){
	    		$this->attributes=fgetcsv($file);
	    		if(count($this->attributes)){
	    			if(count(array_diff($required_fields, $this->attributes))==0){
	    				$row_count=1;
	    				try {
	    					$this->SKUS=array();
	    					$this->SKUS_to_check=array();
			    			while($this->values=fgetcsv($file)){
			    				$this->setAttr('tax_class_id', 'Taxable Goods');
			    				$this->setAttr('page_layout', 'No layout updates');
			    				if($this->getAttributeByName('cartridge_type')=='Original' && $this->values[$i]!='Search'){
    								$this->setAttr('visibility', 'Search');
    							} else {
    								$this->setAttr('visibility', 'Catalog, Search');
    							}
			    				
			    				for($i=0;$i<count($this->attributes);$i++){
			    					try{
				    					if(in_array($this->attributes[$i], $required_fields)){
				    						if(empty($this->values[$i])){
				    							throw new Exception('is empty.');
				    						}
				    					}
				    					if(in_array($this->attributes[$i], $integer_fields) || in_array($this->attributes[$i], $float_fields)){
				    						if(is_numeric($this->values[$i])){
				    							if(in_array($this->attributes[$i], $integer_fields) && ($this->values[$i]-intval($this->values[$i]))!=0){
				    								throw new Exception('is not integer.');
				    							}
				    						} else {
				    							throw new Exception('is not a number.');
				    						}
				    					} else if(in_array($this->attributes[$i], $option_fields)){
				    						$attribute=Mage::getModel('catalog/product')->getResource()->getAttribute($this->attributes[$i]);
	                          				$options = $attribute->getSource()->getAllOptions(false, true);
	                          				$option_validated=false;
	                          				$allowedValues=array();
	                          				foreach($options as $option){
	                          					$allowedValues[]=$option['label'];
	                          					if($this->values[$i]==$option['label']){
	                          						$option_validated=true;
	                          						break;
	                          					}
	                          				}
	                          				if(!$option_validated){
	                          					var_dump($options);
	                          					throw new Exception ('is not a correct value. (Should be one of those values : '.implode(',', $allowedValues).')');
	                          				}
				    					}
				    					switch ($this->attributes[$i]) {
				    						case 'websites':
				    							$validate=false;
				    							$allowedValues=array();
				    							foreach(Mage::app()->getWebsites()as $website){
				    								$allowedValues[]=$website->getCode();
				    							}
				    							if(!empty($this->values[$i])){
					    							foreach(explode(',', $this->values[$i]) as $_website){
						    							if(in_array($_website, $allowedValues)){
						    								$validate=true;break;
						    							}
					    							}
				    							}
				    							if(!$validate){
				    								$this->values[$i]=$default_website;
				    								throw new Exception('Warning : is empty or not valid. (Should be one or several of those values : '.implode(',', $allowedValues).'). It will be replaced by default value : '.$default_website);
				    							}
				    						
				    						break;
				    						
				    						case 'attribute_set':
					    						$attributesets=Mage::getModel('catalog/product_attribute_set_api')->items();
				    							$validate=false;
				    							$allowedValues=array();
				    							foreach($attributesets as $attributeset){
				    								if($this->values[$i]==$attributeset['name']){
				    									$validate=true;break;
				    								}
				    								$allowedValues[]=$attributeset['name'];
				    							}
				    							if(!$validate){
				    								throw new Exception('is not valid. (Should be one of those values : '.implode(',', $allowedValues).')');
				    							}
				    						break;
				    						
				    						case 'type':
				    							$types=Mage::getModel('catalog/product_type_api')->items();
				    							$validate=false;
				    							$allowedValues=array();
				    							foreach($types as $type){
				    								
				    								if($this->values[$i]==$type['type']){
				    									$validate=true;break;
				    								}
				    								$allowedValues[]=$type['type'];
				    							}
				    							if(!$validate){
				    								throw new Exception('is not valid. (Should be one of those values : '.implode(',', $allowedValues).')');
				    							}
				    						break;
				    						
				    						case 'status':
				    							$allowedValues=array('Enabled', 'Disabled');
				    							if(!in_array($this->values[$i], $allowedValues)){
				    								throw new Exception('is not valid. (Should be one of those values : '.implode(',', $allowedValues).')');
				    							}
				    						break;
				    						
				    						case 'category_path':
				    							if(in_array($this->getAttributeByName('attribute_set'), array('Cartridge', 'Toner'))){
					    							if(count(explode('|', $this->values[$i]))!= 5){
					    								throw new Exception('Warning : is not valid. (Should be level 5)');
					    							}
				    							} else {//Printer and Paper
				    								if(count(explode('|', $this->values[$i]))!= 2){
					    								throw new Exception('Warning : is not valid. (Should be level 2)');
					    							}
				    							}
				    						break;
				    						
				    						case 'sku':
				    							if(in_array($this->values[$i], $this->SKUS)){
				    								throw new Exception('is duplicated.');
				    							} else {
				    								$this->SKUS[]=$this->values[$i];
				    							}
				    						break;
				    						
				    						case 'image':
				    						case 'thumbnail':
				    						case 'small_image':
				    							if(!empty($this->values[$i]) && !file_exists('media/import/'.$this->values[$i])){
				    								throw new Exception('Warning : doesn\'t exists.');
				    							}
				    						break;
				    						
				    						case 'related_compatible_sku':
				    							if($this->getAttributeByName('cartridge_type')=='Original' && empty($this->values[$i])){
				    								throw new Exception('Warning : is empty.');
				    							}
				    						case 'related_products_sku':
				    						case 'upsell_products_sku':
				    						case 'crosssell_products_sku':
				    							if(!empty($this->values[$i])){
					    							if(count(explode(',', $this->values[$i]))>1){
					    								foreach(explolde(',', $this->values[$i]) as $_sku){
					    									$this->checkSkuExists($_sku);
					    								}
					    							} else {
					    								$this->checkSkuExists($this->values[$i]);
					    							}
				    							}
				    						break;
				    						
				    						case 'link_category_id':
				    							if(!empty($this->values[$i]) && $this->getAttributeByName('attribute_set')=='Printer'){
				    								if(!Mage::getModel('catalog/category')->load($this->values[$i])){
				    									throw new Exception('Warning : doesn\'t exist');
				    								}
				    							}
				    						break;
				    						
				    						//---------- Attributes required for cartridges/toners only ----------//
				    						case 'short_name':
				    						case 'color':
				    						case 'cartridge_type':
				    							if(in_array($this->getAttributeByName('attribute_set'), array('Cartridge', 'Toner')) && empty($this->values[$i])){
				    								throw new Exception('is empty.');
				    							}
				    						break;
				    					}
			    					} catch(Exception $e){
					    				if(strpos($e->getMessage(), 'Warning : ')===0){
				    						$this->warning('Warning ! Row '.$row_count.' : field "'.$this->attributes[$i].'" '.substr($e->getMessage(), 10));
				    					} else {
				    						throw $e;
				    					}
				    				}
			    				}
			    				if($row_count==1){
			    					fputcsv($write_file, $this->attributes);
			    				}
			    				$row_count++;
			    				fputcsv($write_file, $this->values);
			    			}
	    				} catch (Exception $e){
			    			throw new Exception('Row '.$row_count.' : field "'.$this->attributes[$i].'" '.$e->getMessage());
			    		}
			    		
			    		foreach(Mage::getModel('catalog/product')->getCollection() as $product){
			    			$this->SKUS[]=$product->getSku();
			    		}

	    				if(count(array_diff($this->SKUS_to_check, $this->SKUS))!=0){
		    				$this->warning('Warning ! Some linked SKUS doesn\'t exists : '.implode(', ', array_diff($this->SKUS_to_check, $this->SKUS)));
		    			}
		    			
	    			} else {
	    				throw new Exception('Missing required fields in '.$filename.' ('.implode(',', array_diff($required_fields, $this->attributes)).')');
	    			}
	    		} else {
	    			throw new Exception('No datas found in '.$filename);
	    		}
	    	} else {
	    		throw new Exception($filename.' file is not readable.');
	    	}
	    	echo implode('<br/>', $this->warnings);
    		echo '<p>Validated !</p>';
    	} catch(Exception $e){
    		echo $e->getMessage();
    	}
    }
    protected function setAttr($code, $value){
    	if($key=array_search($code, $this->attributes)){
    		$this->values[$key]=$value;
    	} else {
    		$this->attributes[]=$code;
    		$this->values[]=$value;
    	}
    }
    protected function getAttributeByName($attr){
    	if($key=array_search($attr, $this->attributes)){
    		return $this->values[$key];
    	} else {
    		return null;
    	}
    }
    
    protected function checkSkuExists($sku){
    	if(in_array($sku, $this->SKUS)){
    		return true;
    	} else {
    		$this->SKUS_to_check[]=$sku;
    		return false;
    	}
    }
    
    protected function warning($msg){
    	$this->warnings[]=$msg;
    }
}
?>
