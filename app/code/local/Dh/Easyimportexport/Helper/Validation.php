<?php 
class Dh_Easyimportexport_Helper_Validation extends Mage_Core_Helper_Abstract {
	public $_allowed_img_ext=array('jpg','jpeg','gif','png');
	/**
	 * 
	 * Check if the image exists
	 * @param string $filename
	 */
	public function imageCheck($filename){
		//baotn add
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(in_array($ext, $this->_allowed_img_ext)){
			$media_dir=Mage::getBaseDir('media');
			if(!file_exists($media_dir.DS.'import'.DS.$filename)){
				if(!file_exists($media_dir.DS.'catalog'.DS.'product'.DS.substr($filename, 0, 1).DS.substr($filename, 1, 1).DS.$filename)){
					return 0;
				} else {
					if(copy($media_dir.DS.'catalog'.DS.'product'.DS.substr($filename, 0, 1).DS.substr($filename, 1, 1).DS.$filename, $media_dir.DS.'import'.DS.$filename)){
						return 1;
					} else {
						return 0;
					}
				}
			} else {
				return 1;
			}
		} else {
			return -1;
		}
	}
	/**
	 * 
	 * Checks if the given parameter is a number
	 * @param mixed $nb
	 */
	public function numberCheck($nb){
		return is_numeric($nb);
	}
	/**
	 * 
	 * Checks if the given parameter is an integer
	 * @param mixed $int
	 */
	public function intCheck($int){
		return ($this->numberCheck($int) && ($int-intval($int))===0);
	}
	/**
	 * 
	 * Checks if the given parameter is an existing sku
	 * @param string $sku
	 * @return int|bool false if the sku is unknown, else returns the id of the product
	 */
	public function skuExistsCheck($sku){
		$product=Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
		if($product){
			return $product->getId();
		} else {
			return false;
		}
	}
}
?>