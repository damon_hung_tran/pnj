<?php 
class Dh_Easyimportexport_Block_Adminhtml_Steps extends Mage_Adminhtml_Block_Template
{
	protected $_stepLabels;
	protected $_currentStep;
	public function __construct()
	{
		$this->_stepLabels=array(1=>'Select your importer', 2=>'File\'s upload', 3=>'File\'s validation', 4=>'Summary', 5=>'Import');
		parent::__construct();
	}
	/**
	 * 
	 * Retrieve the number of the current step.
	 */
	public function getCurrentStep(){
		if(!$this->_currentStep){
			if(Mage::app()->getRequest()->getParam('step')){
				$this->_currentStep = Mage::app()->getRequest()->getParam('step');
			} else {
				$this->_currentStep = 1;
			}
			if(!isset($this->_stepLabels[$this->_currentStep])){
				throw new Exception('Invalid step parameter.');
			}
		}
		return $this->_currentStep;
	}
	/**
	 * 
	 * Set the number of the current step.
	 * @param int $step
	 * @throws Exception
	 */
	public function setCurrentStep($step){
		if(isset($this->_stepLabels[$step])){
			$this->_currentStep=$step;
		} else {
			throw new Exception('Try to set an invalid step.');
		}
	}
	/**
	 * 
	 * Retrieve the label of the current step.
	 */
	public function getCurrentStepLabel(){
		return $this->_stepLabels[$this->getCurrentStep()];
	}
	/**
	 * 
	 * Retrieve labels of all steps.
	 */
	public function getStepLabels(){
		return $this->_stepLabels;
	}
}
?>