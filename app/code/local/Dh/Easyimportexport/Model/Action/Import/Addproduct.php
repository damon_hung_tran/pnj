<?php 
class Dh_Easyimportexport_Model_Action_Import_Addproduct extends Dh_Easyimportexport_Model_Action_Abstract
{
	protected $_required_fields=array();
	protected $_required_fields_check=array();
	protected $_attr_resources=array();
	
	public function getAdditionalInfos(){
		$fields_form='<fieldset id="fields_to_import" >';
		$fields_form.='<legend align="top">Fields to import</legend>';
		$fields_form.='<table><tr><th></th><th>Attribute code</th><th>Default value</th><th>Apply default on empty field</th><th>Apply default on missing column</th><th>Expected value(s)</th></tr>';
		$allAttributes=$this->getAllAttributes();
		$session=Mage::getSingleton('core/session');
		$odd=true;
		$currentDate=new Zend_Date();
		foreach($allAttributes as $attributeId=>$attributeCode){
			if($this->_attr_resources[$attributeCode]){
				$input_type=$this->_attr_resources[$attributeCode]->getFrontendInput();
			} else {
				$input_type=null;
			}
			$attr=Mage::getModel('catalog/resource_eav_attribute')->load($attributeCode);
			$required=$this->_required_fields_check[$attributeCode];
			$fields_form.='<tr class="'.($odd ? 'odd' : 'even').'"><td><input type="checkbox" id="import_attribute_'.$attributeId.'_new" name="import_attribute_'.$attributeId.'_new" ';
			if($attributeCode=='sku' || $required){
				$fields_form.=' checked="checked" disabled="disabled" ';
			} else if($this->getParam('attr_'.$attributeCode.'_checked_new')){
				$fields_form.=' checked="checked" ';
			}
			$fields_form.='/>';
			if($attributeCode=='sku' || $required){
				$fields_form.=' <input type="hidden" value="1" name="import_attribute_'.$attributeId.'_new" /> ';
			}
			$fields_form.='</td>';
			
			$fields_form.='<td><label for="import_attribute_'.$attributeId.'_new"> '.$attributeCode.'</label></td>';
			
			$fields_form.='<td>';
			$name='import_attribute_default_value_'.$attributeId.'_new';
			$value=$this->getParam('attr_default_'.$attributeCode.'_value_new');
			if(($attributeCode=='created_at' || $attributeCode=='updated_at')){
				$value=$currentDate->toString(self::DATE_FORMAT);
			}

			$readOnly=$attributeCode=='sku' ? true : false;
			$fields_form.=$this->renderField($name, $attributeId, $input_type, $value, $this->_attr_resources[$attributeCode], $readOnly, $attributeCode);
			
			$fields_form.='</td>';
			
			$fields_form.='<td><input type="checkbox" name="import_attribute_defaultonempty_'.$attributeId.'_new"';
			if($attributeCode=='sku'){
				$fields_form.=' disabled="disabled" ';
			} else if($this->getParam('attr_defaultonempty_'.$attributeCode.'_checked_new') || $attributeCode=='created_at' || $attributeCode=='updated_at'){
				$fields_form.=' checked="checked" ';
			}
			$fields_form.=' /></td>';
			
			$fields_form.='<td><input type="checkbox" name="import_attribute_defaultonmissing_'.$attributeId.'_new"';
			if($attributeCode=='sku'){
				$fields_form.=' disabled="disabled" ';
			} else if($this->getParam('attr_defaultonmissing_'.$attributeCode.'_checked_new') || $attributeCode=='created_at' || $attributeCode=='updated_at'){
				$fields_form.=' checked="checked" ';
			}
			$fields_form.=' /></td>';
			$fields_form.='<td>';
			if($this->_attr_resources[$attributeCode]){
				if($this->_attr_resources[$attributeCode]->usesSource()){
					$options=Mage::getModel('catalog/product_attribute_api')->options($attributeId);
					$options2=array();
					foreach ($options as $option){
						if($option['label']!='-- Please Select --' && $option['label']!=''){
							$options2[]=$option['label'];
						}
					}
					$fields_form.='"'.implode('", "', $options2).'"';
				} else if ($type=$this->_attr_resources[$attributeCode]->getBackendType()) {
					switch($type){
						case 'int':
							$fields_form.='Integer';
						break;
						case 'decimal':
							$fields_form.='Decimal number';
						break;
						case 'datetime':
							$fields_form.='Date (format : "'.self::DATE_FORMAT.'")';
						break;
						case 'varchar':
						case 'text':
							$fields_form.='Text (unknown format)';
						break;
						case 'static':
							if($attributeCode=='sku'){
								$fields_form.= 'Unique ID';
							} else if($attributeCode=='created_at' || $attributeCode=='updated_at'){
								$fields_form.= 'Date (format : "'.self::DATE_FORMAT.'")';
							}
						break;
					}
				}
			} else {
				switch($attributeCode){
					case 'attribute_set':
						if(!isset($attributeSets2)){
							$attributeSets=Mage::getModel('catalog/product_attribute_set_api')->items();
							$attributeSets2=array();
							foreach($attributeSets as $attributeSet){
								$attributeSets2[]=$attributeSet['name'];
							}
						}
						$fields_form.='One of those values : "'.implode('", "', $attributeSets2).'"';
					break;
					case 'image':
					case 'small_image':
					case 'thumbnail':
						$fields_form.='Filename of an image in /media/import directory. (allowed extensions : '.implode(', ', $this->_validHelper->_allowed_img_ext).')';
					break;
					case 'category_path_by_names':
						$fields_form.='One or several categories paths (example : "Default Category|subcat1|subCat2,Default Category|subcat3")';
					break;
					case 'type':
						if(!isset($types2)){
							$types=Mage::getModel('catalog/product_type_api')->items();
							$types2=array();
							foreach($types as $type){
								$types2[]=$type['label'];
							}
						}
						$fields_form.='One of those values : "'.implode('", "', $types2).'"';
					break;
					case 'website_ids':
						$collection=Mage::getModel('core/website')->getCollection();
						$websites_codes=array();
						foreach ($collection as $website){
							$websites_codes[]=$website->getCode();
						}
						$fields_form.='"'.implode('", "', $websites_codes).'"';
					break;
				}
			}
			$fields_form.='</td>';
			$fields_form.='</tr>';
			$odd=($odd ^ true);
		}
		
		$fields_form.='</table></fieldset>';
		return $fields_form;
	}
	
	protected function getAllAttributes(){
		if(!$this->getParam('all_attr')){
			$attributeSets=Mage::getModel('catalog/product_attribute_set_api')->items();
			$allAttributes=array();
			foreach($attributeSets as $attributeSet){
				$attributes=Mage::getModel('catalog/product_attribute_api')->items($attributeSet['set_id']);
				foreach($attributes as $attribute){
					if($attribute['type']  && $attribute['code']!='tier_price'){
						$attr=Mage::getModel('catalog/resource_eav_attribute')->load($attribute['attribute_id']);
						$this->_attr_resources[$attribute['code']]=$attr;
						if($attributeSet['name']=='Default' && count($attr->getApplyTo())===0){
							$this->_required_fields_check[$attribute['code']]=$attribute['required'];
						}
						$allAttributes[$attribute['attribute_id']]=$attribute['code'];
					}
				}
			}
			$allAttributes['a']='category_path_by_names';
			$this->_required_fields_check['category_path_by_names']=0;
			$allAttributes['b']='type';
			$this->_required_fields_check['type']=1;
			$allAttributes['c']='attribute_set';
			$this->_required_fields_check['attribute_set']=1;
			$allAttributes['d']='website_ids';
			$this->_required_fields_check['website_ids']=0;
			$i=0;
			foreach ($this->_image_fields as $image_field){
				$allAttributes['e'.$i]=$image_field;
				$this->_required_fields_check[$image_field]=0;
				$i++;
			}
			asort($allAttributes);
			$this->setParam('all_attr', $allAttributes, false);
			$this->setParam('all_required_attr', $this->_required_fields_check, false);
			$this->setParam('all_resources_attr', $this->_attr_resources, false);
		} else {
			$allAttributes=$this->getParam('all_attr');
			$this->_attr_resources=$this->getParam('all_resources_attr');
			$this->_required_fields_check=$this->getParam('all_required_attr');
		}
		return $allAttributes;
	}
	
	public function step2(){
		$request=Mage::app()->getRequest();
		if(!$request->getParam('go_back')){
			$allAttributes=$this->getAllAttributes();
			foreach($allAttributes as $attributeId=>$attributeCode){
				$this->setParam('attr_'.$attributeCode.'_checked_new', $request->getParam('import_attribute_'.$attributeId.'_new'));
				if($request->getParam('import_attribute_'.$attributeId.'_new')){
					$this->_required_fields[]=$attributeCode;
				}
				$this->setParam('attr_default_'.$attributeCode.'_value_new', $request->getParam('import_attribute_default_value_'.$attributeId.'_new'));
				$this->setParam('attr_defaultonempty_'.$attributeCode.'_checked_new', $request->getParam('import_attribute_defaultonempty_'.$attributeId.'_new'));
				$this->setParam('attr_defaultonmissing_'.$attributeCode.'_checked_new', $request->getParam('import_attribute_defaultonmissing_'.$attributeId.'_new'));
			}
			$this->setParam('required_fields', $this->_required_fields, false);
		}
		parent::step2();
	}
	public function step3(){
		$request=Mage::app()->getRequest();
		if($request->getParam('re_do')){
			foreach($request->getParams() as $key=>$value){
				if(strpos($key, 'import_redo_attribute_value_')===0){
					$this->setParam($key, $value, false);
				}
			}
		}
		parent::step3();
	}
	public function validate($line){
		$file=fopen($this->getParam('import_file'), 'r');
		$this->_attributes = fgetcsv($file);
		if($this->_attributes){
			$requiredFields=$this->getRequiredFields();
			
			$importdatas=is_array($this->getImportDatas()) ? $this->getImportDatas() : array();
			$treatedSkus=is_array($this->getParam('treated_skus')) ? $this->getParam('treated_skus') : array();
			$date_validator=new Zend_Validate_Date();
			$date_validator->setFormat(self::DATE_FORMAT);
			$this->_values=$this->fetchLine($line, $file);
			if(!$this->_values){
				if(feof($file)){
					return self::STATUS_END;
				} else {
					$this->addError('Corrupted datas.');
					return self::STATUS_FATAL;
				}
			}
			$productAttrSet=null;
			$productType=null;
			$importdatas[$line-2]=array();
			$request=Mage::app()->getRequest();
		 
			$productAttrSet=$this->getAttributeByName('attribute_set');
			$productAttrSet=($productAttrSet==null || empty($productAttrSet)) ? $this->getParam('attr_default_attribute_set_value_new') : $productAttrSet ;
			$attributeSets=Mage::getModel('catalog/product_attribute_set_api')->items();
			$validate=false;
			$allowedValues=array();
			foreach($attributeSets as $set){
				$allowedValues[]=$set['name'];
				if($productAttrSet==$set['name']){
					$productAttrSet=$set;
					$validate=true;
					$this->setAttr('attribute_set', $set['set_id']);
					$importdatas[$line-2]['attribute_set']=$set['set_id'];
				}
			}
			if(!$validate){
				$this->addError('attribute_set "'.$productAttrSet.'" doesn\'t exist. (should be one or several of those values : "'.implode('","', $allowedValues).'")');
			}
			$productType=$this->getAttributeByName('type');
			if($this->getParam('attr_defaultonempty_type_checked_new') || $this->getParam('attr_defaultonmissing_type_checked_new')){
				$productType=($productType==null || empty($productType)) ? $this->getParam('attr_default_type_value_new') : $productType ;
			}
			$types=Mage::getModel('catalog/product_type_api')->items();
			$validate=false;
			$allowedValues=array();
			foreach($types as $item){
				$allowedValues[]=$item['label'];
				if($productType==$item['label']){
					$validate=true;
					$this->setAttr('type', $item['type']);
					$productType=$item;
					$importdatas[$line-2]['type']=$item['type'];
				}
			}
			if(!$validate){
				$this->addError('type "'.$this->_values[$i].'" doesn\'t exist. (should be one or several of those values : "'.implode('","', $allowedValues).'")');
			}
			if(is_array($productAttrSet) && is_array($productType)){
				$mandatoryFields=$this->getMandatoryFields($productType, $productAttrSet);
			} else {
				$mandatoryFields=array();
			}
			$requiredFields=array_unique(array_merge($requiredFields, $mandatoryFields));
			
			$missing_fields=array_diff($mandatoryFields, $this->_attributes);
			$missing_fields=array_unique(array_merge($missing_fields, array_diff($requiredFields, $this->_attributes)));
			$missing_fields_to_fill=array();
			
			if(count($missing_fields)){
				foreach ($missing_fields as $missing_field){
					if($this->getParam('attr_defaultonmissing_'.$missing_field.'_checked_new')){
						$this->_attributes[]=$missing_field;
						$missing_fields_to_fill[$missing_field]=$this->getParam('attr_default_'.$missing_field.'_value_new');
					} else {
						if(in_array($missing_field, $mandatoryFields)){
							$this->addError('"'.$missing_field.'" field is missing.');
						} else {
							$this->addWarning('"'.$missing_field.'" field is missing.');
						}
					}
				}
			}
			unset($requiredFields[array_search('type', $requiredFields)]);
			unset($requiredFields[array_search('attribute_set', $requiredFields)]);
			for($i=0;$i<count($this->_attributes);$i++){
				if(in_array($this->_attributes[$i], $requiredFields)){
					$isMandatory=in_array($this->_attributes[$i], $mandatoryFields);
					$count_warnings=count($this->_warnings);
					$count_errors=count($this->_errors);
					$attrId=array_search($this->_attributes[$i], $this->getAllAttributes());
					$attr=Mage::getModel('catalog/resource_eav_attribute')->load($attrId);
					$type=$attr->getBackendType();
					
					if(isset($missing_fields_to_fill[$this->_attributes[$i]])){
						$this->_values[$i]=$missing_fields_to_fill[$this->_attributes[$i]];
					} else if(!isset($this->_values[$i]) || empty($this->_values[$i])){
						if($this->getParam('attr_defaultonempty_'.$this->_attributes[$i].'_checked_new')){
							$this->_values[$i]=$this->getParam('attr_default_'.$this->_attributes[$i].'_value_new');
						} 
					}
					
					if($request->getParam('re_do') && $this->getParam('import_redo_attribute_value_'.$attrId.'_'.$line)!==NULL){
						$this->_values[$i]=$this->getParam('import_redo_attribute_value_'.$attrId.'_'.$line);
					}
					
					if($this->_attributes[$i]=='sku'){
						if($this->_validHelper->skuExistsCheck($this->_values[$i])){
							$this->addError('sku "'.$this->_values[$i].'" exists already.');
						} else if(in_array($this->_values[$i], $treatedSkus)){
							$this->addError('sku "'.$this->_values[$i].'" is duplicated.');
						} else {
							$treatedSkus[]=$this->_values[$i];
						}
					} else if($this->_attributes[$i]=='website_ids'){
						$collection=Mage::getModel('core/website')->getCollection();
						$websites_codes=array();
						$values=explode(',', $this->_values[$i]);
						$valueSet=array();
						foreach ($values as $value) {
							$validate=false;
							foreach ($collection as $website){
								$websites_codes[]=$website->getCode();
								if($value==$website->getCode()){
									$valueSet[]=$website->getId();
									$validate=true;
								}
							}
							if(!$validate){
								$this->addWarning('website "'.$this->_values[$i].'" doesn\'t exists. Should be one of those values "'.implode('", "', $websites_codes).'".');
							}
						}
						if(count($valueSet)==1){
							$this->_values[$i]=$valueSet[0];
						} else {
							$this->_values[$i]=$valueSet;
						}
					} else if(in_array($this->_attributes[$i], $this->_image_fields)){
						$imgCheckResult=$this->_validHelper->imageCheck($this->_values[$i]);
						if($imgCheckResult==0){
							$this->addWarning('image "'.$this->_values[$i].'" not found.');
						} else if ($imgCheckResult==-1){
							$this->addWarning('image "'.$this->_values[$i].'" has wrong format (allowed extensions : '.implode(', ', $this->_validHelper->_allowed_img_ext).').');
						}
					} else if($attr->usesSource()){
						$options=Mage::getModel('catalog/product_attribute_api')->options($attr->getId());
						$availableOptions=array();
						if($this->_attributes[$i]!='visibility'){
							$values=explode(',', $this->_values[$i]);
						} else {
							$values=array($this->_values[$i]);
						}
						$setValues=array();
						$validateAll=true;
						$firstPassage=true;
						foreach ($values as $value){
							$validate=false;
							foreach($options as $option){
								if($firstPassage && !empty($option['value'])){
									$availableOptions[]=$option['label'];
								}
								if($value==$option['label']){
									if(!$option['value']=='' || !$isMandatory){
										$validate=true;
										$setValues[]=$option['value'];
									}
								}
							}
							$firstPassage=false;
							if(!$validate){
								$validateAll=false;
							}
						}
						if(!$validateAll){
							if($isMandatory){
								$this->addError('field "'.$this->_attributes[$i].'" is not a valid value : "'.$this->_values[$i].'" (should be one or several of those values : "'.implode('","', $availableOptions).'").');
							} else {
								$this->addWarning('field "'.$this->_attributes[$i].'" is not a valid value : "'.$this->_values[$i].'" (should be one or several of those values : "'.implode('","', $availableOptions).'").');
							}
						} else {
							if(count($setValues)>1){
								$this->_values[$i]=$setValues;
							} else {
								$this->_values[$i]=$setValues[0];
							}
						}
					} else if($type=='int' || $type=='decimal'){
						if($this->_validHelper->numberCheck($this->_values[$i])){
							if($type=='int' && !$this->_validHelper->intCheck($this->_values[$i])){
								if($isMandatory){
									$this->addError('field "'.$this->_attributes[$i].'" is not a valid integer : "'.$this->_values[$i].'".');
								} else {
									$this->addWarning('field "'.$this->_attributes[$i].'" is not a valid integer : "'.$this->_values[$i].'".');
								}
								
							}
							if($attr->getBackendModel()=='catalog/product_attribute_backend_price' && $this->_values[$i]<0){
								if($isMandatory){
									$this->addError('field "'.$this->_attributes[$i].'" can\'t be negative.');
								} else {
									$this->addWarning('field "'.$this->_attributes[$i].'" can\'t be negative.');
								}
							}
						} else {
							$this->addError('field "'.$this->_attributes[$i].'" is not a valid number "'.$this->_values[$i].'".');
						}
					} else if($type=='datetime'){
						if(!$date_validator->isValid($this->_values[$i])){
							if($isMandatory){
								$this->addError('field "'.$this->_attributes[$i].'" is not a valid date : "'.$this->_values[$i].'".');
							} else {
								$this->addWarning('field "'.$this->_attributes[$i].'" is not a valid date : "'.$this->_values[$i].'".');
							}
						}
					}
					if ($this->_values[$i]==null || $this->_values[$i]=='') {
						if($isMandatory){
							$this->addError('field "'.$this->_attributes[$i].'" is empty and no default value has been specified.');
						} else {
							$this->addWarning('field "'.$this->_attributes[$i].'" is empty and no default value has been specified.');
						}
					}
					unset($mandatoryFields[$this->_attributes[$i]]);
					if(count($this->_warnings)==$count_warnings && $count_errors==count($this->_errors)){
						$importdatas[$line-2][$this->_attributes[$i]]=$this->_values[$i];
					} else {
						$name='import_redo_attribute_value_'.$attrId.'_'.$line;
						$this->addWrongField($name, $attrId, $this->_attributes[$i], $type, $this->_values[$i], $attr);
					}
				}
			}
			$setAttributes=Mage::getModel('catalog/product_attribute_api')->items($productAttrSet['set_id']);
			/*foreach ($setAttributes as $setAttribute) {
				$attr=Mage::getModel('catalog/resource_eav_attribute')->load($setAttribute['attribute_id']);
				$applyTo=$attr->getApplyTo();
				if($setAttribute['required'] && $setAttribute['type'] && (count($applyTo)==0 || in_array($productType['type'], $applyTo))){
					if($this->getAttributeByName($setAttribute['code'])===null){
						$this->addError('field "'.$setAttribute['code'].'" is required for attribute_set "'.$productAttrSet['name'].'"'.(count($applyTo) ? ' and type "'.$productType['label'].'"' : ''));
						$name='import_redo_attribute_value_'.$attrId.'_'.$line;
						$this->addWrongField($name, $setAttribute['attribute_id'], $setAttribute['code'], $setAttribute['type'], '', $attr);
						/*$this->setParam('attr_'.$setAttribute['code'].'_checked_new', 1);
						$requiredFields[]=$setAttribute['code'];
						$this->setParam('required_fields', $requiredFields, false);
						$this->setParam('attr_defaultonmissing_'.$setAttribute['code'].'_checked_new', 1, false);
						$this->setParam('attr_defaultonmissing_'.$setAttribute['code'].'_value_new', '', false);*//*
					}
				}
			}*/
			$this->setParam('treated_skus', $treatedSkus, false);
			$this->setParam('missing_fields_to_fill', $missing_fields_to_fill, false);
		} else {
			$this->addError('Incorrect file.');
			return self::STATUS_FATAL;
		}
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){
			$this->setImportdatas($importdatas);
			$this->setSummary(count($importdatas).' product\'s will be created.<br/>Fields set : '.implode(', ', $requiredFields));
		}
		return $this->_status;
	}
	
	public function getMandatoryFields($productType, $productAttrSet){
		$setAttributes=Mage::getModel('catalog/product_attribute_api')->items($productAttrSet['set_id']);
		$result=array();
		foreach ($setAttributes as $setAttribute) {
			$attr=Mage::getModel('catalog/resource_eav_attribute')->load($setAttribute['attribute_id']);
			$applyTo=$attr->getApplyTo();
			if($setAttribute['required'] && $setAttribute['type'] && (count($applyTo)==0 || in_array($productType['type'], $applyTo))){
				$result[]=$setAttribute['code'];
			}
		}
		return $result;
	}
	
	public function getRequiredFields(){
		if(!count($this->_required_fields)){
			if(count($this->getParam('required_fields'))){
				$this->_required_fields=$this->getParam('required_fields');
			}
		}
		return $this->_required_fields;
	}
	
	public function import($line){
		try{
			$session=Mage::getSingleton('core/session');
			
			$datas=$this->getImportdatas();
			for($i=0;$i<$line;$i++){
				next($datas);
			}
			$line=key($datas);
			/*---------- <Category path handling> ----------*/
			$this->processCategoryPath($datas[$line]);
			/*---------- </Category path handling> ----------*/
			
			$type=$datas[$line]['type'];
			unset($datas[$line]['type']);
			$set=$datas[$line]['attribute_set'];
			unset($datas[$line]['attribute_set']);
			$sku=$datas[$line]['sku'];
			unset($datas[$line]['sku']);
			$productId=Mage::getModel('catalog/product_api')->create($type, $set, $sku, $datas[$line]);
			$product=Mage::getModel('catalog/product')->load($id);
			
			Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
			$product->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
			
			/*---------- <Media handling> ----------*/
			$this->importImages($product, $datas[$line]);
			/*---------- </Media handling> ----------*/
			
		} catch(Mage_Api_Exception $e){
			$this->addError('Failed to add the product : '.$e->getCustomMessage());
		} catch(Exception $e){
			$this->addError($e->getMessage());
		}
		return $line;
	}
	
	public function step5(){
		Mage::getSingleton('core/session')->unsPatchcache();
	}
}
?>