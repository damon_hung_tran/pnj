<?php 
class Dh_Easyimportexport_Model_Action_Import_Updatetiers extends Dh_Easyimportexport_Model_Action_Abstract
{
	public function validate($line){
		$file=fopen($this->getParam('import_file'), 'r');
		$this->_attributes = fgetcsv($file);
		if($this->_attributes){
				$importdatas=is_array($this->getImportDatas()) ? $this->getImportDatas() : array();
				$treatedSkus=is_array($this->getParam('treated_skus')) ? $this->getParam('treated_skus') : array();
				
				$count_warnings=count($this->_warnings);
				$count_errors=count($this->_errors);
				
				$this->_values=$this->fetchLine($line, $file);
				if(!$this->_values){
					if(feof($file)){
						return self::STATUS_END;
					} else {
						$this->addError('Corrupted datas.');
						return self::STATUS_FATAL;
					}
				}
				$sku=$this->getAttributeByName('sku');
				//$price=$this->getAttributeByName('price');
				if(in_array($sku, $treatedSkus)){
					$this->addError('Duplicated sku "'.$sku.'".');
				} else {
					$treatedSkus[]=$sku;
				}
				
				$product=Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
				if(!$product){
					$this->addError('Unknown sku "'.$sku.'".');
				} else {
					$importdatas[$line-2]=array('id'=>$product->getId());
				
					$i=0;
					$treated_tier_keys=array();
					$tier_price=$this->getAttributeByName('tier_price'.$i);
					$set_tier_prices=array();
					
					$collection=Mage::getModel('customer/group')->getCollection();
					$customergroup_codes=array();
					foreach ($collection as $customergroup){
						$customergroup_codes[]=$customergroup->getCode();
					}
					$customergroup_codes[Mage_Customer_Model_Group::CUST_GROUP_ALL]='ALL GROUPS';
					
					$attr=Mage::getModel('catalog/resource_eav_attribute')->loadByCode(4, 'tier_price');
					$websites_codes=array();
					/*if(!$attr->isScopeGlobal()){
						$collection=Mage::getModel('core/website')->getCollection();
						foreach ($collection as $website){
							$websites_codes[$website->getId()]=$website->getCode();
						}
					}*/
					$websites_codes[0]='All Websites';
					
					while($tier_price!==null){
						if(!empty($tier_price)){
							$tier_price=explode(',', $tier_price);
							if(count($tier_price)==4){
								$messages=array();
								if($this->_validHelper->numberCheck($tier_price[3])){
									$set_tier_prices[$i]['price']=$tier_price[3];
								} else {
									$messages[]='price is not a valid number';
								}
								
								if($this->_validHelper->numberCheck($tier_price[2])){
									$set_tier_prices[$i]['qty']=$tier_price[2];
								} else {
									$messages[]='qty is not a valid number';
								}
								
								$website_id=array_search($tier_price[0], $websites_codes);
								if($website_id!==false && ($website_id==0 || in_array($website_id, $product->getWebsiteIds()))){
									$set_tier_prices[$i]['website']=$website_id;
								} else {
									$messages[]='website is not valid';
								}
								
								$customergroup_id=array_search($tier_price[1], $customergroup_codes);
								if($customergroup_id!==false){
									$set_tier_prices[$i]['customer_group_id']=$customergroup_id;
								} else {
									$messages[]='customer group is not valid';
								}
								
								if(count($set_tier_prices[$i])!=4){
									unset($set_tier_prices[$i]);
									$this->addWarning('tier_price'.$i.' is not valid : '.implode(', ', $messages));
								} else {
									$tier_key=$set_tier_prices[$i]['website'].$set_tier_prices[$i]['customer_group_id'].$set_tier_prices[$i]['qty'];
									if(in_array($tier_key, $treated_tier_keys)){
										$this->addWarning('Duplicate website tier price customer group and quantity.');
										unset($set_tier_prices[$i]);
									} else {
										$treated_tier_keys[]=$tier_key;
									}
								}
								
							} else {
								$this->addWarning('tier_price'.$i.' is not valid : wrong parameters count.');
							}
						}
						$i++;
						$tier_price=$this->getAttributeByName('tier_price'.$i);
					}
					
					if(count($set_tier_prices)){
						$importdatas[$line-2]['tier_price']=$set_tier_prices;
					} else {
						$this->addError('no valid datas found.');
					}
				}
				
				$this->setParam('treated_skus', $treatedSkus, false);
		} else {
			$this->addError('Incorrect file.');
			return self::STATUS_FATAL;
		}
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){
			$this->setImportdatas($importdatas);
			$this->setSummary(count($importdatas).' product\'s prices will be updated.');
		}
		return $this->_status;
	}
	
	public function getRequiredFields(){
		return array('sku', 'tierprice');
	}
	
	public function import($lineNb){
		$datas=$this->getImportdatas();
		for($i=0;$i<$lineNb;$i++){
			next($datas);
		}
		$lineNb=key($datas);
		try{
			if(isset($datas[$lineNb])){
				$product=Mage::getModel('catalog/product')->load($datas[$lineNb]['id']);
				if($product){
					if(!Mage::getModel('catalog/product_api')->update($product->getId(), $datas[$lineNb])){
						$this->addError('Update failed.');
					}
				} else {
					$this->addError('Product not found.');
				}
			} else {
				$this->addError('Invalid datas.');
			}
		} catch(Exception $e){
			$this->addError('Update failed : '.$e->getMessage());
		}
		return $lineNb;
	}
	
	public function getAdditionalInfos(){
		$return='Required fields are : sku, and at least one tier_price. (put tier prices in column tier_price0, tier_price1, etc... in this format : "website,customer_group,qty,price".';
		$attr=Mage::getModel('catalog/resource_eav_attribute')->loadByCode(4, 'tier_price');
		$websites_codes=array();
		/*//var_dump($attr->isScopeGlobal());var_dump($attr->getId());
		if(!$attr->isScopeGlobal()){
			$collection=Mage::getModel('core/website')->getCollection();
			foreach ($collection as $website){
				$websites_codes[]=$website->getCode();
			}
		}*/
		$websites_codes[]='All Websites';
		
		$collection=Mage::getModel('customer/group')->getCollection();
		$customergroup_codes=array();
		foreach ($collection as $customergroup){
			$customergroup_codes[]=$customergroup->getCode();
		}
		$customergroup_codes[]='ALL GROUPS';
		$return.='<br/>(website values : "'.implode('", "', $websites_codes).'" ; customer_group values : "'.implode('", "', $customergroup_codes).'")';
		return $return;
	}
}
?>