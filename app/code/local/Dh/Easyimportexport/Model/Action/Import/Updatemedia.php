<?php 
class Dh_Easyimportexport_Model_Action_Import_Updatemedia extends Dh_Easyimportexport_Model_Action_Abstract
{
	public function validate($line){
		$file=fopen($this->getParam('import_file'), 'r');
		$this->_attributes = fgetcsv($file);
		if($this->_attributes){
			$missing_fields=array_diff($this->getRequiredFields(), $this->_attributes);
			if(count($missing_fields)){
				foreach ($missing_fields as $missing_field){
					$this->addError('"'.$missing_field.'" field is missing.');
				}
				return self::STATUS_FATAL;
			} else {
				$importdatas=is_array($this->getImportDatas()) ? $this->getImportDatas() : array();
				$treatedSkus=is_array($this->getParam('treated_skus')) ? $this->getParam('treated_skus') : array();
				$this->_values=$this->fetchLine($line, $file);
				if(!$this->_values){
					if(feof($file)){
						return self::STATUS_END;
					} else {
						$this->addError('Corrupted datas.');
						return self::STATUS_FATAL;
					}
				}
				$sku=$this->getAttributeByName('sku');
				$media['image']=$this->getAttributeByName('image');
				$media['small_image']=$this->getAttributeByName('small_image');
				$media['thumbnail']=$this->getAttributeByName('thumbnail');
				if(in_array($sku, $treatedSkus)){
					$this->addWarning('Duplicated sku "'.$sku.'".');
				} else {
					$treatedSkus[]=$sku;
				}
				foreach ($media as $code=>$value){
					$value=trim($value);
					if(!empty($value)){
						$imgCheckResult=$this->_validHelper->imageCheck($value);
						if($imgCheckResult==0){
							$this->addWarning('image "'.$value.'" not found.');
						} else if ($imgCheckResult==-1){
							$this->addWarning('image "'.$value.'" has wrong format (allowed extensions : '.implode(', ', $this->_validHelper->_allowed_img_ext).').');
						} else {
							$importdatas[$line-2][$code]=$value;
							if($this->getAttributeByName($code.'_label')){
								$importdatas[$line-2][$code.'_label']=$this->getAttributeByName($code.'_label');
							}
						}
					}
				}
				if(count($importdatas[$line-2])){
					$product=Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
					if(!$product){
						$this->addError('Unknown sku "'.$sku.'".');
					} else {
						$importdatas[$line-2]['id']=$product->getId();
					}
					if($this->getAttributeByName('store')){
						$collection=$this->getStoresInfos();
						$stores_codes=array();
						$validate=false;
						foreach ($collection as $store){
							$stores_codes[]=$store['name'];
							if($this->getAttributeByName('store')==$store['name']){
								$importdatas[$line-2]['store']=$store['id'];
								$validate=true;
							}
						}
						if(!$validate){
							$this->addError('store "'.$this->getAttributeByName('store').'" doesn\'t exists. Should be one of those values "'.implode('", "', $stores_codes).'".');
						}
					}
				} else {
					$this->addError('No valid datas found.');
				}
				$this->setParam('treated_skus', $treatedSkus, false);
				$this->setParam('missing_fields_to_fill', $missing_fields_to_fill, false);
			}
		} else {
			$this->addError('Incorrect file.');
			return self::STATUS_FATAL;
		}
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){
			$this->setImportdatas($importdatas);
			$this->setSummary(count($importdatas).' product\'s images will be updated.');
		}
		return $this->_status;
	}
	
	public function getRequiredFields(){
		return array('sku');
	}
	
	public function import($lineNb){
		$datas=$this->getImportdatas();
		for($i=0;$i<$lineNb;$i++){
			next($datas);
		}
		$lineNb=key($datas);
		if(isset($datas[$lineNb])){
			$store = $datas[$lineNb]['store'] ? $datas[$lineNb]['store'] : null;
			$product=Mage::getModel('catalog/product')->load($datas[$lineNb]['id']);
			if($product){
				Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
				$product->setStoreId($store);
				$this->importImages($product, $datas[$lineNb]);
			} else {
				$this->addError('Product not found.');
			}
		} else {
			$this->addError('Invalid datas.');
		}
		return $lineNb;
	}
	
	public function getAdditionalInfos(){
		$return='Required fields are : sku, and at least one of those : '.implode($this->_image_fields, ', ');
		$return.=' (allowed extensions : '.implode(', ', $this->_validHelper->_allowed_img_ext).'). ';
		$return.='You can also set the store view to update.';
		$store_names = array_map(function($item) { return $item['name']; }, $this->getStoresInfos());
		$return.=' (allowed values : "'.implode('", "', $store_names).'")';
		return $return;
	}
}
?>