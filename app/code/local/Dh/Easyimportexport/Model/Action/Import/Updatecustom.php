<?php 
class Dh_Easyimportexport_Model_Action_Import_Updatecustom extends Dh_Easyimportexport_Model_Action_Abstract
{
	protected $_required_fields=array();
	protected $_attr_resources=array();
	
	public function getAdditionalInfos(){
		$fields_form='<fieldset id="fields_to_import" >';
		$fields_form.='<legend align="top">Fields to import</legend>';
		$fields_form.='<table><tr><th></th><th>Attribute code</th><th>Default value</th><th>Apply default on empty field</th><th>Apply default on missing column</th><th>Expected value(s)</th></tr>';
		$allAttributes=$this->getAllAttributes();
		$session=Mage::getSingleton('core/session');
		$odd=true;
		$currentDate=new Zend_Date();
		foreach($allAttributes as $attributeId=>$attributeCode){
			if($this->_attr_resources[$attributeCode]){
				$input_type=$this->_attr_resources[$attributeCode]->getFrontendInput();
			} else {
				$input_type=null;
			}
			$fields_form.='<tr class="'.($odd ? 'odd' : 'even').'"><td><input type="checkbox" id="import_attribute_'.$attributeId.'" name="import_attribute_'.$attributeId.'" ';
			if($attributeCode=='sku'){
				$fields_form.=' checked="checked" disabled="disabled" ';
			} else if($this->getParam('attr_'.$attributeCode.'_checked')){
				$fields_form.=' checked="checked" ';
			}
			$fields_form.='/>';
			if($attributeCode=='sku'){
				$fields_form.=' <input type="hidden" value="1" name="import_attribute_'.$attributeId.'" /> ';
			}
			$fields_form.='</td>';
			
			$fields_form.='<td><label for="import_attribute_'.$attributeId.'"> '.$attributeCode.'</label></td>';
			
			$fields_form.='<td>';
			$name='import_attribute_default_value_'.$attributeId;
			$value=$this->getParam('attr_default_'.$attributeCode.'_value');
			if($attributeCode=='updated_at'){
				$value=$currentDate->toString(self::DATE_FORMAT);
			}
			
			$readOnly=$attributeCode=='sku' ? true : false;
			$fields_form.=$this->renderField($name, $attributeId, $input_type, $value, $this->_attr_resources[$attributeCode], $readOnly, $attributeCode);
			
			$fields_form.='</td>';
			
			$fields_form.='<td><input type="checkbox" name="import_attribute_defaultonempty_'.$attributeId.'"';
			if($attributeCode=='sku'){
				$fields_form.=' disabled="disabled" ';
			} else if($this->getParam('attr_defaultonempty_'.$attributeCode.'_checked')){
				$fields_form.=' checked="checked" ';
			}
			$fields_form.=' /></td>';
			
			$fields_form.='<td><input type="checkbox" name="import_attribute_defaultonmissing_'.$attributeId.'"';
			if($attributeCode=='sku'){
				$fields_form.=' disabled="disabled" ';
			} else if($this->getParam('attr_defaultonmissing_'.$attributeCode.'_checked')){
				$fields_form.=' checked="checked" ';
			}
			$fields_form.=' /></td>';
			
			$fields_form.='<td>';
			if($this->_attr_resources[$attributeCode]){
				if($this->_attr_resources[$attributeCode]->usesSource()){
					$options=Mage::getModel('catalog/product_attribute_api')->options($attributeId);
					$options2=array();
					$options3=array();
					foreach ($options as $option){
						if($option['label']!='-- Please Select --' && $option['label']!=''){
							$options2[]=$option['label'];
						}
					}
					$fields_form.='"'.implode('", "', $options2).'"';
				} else if ($type=$this->_attr_resources[$attributeCode]->getBackendType()) {
					switch($type){
						case 'int':
							$fields_form.='Integer';
						break;
						case 'decimal':
							$fields_form.='Decimal number';
						break;
						case 'datetime':
							$fields_form.='Date (format : "'.self::DATE_FORMAT.'")';
						break;
						case 'varchar':
						case 'text':
							$fields_form.='Text';
						break;
						case 'static':
							if($attributeCode=='sku'){
								$fields_form.= 'Unique ID of the product you want to update';
							} else if($attributeCode=='created_at' || $attributeCode=='updated_at'){
								$fields_form.= 'Date (format : "'.self::DATE_FORMAT.'")';
							}
						break;
					}
				}
			} else {
				switch($attributeCode){
					case 'attribute_set':
						if(!isset($attributeSets2)){
							$attributeSets=Mage::getModel('catalog/product_attribute_set_api')->items();
							$attributeSets2=array();
							foreach($attributeSets as $attributeSet){
								$attributeSets2[]=$attributeSet['name'];
							}
						}
						$fields_form.='One of those values : "'.implode('", "', $attributeSets2).'"';
					break;
					case 'image':
					case 'small_image':
					case 'thumbnail':
						$fields_form.='Filename of an image in /media/import directory. (allowed extensions : '.implode(', ', $this->_validHelper->_allowed_img_ext).')';
					break;
					case 'category_path_by_names':
						$fields_form.='One or several categories paths (example : "Default Category|subcat1|subCat2,Default Category|subcat3")';
					break;
					case 'type':
						if(!isset($types2)){
							$types=Mage::getModel('catalog/product_type_api')->items();
							$types2=array();
							foreach($types as $type){
								$types2[]=$type['label'];
							}
						}
						$fields_form.='One of those values : "'.implode('", "', $types2).'"';
					break;
					case 'website_ids':
						$collection=Mage::getModel('core/website')->getCollection();
						$websites_codes=array();
						foreach ($collection as $website){
							$websites_codes[]=$website->getCode();
						}
						$fields_form.='"'.implode('", "', $websites_codes).'"';
					break;
					case 'store':
						$collection=$this->getStoresInfos();
						$stores_codes=array();
						foreach ($collection as $store){
							$stores_codes[]=$store['name'];
						}
						$fields_form.='"'.implode('", "', $stores_codes).'"';
					break;
				}
			}
			$fields_form.='</td>';
			
			$fields_form.='</tr>';
			$odd=($odd ^ true);
		}
		
		$fields_form.='</table></fieldset>';
		return $fields_form;
	}
	
	protected function getAllAttributes(){
		if(!$this->getParam('all_attr')){
			$attributeSets=Mage::getModel('catalog/product_attribute_set_api')->items();
			$allAttributes=array();
			foreach($attributeSets as $attributeSet){
				$attributes=Mage::getModel('catalog/product_attribute_api')->items($attributeSet['set_id']);
				foreach($attributes as $attribute){
					if($attribute['type'] && $attribute['code']!='tier_price'){
						
						$attr=Mage::getModel('catalog/resource_eav_attribute')->load($attribute['attribute_id']);
						$this->_attr_resources[$attribute['code']]=$attr;
						$allAttributes[$attribute['attribute_id']]=$attribute['code'];
					}
				}
			}
			$allAttributes['a']='category_path_by_names';
			$allAttributes['b']='website_ids';
			$allAttributes['c']='store';
			$i=0;
			foreach ($this->_image_fields as $image_field){
				$allAttributes['d'.$i]=$image_field;
				$i++;
			}
			asort($allAttributes);
			$this->setParam('all_attr', $allAttributes, false);
			$this->setParam('all_resources_attr', $this->_attr_resources, false);
		} else {
			$allAttributes=$this->getParam('all_attr');
			$this->_attr_resources=$this->getParam('all_resources_attr');
		}
		return $allAttributes;
	}
	
	public function step2(){
		$request=Mage::app()->getRequest();
		if(!$request->getParam('go_back')){
			$allAttributes=$this->getAllAttributes();
			foreach($allAttributes as $attributeId=>$attributeCode){
				$this->setParam('attr_'.$attributeCode.'_checked', $request->getParam('import_attribute_'.$attributeId));
				if($request->getParam('import_attribute_'.$attributeId)){
					$this->_required_fields[]=$attributeCode;
				}
				$this->setParam('attr_default_'.$attributeCode.'_value', $request->getParam('import_attribute_default_value_'.$attributeId));
				$this->setParam('attr_defaultonempty_'.$attributeCode.'_checked', $request->getParam('import_attribute_defaultonempty_'.$attributeId));
				$this->setParam('attr_defaultonmissing_'.$attributeCode.'_checked', $request->getParam('import_attribute_defaultonmissing_'.$attributeId));
			}
			$this->setParam('required_fields', $this->_required_fields, false);
		}
		parent::step2();
	}
	
	public function step3(){
		$request=Mage::app()->getRequest();
		if($request->getParam('re_do')){
			foreach($request->getParams() as $key=>$value){
				if(strpos($key, 'import_redo_attribute_value_')===0){
					$this->setParam($key, $value, false);
				}
			}
		}
		parent::step3();
	}
	
	public function validate($line){
		$file=fopen($this->getParam('import_file'), 'r');
		$this->_attributes = fgetcsv($file);
		if(count($this->_attributes)){
			$requiredFields=$this->getRequiredFields();
			$missing_fields=array_diff($requiredFields, $this->_attributes);
			$missing_fields_to_fill= is_array($this->getParam('missing_fields_to_fill')) ? $this->getParam('missing_fields_to_fill') : array();
			if(count($missing_fields)){
				foreach ($missing_fields as $missing_field){
					if($this->getParam('attr_defaultonmissing_'.$missing_field.'_checked')){
						$this->_attributes[]=$missing_field;
						$missing_fields_to_fill[$missing_field]=$this->getParam('attr_default_'.$missing_field.'_value');
					} else {
						$this->addError('"'.$missing_field.'" field is missing.');
						$retour=self::STATUS_FATAL;
					}
				}
				if($retour==self::STATUS_FATAL){
					return $retour;
				}
			} 
			$importdatas=is_array($this->getImportDatas()) ? $this->getImportDatas() : array();
			$date_validator=new Zend_Validate_Date();
			$date_validator->setFormat(self::DATE_FORMAT);
			$treatedSkus=is_array($this->getParam('treated_skus')) ? $this->getParam('treated_skus') : array();
			$this->_values=$this->fetchLine($line, $file);
			if(!$this->_values){
				if(feof($file)){
					return self::STATUS_END;
				} else {
					$this->addError('Corrupted datas.');
					return self::STATUS_FATAL;
				}
			}
			$importdatas[$line-2]=array();
			$request=Mage::app()->getRequest();
			for($i=0;$i<count($this->_attributes);$i++){
				if(in_array($this->_attributes[$i], $requiredFields)){
					$count_warnings=count($this->_warnings);
					$count_errors=count($this->_errors);
					$attrId=array_search($this->_attributes[$i], $this->getAllAttributes());
					if(isset($missing_fields_to_fill[$this->_attributes[$i]])){
						$this->_values[$i]=$missing_fields_to_fill[$this->_attributes[$i]];
					} else if(!isset($this->_values[$i]) || empty($this->_values[$i])){
						if($this->getParam('attr_defaultonempty_'.$this->_attributes[$i].'_checked')){
							$this->_values[$i]=$this->getParam('attr_default_'.$this->_attributes[$i].'_value');
						}
					}

					if($request->getParam('re_do') && $this->getParam('import_redo_attribute_value_'.$attrId.'_'.$line)!==NULL){
						$this->_values[$i]=$this->getParam('import_redo_attribute_value_'.$attrId.'_'.$line);
					}
					
					$attr=Mage::getModel('catalog/resource_eav_attribute')->load($attrId);
					$type=$attr->getBackendType();
					
					if($this->_attributes[$i]=='sku'){
						if(!$this->_validHelper->skuExistsCheck($this->_values[$i])){
							$this->addError('sku "'.$this->_values[$i].'" doesn\'t exist.');
						} else if(in_array($this->_values[$i], $treatedSkus)){
							$this->addError('sku "'.$this->_values[$i].'" is duplicated.');
						} else {
							$treatedSkus[]=$this->_values[$i];
						}
					} else if($this->_attributes[$i]=='store'){
						$collection=$this->getStoresInfos();
						$stores_codes=array();
						$validate=false;
						foreach ($collection as $store){
							$stores_codes[]=$store['name'];
							if($this->_values[$i]==$store['name']){
								$this->_values[$i]=$store['code'];
								$validate=true;
							}
						}
						if(!$validate){
							$this->addError('store "'.$this->_values[$i].'" doesn\'t exists. Should be one of those values "'.implode('", "', $stores_codes).'".');
						}
					} else if($this->_attributes[$i]=='website_ids'){
						$collection=Mage::getModel('core/website')->getCollection();
						$websites_codes=array();
						$values=explode(',', $this->_values[$i]);
						$valueSet=array();
						$availableOptionsSetted=false;
						$validateAll=true;
						foreach ($values as $value) {
							$validate=false;
							foreach ($collection as $website){
								if(!$availableOptionsSetted){
									$websites_codes[]=$website->getCode();
								}
								if($value==$website->getCode()){
									$valueSet[]=$website->getId();
									$validate=true;
								}
							}
							$availableOptionsSetted=true;
							if(!$validate){
								$validateAll=false;
								$this->addWarning('website "'.$value.'" doesn\'t exists. Should be one of those values "'.implode('", "', $websites_codes).'".');
							}
						}
						if($validateAll){
							if(count($valueSet)==1){
								$this->_values[$i]=array($valueSet[0]);
							} else {
								$this->_values[$i]=$valueSet;
							}
						}
					} else if($this->_attributes[$i]=='category_ids'){
                        $this->_attributes[$i] = 'categories';
                        $values=explode(',', $this->_values[$i]); 
                        if(is_array($values)){
                            $values = array($values);
                        }
                        $this->_values[$i] = $values;
                    }else if(in_array($this->_attributes[$i], $this->_image_fields)){
						$imgCheckResult=$this->_validHelper->imageCheck($this->_values[$i]);
						if($imgCheckResult==0){
							$this->addWarning('image "'.$this->_values[$i].'" not found.');
						} else if ($imgCheckResult==-1){
							$this->addWarning('image "'.$this->_values[$i].'" has wrong format (allowed extensions : '.implode(', ', $this->_validHelper->_allowed_img_ext).').');
						}
					} else if($attr->usesSource()){
						$options=Mage::getModel('catalog/product_attribute_api')->options($attr->getId());
						$availableOptions=array();
						if($this->_attributes[$i]!='visibility'){
							$values=explode(',', $this->_values[$i]);
						} else {
							$values=array($this->_values[$i]);
						}
						$setValues=array();
						$validateAll=true;
						$firstPassage=true;
						foreach ($values as $value){
							$validate=false;
							foreach($options as $option){
								if($firstPassage && !empty($option['value'])){
									$availableOptions[]=$option['label'];
								}
								if($value==$option['label']){
									$validate=true;
									$setValues[]=$option['value'];
									break;
								}
							}
							$firstPassage=false;
							if(!$validate){
								$validateAll=false;
								break;
							}
						}
						if(!$validateAll){
							$this->addWarning('field "'.$this->_attributes[$i].'" is not a valid value : "'.$this->_values[$i].'" (should be one or several of these values : "'.implode('","', $availableOptions).'").');
						} else {
							if(count($setValues)>1){
								$this->_values[$i]=$setValues;
							} else {
								$this->_values[$i]=$setValues[0];
							}
						}
					} else if($type=='int' || $type=='decimal'){
						if($this->_validHelper->numberCheck($this->_values[$i])){
							if($type=='int' && !$this->_validHelper->intCheck($this->_values[$i])){
								$this->addWarning('field "'.$this->_attributes[$i].'" is not a valid integer : "'.$this->_values[$i].'".');
							}
						} else {
							$this->addWarning('field "'.$this->_attributes[$i].'" is not a valid number : "'.$this->_values[$i].'".');
						}
					} else if($type=='datetime'){
						if(!$date_validator->isValid($this->_values[$i])){
							$this->addWarning('field "'.$this->_attributes[$i].'" is not a valid date : "'.$this->_values[$i].'".');
						}
					}
					if (empty($this->_values[$i]) && $attr->getData('is_required')) {
						$this->addWarning('field "'.$this->_attributes[$i].'" is empty and no default value has been specified.');
					}
					if(count($this->_warnings)==$count_warnings && $count_errors==count($this->_errors)){
						$importdatas[$line-2][$this->_attributes[$i]]=$this->_values[$i];
					} else {
						$name='import_redo_attribute_value_'.$attrId.'_'.$line;
						$this->addWrongField($name, $attrId, $this->_attributes[$i], $type, $this->_values[$i], $attr);
					}
				}
			}
			$this->setParam('treated_skus', $treatedSkus, false);
			$this->setParam('missing_fields_to_fill', $missing_fields_to_fill, false);
		} else {
			$this->addError('Incorrect file.');
			return self::STATUS_FATAL;
		}
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){ 
			$this->setImportdatas($importdatas);
			$this->setSummary(count($importdatas).' product\'s will be updated.<br/>Fields to update : '.implode(', ', $requiredFields));
		}
		return $this->_status;
	}
	
	public function getRequiredFields(){
		if(!count($this->_required_fields)){
			if(count($this->getParam('required_fields'))){
				$this->_required_fields=$this->getParam('required_fields');
			}
		}
		return $this->_required_fields;
	}
	
	public function import($line){
		try{
			$session=Mage::getSingleton('core/session');
			
			$datas=$this->getImportdatas();
			for($i=0;$i<$line;$i++){
				next($datas);
			}
			$line=key($datas);
			$store = $datas[$line]['store'] ? $datas[$line]['store'] : null;
			
			$product=Mage::getModel('catalog/product')->loadByAttribute('sku', $datas[$line]['sku']);

			if(!$product){
				$this->addError('Unknown product');
				return;
			}
			
			Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
			$product->setStoreId(Mage::app()->getStore($store)->getId());
			$productId=$product->getId();
			
			$this->processCategoryPath($datas[$line]);
			
			/*---------- <Media handling> ----------*/
			$this->importImages($product, $datas[$line]);
			/*---------- </Media handling> ----------*/

			if(!Mage::getModel('catalog/product_api')->update($productId, $datas[$line], $store)){
				$this->addError('Update failed.');
			}
			
		} catch(Mage_Api_Exception $e){
			$this->addError('Failed to update the product : '.$e->getCustomMessage());
		} catch(Exception $e){
			$this->addError($e->getMessage());
		}
		return $line;
	}
	
	public function step5(){
		Mage::getSingleton('core/session')->unsPatchcache();
	}
}
?>