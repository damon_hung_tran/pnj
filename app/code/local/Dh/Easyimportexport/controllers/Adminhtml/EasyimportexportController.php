<?php

class Dh_Easyimportexport_Adminhtml_EasyimportexportController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()->_setActiveMenu('system/convert')->_addBreadcrumb(Mage::helper('easyimportexport')->__('Dh Easyimport'), Mage::helper('easyimportexport')->__('Dh Easyimport'));
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('easyimportexport')->__('Dh Easyimport') . ' / Magento Admin');
        return $this;
    }

    /**
     * 
     * Load the page
     */
    public function indexAction() {
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * 
     * Process one line of the datas. "lineNb" parameter is required
     */
    public function runAction() {
        $line = Mage::app()->getRequest()->getParam('lineNb');
        if (is_numeric($line)) {
            if (Mage::getSingleton('core/session')->hasActionkey()) {
                $action = Mage::getModel(Mage::getSingleton('core/session')->getActionkey());
                if ($action instanceof Dh_Easyimportexport_Model_Action_Abstract) {
                    $line = $action->import($line);
                    $errors = $action->getErrorsString($line + 2);
                    if (strlen($errors)) {
                        echo $errors;
                    } else {
                        echo 1;
                    }
                } else {
                    Mage::getSingleton('core/session')->unsActionkey();
                    echo 'Bad action specified.';
                }
            } else {
                echo 'No action specified.';
            }
        } else {
            echo 'No line number given.';
        }
    }

    /**
     * 
     * Validation of one line. "lineNb" parameter is required
     */
    public function validateAction() {
        $datas = array('status' => -2, 'messages' => 'Wrong parameter sent.');
        $line = Mage::app()->getRequest()->getParam('lineNb');
        if (is_numeric($line)) {
            if (Mage::getSingleton('core/session')->hasActionkey()) {
                $action = Mage::getModel(Mage::getSingleton('core/session')->getActionkey());
                if ($action instanceof Dh_Easyimportexport_Model_Action_Abstract) {
                    $datas['status'] = $action->validate($line);
                    $datas['messages'] = $action->getErrorsString($line) . $action->getWarningsString($line);
                    //$datas['quick_fix']=$action->getWrongFieldsString($line);
                    if ($datas['status'] == 0) {
                        $datas['log_file'] = '/' . $action->getLogFilename();
                    }
                }
            }
        }
        echo json_encode($datas);
    }

    /**
     * 
     * Retrieve the informations to display if the selected action. "actionKey" parameter is required
     */
    public function getadditionalinfosAction() {
        $action = Mage::getModel(Mage::app()->getRequest()->getParam('actionKey'));
        if ($action instanceof Dh_Easyimportexport_Model_Action_Abstract) {
            echo $action->getAdditionalInfos();
        }
    }

}