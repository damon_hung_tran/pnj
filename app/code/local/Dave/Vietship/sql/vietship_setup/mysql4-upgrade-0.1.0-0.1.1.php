<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `{$this->getTable('vietship/insurance')}` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `country_id` varchar(4) DEFAULT '0' COMMENT 'Destination coutry ISO/2 or ISO/3 code',
  `region_id` int(11) DEFAULT '0' COMMENT 'Destination Region Id',
  `district_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Dest district',
  `payment_method` varchar(20) DEFAULT NULL,
  `insurance_cost` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `website_id` (`website_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();