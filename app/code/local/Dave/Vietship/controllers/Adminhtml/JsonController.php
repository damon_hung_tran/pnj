<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Json controller
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dave_Vietship_Adminhtml_JsonController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Return JSON-encoded array of country regions
     *
     * @return string
     */
    public function countryRegionAction()
    {
        $arrRes = array();
		$arrCountryId = array();
		$countriesArray = array();
        $countryId = $this->getRequest()->getParam('parent');
        if(strpos($countryId, '-') !== false){
        	$arrCountryId = explode('-', $countryId);
        }else{
        	$arrCountryId = array($countryId);
        }
        $countriesArray = Mage::getResourceModel('directory/country_collection')
        				->addCountryCodeFilter($arrCountryId)
				        ->toOptionArray(false);
		$this->_countries = array();
        foreach ($countriesArray as $a) {
        	$this->_countries[$a['value']] = $a['label'];
        }
        
        $countryRegions = array();
        if(!empty($arrCountryId)){
        	$regionsCollection = Mage::getResourceModel('directory/region_collection')
        	->addCountryFilter($arrCountryId);
        }else{
        	$arrRegions = Mage::getResourceModel('directory/region_collection')
        	->addCountryFilter($countryId);
        }
        
        foreach ($regionsCollection as $region) {
        	$countryRegions[$region->getCountryId()][$region->getId()] = $region->getDefaultName();
        }
        uksort($countryRegions, array($this, 'sortRegionCountries'));
        
        $this->_options = array();
        foreach ($countryRegions as $countryId=>$regions) {
        	$regionOptions = array();
        	foreach ($regions as $regionId=>$regionName) {
        		$regionOptions[] = array('label'=>$regionName, 'value'=>$regionId);
        	}
        	$this->_options[] = array('label'=>$this->_countries[$countryId], 'value'=>$regionOptions);
        }
        $options = $this->_options;
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($options));
    }
    private function sortRegionCountries($a, $b)
    {
    	return strcmp($this->_countries[$a], $this->_countries[$b]);
    }
    private function sortDistrictRegions($a, $b)
    {
    	return strcmp($this->_regions[$a], $this->_regions[$b]);
    }
    /**
     * Return JSON-encoded array of regions district
     *
     * @return string
     */
    public function regionDistrictAction()
    {
    	$arrDis = array();
    	$arrRegionId = array();
    	$arrRegionCode = array();
    	$regionsArray = array();
    	
    	$regionId = $this->getRequest()->getParam('parent');
    	
    	if(strpos($regionId, '-') !== false){
    		$arrRegionId = explode('-', $regionId);
    	}
    	if(!empty($arrRegionId)){
    		$regionsArray = Mage::getResourceModel('directory/region_collection')
	    		->addFieldToFilter('main_table.region_id', array('in' => $arrRegionId))
	    		->toOptionArray(false);
    	}else{
    		$regionsArray = Mage::getResourceModel('directory/region_collection')
	    		->addFieldToFilter('main_table.region_id', $regionId)
	    		->toOptionArray(false);
    	}
    	
    	$this->_regions = array();
    	
    	foreach ($regionsArray as $a) {
    		$this->_regions[$a['value']] = $a['label'];
    	}
    	
    	$regionDistricts = array();
    	
    	if(!empty($this->_regions)){
    		$districtCollection = Mage::getModel('district/mysql4_district_collection')
    					   ->addFieldToFilter('main_table.region_id',array('in' => array_keys($this->_regions)));
    		
    		foreach ($districtCollection as $district) {
    			$regionDistricts[$district->getRegionId()][$district->getId()] = $district->getDefaultName();
    		}
    		uksort($regionDistricts, array($this, 'sortDisrictRegions'));
    		
    	}
    	$this->_options = array();
    	foreach ($regionDistricts as $regionId=>$districts) {
    		$districtOptions = array();
    		foreach ($districts as $districtId=>$districtName) {
    			$districtOptions[] = array('label'=>$districtName, 'value'=>$districtId);
    		}
    		$this->_options[] = array('label'=>$this->_regions[$regionId], 'value'=>$districtOptions);
    	}
    	$options = $this->_options;
    	$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($options));
    	
    }
}
