<?php
	
class Dave_Vietship_Block_Adminhtml_Carrier_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "vietship";
				$this->_controller = "adminhtml_carrier";
				$this->_updateButton("save", "label", Mage::helper("vietship")->__("Save Carrier"));
				$this->_updateButton("delete", "label", Mage::helper("vietship")->__("Delete Carrier"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("vietship")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);

				
				$this->_addButton('export_csv', array(
						'label'     => Mage::helper('adminhtml')->__('Export Rate CSV'),
						'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/exportRate', array($this->_objectId => $this->getRequest()->getParam($this->_objectId))). '\')',
				), -1);


				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("carrier_data") && Mage::registry("carrier_data")->getId() ){

				    return Mage::helper("vietship")->__("Edit Carrier '%s'", $this->htmlEscape(Mage::registry("carrier_data")->getName()));

				} 
				else{

				     return Mage::helper("vietship")->__("Add Carrier");

				}
		}
}