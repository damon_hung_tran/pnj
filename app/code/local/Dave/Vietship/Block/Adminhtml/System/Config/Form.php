<?php 
class Dave_Vietship_Block_Adminhtml_System_Config_Form extends Mage_Adminhtml_Block_System_Config_Form
{
	/**
	 * Enter description here...
	 *
	 * @return array
	 */
	protected function _getAdditionalElementTypes()
	{	
		return array(
				'exportinsurance'=> Mage::getConfig()->getBlockClassName('vietship/adminhtml_system_config_form_field_exportinsurance'),
				'export'        => Mage::getConfig()->getBlockClassName('adminhtml/system_config_form_field_export'),
				'import'        => Mage::getConfig()->getBlockClassName('adminhtml/system_config_form_field_import'),
				'allowspecific' => Mage::getConfig()
				->getBlockClassName('adminhtml/system_config_form_field_select_allowspecific'),
				'image'         => Mage::getConfig()->getBlockClassName('adminhtml/system_config_form_field_image'),
				'file'          => Mage::getConfig()->getBlockClassName('adminhtml/system_config_form_field_file')
		);
	}
	
}
?>