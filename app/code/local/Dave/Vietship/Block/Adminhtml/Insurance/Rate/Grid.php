<?php

class Dave_Vietship_Block_Adminhtml_Insurance_Rate_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId("rateGrid");
        $this->setDefaultSort("id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel("vietship/insurance_collection");
        $websiteCode = $this->getRequest()->getParam('website');
        $website = Mage::app()->getWebsite($websiteCode);
        if ($website->getId()) {
            $collection->addFieldToFilter('website_id', $website->getId());
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn("country_id", array(
            "header" => Mage::helper("vietship")->__("Country"),
            "align" => "left",
            "index" => "country_id",
        ));

        $this->addColumn("region_id", array(
            "header" => Mage::helper("vietship")->__("Region/City"),
            "align" => "left",
            "index" => "region_id",
        ));

        $this->addColumn("district_id", array(
            "header" => Mage::helper("vietship")->__("District"),
            "align" => "left",
            "index" => "district_id",
        ));

        $this->addColumn("payment_method", array(
            "header" => Mage::helper("vietship")->__("Payment method"),
            "align" => "left",
            "index" => "payment_method",
        ));

        $this->addColumn("insurance_cost", array(
            "header" => Mage::helper("vietship")->__("Rate Insurance"),
            "align" => "right",
            "type" => "number",
            "index" => "insurance_cost",
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_carrier', array(
            'label' => Mage::helper('vietship')->__('Remove Carrier'),
            'url' => $this->getUrl('*/adminhtml_carrier/massRemove'),
            'confirm' => Mage::helper('vietship')->__('Are you sure?')
        ));
        return $this;
    }

}
