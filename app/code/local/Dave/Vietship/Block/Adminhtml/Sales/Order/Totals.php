<?php
class Dave_Vietship_Block_Adminhtml_Sales_Order_Totals extends Mage_Adminhtml_Block_Sales_Order_Totals
{

    protected function _initTotals()
    {
		parent::_initTotals();
    	$shipinsuranceTotal= $this->getSource()->getShipinsuranceTotal();
        
        $this->_totals['shipinsurance_total'] = new Varien_Object(array(
        		'code'      => 'shipinsurance_total',
        		'value'     => $shipinsuranceTotal,
        		'base_value'=> $shipinsuranceTotal,
        		'label'     => Mage::helper ( 'vietship' )->__ ( 'Phí bảo hiểm' )
        ));
        return $this;
    }
}
