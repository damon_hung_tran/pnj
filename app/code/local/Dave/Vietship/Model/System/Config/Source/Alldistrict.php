<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Dave_Vietship_Model_System_Config_Source_Alldistrict
{
    protected $_regions;
    protected $_options;

    public function toOptionArray($isMultiselect=false)
    {
        if (!$this->_options) {
            $regionsArray = Mage::getResourceModel('directory/region_collection')->load()->toOptionArray();
            $this->_regions = array();
            foreach ($regionsArray as $a) {
                $this->_regions[$a['value']] = $a['label'];
            }
            
            $regionDistricts = array();
            $districtArray = Mage::getModel('district/mysql4_district_collection')->getData();
            foreach ($districtArray as $district) {
                $regionDistricts[$district['region_id']][$district['district_id']] = $district['default_name'];
            }
            uksort($regionDistricts, array($this, 'sortRegionDistricts'));

            $this->_options = array();
            foreach ($regionDistricts as $regionId=>$districts) {
                $districtOptions = array();
                foreach ($districts as $districtId=>$districtName) {
                    $districtOptions[] = array('label'=>$districtName, 'value'=>$districtId);
                }
                $this->_options[] = array('label'=>$this->_regions[$regionId], 'value'=>$districtOptions);
            }
        }
        $options = $this->_options;
        if(!$isMultiselect){
            array_unshift($options, array('value'=>'', 'label'=>''));
        }

        return $options;
    }

    public function sortRegionDistricts($a, $b)
    {
        return strcmp($this->_regions[$a], $this->_regions[$b]);
    }
}
