<?php
class Dave_Vietship_Model_Order_Creditmemo_Total_Shipinsurance extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract {
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
		return $this;
		
		$order = $creditmemo->getOrder ();
		$orderShipinsuranceTotal = $order->getShipinsuranceTotal ();
		
		if ($orderShipinsuranceTotal) {
			$creditmemo->setGrandTotal ( $creditmemo->getGrandTotal () + $orderShipinsuranceTotal );
			$creditmemo->setBaseGrandTotal ( $creditmemo->getBaseGrandTotal () + $orderShipinsuranceTotal );
		}
		
		return $this;
	}
}