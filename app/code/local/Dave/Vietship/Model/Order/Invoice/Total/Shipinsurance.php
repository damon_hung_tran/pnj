<?php
class Dave_Vietship_Model_Order_Invoice_Total_Shipinsurance extends Mage_Sales_Model_Order_Invoice_Total_Abstract {
	public function collect(Mage_Sales_Model_Order_Invoice $invoice) {
		$order = $invoice->getOrder ();
		$orderShipinsuranceTotal = $order->getShipinsuranceTotal ();
		if ($orderShipinsuranceTotal && count ( $order->getInvoiceCollection () ) == 0) {
			$invoice->setGrandTotal ( $invoice->getGrandTotal () + $orderShipinsuranceTotal );
			$invoice->setBaseGrandTotal ( $invoice->getBaseGrandTotal () + $orderShipinsuranceTotal );
		}
		return $this;
	}
}