<?php

class Dave_Vietship_Model_Insurance extends Mage_Core_Model_Resource_Db_Abstract {

    /**
     * Import table rates website ID
     *
     * @var int
     */
    protected $_importWebsiteId = 0;

    /**
     * Errors in import process
     *
     * @var array
     */
    protected $_importErrors = array();

    /**
     * Count of imported table rates
     *
     * @var int
     */
    protected $_importedRows = 0;

    /**
     * Array of unique table rate keys to protect from duplicates
     *
     * @var array
     */
    protected $_importUniqueHash = array();

    /**
     * Array of countries keyed by iso2 code
     *
     * @var array
     */
    protected $_importIso2Countries;

    /**
     * Array of countries keyed by iso3 code
     *
     * @var array
     */
    protected $_importIso3Countries;

    /**
     * Associative array of countries and regions
     * [country_id][region_code] = region_id
     *
     * @var array
     */
    protected $_importRegions;

    /**
     * Associative array of regions and district
     * [region_id] = district_id
     *
     * @var array
     */
    protected $_importDistricts;
    protected $_importPayments;

    protected function _construct() {
        $this->_init("vietship/insurance", 'id');
    }

    /**
     * Load directory district
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryDistricts() {
        if (!is_null($this->_importDistricts)) {
            return $this;
        }

        $this->_importDistricts = array();

        /** @var $collection Mage_Directory_Model_Region_Collection */
        $collection = Mage::getModel('district/mysql4_district_collection');
        foreach ($collection->getData() as $row) {
            $this->_importDistricts[$row['region_id']][$row['district_id']] = $row['default_name'];
        }

        return $this;
    }

    /**
     * Load directory regions
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryRegions() {
        if (!is_null($this->_importRegions)) {
            return $this;
        }

        $this->_importRegions = array();

        /** @var $collection Mage_Directory_Model_Resource_Region_Collection */
        $collection = Mage::getResourceModel('directory/region_collection');
        foreach ($collection->getData() as $row) {
            $this->_importRegions[$row['country_id']][$row['region_id']] = (int) $row['region_id'];
        }

        return $this;
    }

    /**
     * Load directory countries
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryCountries() {
        if (!is_null($this->_importIso2Countries) && !is_null($this->_importIso3Countries)) {
            return $this;
        }

        $this->_importIso2Countries = array();
        $this->_importIso3Countries = array();

        /** @var $collection Mage_Directory_Model_Resource_Country_Collection */
        $collection = Mage::getResourceModel('directory/country_collection');
        foreach ($collection->getData() as $row) {
            $this->_importIso2Countries[$row['iso2_code']] = $row['country_id'];
            $this->_importIso3Countries[$row['iso3_code']] = $row['country_id'];
        }

        return $this;
    }

    /**
     * Load directory payments
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryPayments() {
        if (!is_null($this->_importPayments)) {
            return $this;
        }

        $this->_importPayments = array();

        $allAvailablePaymentMethods = Mage::getModel('payment/config')->getAllMethods();

        foreach ($allAvailablePaymentMethods as $paymentMethod) {
            $row = $paymentMethod->getData();
            $this->_importPayments[] = $row['id'];
        }
        return $this;
    }

    /**
     * Validate row for import and return table rate array or false
     * Error will be add to _importErrors array
     *
     * @param array $row
     * @param int $rowNumber
     * @return array|false
     */
    protected function _getImportRow($row, $rowNumber = 0) {
        // validate row
        if (count($row) < 5) {
            $this->_importErrors[] = Mage::helper('vietship')->__('Invalid Table Insurance Rates format in the Row #%s', $rowNumber);
            return false;
        }

        // strip whitespace from the beginning and end of each row
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }

        // validate country
        if (isset($this->_importIso2Countries[$row[0]])) {
            $countryId = $this->_importIso2Countries[$row[0]];
        } elseif (isset($this->_importIso3Countries[$row[0]])) {
            $countryId = $this->_importIso3Countries[$row[0]];
        } elseif ($row[0] == '*' || $row[0] == '') {
            $countryId = '0';
        } else {
            $this->_importErrors[] = Mage::helper('vietship')->__('Invalid Country "%s" in the Row #%s.', $row[0], $rowNumber);
            return false;
        }
        // validate region
        if ($countryId != '0' && isset($this->_importRegions[$countryId][$row[1]])) {
            $regionId = $this->_importRegions[$countryId][$row[1]];
        } elseif ($row[1] == '*' || $row[1] == '') {
            $regionId = 0;
        } else {
            $this->_importErrors[] = Mage::helper('vietship')->__('Invalid Region/City "%s" in the Row #%s.', $row[1], $rowNumber);
            return false;
        }

        // validate district
        if ($regionId != '0' && isset($this->_importDistricts[$regionId][$row[2]])) {
            $districtId = (int) $row[2];
        } elseif ($row[2] == '*' || $row[2] == '') {
            $districtId = 0;
        } else {
            $this->_importErrors[] = Mage::helper('vietship')->__('Invalid District "%s" in the Row #%s.', $row[2], $rowNumber);
            return false;
        }
        //echo '<pre>';print_r($this->_importPayments);exit;
        // detect payment method
        if ($row[3] == '*' || $row[3] == '') {
            $paymentCode = '*';
        } elseif (in_array($row[3], $this->_importPayments)) {
            $paymentCode = $row[3];
        } else {
            $this->_importErrors[] = Mage::helper('vietship')->__('Invalid Payment method "%s" in the Row #%s.', $row[3], $rowNumber);
            return false;
        }

        // validate price
        $price = $this->_parseDecimalValue($row[4]);
        if ($price === false) {
            $this->_importErrors[] = Mage::helper('vietship')->__('Invalid Insurance Price "%s" in the Row #%s.', $row[4], $rowNumber);
            return false;
        }

        // protect from duplicate
        $hash = sprintf("%s-%d-%d-%s", $countryId, $regionId, $districtId, $paymentCode);
        if (isset($this->_importUniqueHash[$hash])) {
            $this->_importErrors[] = Mage::helper('vietship')->__('Duplicate Row #%s (Country "%s", Region/City "%s", District "%s", Payment "%s"  ).', $rowNumber, $row[0], $row[1], $row[2], $row[3]);
            return false;
        }
        $this->_importUniqueHash[$hash] = true;

        return array(
            $this->_importWebsiteId, // website_id
            $countryId, // country_id
            $regionId, // region_id,
            $districtId, // district_id,
            $paymentCode, // payment_method,
            $price                      // price
        );
    }

    public function uploadAndImport(Varien_Object $object) {
        if (empty($_FILES['groups']['tmp_name']['insurances_group']['fields']['import']['value'])) {
            return $this;
        }

        $csvFile = $_FILES['groups']['tmp_name']['insurances_group']['fields']['import']['value'];
        $website = Mage::app()->getWebsite($object->getScopeId());

        $this->_importWebsiteId = (int) $website->getId();
        $this->_importUniqueHash = array();
        $this->_importErrors = array();
        $this->_importedRows = 0;

        $io = new Varien_Io_File();
        $info = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        // check and skip headers
        $headers = $io->streamReadCsv();
        if ($headers === false || count($headers) < 5) {
            $io->streamClose();
            Mage::throwException(Mage::helper('shipping')->__('Invalid Table Rates File Format'));
        }

        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();
        try {
            $rowNumber = 1;
            $importData = array();

            $this->_loadDirectoryCountries();
            $this->_loadDirectoryRegions();
            $this->_loadDirectoryDistricts();
            $this->_loadDirectoryPayments();

            // delete old data by website and condition name
            $condition = array(
                'website_id = ?' => $this->_importWebsiteId,
            );
            $adapter->delete($this->getMainTable(), $condition);

            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;
                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);
                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            $this->_saveImportData($importData);
            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('shipping')->__('An error occurred while import table rates.'));
        }

        $adapter->commit();

        if ($this->_importErrors) {
            $error = Mage::helper('shipping')->__('File has not been imported. See the following list of errors: %s', implode(" \n", $this->_importErrors));
            Mage::throwException($error);
        }

        return $this;
    }

    /**
     * Parse and validate positive decimal value
     * Return false if value is not decimal or is not positive
     *
     * @param string $value
     * @return bool|float
     */
    protected function _parseDecimalValue($value) {
        if (!is_numeric($value)) {
            return false;
        }
        $value = (float) sprintf('%.4F', $value);
        if ($value < 0.0000) {
            return false;
        }
        return $value;
    }

    /**
     * Save import data batch
     *
     * @param array $data
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _saveImportData(array $data) {
        if (!empty($data)) {
            $columns = array('website_id', 'country_id', 'region_id', 'district_id',
                'payment_method', 'insurance_cost');
            $this->_getWriteAdapter()->insertArray($this->getMainTable(), $columns, $data);
            $this->_importedRows += count($data);
        }

        return $this;
    }

//    /**
//     * Return table rate array or false by rate request
//     *
//     * @param Mage_Shipping_Model_Rate_Request $request
//     * @return array|boolean
//     */
//    public function getRate($websiteId, $countryId, $regionId, $districtId, $selectedMethod)
//    {
//    	$adapter = $this->_getReadAdapter();
//    	$bind = array(
//    			':website_id' => (int) $websiteId,
//    			':country_id' => $countryId,
//    			':region_id' => $regionId,
//    			':district_id' => $districtId,
//    			':payment_method' => $selectedMethod,
//    	); 
//    	$select = $adapter->select()
//    	->from($this->getMainTable())
//    	->where('website_id = :website_id')
//    	->order(array('country_id DESC', 'region_id DESC', 'district_id DESC', 'payment_method DESC'))
//    	->limit(1);
//    
//    	// Render destination condition
//    	$orWhere = '(' . implode(') OR (', array(
//    			"payment_method = :payment_method AND country_id = :country_id AND region_id = :region_id AND district_id = :district_id",
//    			"payment_method = :payment_method AND country_id = :country_id AND region_id = :region_id AND district_id = 0",
//    
//    			// Handle asterix in dest_zip field
//    			"payment_method = :payment_method AND country_id = :country_id AND region_id = 0 AND district_id = 0",
//    			"payment_method = :payment_method AND country_id = '0' AND region_id = 0 AND district_id = 0",
//
//    	)) . ')';
//    	$select->where($orWhere);
//    
//    	$result = $adapter->fetchRow($select, $bind);
//    	// Normalize destination zip code
//    	if ($result && $result['district_id'] == '*') {
//    		$result['district_id'] = '';
//    	} 
//    	return $result;
//    }
    /**
     * Return table rate array or false by rate request
     * 
     * Insurance is using only location condition
     * 
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return array|boolean
     */
    public function getRate($websiteId, $countryId, $regionId, $districtId, $selectedMethod) {
        $adapter = $this->_getReadAdapter();
        $bind = array(
            ':website_id' => (int) $websiteId,
            ':country_id' => $countryId,
            ':region_id' => $regionId,
            ':district_id' => $districtId,
            ':payment_method' => $selectedMethod,
        );
        $select = $adapter->select()
                ->from($this->getMainTable())
                ->where('website_id = :website_id')
                ->order(array('country_id DESC', 'region_id DESC', 'district_id DESC'))
                ->limit(1);

        // Render destination condition
        $orWhere = '(' . implode(') OR (', array(
                    "country_id = :country_id AND region_id = :region_id AND district_id = :district_id",
                    "country_id = :country_id AND region_id = :region_id AND district_id = 0",
                    // Handle asterix in dest_zip field
                    "country_id = :country_id AND region_id = 0 AND district_id = 0",
                    "country_id = '0' AND region_id = 0 AND district_id = 0",
                )) . ')';
        $select->where($orWhere);

        $result = $adapter->fetchRow($select, $bind);
        // Normalize destination zip code
        if ($result && $result['district_id'] == '*') {
            $result['district_id'] = '';
        }
        return $result;
    }

}
