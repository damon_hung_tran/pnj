<?php
class Dave_Vietship_Model_Newordertotalobserver {
    public function saveShipinsuranceTotal(Varien_Event_Observer $observer) {
		$order = $observer->getEvent ()->getOrder ();
		$quote = $observer->getEvent ()->getQuote ();
		$shippingAddress = $quote->getShippingAddress ();
		if ($shippingAddress && $shippingAddress->getData ( 'shipinsurance_total' )) {
			$order->setData ( 'shipinsurance_total', $shippingAddress->getData ( 'shipinsurance_total' ) );
		} else {
			$billingAddress = $quote->getBillingAddress ();
			$order->setData ( 'shipinsurance_total', $billingAddress->getData ( 'shipinsurance_total' ) );
		}
		$order->save ();
	}
	public function saveShipinsuranceTotalForMultishipping(Varien_Event_Observer $observer) {
		$order = $observer->getEvent ()->getOrder ();
		$shippingMethod = $order->getShippingMethod();
		Mage::log('$shippingMethod: '.$shippingMethod, null,'vietship.log', true);
		$shipcode = Mage::getModel('vietship/carrier_vnTablerate')->getCode();
		
		if(substr_count($shippingMethod, $shipcode)){
			
			$carrier_code = str_replace($shipcode.'_', '', $shippingMethod);
			if($carrier_code){
				$order = $observer->getEvent ()->getOrder ();
				$address = $observer->getEvent ()->getAddress ();
				$order->setData ( 'shipinsurance_total', $shippingAddress->getData ( 'shipinsurance_total' ) );
				$order->save ();
			}
		}
		
	}
}