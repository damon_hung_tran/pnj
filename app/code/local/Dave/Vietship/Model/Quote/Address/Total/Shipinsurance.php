<?php

class Dave_Vietship_Model_Quote_Address_Total_Shipinsurance extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    var $_percent = null;

    public function __construct() {
        $this->setCode('shipinsurance_total');
    }

    /**
     * Collect totals information about shipinsurance
     *
     * @param Mage_Sales_Model_Quote_Address $address        	
     * @return Mage_Sales_Model_Quote_Address_Total_Shipping
     */
    public function collect(Mage_Sales_Model_Quote_Address $address) {

        parent::collect($address);
        //FB::log($address->getShippingMethod(),'$getShippingMethod');
        $quote = $address->getQuote();
        $totals = $quote->getTotals();
        $subTotal = $totals['subtotal']->getValue();
        $websiteId = Mage::app()->getWebsite()->getId();
        if ($subTotal >= Mage::getStoreConfig('insurances/insurances_group/minimum_to_get_fee', $websiteId))
            return $this;
        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }
        $countryId = $quote->getShippingAddress()->getCountryId();
        $regionId = $quote->getShippingAddress()->getRegionId();
        $shippingParams = Mage::app()->getRequest()->getParam('shipping');
        $billingParams = Mage::app()->getRequest()->getParam('billing');
        $districtName = $shippingParams['city'];
        if (empty($districtName) && $billingParams['use_for_shipping']) {
            $districtName = $billingParams['city'];
        }
        $collectionDistrict = Mage::getModel('district/mysql4_district_collection')
                ->addFieldToFilter('region_id', $regionId)
                ->addFieldToFilter('default_name', $districtName);
        foreach ($collectionDistrict->getData() as $district) {
            $districtId = $district['district_id'];
        }
        $paymentMethod = Mage::app()->getRequest()->getParam('payment');
        $selectedMethod = $paymentMethod['method'];
        if (empty($selectedMethod)) {
            $selectedMethod = Mage::getStoreConfig('onestepcheckout/general/default_payment_method');
        }
//        $rates = Mage::getModel('vietship/insurance')->getRate($websiteId, $countryId, $regionId, $districtId, $selectedMethod);
        $rates = Mage::getModel('vietship/insurance')->getRate($websiteId, $countryId, $regionId, $districtId); // insurance is using only location condition.
        if (!empty($rates['insurance_cost']))
            $shipinsuranceAmount = $rates['insurance_cost'];
        else
            $shipinsuranceAmount = 0;

        if ($shipinsuranceAmount > 0) {
            $this->_percent = $shipinsuranceAmount;
            $shipinsuranceAmount = $shipinsuranceAmount * $subTotal / 100;
        }
        $shipinsuranceAmount = $quote->getStore()->roundPrice($shipinsuranceAmount);
        $this->_setAmount($shipinsuranceAmount)->_setBaseAmount($shipinsuranceAmount);
        $address->setData('shipinsurance_total', $shipinsuranceAmount);

        $address->setGrandTotal($address->getGrandTotal() + $address->getShipinsuranceTotal());
        Mage::log('GrandTotal' . $address->getGrandTotal());
        $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseShipinsuranceTotal());

        return $this;
    }

    public function getRateByRequest(Mage_Shipping_Model_Rate_Request $request) {
        
    }

    /**
     * Add shipinsurance totals information to address object
     *
     * @param Mage_Sales_Model_Quote_Address $address        	
     * @return Mage_Sales_Model_Quote_Address_Total_Shipping
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        parent::fetch($address);
        $amount = $address->getTotalAmount($this->getCode());
        if ($amount != 0) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $this->getLabel(),
                'value' => $amount
            ));
        }

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel() {
        return Mage::helper('vietship')->__('Phí bảo hiểm') . ' (' . $this->_percent . '%):';
    }

}
