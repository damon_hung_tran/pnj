<?php
class Dave_Vietship_Model_Shipping_Carrier_Freeshipping extends Mage_Shipping_Model_Carrier_Freeshipping
{
	public function checkAvailableShipCountries(Mage_Shipping_Model_Rate_Request $request)
	{
		$speCountriesAllow = $this->getConfigData('sallowspecific');
		/*
		 * for specific countries, the flag will be 1
		*/
		if ($speCountriesAllow && $speCountriesAllow == 1){
			$showMethod = $this->getConfigData('showmethod');
			$availableCountries = array();
			if($this->getConfigData('specificcountry')) {
				$availableCountries = explode(',',$this->getConfigData('specificcountry'));
			}
			if ($availableCountries && in_array($request->getDestCountryId(), $availableCountries)) {
				return $this;
			} elseif ($showMethod && (!$availableCountries || ($availableCountries
					&& !in_array($request->getDestCountryId(), $availableCountries)))
			){
				$error = Mage::getModel('shipping/rate_result_error');
				$error->setCarrier($this->_code);
				$error->setCarrierTitle($this->getConfigData('title'));
				$errorMsg = $this->getConfigData('specificerrmsg');
				$error->setErrorMessage($errorMsg ? $errorMsg : Mage::helper('shipping')->__('The shipping module is not available for selected delivery country.'));
				return $error;
			} else {
				/*
				 * The admin set not to show the shipping module if the devliery country is not within specific countries
				*/
				return false;
			}
		}
		return $this;
	}
	
}