<?php
class Dave_Vietship_Helper_Data extends Mage_Core_Helper_Abstract {
	
	public function importRateCsv($carrier_id, $csv_file) {
		
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$query = "DELETE FROM `vn_tablerate_rate` WHERE carrier_id =".$carrier_id;
		$writeConnection->query($query);
		
		$allVNRegions = Mage::getModel('directory/region')->getCollection()
						->addFieldToFilter('country_id', 'VN')->toArray();
		
		$regionMapData =array();
		foreach ($allVNRegions['items'] as $region){
			$regionMapData[$region['default_name']]=$region['region_id'];
		}
		$regionMapData['*'] = '*';
		
		$row = 1;
		
		$header = array();
		if (($handle = fopen ( $csv_file, "r" )) !== FALSE) {
			while ( ($data = fgetcsv ( $handle, 1000, "," )) !== FALSE ) {
				//FB::log($data,'data');
				
				if($row==1){
					for ($i=0;$i<count($data);$i++){
						$header[trim($data[$i])]=$i;
					}
					//FB::log($header, '$header');
				}else{
					$regionCode = null;
					if(isset($regionMapData[ $data[$header['Tỉnh thành']]])){
						$regionCode = $regionMapData[ $data[$header['Tỉnh thành']]];
					}else{
						
					}
					
					$md = Mage::getModel('vietship/rate');
					$md->setData('carrier_id', $carrier_id);
					$md->setData('country_code', $data[$header['Quốc gia']]);
					$md->setData('region_code', $regionCode);
					$md->setData('region_name', mb_convert_encoding($data[$header['Tỉnh thành']], 'UTF-8', mb_detect_encoding($data[$header['Tỉnh thành']])) );
					$md->setData('zip', mb_convert_encoding($data[$header['Quận huyện']], 'UTF-8', mb_detect_encoding($data[$header['Quận huyện']]) ));
					$md->setData('weight_from', $data[$header['Khối lượng']]);
					$md->setData('shipping_cost', $data[$header['Giá (VNĐ)']]);
					$md->save();
				}
				$row ++;
			}
			fclose ( $handle );
		}
	}
	
	public function getCarrierByMethodCode($methodCode){
		$shipcode = Mage::getModel('vietship/carrier_vnTablerate')->getCode();
		//FB::log($shipcode,'$shipcode');
		
		$carrier_code = str_replace($shipcode.'_', '', $methodCode);
		//FB::log($carrier_code,'$$carrier_code');
		$carrier = Mage::getModel('vietship/carrier')->load($carrier_code,'code');
		return $carrier;
	}
}
	 