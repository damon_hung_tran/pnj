<?php

class Dave_Vietship_Block_Adminhtml_Carrier_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("carrierGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("vietship/carrier")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("vietship")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
				
				$this->addColumn("code", array(
						"header" => Mage::helper("vietship")->__("Code"),
						"align" =>"left",
						"type" => "text",
						"index" => "code",
				));
				$this->addColumn("name", array(
						"header" => Mage::helper("vietship")->__("Name"),
						"align" =>"left",
						"type" => "text",
						"index" => "name",
				));
				
				$this->addColumn("address", array(
						"header" => Mage::helper("vietship")->__("Address"),
						"align" =>"left",
						"type" => "text",
						"index" => "address",
				));
				
				$this->addColumn("insurance_cost", array(
						"header" => Mage::helper("vietship")->__("Insurance Cost"),
						"align" =>"right",
						"type" => "number",
						"index" => "insurance_cost",
				));
				$this->addColumn("is_active", array(
						"header" => Mage::helper("vietship")->__("Is Active"),
						"align" =>"right",
						"type" => "options",
						"index" => "is_active",
						'options' => array(1=>Mage::helper("vietship")->__("Yes"), 0=>Mage::helper("vietship")->__("No"))
				));
                
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_carrier', array(
					 'label'=> Mage::helper('vietship')->__('Remove Carrier'),
					 'url'  => $this->getUrl('*/adminhtml_carrier/massRemove'),
					 'confirm' => Mage::helper('vietship')->__('Are you sure?')
				));
			return $this;
		}
			

}