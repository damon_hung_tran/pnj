<?php
class Dave_Vietship_Block_Adminhtml_Carrier_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("carrier_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("vietship")->__("Carrier"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("vietship")->__("Information"),
				"title" => Mage::helper("vietship")->__("Information"),
				"content" => $this->getLayout()->createBlock("vietship/adminhtml_carrier_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
