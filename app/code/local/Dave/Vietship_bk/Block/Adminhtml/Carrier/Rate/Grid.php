<?php

class Dave_Vietship_Block_Adminhtml_Carrier_Rate_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("rateGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("vietship/rate")->getCollection();
				
				
				$id = $this->getRequest()->getParam('id');
				if($id){
					$collection->addFieldToFilter('carrier_id', $id);
				}
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("country_code", array(
						"header" => Mage::helper("vietship")->__("Quốc gia"),
						"align" =>"left",
						"index" => "country_code",
				));
				
				$this->addColumn("region_name", array(
						"header" => Mage::helper("vietship")->__("Tỉnh thành"),
						"align" =>"left",
						"index" => "region_name",
				));
				
				$this->addColumn("zip", array(
						"header" => Mage::helper("vietship")->__("Quận huyện"),
						"align" =>"left",
						"index" => "zip",
				));
				
				$this->addColumn("weight_from", array(
						"header" => Mage::helper("vietship")->__("Khối lượng"),
						"align" =>"left",
						"index" => "weight_from",
				));
				
				$this->addColumn("shipping_cost", array(
						"header" => Mage::helper("vietship")->__("Giá (VNĐ)"),
						"align" =>"right",
						"type" => "number",
						"index" => "shipping_cost",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_carrier', array(
					 'label'=> Mage::helper('vietship')->__('Remove Carrier'),
					 'url'  => $this->getUrl('*/adminhtml_carrier/massRemove'),
					 'confirm' => Mage::helper('vietship')->__('Are you sure?')
				));
			return $this;
		}
			

}