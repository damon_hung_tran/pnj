<?php
class Dave_Vietship_Block_Adminhtml_Carrier_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("vietship_form", array("legend"=>Mage::helper("vietship")->__("Item information")));
				
				$fieldset->addField('code', 'text', array(
						'name'      => 'code',
						'label'     => Mage::helper('vietship')->__('Carrier Code'),
						'title'     => Mage::helper('vietship')->__('Carrier Code'),
						'required'  => true,
				));
				
				$fieldset->addField('name', 'text', array(
						'name'      => 'name',
						'label'     => Mage::helper('vietship')->__('Carrier Name'),
						'title'     => Mage::helper('vietship')->__('Carrier Name'),
						'required'  => true,
				));
				
				$fieldset->addField('is_active', 'select', array(
						'name'      => 'is_active',
						'label'     => Mage::helper('vietship')->__('Active'),
						'title'     => Mage::helper('vietship')->__('Active'),
						'values'  => array('1'=>Mage::helper('vietship')->__('Yes'), '0'=>Mage::helper('vietship')->__('No')),
						
				));
				
				$fieldset->addField('address', 'text', array(
						'name'      => 'address',
						'label'     => Mage::helper('vietship')->__('Carrier Address'),
						'title'     => Mage::helper('vietship')->__('Carrier Address'),
						//'required'  => true,
				));
				
				$fieldset->addField('insurance_cost', 'text', array(
						'name'      => 'insurance_cost',
						'label'     => Mage::helper('vietship')->__('Insurance Cost'),
						'title'     => Mage::helper('vietship')->__('Insurance Cost'),
						'required'  => false,
						'after_element_html' => '(%)'
				));
				
				$fieldset->addField('file', 'file', array(
						'name'      => 'rate_file',
						'label'     => Mage::helper('vietship')->__('Upload Rate CSV'),
						'title'     => Mage::helper('vietship')->__('Upload Rate CSV'),
						//'required'  => false,
						'after_element_html' => '<br/>
						<small>
						Reference and Sample CSV<br/>
						- <a href="'.Mage::getBaseUrl('media').'vietship/sample.csv">Sample file</a></small><br/>
						- <a href="'.Mage::getBaseUrl('media').'vietship/directory_country_region_vn.csv">Region Code</a></small><br/>
						</small>',
				));

				if (Mage::getSingleton("adminhtml/session")->getCarrierData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getCarrierData());
					Mage::getSingleton("adminhtml/session")->setCarrierData(null);
				} 
				elseif(Mage::registry("carrier_data")) {
				    $form->setValues(Mage::registry("carrier_data")->getData());
				}
				return parent::_prepareForm();
		}
}
