<?php
class Dave_Vietship_Block_Adminhtml_Carrier extends Mage_Adminhtml_Block_Widget_Grid_Container {
	public function __construct() {
		$this->_controller = "adminhtml_carrier";
		$this->_blockGroup = "vietship";
		$this->_headerText = Mage::helper ( "vietship" )->__ ( "Carrier Manager" );
		$this->_addButtonLabel = Mage::helper ( "vietship" )->__ ( "Add New Carrier" );
		parent::__construct ();
	}
}