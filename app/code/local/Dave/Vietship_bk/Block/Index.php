<?php   
class Dave_Vietship_Block_Index extends Mage_Core_Block_Template{   


	public function getCarrier(){
		$carrier_code = $this->getRequest()->getParam('carrierCode');
		return Mage::getModel('vietship/carrier')->load($carrier_code,'code');
	}
	public function getRates($carrier){
		//$carrier_code = $this->getRequest()->getParam('carrierCode');
		//$carrier = Mage::getModel('vietship/carrier')->load($carrier_code,'code');
		//FB::log($carrier->getData());
		
		$rates = Mage::getModel('vietship/rate')->getCollection()
				->addFieldToFilter('carrier_id', $carrier->getId())
				//->getSelect()->order(array('country_code ASC','region_name ASC', 'zip ASC', 'weight_from ASC'))
				//->setOrder('country_code', 'ASC')
				;
		/* @var $rates Dave_Vietship_Model_Mysql4_Rate_Collection */
		$rates->addOrder('country_code','ASC');
		$rates->addOrder('region_name','ASC');
		$rates->addOrder('zip','ASC');
		$rates->addOrder('weight_from','ASC');
		//echo
		//FB::log($rates->load()->getSelectSql(true));
		//FB::log($rates->toArray(), '$rates');
		return $rates;
		
	}


}