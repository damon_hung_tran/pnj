<?php  
	require_once 'Fb.php';
    class Dave_Vietship_Model_Carrier_VnTablerate     
		extends Mage_Shipping_Model_Carrier_Abstract
		implements Mage_Shipping_Model_Carrier_Interface
	{  
        protected $_code = 'vn_tablerate';  
      
        public function getCode(){
        	return $this->_code;
        }
        /** 
        * Collect rates for this shipping method based on information in $request 
        * 
        * @param Mage_Shipping_Model_Rate_Request $data 
        * @return Mage_Shipping_Model_Rate_Result 
        */  
        public function collectRates(Mage_Shipping_Model_Rate_Request $request){
        	
        	if (!$this->getConfigFlag('active')) {
        		return false;
        	}
        	
        	// exclude Virtual products price from Package value if pre-configured
        	if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
        		foreach ($request->getAllItems() as $item) {
        			if ($item->getParentItem()) {
        				continue;
        			}
        			if ($item->getHasChildren() && $item->isShipSeparately()) {
        				foreach ($item->getChildren() as $child) {
        					if ($child->getProduct()->isVirtual()) {
        						$request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
        					}
        				}
        			} elseif ($item->getProduct()->isVirtual()) {
        				$request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
        			}
        		}
        	}
        	
        	// Free shipping by qty
        	$freeQty = 0;
        	if ($request->getAllItems()) {
        		$freePackageValue = 0;
        		foreach ($request->getAllItems() as $item) {
        			if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
        				continue;
        			}
        	
        			if ($item->getHasChildren() && $item->isShipSeparately()) {
        				foreach ($item->getChildren() as $child) {
        					if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
        						$freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
        						$freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
        					}
        				}
        			} elseif ($item->getFreeShipping()) {
        				$freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
        				$freeQty += $item->getQty() - $freeShipping;
        				$freePackageValue += $item->getBaseRowTotal();
        			}
        		}
        		$oldValue = $request->getPackageValue();
        		$request->setPackageValue($oldValue - $freePackageValue);
        	}
        	
        	if ($freePackageValue) {
        		$request->setPackageValue($request->getPackageValue() - $freePackageValue);
        	}
//         	if (!$request->getConditionName()) {
//         		$conditionName = $this->getConfigData('condition_name');
//         		$request->setConditionName($conditionName ? $conditionName : $this->_default_condition_name);
//         	}
        	
        	// Package weight and qty free shipping
        	$oldWeight = $request->getPackageWeight();
        	$oldQty = $request->getPackageQty();
        	
        	$request->setPackageWeight($request->getFreeMethodWeight());
        	$request->setPackageQty($oldQty - $freeQty);
        	
        	$result = Mage::getModel('shipping/rate_result');
        	
        	
        	$request->setPackageWeight($oldWeight);
        	$request->setPackageQty($oldQty);
        	
        	
        	
        	$carriers = Mage::getModel('vietship/carrier')->getCollection()
        	->addFieldToFilter('is_active',1);
        	 
        	foreach ($carriers as $carrier){
        		
        		$rate = $this->getRateByRequest($carrier->getId(), $request);
        		//FB::log($rate, $carrier->getId().' RATE');
        		//exit();
        		if (!empty($rate) && $rate['price'] >= 0) {
        			if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
        				$shippingPrice = 0;
        			} else {
        				$shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
        				//FB::log($carrier->getId().' '.$shippingPrice);
        			}
        			
        			$method = Mage::getModel('shipping/rate_result_method');
        			$method->setCarrier($this->_code);
        			$method->setCarrierTitle($this->getConfigData('title'));
        			$method->setMethod($carrier->getCode());
        			$method->setMethodTitle($carrier->getName());
        			$method->setPrice($shippingPrice);
        			$method->setCost($rate['cost']);
        			$result->append($method);
        		}elseif (empty($rate) && $request->getFreeShipping() === true) {
	        		/**
	        		 * was applied promotion rule for whole cart
	        		 * other shipping methods could be switched off at all
	        		 * we must show table rate method with 0$ price, if grand_total more, than min table condition_value
	        		 * free setPackageWeight() has already was taken into account
	        		 */
	        		$request->setPackageValue($freePackageValue);
	        		$request->setPackageQty($freeQty);
	        		$rate = $this->getRateByRequest($carrier->getId(), $request);
	        		if (!empty($rate) && $rate['price'] >= 0) {
	        			$method = Mage::getModel('shipping/rate_result_method');
	        	
	        			$method->setCarrier($this->_code);
	        			$method->setCarrierTitle($this->getConfigData('title'));
	        	
	        			$method->setMethod($carrier->getCode());
	        			$method->setMethodTitle($carrier->getName());
	        	
	        			$method->setPrice(0);
	        			$method->setCost(0);
	        			$result->append($method);
	        		}
	        	}
	        	/*
	        	 else {
	        		$error = Mage::getModel('shipping/rate_result_error');
	        		$error->setCarrier($this->_code);
	        		$error->setCarrierTitle($this->getConfigData('title'));
	        		$error->setErrorMessage($this->getConfigData('specificerrmsg'));
	        		$result->append($error);
	        	}
	        	*/
        		
        	}
        	//FB::log($result,'result');
            return $result;  
        }  

		/**
		 * Get allowed shipping methods
		 *
		 * @return array
		 */
		public function getAllowedMethods()
		{
			return array($this->_code=>$this->getConfigData('name'));
		}
		
		public function getRate($CarrierId, $DestCountryId, $DestRegionId, $DestCity, $package_weight ){
			$rates = Mage::getModel('vietship/rate')->getCollection()
			->addFieldToFilter('carrier_id', $CarrierId)
			->setOrder('weight_from', 'asc');
			
			$rate_arr = array();
			foreach ($rates as $rate){
				$rate_arr[$rate->getCountryCode().'-'.trim($rate->getRegionCode()).'-'.trim($rate->getZip())][$rate->getWeightFrom()]=$rate->getShippingCost();
			}
			FB::log($rate_arr, '$rate_arr_'.$CarrierId);
			FB::log($package_weight, '$package_weight');
			
			$rate_price = null;
			$key =  $DestCountryId.'-'.$DestRegionId.'-'.$DestCity;
			FB::log($key,'$key check');
			if(isset($rate_arr[$key])){
				
				foreach ($rate_arr[$key] as $w=>$cost){
					
					if($w > $package_weight) break;
					else{$rate_price = $cost;}
					
				}
				if(!$rate_price){
					//FB::log('APPLY NO RATE');
					foreach ($rate_arr[$key] as $w=>$cost){
						$rate_price = $cost;
					}
				}
			}else{
				$key =  $DestCountryId.'-'.$DestRegionId.'-*';
				FB::log($key,'$key check');
				if(isset($rate_arr[$key])){
					foreach ($rate_arr[$key] as $w=>$cost){
						if($w >  $package_weight) break;
						else{$rate_price = $cost;}
						
					}
					if(!$rate_price){
						FB::log('APPLY NO RATE');
						foreach ($rate_arr[$key] as $w=>$cost){
							$rate_price = $cost;
						}
					}
				}else{
					$key =  $DestCountryId.'-*-*';
					FB::log($key,'$key check');
					if(isset($rate_arr[$key])){
						
						foreach ($rate_arr[$key] as $w=>$cost){
							if($w >  $package_weight) break;
							else{$rate_price = $cost;}
							
						}
						if(!$rate_price){
							//FB::log('APPLY NO RATE');
							foreach ($rate_arr[$key] as $w=>$cost){
								$rate_price = $cost;
							}
						}
					}else{
						$key =  '*-*-*';
						FB::log($key,'$key check');
						if(isset($rate_arr[$key])){
							
							foreach ($rate_arr[$key] as $w=>$cost){
								if($w >  $package_weight) break;
								else{$rate_price = $cost;}
								
							}
							if(!$rate_price){
								//FB::log('APPLY NO RATE');
								foreach ($rate_arr[$key] as $w=>$cost){
									$rate_price = $cost;
								}
							}
						}
					}
				}
			}
			
			//FB::log($all_rates, '$all_rates');
			
			if($rate_price!==null){
				$result['price'] = $rate_price;
				$result['cost'] = $rate_price;
				FB::log($result, 'RESULT_'.$DestCountryId.'_'.$DestRegionId.'_'.$DestCity.'_'.$package_weight);
			}else{
				//FB::log('NO RATE');
				return null;
			}
			return $result;
		}
		
		public function getRateByRequest($carrier_id, Mage_Shipping_Model_Rate_Request $request){
			$request->setAllItems(array());
			FB::log($request->getData(),'request');
			$param = Mage::app()->getRequest()->getParams();
			FB::log($param, '$param');
			if(!isset($param['billing']['use_for_shipping']) && isset($param['shipping_address_id']) && $param['shipping_address_id']){
				$addr = Mage::getModel('customer/address')->load($param['shipping_address_id']);
				FB::log($addr->getData(),'addr');
				$regionId = $addr->getRegionId();
			}else{
				$regionId = $request->getDestRegionId();
				FB::log($regionId, '$regionId');
				if(!$regionId){
					$collection = Mage::getModel('directory/region')->getResourceCollection()
								->addCountryFilter($request->getDestCountryId())
								->load();
					$regionNameMap = array();
					foreach ($collection as  $item){
						$regionNameMap[$item->getDefaultName()]=$item->getId();
					}
					FB::log($regionNameMap, '$regionNameMap');
					if(isset($regionNameMap[$request->getDestRegionCode()])){
						$regionId = $regionNameMap[$request->getDestRegionCode()];
					}else{
						
						if(isset($param['shipping'])  && isset($param['shipping']['region_id'])){
							$regionId = $param['shipping']['region_id'];
						}else{
							$regionId = $param['billing']['region_id'];
						}
					}
				}
			}
			return $this->getRate($carrier_id, $request->getDestCountryId(), $regionId, $request->getDestCity(), $request->getPackageWeight());
		}
		
		
		public function getRateByRequest1($carrier_id, Mage_Shipping_Model_Rate_Request $request)
    	{
    		$request->setAllItems(array());
    		//FB::log($request->getData(),'request');
    		/*
    		dest_country_id
    		dest_region_id
    		dest_city
    		package_weight
    		*/
    		$package_weight = $request->getPackageWeight();
    		//FB::log($package_weight, '$package_weight');
    		//FB::log($request,'$request');
    		
    		$rates = Mage::getModel('vietship/rate')->getCollection()
    				->addFieldToFilter('carrier_id', $carrier_id)
    				//->addFieldToFilter('country_code', $request->getDestCountryId())
    				//->addFieldToFilter('country_code', $request->getDestCountryId())
    				->setOrder('weight_from', 'asc');
    		
    		$rate_arr = array();
    		foreach ($rates as $rate){
    			$rate_arr[$rate->getCountryCode().'-'.$rate->getRegionCode().'-'.$rate->getZip()][$rate->getWeightFrom()]=$rate->getShippingCost();
    		}
    		//FB::log($rate_arr, '$rate_arr');
    		
    		$rate_price = null;
    		$key =  $request->getDestCountryId().'-'.$request->getDestRegionId().'-'.$request->getDestCity();
    		//FB::log($key,'$key rate');
    		if(isset($rate_arr[$key])){
    			//FB::log(__LINE__);
    			foreach ($rate_arr[$key] as $w=>$cost){
    				if($w >  $package_weight) break;
    				$rate_price = $cost;
    			}
    			if(!$rate_price){
    				foreach ($rate_arr[$key] as $w=>$cost){
    					$rate_price = $cost;
    				}
    			}
    		}else{
    			$key =  $request->getDestCountryId().'-'.$request->getDestRegionId().'-*';
    			if(isset($rate_arr[$key])){
    				//FB::log(__LINE__);
	    			foreach ($rate_arr[$key] as $w=>$cost){
	    				if($w >  $package_weight) break;
	    				$rate_price = $cost;
	    			}
	    			if(!$rate_price){
	    				foreach ($rate_arr[$key] as $w=>$cost){
	    					$rate_price = $cost; 
	    				}
	    			}
    			}else{
    				$key =  $request->getDestCountryId().'-*-*';
    				if(isset($rate_arr[$key])){
    					//FB::log(__LINE__);
	    				foreach ($rate_arr[$key] as $w=>$cost){
		    				if($w >  $package_weight) break;
		    				$rate_price = $cost;
		    			}
		    			if(!$rate_price){
		    				foreach ($rate_arr[$key] as $w=>$cost){
		    					$rate_price = $cost; 
		    				}
		    			}
    				}else{
    					$key =  '*-*-*';
    					if(isset($rate_arr[$key])){
    						//FB::log(__LINE__);
	    					foreach ($rate_arr[$key] as $w=>$cost){
			    				if($w >  $package_weight) break;
			    				$rate_price = $cost;
			    			}
			    			if(!$rate_price){
			    				//FB::log('APPLY NO RATE');
			    				foreach ($rate_arr[$key] as $w=>$cost){
			    					$rate_price = $cost; 
			    				}
			    			}
    					}
    				}
    			}
    		}
    		
    		//FB::log($all_rates, '$all_rates');
    		
    		if($rate_price!==null){
    			$result['price'] = $rate_price;
    			$result['cost'] = $rate_price;
    		}else{
    			return null;
    		}
//     		$rate['price'] = 5;
//     		$rate['cost'] = 5;
			return $result;
		}
    }  
