<?php
class Dave_Vietship_Model_Quote_Address_Total_Shipinsurance extends Mage_Sales_Model_Quote_Address_Total_Abstract {

	var $_percent = null;
	
	public function __construct() {
		$this->setCode ( 'shipinsurance_total' );
	}
	/**
	 * Collect totals information about shipinsurance
	 *
	 * @param Mage_Sales_Model_Quote_Address $address        	
	 * @return Mage_Sales_Model_Quote_Address_Total_Shipping
	 */
	public function collect(Mage_Sales_Model_Quote_Address $address) {
        
		parent::collect ( $address );
		//FB::log($address->getShippingMethod(),'$getShippingMethod');
		$items = $this->_getAddressItems ( $address );
		if (! count ( $items )) {
			return $this;
		}
		$quote = $address->getQuote();
		
		$ship_method = $address->getShippingMethod();
		$shipcode = Mage::getModel('vietship/carrier_vnTablerate')->getCode();
		FB::log($shipcode,'$shipcode');
		
		$shipinsuranceAmount = 0;
		
		if(substr_count($ship_method, $shipcode)){
			
			$carrier_code = str_replace($shipcode.'_', '', $ship_method);
			
			if($carrier_code){
				FB::log($carrier_code,'$$carrier_code');
				$carrier = Mage::getModel('vietship/carrier')->load($carrier_code,'code');
				FB::log($carrier->getData(),'$carrier');
				
				// amount definition
				$totals = $quote->getTotals();
				//FB::log($totals);
				//echo "<pre>";print_r($totals['subtotal']);echo "<pre>";
				//FB::log($totals['subtotal']->toArray(), 'total');
				$this->_percent = $carrier->getData('insurance_cost');
				$shipinsuranceAmount = $carrier->getData('insurance_cost')*$totals['subtotal']->getValue()/100;
			}
		}
			// amount definition
			
		$shipinsuranceAmount = $quote->getStore ()->roundPrice ( $shipinsuranceAmount );
		$this->_setAmount ( $shipinsuranceAmount )->_setBaseAmount ( $shipinsuranceAmount );
		$address->setData ( 'shipinsurance_total', $shipinsuranceAmount );
		
		$address->setGrandTotal($address->getGrandTotal() + $address->getShipinsuranceTotal());
		$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseShipinsuranceTotal());

		return $this;
	}
	
	/**
	 * Add shipinsurance totals information to address object
	 *
	 * @param Mage_Sales_Model_Quote_Address $address        	
	 * @return Mage_Sales_Model_Quote_Address_Total_Shipping
	 */
	public function fetch(Mage_Sales_Model_Quote_Address $address) {
		parent::fetch ( $address );
		$amount = $address->getTotalAmount ( $this->getCode () );
		if ($amount != 0) {
			$address->addTotal ( array (
					'code' => $this->getCode (),
					'title' => $this->getLabel (),
					'value' => $amount 
			) );
		}
		
		return $this;
	}
	
	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return Mage::helper ( 'vietship' )->__ ( 'Phí bảo hiểm' ).' ('.$this->_percent.'%):';
	}
}