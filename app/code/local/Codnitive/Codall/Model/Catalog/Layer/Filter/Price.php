<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Layer price filter
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */

/**
 * @method Mage_Catalog_Model_Layer_Filter_Price setInterval(array)
 * @method array getInterval()
 */
class Codnitive_Codall_Model_Catalog_Layer_Filter_Price extends Mage_Catalog_Model_Layer_Filter_Price
{
    const XML_PATH_RANGE_ARRAY     = 'catalog/layered_navigation/price_range_array';
    const RANGE_CALCULATION_ARRAY   = 'array';
    const ONE = 1;
    /**
     * Get information about products count in range
     *
     * @param   int $range
     * @return  int
     */
    public function getRangeItemCounts($range)
    {
        $rangeKey = 'range_item_counts_' . $range;
        $items = $this->getData($rangeKey);
        if (is_null($items)) {
            $items = $this->_getResource()->getCount($this, $range); 
            // checking max number of intervals
            $i = 0;
            $lastIndex = null;
            $maxIntervalsNumber = $this->getMaxIntervalsNumber();
            $calculation = Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION);
            foreach ($items as $k => $v) {
                ++$i;
                if ($calculation == self::RANGE_CALCULATION_MANUAL && $i > 1 && $i > $maxIntervalsNumber) {
                    $items[$lastIndex] += $v;
                    unset($items[$k]);
                } else {
                    $lastIndex = $k;
                }
            }
            $this->setData($rangeKey, $items);
        }
    
        return $items;
    }
    public function getPriceRange()
    {
        $range = $this->getData('price_range');
		
        if (!$range) {
            $currentCategory = Mage::registry('current_category_filter');
            if ($currentCategory) {
                $range = $currentCategory->getFilterPriceRange();
            } else {
                $range = $this->getLayer()->getCurrentCategory()->getFilterPriceRange();
            }
            
            $maxPrice = $this->getMaxPriceInt();
            if (!$range) {
                $calculation = Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION);
				
                if ($calculation == self::RANGE_CALCULATION_AUTO) {
                    $index = 1;
                    do {
                        $range = pow(10, (strlen(floor($maxPrice)) - $index));
                        $items = $this->getRangeItemCounts($range);
                        $index++;
                    }
                    while($range > self::MIN_RANGE_POWER && count($items) < 2);
                }elseif($calculation == self::RANGE_CALCULATION_ARRAY){
                    $range = (string)Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_ARRAY);
                } else {
                    $range = (float)Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_STEP);
                }
            }
            $this->setData('price_range', $range);
        }
    
        return $range;
    }
    protected function _getItemsArrayData($dbRanges, $range){
        $rangeArray = explode(',', $range);
        $rangeArray[] = '';
         
        $rangeData = array();
        
        $index = 0;
        $fromPrice = 0;
        $count = 0;
        $toPrice = $rangeArray[$index];
        
        foreach($dbRanges as $item => $itemCount){
            $priceStepTo = $item*$rangeArray[0];
			
            if($priceStepTo > $toPrice && $toPrice != '' ){
                while($priceStepTo > $toPrice && $rangeArray[$index] != '' ){
                    $index++;
                    $fromPrice  = $toPrice;
                    $toPrice = $rangeArray[$index]; 
                }
                $count = 0;
            }
			
            if($priceStepTo <= $toPrice && $priceStepTo > $fromPrice || $toPrice == '' ){
                 
                $count += $itemCount;
                $rangeData[$toPrice] = array('fromPrice'=>$fromPrice, 'toPrice' => $toPrice, 'count' => $count );
            }
        
        }
        return $rangeData;
    }
    
    /**
     * Prepare text of range label
     *
     * @param float|string $fromPrice
     * @param float|string $toPrice
     * @return string
     */
    protected function _renderRangeLabel($fromPrice, $toPrice)
    {
        $store      = Mage::app()->getStore();
        $formattedFromPrice  = $store->formatPrice($fromPrice);
        if ($toPrice === '') {
            return Mage::helper('catalog')->__('%s and above', $formattedFromPrice);
        } elseif ($fromPrice == $toPrice && Mage::app()->getStore()->getConfig(self::XML_PATH_ONE_PRICE_INTERVAL)) {
            return $formattedFromPrice;
        } else {
            if ($fromPrice != $toPrice) {
                $toPrice -= self::ONE;
            }
            return Mage::helper('catalog')->__('%s - %s', $formattedFromPrice, $store->formatPrice($toPrice));
        }
    }
    /**
     * Get data for build price filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        if (Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION) == self::RANGE_CALCULATION_IMPROVED) {
            return $this->_getCalculatedItemsData();
        } elseif ($this->getInterval()) {
            return array();
        }
    
        $range      = $this->getPriceRange();
        $dbRanges   = $this->getRangeItemCounts($range);
        $data       = array();
        
        $calculation = Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION);
        
        if($calculation == self::RANGE_CALCULATION_ARRAY && !empty($dbRanges)){
           $rangeData = $this->_getItemsArrayData($dbRanges, $range);
           foreach ($rangeData as $index => $value) {
               $data[] = array(
                       'label' => $this->_renderRangeLabel($value['fromPrice'], $value['toPrice']),
                       'value' => $value['fromPrice'] . '-' . $value['toPrice'],
                       'count' => $value['count'],
               );
           }
        }else{
            if (!empty($dbRanges)) {
                $lastIndex = array_keys($dbRanges);
                $lastIndex = $lastIndex[count($lastIndex) - 1];
            
                foreach ($dbRanges as $index => $count) {
                    $fromPrice = ($index == 1) ? '' : (($index - 1) * $range);
                    $toPrice = ($index == $lastIndex) ? '' : ($index * $range);
                    $data[] = array(
                            'label' => $this->_renderRangeLabel($fromPrice, $toPrice),
                            'value' => $fromPrice . '-' . $toPrice,
                            'count' => $count,
                    );
                }
            }
        }
        
        
        return $data;
    }
}
