<?php

class King_Extreport_Adminhtml_ReportController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Reports'))->_title($this->__('Ext Report'));

        Mage::getModel("core/session")->setData("filter_condition",$this->getRequest()->getParam("exportcondition"));
        $this->_initAction()
            ->_setActiveMenu('extreport/report')
            ->_addBreadcrumb(
                Mage::helper('extreport')->__('Ext Report'),
                Mage::helper('extreport')->__('Ext Report')
            );
        $this->loadLayout();
        $ajax = $this->getRequest()->getParam('ajax');
        $isAjax	= $this->getRequest()->getParam('isAjax');
        if($ajax === 'true' && $isAjax === 'true'){
            $this->getResponse()->setBody($this->getLayout()->getBlock('report')->toHtml());
        }else{
            $this->renderLayout();
        }
    }

    public function _initAction()
    {
        $this->loadLayout()
            ->_addBreadcrumb(Mage::helper('extreport')->__('Ext Report'), Mage::helper('extreport')->__('Ext Report'));

        return $this;
    }
    public function exportCsvAction()
    {
        $fileName = 'doanhso.csv';
        $content = $this->getLayout()->createBlock('extreport/adminhtml_report_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName   = 'doanhso.xls';
        $content    = $this->getLayout()->createBlock('extreport/adminhtml_report_grid');
        //print_r($content->getXmlFile());die;
        $this->_prepareDownloadResponse($fileName, $content->getExcelFile());
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    public function gridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('extreport/adminhtml_report_condition_product')->toHtml()
        );
    }
}