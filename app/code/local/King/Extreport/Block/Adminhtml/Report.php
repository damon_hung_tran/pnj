<?php  

class King_Extreport_Block_Adminhtml_Report extends Mage_Adminhtml_Block_Widget_Grid_Container {
    public function __construct()
    {
        $this->_blockGroup = 'extreport';
        $this->_controller = 'adminhtml_report';
        $this->_headerText = '';// Mage::helper('extreport')->__('');
        parent::__construct();
        $this->_removeButton('add');
        $this->setTemplate('king/custom_grid_container.phtml');
    }
    public function getTotalPrice(){
        $collection = $this->getChild('grid')->getCollection();
        $subQuery = clone $collection->getSelect();
        $subQuery->reset(Zend_Db_Select::COLUMNS);
        $subQuery->columns('grand_total');
        $subQuery = new Zend_Db_Expr("({$subQuery})");

        $sumSelect = "SELECT SUM(`sub_tbl`.`grand_total`) FROM {$subQuery} AS `sub_tbl`";
        return Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sumSelect);
    }
    public function getFormatTotalPrice(){
        return Mage::helper('extreport')->numberFormat($this->getTotalPrice());
    }
}