<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Program extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/program.phtml");
    }

    public function getProgramArray(){
        $collection = Mage::getModel("salesrule/rule")->getCollection();
		
        $result = array();
        foreach($collection as $item){
            $result[$item->getId()] = $item->getName();
        }
        return $result;
    }
}