<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Payment extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate("extreport/condition/payment.phtml");
    }

    public function getPaymentRate(){
        foreach($this->getActivPaymentMethods() as $key=>$item){
            $result[$key] = $item;
        }
        return $result;
    }

    protected function _getPaymentCollect()
    {
        return array(
            "Paypal"          => "Paypal",
            "NganLuong"       => "Ngân Lượng",
            "BaoKim"          => "Bảo Kim",
            "VNBC"            => "VNBC",
            "InternetBanking" => "Thanh toán Internet banking",
            "COD"             => "Giao hàng rồi thanh toán",
            "123Pay"          => "Thanh toán qua cổng 123Pay"
        );
    }
    
    public function getActivPaymentMethods()
    {
    	$payments = Mage::getSingleton('payment/config')->getActiveMethods();
    	//$methods = array(array('value'=>'', 'label'=>Mage::helper('adminhtml')->__('--Please Select--')));
    	$methods = array();
    	foreach ($payments as $paymentCode=>$paymentModel) {
    		$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
//     		$methods[$paymentCode] = array(
//     				'label'   => $paymentTitle,
//     				'value' => $paymentCode,
//     		);

    		$methods[$paymentCode] = $paymentTitle;
    	}
    	return $methods;
    }
}