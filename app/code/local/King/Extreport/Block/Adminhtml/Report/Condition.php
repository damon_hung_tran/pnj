<?php

class King_Extreport_Block_Adminhtml_Report_Condition extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("anneclub_form", array("legend" => Mage::helper("extreport")->__("Điều Kiện Report")));

        /* @var $conditionOb King_Extreport_Model_Condition */
        $conditionOb = Mage::getModel("extreport/condition")->setData(Mage::getModel("core/session")->getData("filter_condition"));

        $fieldset->addField("time", "reportcondition", array(
            'name'         => 'time',
            'label'        => Mage::helper('catalog')->__('Thời Gian'),
            'condition_block' => "extreport/adminhtml_report_condition_time",
            'time_from' => $conditionOb->getTimeFrom(),
            'time_to' => $conditionOb->getTimeTo()
        ));

        $fieldset->addField("timeinterval", "reportcondition", array(
            'name'         => 'timeinterval',
            'label'        => Mage::helper('catalog')->__('Khoảng Thời Gian'),
            'condition_block' => "extreport/adminhtml_report_condition_timeinterval",
            'timeinterval' => $conditionOb->getTimeinterval()
        ));

        $fieldset->addField("location", "reportcondition", array(
            'name'         => 'location',
            'label'        => Mage::helper('catalog')->__('Location'),
            'condition_block' => "extreport/adminhtml_report_condition_location",
            'location' => $conditionOb->getLocation()
        ));

        $fieldset->addField("program", "reportcondition", array(
            'name'         => 'program',
            'label'        => Mage::helper('catalog')->__('Chương Trình'),
            'condition_block' => "extreport/adminhtml_report_condition_program",
            'program' => $conditionOb->getProgram()
        ));

        $fieldset->addField("payment", "reportcondition", array(
            'name'         => 'payment',
            'label'        => Mage::helper('catalog')->__('Thanh Toán'),
            'condition_block' => "extreport/adminhtml_report_condition_payment",
            'payment' => $conditionOb->getPayment()
        ));

        $fieldset->addField("shipping", "reportcondition", array(
            'name'         => 'shipping',
            'label'        => Mage::helper('catalog')->__('Vận chuyển'),
            'condition_block' => "extreport/adminhtml_report_condition_shipping",
            'shipping' => $conditionOb->getShipping()
        ));
        
        $fieldset->addField("category", "reportcondition", array(
            'name'         => 'category',
            'label'        => Mage::helper('catalog')->__('Danh mục sản phẩm'),
            'condition_block' => "extreport/adminhtml_report_condition_category",
            'category' => $conditionOb->getCategory()
        ));

        $fieldset->addField("product", "reportcondition", array(
            'name'         => 'product',
            'label'        => Mage::helper('catalog')->__('Sản phẩm'),
            'condition_block' => "extreport/adminhtml_report_condition_product",
            'product' => $conditionOb->getProduct()
        ));

        $fieldset->addField("customer", "reportcondition", array(
            'name'         => 'customer',
            'label'        => Mage::helper('catalog')->__('Khách hàng'),
            'condition_block' => "extreport/adminhtml_report_condition_customer",
            'customer' => $conditionOb->getCustomer()
        ));

//        $fieldset->addField("status", "reportcondition", array(
//            'name'         => 'status',
//            'label'        => Mage::helper('catalog')->__('Trạng thái'),
//            'condition_block' => "extreport/adminhtml_report_condition_status",
//            'status'  => $conditionOb->getStatus()
//        ));
        if (Mage::getSingleton("adminhtml/session")->getClubcustomerData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getClubcustomerData());
            Mage::getSingleton("adminhtml/session")->setClubcustomerData(null);
        } elseif (Mage::registry("clubcustomer_data")) {
            $form->setValues(Mage::registry("clubcustomer_data")->getData());
        }

        return parent::_prepareForm();
    }
}