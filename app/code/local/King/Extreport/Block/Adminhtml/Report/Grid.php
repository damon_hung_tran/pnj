<?php

class King_Extreport_Block_Adminhtml_Report_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("extreportGrid");
        $this->setDefaultSort("main_table.increment_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
        $this->setFilterVisibility(false);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("extreport/doanhso")->getCollection();
		$collection->getSelect()->join( array('order_flat'=> 'sales_flat_order'), 'order_flat.entity_id = main_table.order_id', array('order_flat.coupon_code','order_flat.customer_email'));
		$collection->getSelect()->group('order_id');
        /* @var $conditionOb King_Extreport_Model_Condition */
        $conditionOb = Mage::getModel("extreport/condition")->setData(Mage::getModel("core/session")->getData("filter_condition"));
        $collection = $conditionOb->setCollection($collection)->processFilter();
        //FB::log($collection->getSelectSql(true));
        $this->setCollection($collection);
        //echo $collection->getSelectSql();
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("increment_id", array(
            "header" => Mage::helper("extreport")->__("Mã đơn hàng"),
            "align"  => "left",
            "index"  => "increment_id",
        ));
        $this->addColumn("billing_name", array(
            "header" => Mage::helper("extreport")->__("Tên người đặt hàng"),
            "align"  => "left",
            "index"  => "billing_name",
        ));
        $this->addColumn("shipping_name", array(
            "header" => Mage::helper("extreport")->__("Tên người nhập hàng"),
            "align"  => "left",
            "index"  => "shipping_name",
        ));
        $this->addColumn("billing_address", array(
            "header" => Mage::helper("extreport")->__("Địa chỉ người đặt hàng"),
            "align"  => "left",
            "index"  => "billing_address",
        ));
        $this->addColumn("customer_email", array(
            "header" => Mage::helper("extreport")->__("Email KH"),
            "align" => "left",
            "index" => "customer_email",
        ));
		$this->addColumn("coupon_code", array(
            "header" => Mage::helper("extreport")->__("Mã giảm giá"),
            "align" => "left",
            "index" => "coupon_code",
        ));
        $this->addColumn("qty_ordered", array(
            "header" => Mage::helper("extreport")->__("SL sản phẩm"),
            "type"   => "number",
            "index"  => "qty_ordered",
        ));
        $this->addColumn("subtotal", array(
            "header"   => Mage::helper("extreport")->__("Số tiền đơn hàng"),
            "align"    => "right",
            "index"    => "subtotal",
            'renderer' => 'King_Extreport_Block_Adminhtml_Report_Grid_Number'
        ));
        $this->addColumn("grand_total", array(
            "header"   => Mage::helper("extreport")->__("Tổng số tiền (VND)"),
            "align"    => "right",
            "index"    => "grand_total",
            'renderer' => 'King_Extreport_Block_Adminhtml_Report_Grid_Number'
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("adminhtml/sales_order/view", array("order_id" => $row->getOrderId()));
    }


}