<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Product extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('catalog_products_filter');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in category flag
        if ($column->getId() == 'product_filter') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    protected function _prepareCollection()
    {
        $selected_product = $this->_getSelectedProducts();
//        if (!empty($selected_product)) {
//            $this->setDefaultFilter(array('product_filter' => 1));
//        }

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('*')
            ->addStoreFilter($this->getRequest()->getParam('store'));
        $collection->getSelect()->where('e.entity_id IN (SELECT DISTINCT `sfoi`.`product_id` FROM `sales_flat_order_item` sfoi)');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('product_filter', array(
            'header_css_class' => 'a-center',
            'type'             => 'checkbox',
            'name'             => 'product_filter',
            'values'           => $this->_getSelectedProducts(),
            'align'            => 'center',
            'index'            => 'entity_id',
            'field_name'       => 'exportcondition[product][]'
        ));

        $this->addColumn('entity_id', array(
            'header' => Mage::helper('catalog')->__('ID'),
            'index'  => 'entity_id',
        ));

        $this->addColumn('sku', array(
            'header' => Mage::helper('catalog')->__('SKU'),
            'index'  => 'sku',
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'index'  => 'name',
        ));

//         $this->addColumn('type', array(
//             'header'  => Mage::helper('catalog')->__('Type'),
//         	'width'  => '100px',
//             'index'   => 'type_id',
//             'type'    => 'options',
//             'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
//         ));

//        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
//            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
//            ->load()
//            ->toOptionHash();
//
//        $this->addColumn('set_name', array(
//            'header'  => Mage::helper('catalog')->__('Attrib. Set Name'),
//            'index'   => 'attribute_set_id',
//            'type'    => 'options',
//            'options' => $sets,
//        ));

//         $this->addColumn('status', array(
//             'header'  => Mage::helper('catalog')->__('Status'),
//         	'width'  => '100px',
//             'index'   => 'status',
//             'type'    => 'options',
//             'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        	
//         ));

//        $this->addColumn('visibility', array(
//            'header'  => Mage::helper('catalog')->__('Visibility'),
//            'index'   => 'visibility',
//            'type'    => 'options',
//            'options' => Mage::getSingleton('catalog/product_visibility')->getOptionArray(),
//            'width'  => '100',
//        ));

//         $this->addColumn('price', array(
//             'header'        => Mage::helper('catalog')->__('Price'),
//             'type'          => 'currency',
//             'currency_code' => (string)Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
//             'index'         => 'price',
//         ));


        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _getSelectedProducts()
    {
        $data = $this->getRequest()->getPost('exportcondition');
        $products = $data["product"];
        return $products;
    }
}