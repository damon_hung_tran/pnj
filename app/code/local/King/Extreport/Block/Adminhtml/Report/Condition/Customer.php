<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Customer extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/customer.phtml");
    }

    public function getCustomerCollection(){
        $store_id = Mage::app()->getWebsite(1)->getDefaultStore()->getId();
        $store_model = Mage::getModel('core/store')->load($store_id);
        Mage::app()->setCurrentStore($store_model->getCode());
        $collection = Mage::getModel("customer/customer")->getCollection();
        $collection->addAttributeToSelect("firstname");
        $collection->setOrder("firstname");
        //$collection->getSelect()->limit(300);
        Mage::app()->setCurrentStore("admin");
        return $collection;
    }
}