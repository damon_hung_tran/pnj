<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Location extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/location.phtml");
    }

    public function getLocationArray(){
        return Mage::getModel("store/store")->getListRegionOption();
    }
}