<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Category extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/category.phtml");
    }

    function loadCategories($category){
        if ($this->getCategory() == $category->getId()) { $selected = " SELECTED"; } else { $selected = ""; }
        echo '<option value="'. $category->getId() .'"'.$selected.'>'.str_repeat('-', $category->getLevel()-2).' '.$category->getName().'</option>'."\n";
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        if($children && $childrenCount){
            foreach ($children as $child) {
                if ($child->getIsActive()) {
                    $this->loadCategories($child);
                }
            }
        }
        return true;
    }

    function printTree(){
        $store_id = Mage::app()->getWebsite(1)->getDefaultStore()->getId();
        $store_model = Mage::getModel('core/store')->load($store_id);
        Mage::app()->setCurrentStore($store_model->getCode());
        $helper = Mage::helper('catalog/category');
        $categories = $helper->getStoreCategories();
        echo "<option value=\"\">".$this->__("Chọn")."</option>";
        foreach($categories as $_cat){
//            echo $_cat->getName();
            if($_cat->getIsActive()){
                $this->loadCategories($_cat);
            }
        }
        Mage::app()->setCurrentStore("admin");
        return true;
    }
}