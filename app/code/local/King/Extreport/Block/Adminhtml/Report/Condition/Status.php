<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Status extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/status.phtml");
    }

    public function getOrderInvoiceStatus(){
        return array(
            "canceled" => Mage::helper('catalog')->__('Huỷ'),
            "complete "=> Mage::helper('catalog')->__('Thành Công'),
            "processing"  => Mage::helper('catalog')->__('Đang xử lý')
        );
    }
}