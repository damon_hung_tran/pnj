<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Abstract extends Mage_Adminhtml_Block_Template
{
    public function prepareFieldName($name,$array=""){
        return "exportcondition[$name]".$array;
    }
    public function getParamExportcondition($namespace, $implement) {
        $getParams = Mage::app()->getRequest()->getParams();
        $param = $getParams[$namespace][$implement];
        return $param;
    }
    public function getParamExportconditionadvanced($namespace,$implement){
        $getParams = Mage::app()->getRequest()->getParams();
        $array = $getParams[$namespace][$implement];
        return $array;
    }
}