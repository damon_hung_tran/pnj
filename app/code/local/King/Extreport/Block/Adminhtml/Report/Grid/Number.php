<?php
/**
 * Created by Nick Howard, Magento Developer.
 */
class King_Extreport_Block_Adminhtml_Report_Grid_Number extends  Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        return Mage::helper('extreport')->numberFormat($value);
    }
}