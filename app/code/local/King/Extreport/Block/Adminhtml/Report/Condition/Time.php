<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Time extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/time.phtml");
    }
}