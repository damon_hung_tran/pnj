<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Timeinterval extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/timeinterval.phtml");
    }

    public function getIntervalArray(){
        return array(
            "week" => Mage::helper("extreport")->__("Tuần"),
            "month" => Mage::helper("extreport")->__("Tháng"),
            "quater" => Mage::helper("extreport")->__("Quý"),
            "year"  => Mage::helper("extreport")->__("Năm")
        );
    }
}