<?php

class King_Extreport_Block_Adminhtml_Report_Condition_Shipping extends King_Extreport_Block_Adminhtml_Report_Condition_Abstract
{
    protected function _construct(){
        parent::_construct();
        $this->setTemplate("extreport/condition/shipping.phtml");
    }

    public function getShippingRate(){
    	
    	$parent_code = Mage::getModel("vietship/carrier_vnTablerate")->getCode();
        foreach(Mage::getModel("vietship/carrier")->getCollection() as $item){
            //$result[$item->getId()] = $item->getName();
        	$result[$parent_code.'_'.$item->getCode()] = $item->getName();
        }
        return $result;
    }

}