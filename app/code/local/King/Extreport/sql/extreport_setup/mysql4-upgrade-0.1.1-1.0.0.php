<?php
/**
 * Created by Nick Howard, Magento Developer.
 */

$installer = $this;

$installer->startSetup();
$installer->run("TRUNCATE TABLE {$this->getTable('doanh_so')};
				TRUNCATE TABLE {$this->getTable('chuong_trinh_khuyen_mai')};");
$orderCollection = Mage::getModel('sales/order')->getCollection();
foreach ($orderCollection as $order){
    Mage::getModel('extreport/observer')->updateDoanhSo($order);
}

$installer->endSetup();