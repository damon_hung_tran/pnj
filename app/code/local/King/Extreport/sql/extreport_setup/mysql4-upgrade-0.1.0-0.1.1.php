<?php
/**
 * Created by Nick Howard, Magento Developer.
 */

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('doanh_so')};

CREATE TABLE {$this->getTable('doanh_so')} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `subtotal` decimal(15,4) DEFAULT NULL COMMENT 'Subtotal',
  `grand_total` decimal(15,4) DEFAULT NULL COMMENT 'Grand Total',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `billing_name` text,
  `billing_address` text,
  `shipping_name` text,
  `payment_method` varchar(255) DEFAULT NULL COMMENT 'Payment Method Code',
  `shipping_method` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('chuong_trinh_khuyen_mai')};

CREATE TABLE {$this->getTable('chuong_trinh_khuyen_mai')} (
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `order_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`item_id`,`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
/*$orderCollection = Mage::getModel('sales/order')->getCollection();
foreach ($orderCollection as $order){
    Mage::getModel('extreport/observer')->updateDoanhSo($order);
}*/
$installer->endSetup();