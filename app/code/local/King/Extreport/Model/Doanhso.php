<?php
/**
 * Created by Nick Howard, Magento Developer.
 */
class King_Extreport_Model_Doanhso extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'doanhso';
    protected $_eventObject = 'doanhso';
    public function _construct()
    {
        parent::_construct();
        $this->_init('extreport/doanhso');
    }
}