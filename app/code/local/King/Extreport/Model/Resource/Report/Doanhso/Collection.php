<?php
/**
 * Created by Nick Howard, Magento Developer.
 */
class King_Extreport_Model_Resource_Report_Doanhso_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('extreport/doanhso');
    }

    protected function _joinFields($from = '', $to = '')
    {
        $this->addFieldToFilter('created_at' , array("from" => $from, "to" => $to, "datetime" => true));
        $this->getSelect()->columns(array('value' => 'SUM(row_total)'));

        return $this;
    }

    public function setDateRange($from, $to)
    {
        $this->_reset()
            ->_joinFields($from, $to);
        return $this;
    }

    public function load($printQuery = false, $logQuery = false)
    {
        if ($this->isLoaded()) {
            return $this;
        }
        parent::load($printQuery, $logQuery);
        return $this;
    }

    public function setStoreIds($storeIds)
    {
        return $this;
    }
}