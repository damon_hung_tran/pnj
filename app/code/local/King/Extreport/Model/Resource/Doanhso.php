<?php
/**
 * Created by Nick Howard, Magento Developer.
 */
class King_Extreport_Model_Resource_Doanhso extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('extreport/doanhso', 'id');
    }
}