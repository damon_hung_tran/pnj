<?php

/**
 * Created by Nick Howard, Magento Developer.
 */
class King_Extreport_Model_Observer
{
    public function updateOrderReport($ob)
    {
        //print_r($ob->debug());die('test');
        $order = $ob->getOrder();
        $this->updateDoanhSo($order);
    }

    public function updateDoanhSo($order)
    {
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        $read = $resource->getConnection('core_read');
        //$order->load($order->getId()); // comment by phong.tran@giaiphapso.com
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();
        $paymentMethod = null;
        if ($order->getPayment()) {
            try {
                $paymentMethod = $order->getPayment()->getMethodInstance()->getCode();
            } catch (Exception $e) {
                $paymentMethod = $order->getPayment()->getMethod();
            }
        }
        $shippingCode = $order->getShippingMethod();
        
        foreach ($order->getAllVisibleItems() as $item) {
            $doanhso = Mage::getModel('extreport/doanhso')->load($item->getId(), 'item_id');
            $doanhso->setData(array(
                'id'              => $doanhso->getId(),
                'item_id'         => $item->getId(),
                'order_id'        => $order->getId(),
                'product_id'      => $item->getProductId(),
                'sku'             => $item->getSku(),
                'qty_ordered'     => $order->getTotalQtyOrdered(),
                'subtotal'        => $order->getSubtotal(),
                'grand_total'     => $order->getGrandTotal(),
                'region'          => $billingAddress->getRegion(),
                'status'          => $order->getState(),
                'customer_id'     => $order->getCustomerId(),
                'increment_id'    => $order->getIncrementId(),
                'billing_name'    => $billingAddress->getFirstname(),
                'billing_address' => implode(', ', $billingAddress->getStreet()) . ", {$billingAddress->getCity()}, {$billingAddress->getRegion()}",
                'shipping_name'   => ($shippingAddress)? $shippingAddress->getFirstname() : '',
                'payment_method'  => $paymentMethod,
                'shipping_method'  => $shippingCode,
                'created_at'      => $order->getCreatedAt()
            ))->save();
            if ($item->getAppliedRuleIds()) {
                $write->query("DELETE FROM `{$resource->getTableName('chuong_trinh_khuyen_mai')}` WHERE `item_id` = {$item->getId()};");
                $tmpArray = array();
                foreach (explode(",", $item->getAppliedRuleIds()) as $ruleID) {
                    array_push($tmpArray, "({$item->getId()},{$ruleID},{$order->getId()})");
                }
                $write->query('insert into ' . $resource->getTableName('chuong_trinh_khuyen_mai') . ' (`item_id`,`rule_id`,`order_id`) values ' . implode(',', $tmpArray) . ';');
            }
        }
    }
}