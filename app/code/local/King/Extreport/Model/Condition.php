<?php

/**
 * Created by Nick Howard, Magento Developer.
 * @method string getTimeFrom()
 * @method string getTimeTo()
 * @method string getTimeinterval()
 * @method string getLocation()
 * @method string getCategory()
 * @method string getProduct()
 * @method string getCustomer()
 * @method string getStatus()
 */

class King_Extreport_Model_Condition extends Varien_Object
{
    /* @var $_collection King_Extreport_Model_Resource_Doanhso_Collection */
    protected $_collection = null;
    public function setCollection($collect){
        $this->_collection = $collect;
        return $this;
    }

    /**
     * @return King_Extreport_Model_Resource_Doanhso_Collection
     * @throws Exception
     */
    public function getCollection(){
        if($this->_collection == null){
            throw new Exception("Collection can't null");
        }
		
        return $this->_collection;
    }

    public function processFilter(){
    	$this->filterTimeFromTo();
        $this->filterTimeInterval();
        $this->filterRegion();
        $this->filterProgram();
        $this->filterPayment();
        $this->filterShipping();
        $this->filterCategory();
        $this->filterProduct();
        $this->filterCustomer();
        $this->filterStatus();
        return $this->getCollection();
    }

    protected function filterProgram(){
        if($this->getProgram()){
            $resource = Mage::getSingleton('core/resource');
            $tmp = implode(",",$this->getProgram());
            $sql = "select DISTINCT order_id from chuong_trinh_khuyen_mai
                    where rule_id in ({$tmp})";

            $read = $resource->getConnection('core_read');
            $orderIds = $read->fetchCol($sql);
            $this->getCollection()->addFieldToFilter("order_id",array("in"=>$orderIds));
        }
    }

    protected function filterStatus(){
        if($this->getStatus()){
            $this->getCollection()->addFieldToFilter("status",array("in"=>$this->getStatus()));
        }
    }

    protected function filterCustomer(){
        if($cus = $this->getCustomer()){
            $tmparray = array("client_maxbuytime","client_maxbuymoney");
            if(in_array($cus,$tmparray)){
                $this->filterSpecialForCustomer($cus);
            }else{
                $this->getCollection()->addFieldToFilter("customer_id",$this->getCustomer());
            }
        }
    }

    protected function filterSpecialForCustomer($type){
        switch($type){
            case "client_maxbuytime":
                $resource = Mage::getSingleton('core/resource');
                $sql = "select customer_id from sales_flat_order
                where customer_id != '' and (state IN ('complete','new'))
                GROUP BY customer_id
                ORDER BY count(entity_id) DESC
                LIMIT 1";

                $read = $resource->getConnection('core_read');
                $customerId = $read->fetchOne($sql);
                $this->getCollection()->addFieldToFilter("customer_id",$customerId);
                return;
            case "client_maxbuymoney":
                $resource = Mage::getSingleton('core/resource');
                $sql = "select customer_id from sales_flat_order
                where customer_id != '' and (state IN ('complete','new'))
                GROUP BY customer_id
                ORDER BY sum(grand_total) DESC
                LIMIT 1 
                ";

                $read = $resource->getConnection('core_read');
                $customerId = $read->fetchOne($sql);
                $this->getCollection()->addFieldToFilter("customer_id",$customerId);
                return;
        }
    }

    protected function filterTimeFromTo(){
    	$fromDate = explode('/', $this->getTimeFrom());
    	$toDate = explode('/', $this->getTimeTo());
    	if(count($fromDate)==3 &&  count($toDate)==3){
    		$this->_collection->addFieldToFilter('main_table.created_at', array('from'=>$fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1], 'to'=>$toDate[2].'-'.$toDate[0].'-'.$toDate[1]));
    	}
    }

    protected function filterTimeInterval(){
        if($this->getTimeinterval()){
            switch($this->getTimeinterval()){
                case "week":{
                    $fromDate = new Zend_Date(Mage::helper("extreport")->firstDayOf("week")->getTimestamp());
                    $toDate = new Zend_Date(Mage::helper("extreport")->lastDayOf("week")->getTimestamp());
                    break;
                }
                case "month":{
                    $fromDate = new Zend_Date(Mage::helper("extreport")->firstDayOf("month")->getTimestamp());
                    $toDate = new Zend_Date(Mage::helper("extreport")->lastDayOf("month")->getTimestamp());
                    break;
                }
                case "quater":{
                    $fromDate = new Zend_Date(Mage::helper("extreport")->firstDayOf("quarter")->getTimestamp());
                    $toDate = new Zend_Date(Mage::helper("extreport")->lastDayOf("quarter")->getTimestamp());
                    break;
                }
                case "year":{
                    $fromDate = new Zend_Date(Mage::helper("extreport")->firstDayOf("year")->getTimestamp());
                    $toDate = new Zend_Date(Mage::helper("extreport")->lastDayOf("year")->getTimestamp());
                    break;
                }
            }
            $this->_collection->addFieldToFilter('main_table.created_at', array('from'=>$fromDate->toString("y-MM-dd"), 'to'=>$toDate->toString("y-MM-dd")));
        }
    }

    protected function filterRegion(){
    	$region = $this->getLocation();
    	if( $region ){
    		$this->_collection->addFieldToFilter('region', $region);
    	}
    }
    
    protected function filterShipping(){
    	$shippings = $this->getShipping();
    	if( $shippings ){
    		$this->_collection->addFieldToFilter('shipping_method', array('in'=>$shippings));
    	}
    }
    
	protected function filterProduct(){
    	$pIds = $this->getProduct();
    	if( is_array($pIds) ){
    		$this->_collection->addFieldToFilter('product_id', array('in'=>$pIds));
    	}
    }
    
    protected function filterPayment(){
    	$payments = $this->getPayment();
    	if( is_array($payments)  && $payments){
    		$this->_collection->addFieldToFilter('payment_method', array('in'=>$payments));
    	}
    }
    
    
    protected function filterCategory(){
    	$catId = $this->getCategory();
    	if( $catId ){
    		$this->_collection->getSelect()->where( "`main_table`.product_id IN (SELECT catpro.`product_id` FROM `catalog_category_product` catpro WHERE catpro.`category_id`=".$catId.")" );
    	}
    }


    public function getPayment(){
    	if(!is_array($this->getData('payment')) && !$this->getData('payment') ){
    		return array();
    	}
    	return array($this->getData('payment'));
    }
    
    public function getShipping(){
		if(!is_array($this->getData('shipping')) && !$this->getData('shipping') ){
    		return array();
    	}
    	return array($this->getData('shipping'));
    }

    public function getProgram(){
        if(!is_array($this->getData('program')) && !$this->getData('program')){
            return array();
        }
        return array($this->getData('program'));
    }

    public function getStatus(){
        if(!is_array($this->getData('status'))){
            return array();
        }
        return $this->getData('status');
    }
}