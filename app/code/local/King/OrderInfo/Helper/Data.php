<?php

class King_OrderInfo_Helper_Data extends Mage_Core_Helper_Abstract {
	public function getCommentSubject($comments, $tagname){
		$pattern = "/<$tagname>(.*?)<\/$tagname>/";
		preg_match($pattern, $comments, $matches);
		return $matches[1];
	}
}