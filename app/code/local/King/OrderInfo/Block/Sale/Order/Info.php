<?php

class King_OrderInfo_Block_Sale_Order_Info extends Mage_Core_Block_Template
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('king/sale_info/order.phtml');
    }
}
