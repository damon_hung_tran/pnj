<?php
class King_OrderInfo_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $data = array();
        $data['status'] = false;
        $data['messages'] = '';

        $data['html'] = '';
        $orderId = $this->getRequest()->getParam('order_id');
        $layout = $this->getLayout();
        if ($orderId != '') {
            $order = Mage::getModel('sales/order')->loadByAttribute('protect_code', $orderId);;
            if (!$order->getId()) {
                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            }
            if ($order->getId()) {
                Mage::register('current_order', $order);
                $data['status'] = true;
                $block_order_totals = $layout->createBlock("sales/order_totals", "order_totals")
                    ->setTemplate("sales/order/totals.phtml")
                    ->setLabelProperties('colspan="4" class="a-right"')
                    ->setValueProperties('class="last a-right"')
                    ->setChild("tax", $layout->createBlock("tax/sales_order_tax", "tax")->setTemplate("tax/order/tax.phtml"));
                $block_order_items = $layout->createBlock("sales/order_items", "order_items")
                    ->setTemplate("sales/order/items.phtml")
                    ->addItemRender("default", "sales/order_item_renderer_default", "king/sale/order/items/renderer/default.phtml")
                    ->addItemRender("grouped", "sales/order_item_renderer_grouped", "king/sale/order/items/renderer/default.phtml")
                    ->setChild("order_totals", $block_order_totals);

                $data['html'] = $layout->createBlock('sales/order_view', "sales.order.view")
                    ->setTemplate('king/sale/order/view.phtml')
                    ->setChild("order_items", $block_order_items)->toHtml();
                $data['order'] = $order->getData();


            } else {
                $data['html'] = $layout->createBlock('core/template')->setTemplate('king/sale/order/404.phtml')->setOrderNumber($orderId)->toHtml();
            }

        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }
    /*
     *  <block type="sales/order_view" name="sales.order.view">
                    <block type="sales/order_items" name="order_items" template="sales/order/items.phtml">
                        <action method="addItemRender"><type>default</type><block>sales/order_item_renderer_default</block><template>sales/order/items/renderer/default.phtml</template></action>
                        <action method="addItemRender"><type>grouped</type><block>sales/order_item_renderer_grouped</block><template>sales/order/items/renderer/default.phtml</template></action>
                        <block type="sales/order_totals" name="order_totals" template="sales/order/totals.phtml">
                            <action method="setLabelProperties"><value>colspan="4" class="a-right"</value></action>
                            <action method="setValueProperties"><value>class="last a-right"</value></action>
                            <block type="tax/sales_order_tax" name="tax" template="tax/order/tax.phtml" />
                        </block>
                    </block>
                </block>
     */
}
