<?php

require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';

class King_OrderInfo_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
    public function addNoteShippingAction()
    {
        if ($order = $this->_initOrder()) {
            try {
                $response = false;
                $data = $this->getRequest()->getPost('status_shipping');
				$comments = '<subject>'.$data['comment_subject'].'</subject>';
				$comments .= '<description>'.$data['comment_description'].'</description>';
                $order->setData('note_shipping',$comments);
                $order->save();
                $this->loadLayout('empty');
                $this->renderLayout();
            } catch (Mage_Core_Exception $e) {
                $response = array(
                    'error' => true,
                    'message' => $e->getMessage(),
                );
            }
            catch (Exception $e) {
                $response = array(
                    'error' => true,
                    'message' => $this->__('Cannot add order history.')
                );
            }
            if (is_array($response)) {
                $response = Mage::helper('core')->jsonEncode($response);
                $this->getResponse()->setBody($response);
            }
        }
    }
}