<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-L.txt
 *
 * @category   AW
 * @package    AW_Blog
 * @copyright  Copyright (c) 2009-2010 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-L.txt
 */

class AW_Blog_Block_Manage_Cat_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('cat_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('blog')->__('Cat Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('blog')->__('Cat Information'),
          'title'     => Mage::helper('blog')->__('Cat Information'),
          'content'   => $this->getLayout()->createBlock('blog/manage_cat_edit_tab_form')->toHtml(),
      ));
	  
	  $this->addTab('productcategory_section', array(
          'label'     => Mage::helper('blog')->__('Product Category'),
          'title'     => Mage::helper('blog')->__('Product Category'),
          'content'   => $this->getLayout()->createBlock('blog/manage_cat_edit_tab_productcategory')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}
