<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('blog/blog')} ADD `image` VARCHAR( 255 ) NULL;
");
$installer->endSetup();