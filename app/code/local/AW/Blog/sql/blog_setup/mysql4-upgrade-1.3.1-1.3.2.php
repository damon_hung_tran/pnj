<?php

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('blog/productcategory')};
CREATE TABLE {$this->getTable('blog/productcategory')} (
`productcategory_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
`product_category_id` int(11) unsigned NOT NULL,
`article_category_id` int(11) unsigned NOT NULL,
PRIMARY KEY ( `productcategory_id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
");

$installer->endSetup();