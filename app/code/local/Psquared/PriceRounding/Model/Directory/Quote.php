<?php
class Psquared_PriceRounding_Model_Directory_Quote extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    /**
     * Convert price to currency format
     *
     * @param   double $price
     * @param   string $toCurrency
     * @return  double
     */
     public function collect(Mage_Sales_Model_Quote_Address $address) {
        parent::collect($address);
        Mage::log('hungtest');
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
        $this->_setAmount(0);
        $this->_setBaseAmount(0);
        return floor($this);
    }
}
		