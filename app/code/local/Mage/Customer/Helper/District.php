<?php
class Mage_Customer_Helper_District extends Mage_Core_Helper_Abstract
{
	
	public function getDistricts($province_id) {
		$query    = "SELECT * FROM directory_country_district WHERE region_id=$province_id";
        $districts = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($query); 
        $options = '';
        if( !empty($districts) )
        {
            foreach( $districts as $district )
            {
				$options[$district['default_name']] = $district['default_name'];
            }    
        }
		return $options;
	}
	
	
}