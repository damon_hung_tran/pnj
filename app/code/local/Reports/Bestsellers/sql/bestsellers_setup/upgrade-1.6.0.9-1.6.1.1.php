<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    local
 * @package     Reports_Bestsellers
 * @copyright   Copyright (c) Hung Tran (whatiswhat10@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("
-- Update sku sales_bestsellers_aggregated_monthly
UPDATE `sales_bestsellers_aggregated_monthly` 
JOIN `catalog_product_entity` 
ON catalog_product_entity.entity_id=sales_bestsellers_aggregated_monthly.product_id
SET sales_bestsellers_aggregated_monthly.product_sku=catalog_product_entity.sku
WHERE sales_bestsellers_aggregated_monthly.product_sku IS NULL;

-- Update sku sales_bestsellers_aggregated_daily
UPDATE `sales_bestsellers_aggregated_daily` 
JOIN `catalog_product_entity` 
ON catalog_product_entity.entity_id=sales_bestsellers_aggregated_daily.product_id
SET sales_bestsellers_aggregated_daily.product_sku=catalog_product_entity.sku
WHERE sales_bestsellers_aggregated_daily.product_sku IS NULL;

-- Update sku sales_bestsellers_aggregated_yearly
UPDATE `sales_bestsellers_aggregated_yearly` 
JOIN `catalog_product_entity` 
ON catalog_product_entity.entity_id=sales_bestsellers_aggregated_yearly.product_id
SET sales_bestsellers_aggregated_yearly.product_sku=catalog_product_entity.sku
WHERE sales_bestsellers_aggregated_yearly.product_sku IS NULL;
");
$installer->endSetup();