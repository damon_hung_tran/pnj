<?php

/**
 * @name            :  Super Deals
 * @version         :  1.1
 * @author          :  Apptha - http://www.apptha.com
 * @copyright       :  Copyright (C) 2013 Powered by Apptha
 * @license         :  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @Creation Month  :  April 2013
 *
 * */

class Apptha_Superdeals_Adminhtml_Product_DealzController extends Mage_Adminhtml_Controller_Action
{
    /**
     * fucntion to view deals product details
     */
    protected function _initProduct() 
    {
    
        $product = Mage::getModel('catalog/product')
        ->setStoreId($this->getRequest()->getParam('store', 0));
    
    
        if ($setId = (int) $this->getRequest()->getParam('set')) {
            $product->setAttributeSetId($setId);
        }
    
        if ($typeId = $this->getRequest()->getParam('type')) {
            $product->setTypeId($typeId);
        }
    
        $product->setData('_edit_mode', true);
    
        Mage::register('product', $product);
    
        return $product;
    }
	/**
	 * fucntion to view deals order details
	 */
	public function indexAction()
	{
		$this->_title($this->__('Reports'))
		->_title($this->__('Super Deal Product'));

		$this->loadLayout()
    		->_setActiveMenu('superdeals/dealz')
    		->_addBreadcrumb(Mage::helper('reports')->__('Super Deal Products'), Mage::helper('reports')->__('Super Deal Products'))
    		->_addContent($this->getLayout()->createBlock('superdeals/adminhtml_product_dealz'))
    		->renderLayout();
	}
	
}

