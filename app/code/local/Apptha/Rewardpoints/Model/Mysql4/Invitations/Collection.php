<?php

    class Apptha_Rewardpoints_Model_Mysql4_Invitations_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
    {
        public function _construct()
        {
            parent::_construct();
            $this->_init('rewardpoints/invitations');
        }

        public function checkInvited($customer_id, $friend_email){            
            $this->getSelect()
            ->where('customer_id='.$customer_id)
            ->where('friend_email='."'$friend_email'");
            return $this->getFirstItem();
        }
        
        public function checkExist($customer_id, $friend_email, $referral_key){            
            $this->getSelect()
            ->where('customer_id='.$customer_id)
            ->where('friend_email='."'$friend_email'")
            ->where('referral_key='."'$referral_key'")
            ->where('status=0');
            return $this->getFirstItem();
        }

        public function updateStatus($id){            
            $data = array('status'=>1);
            $model = Mage::getModel('rewardpoints/invitations')->load($id)->addData($data);
            try {
                $model->setId($id)->save();

            } catch (Exception $e){
                echo $e->getMessage();
            }
        }
        //get referral key
        public function getReferralKey($strCusId){
            $this->getSelect()->where('customer_id='.intval($strCusId));
            return $this;
        }
        //check valid referral link
        public function getResult($strRefKey,$strCusId){			
            $this->getSelect() ->where('referral_key='."'$strRefKey'")
            ->where('customer_id='.intval($strCusId))
            ->where('status='.Apptha_Rewardpoints_Model_Status::COMPLETE);
            return $this;
        }
}