<?php
    class Apptha_Rewardpoints_IndexController extends Mage_Core_Controller_Front_Action
    {
        /**
        * Retrieve customer session model object
        *
        * @return Mage_Customer_Model_Session
        */
        protected function _getSession()
        {
            return Mage::getSingleton('customer/session');
        }
        //action for invite friend via referral link
        public function indexAction()
        {
            if(!(Mage::helper('rewardpoints')->moduleEnabled()))
            {
                $this->norouteAction();
                return;
            }
            if(!Mage::helper('rewardpoints')->isInviteEnabled()){
                $this->norouteAction();
                return;
            }
            if(Mage::helper('rewardpoints')->isRefLinkEnabled())
            {
                // phong.tran@giaiphapso.com
                // Get param from url
                $invite = $this->getRequest()->getParam('c');
                $customer_id  = $this->getRequest()->getParam('c_id');
                $friend_email = $this->getRequest()->getParam('f_email');
                $referral_key = $this->getRequest()->getParam('f_key');

                // Validate data
                $objInvited = Mage::getModel('rewardpoints/invitations')->getCollection();
                $result = $objInvited->checkInvited(base64_decode($customer_id), base64_decode($friend_email));

                $flag    = '';
                if( $result->getId() > 0 ){ // has invited this $friend_email
                    if( $result->getData('status') == '1' ){ // has been add point
                        $flag = 1;
                    }else{
                        $flag = 0;
                    }// else do nothing
                }else{
                    $flag = 0;
                }

                if( $flag == 0 ){
                    // Check current referral link
                    $result = $objInvited->checkExist(base64_decode($customer_id), base64_decode($friend_email), base64_decode($referral_key));                
                    if( $result->getId() > 0 ){ // Exists in DB
                        //points add to customer
                        $customer = Mage::getModel('customer/customer');
                        $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($invite);        

                        Mage::dispatchEvent('invitation_referral_link_click',array('invite'=>$invite,'request'=>$this->getRequest()));

                        Mage::getModel('rewardpoints/invitations')->getCollection()->updateStatus($result->getId());
                    } 
                }      
            }
            // phong.tran@giaiphapso.com
            //Mage::getSingleton('core/session')->addSuccess(Mage::helper('rewardpoints')->__('Thank you for visiting our site'));
            //$this->_redirectUrl(Mage::getUrl('customer/account/login'));
            $this->_redirectUrl(Mage::getUrl('loyalty/success'));
        }
}