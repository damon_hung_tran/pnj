<?php

class TTS_Baokim_Block_Form_Baokim extends Mage_Payment_Block_Form
{
     private $get_seller_info = '/payment/rest/payment_pro_api/get_seller_info';
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payment/form/baokim.phtml');
    }
    public function baokimGetSellerInfo() {
        require_once(Mage::getBaseDir('lib') . '/Baokim/function.php');
//        $url = '/payment/rest/payment_pro_api/get_seller_info';
        $arrayGet = array('business' => Mage::getSingleton('baokim/baokim')->getConfigData('email_bussiness'));
        $signature = makeSignature('GET', $this->get_seller_info, $arrayGet, array(), PRIVATE_KEY_BAOKIM);
        $url_signature = $uri . $this->get_seller_info . '?signature=' . $signature . '&business=' . $arrayGet['business'];
        
        $curl = curl_init($url_signature);
        curl_setopt_array($curl, array(
            CURLOPT_POST => false,
            CURLOPT_HEADER => false,
            CURLINFO_HEADER_OUT => true,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPAUTH => CURLAUTH_DIGEST | CURLAUTH_BASIC,
            CURLOPT_USERPWD => Mage::getSingleton('baokim/baokim')->getConfigData('merchant_id') . ':' . Mage::getSingleton('baokim/baokim')->getConfigData('secure_pass'),
        ));
        $data = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $result = json_decode($data, true);
       
        try {
            if ($status == 200) {
              
                return $result['bank_payment_methods'];
            } else {
                echo $result['error'];
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

}
