<?php

class TTS_Baokim_Model_Mysql4_Order extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the magento core id refers to the key field in your database table.
        $this->_init('baokim/order', 'transaction_id');
    }
}