<?php

class TTS_Baokim_Model_Baokim extends Mage_Payment_Model_Method_Abstract {

    private $baokim_url = '/payment/rest/payment_pro_api/pay_by_card';
    protected $_code = 'baokim';
    protected $_formBlockType = 'baokim/form_baokim';

//    protected $_infoBlockType = 'baokim/info_baokim';

    public function getOrderPlaceRedirectUrl() {
        $bankcode = $this->getBankCode();
        $_SESSION['bank_code'] = $bankcode;
        return Mage::getUrl('baokim/index/redirect', array('_secure' => true));
    }

    public function getBankCode() {
        $post = Mage::app()->getRequest()->getPost();
        $bankcode = '';
        $bankcode = $post['payment']['bankcode'];
        return $bankcode;
    }

    public function MarkOrderCancelled($order) {
        $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, 'canceled', false)->save();
    }

    public function MarkOrderFail($order) {
        $order->setState(Mage_Sales_Model_Order::STATE_NEW, 'pending', false)->save();
    }

    public function MarkOrderSuccess(array $params) {
        if ($params) {
            ////////////////////// Using Model ////////////////////////////////////
            $model_transaction = Mage::getSingleton('baokim/order');
            $model_order = Mage::getSingleton('sales/order');
            $orderId = $params['order_id'];
            $session_orderId = $model_order->load(Mage::getSingleton('checkout/session')->getLastOrderId())->getIncrementId();
            ////////////////////// Verify exits transaction ///////////////////////
            if ($session_orderId !== $orderId) {
                $this->_forward('404');
                return;
            }
            ////////////////////// UPDATE ORDER STATUS ////////////////////////////
            $order = $model_order->loadByIncrementId($session_orderId);
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, 'processing', false)->save();
            ////////////////////// SAVE TRANSACTION ////////////////////////////
            /* SAVE IN TABLE order_payment_transaction */
            $bk_transaction_id = (string) $params['transaction_id'];
            $bk_signed_date_time = gmdate("Y-m-d\TH:i:s\Z");
            $entity_id = $order->getEntity_id();
            $bl_paymentid = Mage::getModel('sales/order_payment')->load($entity_id, 'parent_id')->getEntity_id();
            $data_transaction['order_id'] = $entity_id;
            $data_transaction['payment_id'] = $bl_paymentid;
            $data_transaction['txn_id'] = $bk_transaction_id;
            $data_transaction['txn_type'] = 'Order';
            $data_transaction['created_at'] = $bk_signed_date_time;
            $data_transaction['is_closed'] = 0;
            $data_transaction['additional_information'] = '';
            $model_transaction->setData($data_transaction);
            $model_transaction->save();
            ////////////////////// SEND EMAIL ////////////////////////////
            $order->sendNewOrderEmail();
        }
    }

    public function verifyResponseUrl($url_params = array()) {
        if (empty($url_params['checksum'])) {
            echo "invalid parameters: checksum is missing";
            return FALSE;
        }
        $checksum = $url_params['checksum'];
        unset($url_params['checksum']);
        ksort($url_params);
        if (strcasecmp($checksum, hash_hmac('SHA1', implode('', $url_params), $this->getConfigData('secure_pass_manual'))) === 0) {
            return true;
        } else {
            return false;
        }
    }

    public function baokimCheckout($order_id, $bank_code) {
        require_once(Mage::getBaseDir('lib') . '/Baokim/function.php');
        $homeUrl = Mage::helper('core/url')->getHomeUrl();
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
        $getGrandTotal = $order->getGrandTotal();
        $getGrandTotalArr = explode(".", $getGrandTotal);
        $getGrandTotalArr0 = $getGrandTotalArr[0];
        $getGrandTotalArr1 = $getGrandTotalArr[1];
        $getGrandTotalArr1 = substr($getGrandTotalArr1, 0, 2);
        $amount_total = $getGrandTotalArr0 . '.' . $getGrandTotalArr1;
        $customer = $order->getShippingAddress()->getData();
        $bk_params['order_id'] = $order_id;
        $bk_params['business'] = $this->getConfigData('email_bussiness');
        $bk_params['order_description'] = 'Mua hàng từ website PNJ';
        $bk_params['total_amount'] = $getGrandTotalArr0;
        $bk_params['tax_fee'] = '0';
        $bk_params['shipping_fee'] = '0';
        $bk_params['url_success'] = $homeUrl . $this->getConfigData('url_success');
        $bk_params['url_cancel'] = $this->getConfigData('cancel_url');
        $bk_params['escrow_timeout'] = '1';
        $bk_params['bank_payment_method_id'] = $bank_code;
        $bk_params['transaction_mode_id'] = '2';
        $bk_params['mui'] = 'charge';
        $bk_params['message'] = '';
        $bk_params['currency'] = $this->getConfigData('currency');
        $bk_params['order_payment_fee_for_sender'] = '0';
        $bk_params['payer_name'] = $order->getBillingAddress()->getName();
        $bk_params['payer_email'] = $order->getBillingAddress()->getEmail();
        $bk_params['payer_phone_no'] = $customer['telephone'];
        $bk_params['payer_address'] = $customer['street'];
        $signature = makeSignature('POST', $this->baokim_url, array(), $bk_params, PRIVATE_KEY_BAOKIM);
        $url_signature = $uri . $this->baokim_url . '?signature=' . $signature;
        $curl = curl_init($url_signature);
        curl_setopt_array($curl, array(
            CURLOPT_POST => true,
            CURLOPT_HEADER => false,
            CURLINFO_HEADER_OUT => true,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPAUTH => CURLAUTH_DIGEST | CURLAUTH_BASIC,
            CURLOPT_USERPWD => $this->getConfigData('merchant_id') . ':' . $this->getConfigData('secure_pass'),
            CURLOPT_POSTFIELDS => $bk_params
        ));
        $data = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $result = json_decode($data, true);
        if ($status == 200) {
            switch ($result['next_action']) {
                case 'redirect' :
                    $url = $result['redirect_url'];
                    echo "<script>window.location='" . $url . "'</script>";
                case 'display_guide' :
                    $url = $result['guide_url'];
                    echo "<script>window.location='" . $url . "'</script>";
            }
        } elseif ($status == 450) {
            var_dump($result);
        } else {
            var_dump($result['error']);
        }
    }

}
