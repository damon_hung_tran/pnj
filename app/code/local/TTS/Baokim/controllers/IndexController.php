<?php

class TTS_Baokim_IndexController extends Mage_Core_Controller_Front_Action {

    public function redirectAction() {
        $session = Mage::getSingleton('checkout/session');
        $order_id = $session->getLastRealOrderId();
        $session_bankcode = $_SESSION['bank_code'];
//        if (empty($session_bankcode)) {
//            Mage::getSingleton('core/session')->addError($this->__('Please choose bank payment'));
//            $this->_redirectUrl('onestepcheckout');
//        }
        if ($order_id == '') {
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        } else {
            try {
                $url = Mage::getModel('baokim/baokim')->baokimCheckout($order_id, $session_bankcode);
                unset($_SESSION['bank_code']);
            } catch (Exception $e) {
                $this->_redirectUrl('checkout/cart');
            }
        }
        $this->_redirectUrl($url);
    }

    public function successAction() {
        ///////////////////// GET RETURN REQUEST ///////////////////////////////
        $params = $this->getRequest()->getParams();
        if (empty($params)) {
            //////////////////// TRADE CANCEL /////////////////////////////////////
            $order = Mage::getSingleton('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
            $method = Mage::getSingleton('baokim/baokim');
            $method->MarkOrderCancelled($order);
            Mage::getSingleton('core/session')->addError($this->__('You has been canceled transaction'));
            $this->_redirectUrl(Mage::getUrl('checkout/cart'));
        } else {
            $method = Mage::getSingleton('baokim/baokim');
            $check_success = $method->verifyResponseUrl($params);
            if ($check_success == true) {
                //////////////////// TRADE SUCCESS /////////////////////////////////////
                $method->MarkOrderSuccess($params);
                /////////////////////// REDIRECT ONEPAY SUCCESS//////////////////////////
                $this->_redirect('checkout/onepage/success');
            } else {
                //////////////////// TRADE FAIL /////////////////////////////////////
                $order = Mage::getSingleton('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
                $method->MarkOrderFail($order);
                Mage::getSingleton('core/session')->addError($this->__('Service team will contact with you about this failed transaction'));
                /////////////////////// REDIRECT ONEPAY SUCCESS//////////////////////////
                $this->_redirect('checkout/onepage/success');
            }
        }
    }

}
