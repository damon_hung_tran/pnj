<?php



class TTS_Direct_Block_Direct extends Mage_Checkout_Block_Onepage_Success {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getDirect()     
     { 
        if (!$this->hasData('direct')) {
            $this->setData('direct', Mage::registry('direct'));
        }
        return $this->getData('direct');
        
    } 
    public function getClientIp() {
        $clientIp = $_SERVER['REMOTE_ADDR'];
        if (empty($clientIp))
            $clientIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (empty($clientIp))
            $clientIp = $_SERVER['SERVER_ADDR'];
        if ($clientIp == '::1')
            $clientIp = '127.0.0.1';
        return $clientIp;
    }

   

}