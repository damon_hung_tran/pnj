<?php

class TTS_Direct_Block_Info_Direct extends Mage_Payment_Block_Info {


    public function toPdf() {
        $this->setTemplate('payment/info/pdf/direct.phtml');
        return $this->toHtml();
    }

}

?>