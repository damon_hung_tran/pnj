<?php

class TTS_Direct_Model_Mysql4_Direct extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the p123pay_id refers to the key field in your database table.
        $this->_init('direct/direct', 'direct_id');
    }
}