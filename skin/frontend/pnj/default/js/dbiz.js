jQuery(document).ready(function()
{
    // Open support form
    jQuery("#ab_support").click(function(){
         jQuery(".support_chat").css("display","block");
         jQuery(this).css("display","none");
         
         jQuery('.scroll-top').css({
            'bottom': 520
        });
    });
    
    // Close support form
    jQuery(".sp_close").click(function(){
         close_support_form();
    });
    jQuery(document).click(function() {
        close_support_form();
    });
    jQuery(".ab_support, .scroll-top").click(function(e) {
        e.stopPropagation(); // This is the preferred method.
        return false;        // This should not be used unless you do not want
                             // any click events registering inside the div
    });

    // Move to top
    jQuery('.scroll-top').click(function(){
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
    });
    
    jQuery(window).scroll(function () {
        if(jQuery(window).scrollTop() != 0) {
            jQuery('.scrollTop').fadeIn();
        } else {
            jQuery('.scrollTop').fadeOut();
        }
    });
    
	/* scale header top menu */
	scale_top_header();
	/* Hover for Products */
	jQuery('.grid .item').mouseenter (function(){
		jQuery(this).addClass('item-hover');
	});
	jQuery('.grid .item').mouseleave (function(){
		jQuery(this).removeClass('item-hover');
	});
	
	/* Show popup Newsletter */
	jQuery("footer .newsletter a").fancybox({
		overlayColor	: '#003064',
		autoDimensions	: false,
		autoSize		: false,
		fitToView		: false,
		width			: 440,
		height			: 80
	});
	
	jQuery("#cbo_category").selectbox({});
	
	var _web_links_status = false;
	jQuery('.web_links .web_link_contain').hide();
	
	jQuery('.web_links span').click(function(){
		if (_web_links_status) {
			_web_links_status = false;
		} else {
			_web_links_status = true;
		}
		show_hide_weblink();
	});
	jQuery('.web_links .web_link_contain a').click(function(){
		_web_links_status = false;
		show_hide_weblink();
	});
	jQuery('.web_links .web_link_contain').mouseleave(function(){
		_web_links_status = false;
		show_hide_weblink();
	});
	
    // Click quickview bottom
    jQuery('.btn-qview').click(function(){
        jQuery('#md_quickview_handler').click();
        return false;
    });
});
    
function scale_top_header() {
	var _width = 0;
	jQuery('#top_hotline').css('width', 'auto');
	jQuery('#top_nav').css('margin-left', '0');
	_width = jQuery('#top_hotline').width() + jQuery('#top_nav').outerWidth() + jQuery('#right_top_nav').width() ;
	
	var _w = jQuery('#top_hotline').parent().width();
	_w = _w - jQuery('#top_hotline').width();
	_w = _w - jQuery('#right_top_nav').width();
	_w = _w - jQuery('#top_nav').width();
	jQuery('#top_nav').css({
		'margin-left': parseInt((_w - 5) / 2)
	});
}

function show_hide_weblink() {
    if (_web_links_status) {
        jQuery('.web_links .web_link_contain').stop().slideDown(100);
    } else {
        jQuery('.web_links .web_link_contain').stop().slideUp(100);
    }
}

function close_support_form(){
    jQuery("#ab_support").css("display","block");
    jQuery(".support_chat").css("display","none");

    jQuery('.scroll-top').css({
        'bottom': 150
    });   
}