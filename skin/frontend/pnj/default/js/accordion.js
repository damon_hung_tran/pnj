jQuery(document).ready(function() {
    jQuery('#accordion > div:not(:first)').hide();
    jQuery('#accordion > h3:first').addClass('active');
    jQuery('#accordion > h3').click(function() {
        jQuery('#accordion .active').removeClass('active');
        jQuery('#accordion > div').slideUp('normal');
        if(jQuery(this).next('div').is(':hidden') == true) {
        jQuery(this).addClass('active');
        jQuery(this).next('div').slideDown('normal');
        }
    });
    jQuery('#accordion > h3').hover(function(){//over
        jQuery(this).addClass('on');
    },function() {//out
        jQuery(this).removeClass('on');
    });
});