jQuery(document).ready(function(){
	jQuery('#bt_select_size').click(function(){
		if (jQuery('.select_area').css('display') == 'none') {
			jQuery('.select_area').slideDown(100);
		} else {
			jQuery('.select_area').slideUp(100);
		}
	});
	jQuery('.select_area').mouseleave(function(){
		jQuery(this).slideUp(100);
	});
	jQuery('.select_area input').click(function(){
		jQuery('.select_area').slideUp(100);
	});
	jQuery('#customer-reviews .content-review .a-left').addClass('short-item');
	
	jQuery('#customer-reviews .comments_content_more a').click(function(e){
		e.preventDefault();
		if (jQuery(this).html() == 'Xem thêm +') {
			jQuery(this).html('Thu nhỏ -');
		} else {
			jQuery(this).html('Xem thêm +');
		}
		var _cm = jQuery(this).parent().parent().find('.a-left');
		if (jQuery(_cm).hasClass('short-item')) {
			jQuery(_cm).stop().removeClass('short-item', 300);
		} else {
			jQuery(_cm).stop().addClass('short-item', 300);
		}
	});
    
    // Start Fixed button add to cart
    var btnCart = jQuery('.product_nav > .btn-cart').offset().top;
    jQuery(window).scroll(function(){
        if( jQuery(window).scrollTop() >= btnCart - 54 )
            jQuery('#liAddToCart').show();
        else
            jQuery('#liAddToCart').hide();
    })
    // End Fixed button add to cart
});

jQuery(window).load(function() {
    // Start Fixed tabs
    var tabs = jQuery('#divNavTabs').offset().top;
    jQuery(window).scroll(function(){
        if( jQuery(window).scrollTop() >= tabs ){
            jQuery('#divNavTabs').addClass('fixedTabs');
            jQuery('#divNavTabs > div').addClass('divCenterTabs');
        }else{
            jQuery('#divNavTabs').removeClass('fixedTabs');
            jQuery('#divNavTabs > div').removeClass('divCenterTabs');
        }
    })
    // End Fixed tabs
});