var t_menu_slide = 10;

jQuery.browser.chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
jQuery.browser.safari = /safari/.test(navigator.userAgent.toLowerCase());

jQuery(function() {
	/** menu active **/
	jQuery('#mainmenu li.level-0').mouseenter(function() {
		var _l = jQuery(this).position().left;
		_l = 0 - _l;
		if (jQuery.browser.chrome) {
			_l = _l - 1;
		}
		jQuery(this).find('.submenu').css({
			left: _l
		});

		jQuery(this).find('.submenu').stop().delay(500).fadeIn(100);
	});

	jQuery('#mainmenu li.level-0').mouseleave(function() {
		jQuery(this).find('.submenu').stop().delay(10).hide();
	});

	jQuery('#menu_area li.level-0').each(function() {
		var self = this;
		href = jQuery(this).find('a');
		for (var i = 0; i < href.length; i++) {
			if (jQuery(href[i]).attr('href') == window.location.href) {
				jQuery(self).addClass('active');
				break;
			}
		}
	});
});
