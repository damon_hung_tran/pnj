jQuery(document).ready(function()
{
	jQuery('.menu_left li a').click(function(e)
	{
		if (jQuery(this).parent().find('ul').length > 0)
		{
			e.preventDefault();
			jQuery(this).parent().find('ul').toggle(300);
		}
	});
	
	jQuery('.menu_left li.par ul li').mouseenter(function(){
		jQuery(this).find('ul').fadeIn(300);
	});
	jQuery('.menu_left li.par ul li').mouseleave(function(){
		jQuery(this).find('ul').fadeOut(300);
	});
});