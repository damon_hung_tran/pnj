var Magic360 = (function () {
    var h, g;
    h = g = (function () {
        var k = {
            version: "v3.0.b3-6-g35da456",
            UUID: 0,
            storage: {},
            $uuid: function (m) {
                return (m.$J_UUID || (m.$J_UUID = ++j.UUID))
            },
            getStorage: function (m) {
                return (j.storage[m] || (j.storage[m] = {}))
            },
            $F: function () {},
            $false: function () {
                return false
            },
            $true: function () {
                return true
            },
            stylesId: "mjs-" + Math.floor(Math.random() * new Date().getTime()),
            defined: function (m) {
                return (undefined != m)
            },
            ifndef: function (n, m) {
                return (undefined != n) ? n : m
            },
            exists: function (m) {
                return !!(m)
            },
            jTypeOf: function (m) {
                if (!j.defined(m)) {
                    return false
                }
                if (m.$J_TYPE) {
                    return m.$J_TYPE
                }
                if ( !! m.nodeType) {
                    if (1 == m.nodeType) {
                        return "element"
                    }
                    if (3 == m.nodeType) {
                        return "textnode"
                    }
                }
                if (m.length && m.item) {
                    return "collection"
                }
                if (m.length && m.callee) {
                    return "arguments"
                }
                if ((m instanceof window.Object || m instanceof window.Function) && m.constructor === j.Class) {
                    return "class"
                }
                if (m instanceof window.Array) {
                    return "array"
                }
                if (m instanceof window.Function) {
                    return "function"
                }
                if (m instanceof window.String) {
                    return "string"
                }
                if (j.jBrowser.trident) {
                    if (j.defined(m.cancelBubble)) {
                        return "event"
                    }
                } else {
                    if (m === window.event || m.constructor == window.Event || m.constructor == window.MouseEvent || m.constructor == window.UIEvent || m.constructor == window.KeyboardEvent || m.constructor == window.KeyEvent) {
                        return "event"
                    }
                } if (m instanceof window.Date) {
                    return "date"
                }
                if (m instanceof window.RegExp) {
                    return "regexp"
                }
                if (m === window) {
                    return "window"
                }
                if (m === document) {
                    return "document"
                }
                return typeof (m)
            },
            extend: function (v, u) {
                if (!(v instanceof window.Array)) {
                    v = [v]
                }
                for (var s = 0, n = v.length; s < n; s++) {
                    if (!j.defined(v)) {
                        continue
                    }
                    for (var q in (u || {})) {
                        try {
                            v[s][q] = u[q]
                        } catch (m) {}
                    }
                }
                return v[0]
            },
            implement: function (u, s) {
                if (!(u instanceof window.Array)) {
                    u = [u]
                }
                for (var q = 0, m = u.length; q < m; q++) {
                    if (!j.defined(u[q])) {
                        continue
                    }
                    if (!u[q].prototype) {
                        continue
                    }
                    for (var n in (s || {})) {
                        if (!u[q].prototype[n]) {
                            u[q].prototype[n] = s[n]
                        }
                    }
                }
                return u[0]
            },
            nativize: function (q, n) {
                if (!j.defined(q)) {
                    return q
                }
                for (var m in (n || {})) {
                    if (!q[m]) {
                        q[m] = n[m]
                    }
                }
                return q
            },
            $try: function () {
                for (var n = 0, m = arguments.length; n < m; n++) {
                    try {
                        return arguments[n]()
                    } catch (o) {}
                }
                return null
            },
            $A: function (p) {
                if (!j.defined(p)) {
                    return j.$([])
                }
                if (p.toArray) {
                    return j.$(p.toArray())
                }
                if (p.item) {
                    var n = p.length || 0,
                        m = new Array(n);
                    while (n--) {
                        m[n] = p[n]
                    }
                    return j.$(m)
                }
                return j.$(Array.prototype.slice.call(p))
            },
            now: function () {
                return new Date().getTime()
            },
            detach: function (u) {
                var q;
                switch (j.jTypeOf(u)) {
                    case "object":
                        q = {};
                        for (var s in u) {
                            q[s] = j.detach(u[s])
                        }
                        break;
                    case "array":
                        q = [];
                        for (var n = 0, m = u.length; n < m; n++) {
                            q[n] = j.detach(u[n])
                        }
                        break;
                    default:
                        return u
                }
                return j.$(q)
            },
            $: function (n) {
                if (!j.defined(n)) {
                    return null
                }
                if (n.$J_EXT) {
                    return n
                }
                switch (j.jTypeOf(n)) {
                    case "array":
                        n = j.nativize(n, j.extend(j.Array, {
                            $J_EXT: j.$F
                        }));
                        n.jEach = n.forEach;
                        return n;
                        break;
                    case "string":
                        var m = document.getElementById(n);
                        if (j.defined(m)) {
                            return j.$(m)
                        }
                        return null;
                        break;
                    case "window":
                    case "document":
                        j.$uuid(n);
                        n = j.extend(n, j.Doc);
                        break;
                    case "element":
                        j.$uuid(n);
                        n = j.extend(n, j.Element);
                        break;
                    case "event":
                        n = j.extend(n, j.Event);
                        break;
                    case "textnode":
                        return n;
                        break;
                    case "function":
                    case "array":
                    case "date":
                    default:
                        break
                }
                return j.extend(n, {
                    $J_EXT: j.$F
                })
            },
            $new: function (m, o, n) {
                return j.$(j.doc.createElement(m)).setProps(o || {}).jSetCss(n || {})
            },
            addCSS: function (n, q, w) {
                var s, o, u, v = [],
                    m = -1;
                w || (w = j.stylesId);
                s = j.$(w) || (document.head || document.body).appendChild(j.$new("style", {
                    id: w,
                    type: "text/css"
                }));
                o = s.sheet || s.styleSheet;
                if ("string" != j.jTypeOf(q)) {
                    for (var u in q) {
                        v.push(u + ":" + q[u])
                    }
                    q = v.join(";")
                }
                if (o.insertRule) {
                    m = o.insertRule(n + " {" + q + "}", o.cssRules.length)
                } else {
                    m = o.addRule(n, q)
                }
                return m
            },
            generateUUID: function () {
                return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (o) {
                    var n = Math.random() * 16 | 0,
                        m = o == "x" ? n : (n & 3 | 8);
                    return m.toString(16)
                }).toUpperCase()
            },
            getAbsoluteURL: function (m) {
                if (/http(s)?:\/\//.test(m)) {
                    return m
                }
                var n = document.createElement("div");
                n.innerHTML = '<a href="' + m + '">x</a>';
                return n.firstChild.href
            },
            getHashCode: function (o) {
                var p = 0,
                    m = o.length;
                for (var n = 0; n < m; ++n) {
                    p = 31 * p + o.charCodeAt(n);
                    p %= 4294967296
                }
                return p
            }
        };
        var j = k;
        var i = k.$;
        if (!window.magicJS) {
            window.magicJS = k;
            window.$mjs = k.$
        }
        j.Array = {
            $J_TYPE: "array",
            indexOf: function (p, q) {
                var m = this.length;
                for (var n = this.length, o = (q < 0) ? Math.max(0, n + q) : q || 0; o < n; o++) {
                    if (this[o] === p) {
                        return o
                    }
                }
                return -1
            },
            contains: function (m, n) {
                return this.indexOf(m, n) != -1
            },
            forEach: function (m, q) {
                for (var p = 0, n = this.length; p < n; p++) {
                    if (p in this) {
                        m.call(q, this[p], p, this)
                    }
                }
            },
            filter: function (m, u) {
                var s = [];
                for (var q = 0, n = this.length; q < n; q++) {
                    if (q in this) {
                        var p = this[q];
                        if (m.call(u, this[q], q, this)) {
                            s.push(p)
                        }
                    }
                }
                return s
            },
            map: function (m, s) {
                var q = [];
                for (var p = 0, n = this.length; p < n; p++) {
                    if (p in this) {
                        q[p] = m.call(s, this[p], p, this)
                    }
                }
                return q
            }
        };
        j.implement(String, {
            $J_TYPE: "string",
            jTrim: function () {
                return this.replace(/^\s+|\s+$/g, "")
            },
            eq: function (m, n) {
                return (n || false) ? (this.toString() === m.toString()) : (this.toLowerCase().toString() === m.toLowerCase().toString())
            },
            jCamelize: function () {
                return this.replace(/-\D/g, function (n) {
                    return n.charAt(1).toUpperCase()
                })
            },
            dashize: function () {
                return this.replace(/[A-Z]/g, function (n) {
                    return ("-" + n.charAt(0).toLowerCase())
                })
            },
            jToInt: function (m) {
                return parseInt(this, m || 10)
            },
            toFloat: function () {
                return parseFloat(this)
            },
            jToBool: function () {
                return !this.replace(/true/i, "").jTrim()
            },
            has: function (n, m) {
                m = m || "";
                return (m + this + m).indexOf(m + n + m) > -1
            }
        });
        k.implement(Function, {
            $J_TYPE: "function",
            jBind: function () {
                var p = j.$A(arguments),
                    n = this,
                    q = p.shift();
                return function () {
                    return n.apply(q || null, p.concat(j.$A(arguments)))
                }
            },
            jBindAsEvent: function () {
                var p = j.$A(arguments),
                    n = this,
                    q = p.shift();
                return function (m) {
                    return n.apply(q || null, j.$([m || window.event]).concat(p))
                }
            },
            jDelay: function () {
                var o = j.$A(arguments),
                    n = this,
                    p = o.shift();
                return window.setTimeout(function () {
                    return n.apply(n, o)
                }, p || 0)
            },
            jDefer: function () {
                var o = j.$A(arguments),
                    n = this;
                return function () {
                    return n.jDelay.apply(n, o)
                }
            },
            interval: function () {
                var o = j.$A(arguments),
                    n = this,
                    p = o.shift();
                return window.setInterval(function () {
                    return n.apply(n, o)
                }, p || 0)
            }
        });
        var l = navigator.userAgent.toLowerCase();
        j.jBrowser = {
            features: {
                xpath: !! (document.evaluate),
                air: !! (window.runtime),
                query: !! (document.querySelector),
                fullScreen: !! (document.fullscreenEnabled || document.exitFullscreen || document.cancelFullScreen || document.webkitexitFullscreen || document.webkitCancelFullScreen || document.mozCancelFullScreen || document.oCancelFullScreen || document.msCancelFullScreen),
                xhr2: !! (window.ProgressEvent) && !! (window.FormData) && (window.XMLHttpRequest && "withCredentials" in new XMLHttpRequest)
            },
            touchScreen: function () {
                return "ontouchstart" in window || (window.DocumentTouch && document instanceof DocumentTouch)
            }(),
            mobile: l.match(/android|tablet|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(jBrowser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/) ? true : false,
            engine: (window.opera) ? "presto" : !! (window.ActiveXObject) ? "trident" : (undefined != document.getBoxObjectFor || null != window.mozInnerScreenY) ? "gecko" : (null != window.WebKitPoint || !navigator.taintEnabled) ? "webkit" : "unknown",
            version: "",
            cssPrefix: "",
            cssDomPrefix: "",
            domPrefix: "",
            ieMode: 0,
            platform: l.match(/ip(?:ad|od|hone)/) ? "ios" : (l.match(/(?:webos|android)/) || navigator.platform.match(/mac|win|linux/i) || ["other"])[0].toLowerCase(),
            backCompat: document.compatMode && "backcompat" == document.compatMode.toLowerCase(),
            getDoc: function () {
                return (document.compatMode && "backcompat" == document.compatMode.toLowerCase()) ? document.body : document.documentElement
            },
            ready: false,
            onready: function () {
                if (j.jBrowser.ready) {
                    return
                }
                j.jBrowser.ready = true;
                j.body = j.$(document.body);
                j.win = j.$(window);
                (function () {
                    j.jBrowser.css3Transformations = {
                        capable: false,
                        prefix: ""
                    };
                    if (typeof document.body.style.transform !== "undefined") {
                        j.jBrowser.css3Transformations.capable = true
                    } else {
                        var o = "Webkit Moz O ms Khtml".split(" ");
                        for (var n = 0, m = o.length; n < m; n++) {
                            j.jBrowser.css3Transformations.prefix = o[n];
                            if (typeof document.body.style[j.jBrowser.css3Transformations.prefix + "Transform"] !== "undefined") {
                                j.jBrowser.css3Transformations.capable = true;
                                break
                            }
                        }
                    }
                })();
                (function () {
                    j.jBrowser.css3Animation = {
                        capable: false,
                        prefix: ""
                    };
                    if (typeof document.body.style.animationName !== "undefined") {
                        j.jBrowser.css3Animation.capable = true
                    } else {
                        var o = "Webkit Moz O ms Khtml".split(" ");
                        for (var n = 0, m = o.length; n < m; n++) {
                            j.jBrowser.css3Animation.prefix = o[n];
                            if (typeof document.body.style[j.jBrowser.css3Animation.prefix + "AnimationName"] !== "undefined") {
                                j.jBrowser.css3Animation.capable = true;
                                break
                            }
                        }
                    }
                })();
                j.$(document).jCallEvent("domready")
            }
        };
        (function () {
            function m() {
                return !!(arguments.callee.caller)
            }
            j.jBrowser.version = ("presto" == j.jBrowser.engine) ? !! (document.head) ? 270 : !! (window.applicationCache) ? 260 : !! (window.localStorage) ? 250 : (j.jBrowser.features.query) ? 220 : ((m()) ? 211 : ((document.getElementsByClassName) ? 210 : 200)) : ("trident" == j.jBrowser.engine) ? !! (window.msPerformance || window.performance) ? 900 : !! (window.XMLHttpRequest && window.postMessage) ? 6 : ((window.XMLHttpRequest) ? 5 : 4) : ("webkit" == j.jBrowser.engine) ? ((j.jBrowser.features.xpath) ? ((j.jBrowser.features.query) ? 525 : 420) : 419) : ("gecko" == j.jBrowser.engine) ? !! (document.head) ? 200 : !! document.readyState ? 192 : !! (window.localStorage) ? 191 : ((document.getElementsByClassName) ? 190 : 181) : "";
            j.jBrowser[j.jBrowser.engine] = j.jBrowser[j.jBrowser.engine + j.jBrowser.version] = true;
            if (window.chrome) {
                j.jBrowser.chrome = true
            }
            var n = ({
                gecko: ["-moz-", "Moz", "moz"],
                webkit: ["-webkit-", "Webkit", "webkit"],
                trident: ["-ms-", "ms", "ms"],
                presto: ["-o-", "O", "o"]
            })[j.jBrowser.engine] || ["", "", ""];
            j.jBrowser.cssPrefix = n[0];
            j.jBrowser.cssDomPrefix = n[1];
            j.jBrowser.domPrefix = n[2];
            j.jBrowser.ieMode = (!j.jBrowser.trident) ? 0 : (document.documentMode) ? document.documentMode : function () {
                var o = 0;
                if (j.jBrowser.backCompat) {
                    return 5
                }
                switch (j.jBrowser.version) {
                    case 4:
                        o = 6;
                        break;
                    case 5:
                        o = 7;
                        break;
                    case 6:
                        o = 8;
                        break;
                    case 900:
                        o = 9;
                        break
                }
                return o
            }()
        })();
        (function () {
            j.jBrowser.fullScreen = {
                capable: j.jBrowser.features.fullScreen,
                enabled: function () {
                    return !!(document.fullscreenElement || document.fullScreen || document.webkitIsFullScreen || document[j.jBrowser.domPrefix + "FullScreen"])
                },
                request: function (m, n) {
                    n || (n = {});
                    if (this.capable) {
                        j.$(document).jAddEvent(this.changeEventName, this.onchange = function (o) {
                            if (this.enabled()) {
                                n.onEnter && n.onEnter()
                            } else {
                                j.$(document).jRemoveEvent(this.changeEventName, this.onchange);
                                n.onExit && n.onExit()
                            }
                        }.jBindAsEvent(this));
                        j.$(document).jAddEvent(this.errorEventName, this.onerror = function (o) {
                            n.fallback && n.fallback();
                            j.$(document).jRemoveEvent(this.errorEventName, this.onerror)
                        }.jBindAsEvent(this));
                        m[j.jBrowser.domPrefix + "RequestFullScreen"] ? m[j.jBrowser.domPrefix + "RequestFullScreen"]() : m.requestFullscreen()
                    } else {
                        if (n.fallback) {
                            n.fallback()
                        }
                    }
                },
                cancel: (document.exitFullscreen || document.cancelFullScreen || document[j.jBrowser.domPrefix + "ExitFullscreen"] || document[j.jBrowser.domPrefix + "CancelFullScreen"] || function () {}).jBind(document),
                changeEventName: (document.exitFullscreen ? "" : j.jBrowser.domPrefix) + "fullscreenchange",
                errorEventName: (document.exitFullscreen ? "" : j.jBrowser.domPrefix) + "fullscreenerror",
                prefix: j.jBrowser.domPrefix,
                activeElement: null
            }
        })();
        j.Element = {
            jHasClass: function (m) {
                return this.className.has(m, " ")
            },
            jAddClass: function (m) {
                if (m && !this.jHasClass(m)) {
                    this.className += (this.className ? " " : "") + m
                }
                return this
            },
            jRemoveClass: function (m) {
                m = m || ".*";
                this.className = this.className.replace(new RegExp("(^|\\s)" + m + "(?:\\s|$)"), "$1").jTrim();
                return this
            },
            jToggleClass: function (m) {
                return this.jHasClass(m) ? this.jRemoveClass(m) : this.jAddClass(m)
            },
            jGetCss: function (o) {
                o = (o == "float" && this.currentStyle) ? "styleFloat" : o.jCamelize();
                var m = null,
                    n = null;
                if (this.currentStyle) {
                    m = this.currentStyle[o]
                } else {
                    if (document.defaultView && document.defaultView.getComputedStyle) {
                        n = document.defaultView.getComputedStyle(this, null);
                        m = n ? n.getPropertyValue([o.dashize()]) : null
                    }
                } if (!m) {
                    m = this.style[o]
                }
                if ("opacity" == o) {
                    return j.defined(m) ? parseFloat(m) : 1
                }
                if (/^(border(Top|Bottom|Left|Right)Width)|((padding|margin)(Top|Bottom|Left|Right))$/.test(o)) {
                    m = parseInt(m) ? m : "0px"
                }
                return ("auto" == m ? null : m)
            },
            jSetCssProp: function (n, m) {
                try {
                    if ("opacity" == n) {
                        this.jSetOpacity(m);
                        return this
                    } else {
                        if ("float" == n) {
                            this.style[("undefined" === typeof (this.style.styleFloat)) ? "cssFloat" : "styleFloat"] = m;
                            return this
                        } else {
                            if (j.jBrowser.css3Transformations && /transform/.test(n)) {}
                        }
                    }
                    this.style[n.jCamelize()] = m + (("number" == j.jTypeOf(m) && !j.$(["zIndex", "zoom"]).contains(n.jCamelize())) ? "px" : "")
                } catch (o) {}
                return this
            },
            jSetCss: function (n) {
                for (var m in n) {
                    this.jSetCssProp(m, n[m])
                }
                return this
            },
            jGetStyles: function () {
                var m = {};
                j.$A(arguments).jEach(function (n) {
                    m[n] = this.jGetCss(n)
                }, this);
                return m
            },
            jSetOpacity: function (p, n) {
                var o;
                n = n || false;
                p = parseFloat(p);
                if (n) {
                    if (p == 0) {
                        if ("hidden" != this.style.visibility) {
                            this.style.visibility = "hidden"
                        }
                    } else {
                        if ("visible" != this.style.visibility) {
                            this.style.visibility = "visible"
                        }
                    }
                }
                if (j.jBrowser.ieMode && j.jBrowser.ieMode < 9) {
                    if (!this.currentStyle || !this.currentStyle.hasLayout) {
                        this.style.zoom = 1
                    }
                    try {
                        o = this.filters.item("DXImageTransform.Microsoft.Alpha");
                        o.enabled = (1 != p);
                        o.opacity = p * 100
                    } catch (m) {
                        o = this.currentStyle && this.currentStyle.filter || this.style.filter || "";
                        o += (1 == p) ? "" : " progid:DXImageTransform.Microsoft.Alpha(enabled=true,opacity=" + p * 100 + ")";
                        this.style.filter = o
                    }
                }
                this.style.opacity = p;
                return this
            },
            setProps: function (m) {
                for (var n in m) {
                    this.setAttribute(n, "" + m[n])
                }
                return this
            },
            hide: function () {
                return this.jSetCss({
                    display: "none",
                    visibility: "hidden"
                })
            },
            show: function () {
                return this.jSetCss({
                    display: "block",
                    visibility: "visible"
                })
            },
            jGetSize: function () {
                return {
                    width: this.offsetWidth,
                    height: this.offsetHeight
                }
            },
            getInnerSize: function (n) {
                var m = this.jGetSize();
                m.width -= (parseFloat(this.jGetCss("border-left-width")) + parseFloat(this.jGetCss("border-right-width")));
                m.height -= (parseFloat(this.jGetCss("border-top-width")) + parseFloat(this.jGetCss("border-bottom-width")));
                if (!n) {
                    m.width -= (parseFloat(this.jGetCss("padding-left")) + parseFloat(this.jGetCss("padding-right")));
                    m.height -= (parseFloat(this.jGetCss("padding-top")) + parseFloat(this.jGetCss("padding-bottom")))
                }
                return m
            },
            jGetScroll: function () {
                return {
                    top: this.scrollTop,
                    left: this.scrollLeft
                }
            },
            jGetFullScroll: function () {
                var m = this,
                    n = {
                        top: 0,
                        left: 0
                    };
                do {
                    n.left += m.scrollLeft || 0;
                    n.top += m.scrollTop || 0;
                    m = m.parentNode
                } while (m);
                return n
            },
            jGetPosition: function () {
                if (j.defined(document.documentElement.getBoundingClientRect)) {
                    var m = this.getBoundingClientRect(),
                        o = j.$(document).jGetScroll(),
                        q = j.jBrowser.getDoc();
                    return {
                        top: m.top + o.y - q.clientTop,
                        left: m.left + o.x - q.clientLeft
                    }
                }
                var p = this,
                    n = t = 0;
                do {
                    n += p.offsetLeft || 0;
                    t += p.offsetTop || 0;
                    p = p.offsetParent
                } while (p && !(/^(?:body|html)$/i).test(p.tagName));
                return {
                    top: t,
                    left: n
                }
            },
            jGetRect: function () {
                var n = this.jGetPosition();
                var m = this.jGetSize();
                return {
                    top: n.top,
                    bottom: n.top + m.height,
                    left: n.left,
                    right: n.left + m.width
                }
            },
            changeContent: function (n) {
                try {
                    this.innerHTML = n
                } catch (m) {
                    this.innerText = n
                }
                return this
            },
            jRemove: function () {
                return (this.parentNode) ? this.parentNode.removeChild(this) : this
            },
            kill: function () {
                j.$A(this.childNodes).jEach(function (m) {
                    if (3 == m.nodeType || 8 == m.nodeType) {
                        return
                    }
                    j.$(m).kill()
                });
                this.jRemove();
                this.jClearEvents();
                if (this.$J_UUID) {
                    j.storage[this.$J_UUID] = null;
                    delete j.storage[this.$J_UUID]
                }
                return null
            },
            append: function (q, n) {
                n = n || "bottom";
                var m = this.firstChild;
                ("top" == n && m) ? this.insertBefore(q, m) : this.appendChild(q);
                return this
            },
            jAppendTo: function (q, n) {
                var m = j.$(q).append(this, n);
                return this
            },
            enclose: function (m) {
                this.append(m.parentNode.replaceChild(this, m));
                return this
            },
            hasChild: function (m) {
                if (!(m = j.$(m))) {
                    return false
                }
                return (this == m) ? false : (this.contains && !(j.jBrowser.webkit419)) ? (this.contains(m)) : (this.compareDocumentPosition) ? !! (this.compareDocumentPosition(m) & 16) : j.$A(this.byTag(m.tagName)).contains(m)
            }
        };
        j.Element.jGetStyle = j.Element.jGetCss;
        j.Element.jSetStyle = j.Element.jSetCss;
        if (!window.Element) {
            window.Element = j.$F;
            if (j.jBrowser.engine.webkit) {
                window.document.createElement("iframe")
            }
            window.Element.prototype = (j.jBrowser.engine.webkit) ? window["[[DOMElement.prototype]]"] : {}
        }
        j.implement(window.Element, {
            $J_TYPE: "element"
        });
        j.Doc = {
            jGetSize: function () {
                if (j.jBrowser.touchScreen || j.jBrowser.presto925 || j.jBrowser.webkit419) {
                    return {
                        width: window.innerWidth,
                        height: window.innerHeight
                    }
                }
                return {
                    width: j.jBrowser.getDoc().clientWidth,
                    height: j.jBrowser.getDoc().clientHeight
                }
            },
            jGetScroll: function () {
                return {
                    x: window.pageXOffset || j.jBrowser.getDoc().scrollLeft,
                    y: window.pageYOffset || j.jBrowser.getDoc().scrollTop
                }
            },
            jGetFullSize: function () {
                var m = this.jGetSize();
                return {
                    width: Math.max(j.jBrowser.getDoc().scrollWidth, m.width),
                    height: Math.max(j.jBrowser.getDoc().scrollHeight, m.height)
                }
            }
        };
        j.extend(document, {
            $J_TYPE: "document"
        });
        j.extend(window, {
            $J_TYPE: "window"
        });
        j.extend([j.Element, j.Doc], {
            jFetch: function (q, n) {
                var m = j.getStorage(this.$J_UUID),
                    o = m[q];
                if (undefined != n && undefined == o) {
                    o = m[q] = n
                }
                return (j.defined(o) ? o : null)
            },
            jStore: function (o, n) {
                var m = j.getStorage(this.$J_UUID);
                m[o] = n;
                return this
            },
            jDel: function (n) {
                var m = j.getStorage(this.$J_UUID);
                delete m[n];
                return this
            }
        });
        if (!(window.HTMLElement && window.HTMLElement.prototype && window.HTMLElement.prototype.getElementsByClassName)) {
            j.extend([j.Element, j.Doc], {
                getElementsByClassName: function (m) {
                    return j.$A(this.getElementsByTagName("*")).filter(function (p) {
                        try {
                            return (1 == p.nodeType && p.className.has(m, " "))
                        } catch (n) {}
                    })
                }
            })
        }
        j.extend([j.Element, j.Doc], {
            byClass: function () {
                return this.getElementsByClassName(arguments[0])
            },
            byTag: function () {
                return this.getElementsByTagName(arguments[0])
            }
        });
        if (j.jBrowser.fullScreen.capable && !document.requestFullScreen) {
            j.Element.requestFullScreen = function () {
                j.jBrowser.fullScreen.request(this)
            }
        }
        j.Event = {
            $J_TYPE: "event",
            isQueueStopped: j.$false,
            stop: function () {
                return this.stopDistribution().stopDefaults()
            },
            stopDistribution: function () {
                if (this.stopPropagation) {
                    this.stopPropagation()
                } else {
                    this.cancelBubble = true
                }
                return this
            },
            stopDefaults: function () {
                if (this.preventDefault) {
                    this.preventDefault()
                } else {
                    this.returnValue = false
                }
                return this
            },
            stopQueue: function () {
                this.isQueueStopped = j.$true;
                return this
            },
            jGetPageXY: function () {
                var n, m;
                n = ((/touch/i).test(this.type)) ? this.changedTouches[0] : this;
                return (!j.defined(n)) ? {
                    x: 0,
                    y: 0
                } : {
                    x: n.pageX || n.clientX + j.jBrowser.getDoc().scrollLeft,
                    y: n.pageY || n.clientY + j.jBrowser.getDoc().scrollTop
                }
            },
            getTarget: function () {
                var m = this.target || this.srcElement;
                while (m && 3 == m.nodeType) {
                    m = m.parentNode
                }
                return m
            },
            getRelated: function () {
                var n = null;
                switch (this.type) {
                    case "mouseover":
                        n = this.relatedTarget || this.fromElement;
                        break;
                    case "mouseout":
                        n = this.relatedTarget || this.toElement;
                        break;
                    default:
                        return n
                }
                try {
                    while (n && 3 == n.nodeType) {
                        n = n.parentNode
                    }
                } catch (m) {
                    n = null
                }
                return n
            },
            getButton: function () {
                if (!this.which && this.button !== undefined) {
                    return (this.button & 1 ? 1 : (this.button & 2 ? 3 : (this.button & 4 ? 2 : 0)))
                }
                return this.which
            }
        };
        j._event_add_ = "addEventListener";
        j._event_del_ = "removeEventListener";
        j._event_prefix_ = "";
        if (!document.addEventListener) {
            j._event_add_ = "attachEvent";
            j._event_del_ = "detachEvent";
            j._event_prefix_ = "on"
        }
        j.Event.Custom = {
            type: "",
            x: null,
            y: null,
            timeStamp: null,
            button: null,
            target: null,
            relatedTarget: null,
            $J_TYPE: "event.custom",
            isQueueStopped: j.$false,
            events: j.$([]),
            pushToEvents: function (m) {
                var n = m;
                this.events.push(n)
            },
            stop: function () {
                return this.stopDistribution().stopDefaults()
            },
            stopDistribution: function () {
                this.events.jEach(function (n) {
                    try {
                        n.stopDistribution()
                    } catch (m) {}
                });
                return this
            },
            stopDefaults: function () {
                this.events.jEach(function (n) {
                    try {
                        n.stopDefaults()
                    } catch (m) {}
                });
                return this
            },
            stopQueue: function () {
                this.isQueueStopped = j.$true;
                return this
            },
            jGetPageXY: function () {
                return {
                    x: this.x,
                    y: this.y
                }
            },
            getTarget: function () {
                return this.target
            },
            getRelated: function () {
                return this.relatedTarget
            },
            getButton: function () {
                return this.button
            }
        };
        j.extend([j.Element, j.Doc], {
            jAddEvent: function (v, u, s, p) {
                var q, o, n, m;
                if (j.jTypeOf(v) == "array") {
                    j.$(v).jEach(this.jAddEvent.jBindAsEvent(this, u, s));
                    return this
                }
                if (!v || !u || j.jTypeOf(v) != "string" || j.jTypeOf(u) != "function") {
                    return this
                }
                if (v == "domready" && j.jBrowser.ready) {
                    u.call(this);
                    return this
                }
                s = parseInt(s || 50);
                if (!u.$J_EUID) {
                    u.$J_EUID = Math.floor(Math.random() * j.now())
                }
                q = this.jFetch("_EVENTS_", {});
                o = q[v];
                if (!o) {
                    q[v] = o = j.$([]);
                    n = this;
                    if (j.Event.Custom[v]) {
                        j.Event.Custom[v].handler.add.call(this, p)
                    } else {
                        o.handle = function (w) {
                            w = j.extend(w || window.e, {
                                $J_TYPE: "event"
                            });
                            n.jCallEvent(v, j.$(w))
                        };
                        this[j._event_add_](j._event_prefix_ + v, o.handle, false)
                    }
                }
                m = {
                    type: v,
                    fn: u,
                    priority: s,
                    euid: u.$J_EUID
                };
                o.push(m);
                o.sort(function (x, w) {
                    return x.priority - w.priority
                });
                return this
            },
            jRemoveEvent: function (s) {
                var p = this.jFetch("_EVENTS_"),
                    o, m, n, u, q;
                if (!s || j.jTypeOf(s) != "string" || !p || !p[s]) {
                    return this
                }
                o = p[s] || [];
                q = arguments[1] || null;
                for (n = 0; n < o.length; n++) {
                    m = o[n];
                    if (!q || q.$J_EUID === m.euid) {
                        u = o.splice(n--, 1);
                        delete u
                    }
                }
                if (0 === o.length) {
                    if (j.Event.Custom[s]) {
                        j.Event.Custom[s].handler.jRemove.call(this)
                    } else {
                        this[j._event_del_](j._event_prefix_ + s, o.handle, false)
                    }
                    delete p[s]
                }
                return this
            },
            jCallEvent: function (q, u) {
                var p = this.jFetch("_EVENTS_"),
                    o, m, n;
                if (!q || j.jTypeOf(q) != "string" || !p || !p[q]) {
                    return this
                }
                try {
                    u = j.extend(u || {}, {
                        type: q
                    })
                } catch (s) {}
                if (undefined === u.timeStamp) {
                    u.timeStamp = j.now()
                }
                o = p[q] || [];
                for (n = 0; n < o.length && !(u.isQueueStopped && u.isQueueStopped()); n++) {
                    o[n].fn.call(this, u)
                }
            },
            jRaiseEvent: function (n, m) {
                var s = ("domready" == n) ? false : true,
                    q = this,
                    p;
                if (!s) {
                    this.jCallEvent(n);
                    return this
                }
                if (q === document && document.createEvent && !q.dispatchEvent) {
                    q = document.documentElement
                }
                if (document.createEvent) {
                    p = document.createEvent(n);
                    p.initEvent(m, true, true)
                } else {
                    p = document.createEventObject();
                    p.eventType = n
                } if (document.createEvent) {
                    q.dispatchEvent(p)
                } else {
                    q.fireEvent("on" + m, p)
                }
                return p
            },
            jClearEvents: function () {
                var n = this.jFetch("_EVENTS_");
                if (!n) {
                    return this
                }
                for (var m in n) {
                    this.jRemoveEvent(m)
                }
                this.jDel("_EVENTS_");
                return this
            }
        });
        (function (m) {
            if ("complete" === document.readyState) {
                return m.jBrowser.onready.jDelay(1)
            }
            if (m.jBrowser.webkit && m.jBrowser.version < 420) {
                (function () {
                    (m.$(["loaded", "complete"]).contains(document.readyState)) ? m.jBrowser.onready() : arguments.callee.jDelay(50)
                })()
            } else {
                if (m.jBrowser.trident && m.jBrowser.ieMode < 9 && window == top) {
                    (function () {
                        (m.$try(function () {
                            m.jBrowser.getDoc().doScroll("left");
                            return true
                        })) ? m.jBrowser.onready() : arguments.callee.jDelay(50)
                    })()
                } else {
                    m.$(document).jAddEvent("DOMContentLoaded", m.jBrowser.onready);
                    m.$(window).jAddEvent("load", m.jBrowser.onready)
                }
            }
        })(k);
        j.Class = function () {
            var s = null,
                n = j.$A(arguments);
            if ("class" == j.jTypeOf(n[0])) {
                s = n.shift()
            }
            var m = function () {
                for (var w in this) {
                    this[w] = j.detach(this[w])
                }
                if (this.constructor.$parent) {
                    this.$parent = {};
                    var y = this.constructor.$parent;
                    for (var x in y) {
                        var v = y[x];
                        switch (j.jTypeOf(v)) {
                            case "function":
                                this.$parent[x] = j.Class.wrap(this, v);
                                break;
                            case "object":
                                this.$parent[x] = j.detach(v);
                                break;
                            case "array":
                                this.$parent[x] = j.detach(v);
                                break
                        }
                    }
                }
                var u = (this.init) ? this.init.apply(this, arguments) : this;
                delete this.caller;
                return u
            };
            if (!m.prototype.init) {
                m.prototype.init = j.$F
            }
            if (s) {
                var q = function () {};
                q.prototype = s.prototype;
                m.prototype = new q;
                m.$parent = {};
                for (var o in s.prototype) {
                    m.$parent[o] = s.prototype[o]
                }
            } else {
                m.$parent = null
            }
            m.constructor = j.Class;
            m.prototype.constructor = m;
            j.extend(m.prototype, n[0]);
            j.extend(m, {
                $J_TYPE: "class"
            });
            return m
        };
        k.Class.wrap = function (m, n) {
            return function () {
                var p = this.caller;
                var o = n.apply(m, arguments);
                return o
            }
        };
        j.Event.Custom.btnclick = new j.Class(j.extend(j.Event.Custom, {
            type: "btnclick",
            init: function (o, n) {
                var m = n.jGetPageXY();
                this.x = m.x;
                this.y = m.y;
                this.timeStamp = n.timeStamp;
                this.button = n.getButton();
                this.target = o;
                this.pushToEvents(n)
            }
        }));
        j.Event.Custom.btnclick.handler = {
            options: {
                threshold: 200,
                button: 1
            },
            add: function (m) {
                this.jStore("event:btnclick:options", j.extend(j.detach(j.Event.Custom.btnclick.handler.options), m || {}));
                this.jAddEvent("mousedown", j.Event.Custom.btnclick.handler.handle, 1);
                this.jAddEvent("mouseup", j.Event.Custom.btnclick.handler.handle, 1);
                this.jAddEvent("click", j.Event.Custom.btnclick.handler.onclick, 1);
                if (j.jBrowser.trident && j.jBrowser.ieMode < 9) {
                    this.jAddEvent("dblclick", j.Event.Custom.btnclick.handler.handle, 1)
                }
            },
            jRemove: function () {
                this.jRemoveEvent("mousedown", j.Event.Custom.btnclick.handler.handle);
                this.jRemoveEvent("mouseup", j.Event.Custom.btnclick.handler.handle);
                this.jRemoveEvent("click", j.Event.Custom.btnclick.handler.onclick);
                if (j.jBrowser.trident && j.jBrowser.ieMode < 9) {
                    this.jRemoveEvent("dblclick", j.Event.Custom.btnclick.handler.handle)
                }
            },
            onclick: function (m) {
                m.stopDefaults()
            },
            handle: function (p) {
                var o, m, n;
                m = this.jFetch("event:btnclick:options");
                if (p.type != "dblclick" && p.getButton() != m.button) {
                    return
                }
                if ("mousedown" == p.type) {
                    p.stop();
                    o = new j.Event.Custom.btnclick(this, p);
                    this.jStore("event:btnclick:btnclickEvent", o)
                } else {
                    if ("mouseup" == p.type) {
                        o = this.jFetch("event:btnclick:btnclickEvent");
                        if (!o) {
                            return
                        }
                        n = p.jGetPageXY();
                        this.jDel("event:btnclick:btnclickEvent");
                        o.pushToEvents(p);
                        if (p.timeStamp - o.timeStamp <= m.threshold && o.x == n.x && o.y == n.y) {
                            this.jCallEvent("btnclick", o)
                        }
                    } else {
                        if (p.type == "dblclick") {
                            o = new j.Event.Custom.btnclick(this, p);
                            this.jCallEvent("btnclick", o)
                        }
                    }
                }
            }
        };
        j.Event.Custom.mousedrag = new j.Class(j.extend(j.Event.Custom, {
            type: "mousedrag",
            state: "dragstart",
            init: function (p, o, n) {
                var m = o.jGetPageXY();
                this.x = m.x;
                this.y = m.y;
                this.timeStamp = o.timeStamp;
                this.button = o.getButton();
                this.target = p;
                this.pushToEvents(o);
                this.state = n
            }
        }));
        j.Event.Custom.mousedrag.handler = {
            add: function () {
                this.jAddEvent("mousedown", j.Event.Custom.mousedrag.handler.handleMouseDown, 1);
                this.jAddEvent("mouseup", j.Event.Custom.mousedrag.handler.handleMouseUp, 1);
                this.jAddEvent("mousemove", j.Event.Custom.mousedrag.handler.handleMouseMove, 1);
                document.jAddEvent("mouseup", j.Event.Custom.mousedrag.handler.handleMouseUp.jBindAsEvent(this), 1)
            },
            jRemove: function () {
                this.jRemoveEvent("mousedown", j.Event.Custom.mousedrag.handler.handleMouseDown);
                this.jRemoveEvent("mouseup", j.Event.Custom.mousedrag.handler.handleMouseUp);
                this.jRemoveEvent("mousemove", j.Event.Custom.mousedrag.handler.handleMouseMove);
                document.jRemoveEvent("mouseup", j.Event.Custom.mousedrag.handler.handleMouseUp)
            },
            handleMouseDown: function (n) {
                var m;
                n.stopDefaults();
                m = new j.Event.Custom.mousedrag(this, n, "dragstart");
                this.jStore("event:mousedrag:dragstart", m);
                this.jCallEvent("mousedrag", m)
            },
            handleMouseUp: function (n) {
                var m;
                n.stopDefaults();
                m = this.jFetch("event:mousedrag:dragstart");
                if (!m) {
                    return
                }
                m = new j.Event.Custom.mousedrag(this, n, "dragend");
                this.jDel("event:mousedrag:dragstart");
                this.jCallEvent("mousedrag", m)
            },
            handleMouseMove: function (n) {
                var m;
                n.stopDefaults();
                m = this.jFetch("event:mousedrag:dragstart");
                if (!m) {
                    return
                }
                m = new j.Event.Custom.mousedrag(this, n, "dragmove");
                this.jCallEvent("mousedrag", m)
            }
        };
        j.Event.Custom.dblbtnclick = new j.Class(j.extend(j.Event.Custom, {
            type: "dblbtnclick",
            timedout: false,
            tm: null,
            init: function (o, n) {
                var m = n.jGetPageXY();
                this.x = m.x;
                this.y = m.y;
                this.timeStamp = n.timeStamp;
                this.button = n.getButton();
                this.target = o;
                this.pushToEvents(n)
            }
        }));
        j.Event.Custom.dblbtnclick.handler = {
            options: {
                threshold: 200
            },
            add: function (m) {
                this.jStore("event:dblbtnclick:options", j.extend(j.detach(j.Event.Custom.dblbtnclick.handler.options), m || {}));
                this.jAddEvent("btnclick", j.Event.Custom.dblbtnclick.handler.handle, 1)
            },
            jRemove: function () {
                this.jRemoveEvent("btnclick", j.Event.Custom.dblbtnclick.handler.handle)
            },
            handle: function (o) {
                var n, m;
                n = this.jFetch("event:dblbtnclick:event");
                m = this.jFetch("event:dblbtnclick:options");
                if (!n) {
                    n = new j.Event.Custom.dblbtnclick(this, o);
                    n.tm = setTimeout(function () {
                        n.timedout = true;
                        o.isQueueStopped = j.$false;
                        this.jCallEvent("btnclick", o)
                    }.jBind(this), m.threshold + 10);
                    this.jStore("event:dblbtnclick:event", n);
                    o.stopQueue()
                } else {
                    clearTimeout(n.tm);
                    this.jDel("event:dblbtnclick:event");
                    if (!n.timedout) {
                        n.pushToEvents(o);
                        o.stopQueue().stop();
                        this.jCallEvent("dblbtnclick", n)
                    } else {}
                }
            }
        };
        j.Event.Custom.tap = new j.Class(j.extend(j.Event.Custom, {
            type: "tap",
            id: null,
            init: function (n, m) {
                this.id = m.targetTouches[0].identifier;
                this.x = m.targetTouches[0].clientX;
                this.y = m.targetTouches[0].clientY;
                this.timeStamp = m.timeStamp;
                this.button = 0;
                this.target = n;
                this.pushToEvents(m)
            }
        }));
        j.Event.Custom.tap.handler = {
            options: {
                threshold: 200,
                radius: 10
            },
            add: function (m) {
                this.jStore("event:tap:options", j.extend(j.detach(j.Event.Custom.tap.handler.options), m || {}));
                this.jAddEvent("touchstart", j.Event.Custom.tap.handler.handle, 1);
                this.jAddEvent("touchend", j.Event.Custom.tap.handler.handle, 1);
                this.jAddEvent("click", j.Event.Custom.tap.handler.onclick, 1)
            },
            jRemove: function () {
                this.jRemoveEvent("touchstart", j.Event.Custom.tap.handler.handle);
                this.jRemoveEvent("touchend", j.Event.Custom.tap.handler.handle);
                this.jRemoveEvent("click", j.Event.Custom.tap.handler.onclick)
            },
            onclick: function (m) {
                m.stopDefaults()
            },
            handle: function (p) {
                var n, o, m;
                m = this.jFetch("event:tap:options");
                if ("touchstart" == p.type) {
                    if (p.targetTouches.length > 1) {
                        return
                    }
                    o = new j.Event.Custom.tap(this, p);
                    p.stop();
                    this.jStore("event:tap:event", o)
                } else {
                    if ("touchend" == p.type) {
                        o = this.jFetch("event:tap:event");
                        n = j.now();
                        if (!o || p.changedTouches.length > 1) {
                            return
                        }
                        this.jDel("event:tap:event");
                        if (o.id == p.changedTouches[0].identifier && p.timeStamp - o.timeStamp <= m.threshold && Math.sqrt(Math.pow(p.changedTouches[0].clientX - o.x, 2) + Math.pow(p.changedTouches[0].clientY - o.y, 2)) <= m.radius) {
                            this.jCallEvent("tap", o)
                        }
                    }
                }
            }
        };
        j.Event.Custom.dbltap = new j.Class(j.extend(j.Event.Custom, {
            type: "dbltap",
            timedout: false,
            tm: null,
            init: function (n, m) {
                this.x = m.x;
                this.y = m.y;
                this.timeStamp = m.timeStamp;
                this.button = 0;
                this.target = n;
                this.pushToEvents(m)
            }
        }));
        j.Event.Custom.dbltap.handler = {
            options: {
                threshold: 300
            },
            add: function (m) {
                this.jStore("event:dbltap:options", j.extend(j.detach(j.Event.Custom.dbltap.handler.options), m || {}));
                this.jAddEvent("tap", j.Event.Custom.dbltap.handler.handle, 1)
            },
            jRemove: function () {
                this.jRemoveEvent("tap", j.Event.Custom.dbltap.handler.handle)
            },
            handle: function (o) {
                var n, m;
                n = this.jFetch("event:dbltap:event");
                m = this.jFetch("event:dbltap:options");
                if (!n) {
                    n = new j.Event.Custom.dbltap(this, o);
                    n.tm = setTimeout(function () {
                        n.timedout = true;
                        o.isQueueStopped = j.$false;
                        this.jCallEvent("tap", o)
                    }.jBind(this), m.threshold + 10);
                    this.jStore("event:dbltap:event", n);
                    o.stopQueue()
                } else {
                    clearTimeout(n.tm);
                    this.jDel("event:dbltap:event");
                    if (!n.timedout) {
                        n.pushToEvents(o);
                        o.stopQueue().stop();
                        this.jCallEvent("dbltap", n)
                    } else {}
                }
            }
        };
        j.Event.Custom.touchdrag = new j.Class(j.extend(j.Event.Custom, {
            type: "touchdrag",
            state: "dragstart",
            id: null,
            dragged: false,
            init: function (o, n, m) {
                var p = null;
                if ("touchstart" == n.type) {
                    p = n.touches[0]
                } else {
                    p = n.changedTouches[0]
                }
                this.id = p.identifier;
                this.x = p.clientX;
                this.y = p.clientY;
                this.timeStamp = n.timeStamp;
                this.button = 0;
                this.target = o;
                this.pushToEvents(n);
                this.state = m
            }
        }));
        j.Event.Custom.touchdrag.handler = {
            add: function () {
                this.jAddEvent("touchstart", j.Event.Custom.touchdrag.handler.handleTouchStart, 1);
                this.jAddEvent("touchend", j.Event.Custom.touchdrag.handler.handleTouchEnd, 1);
                this.jAddEvent("touchmove", j.Event.Custom.touchdrag.handler.handleTouchMove, 1)
            },
            jRemove: function () {
                this.jRemoveEvent("touchstart", j.Event.Custom.touchdrag.handler.handleTouchStart);
                this.jRemoveEvent("touchend", j.Event.Custom.touchdrag.handler.handleTouchEnd);
                this.jRemoveEvent("touchmove", j.Event.Custom.touchdrag.handler.handleTouchMove)
            },
            handleTouchStart: function (n) {
                var m;
                if (n.touches.length > 1) {
                    this.jDel("event:touchdrag:dragstart");
                    return
                }
                m = new j.Event.Custom.touchdrag(this, n, "dragstart");
                this.jStore("event:touchdrag:dragstart", m)
            },
            handleTouchEnd: function (n) {
                var m;
                m = this.jFetch("event:touchdrag:dragstart");
                if (!m || n.changedTouches.length > 1) {
                    this.jDel("event:touchdrag:dragstart");
                    return
                }
                if (m.id != n.changedTouches[0].identifier) {
                    this.jDel("event:touchdrag:dragstart");
                    return
                }
                if (!m.dragged) {
                    return
                }
                m = new j.Event.Custom.touchdrag(this, n, "dragend");
                this.jDel("event:touchdrag:dragstart");
                this.jCallEvent("touchdrag", m)
            },
            handleTouchMove: function (n) {
                var m;
                m = this.jFetch("event:touchdrag:dragstart");
                if (!m || n.changedTouches.length > 1) {
                    this.jDel("event:touchdrag:dragstart");
                    return
                }
                if (m.id != n.changedTouches[0].identifier) {
                    this.jDel("event:touchdrag:dragstart");
                    return
                }
                if (!m.dragged) {
                    m.dragged = true;
                    this.jCallEvent("touchdrag", m)
                }
                m = new j.Event.Custom.touchdrag(this, n, "dragmove");
                this.jCallEvent("touchdrag", m)
            }
        };
        j.Event.Custom.touchpinch = new j.Class(j.extend(j.Event.Custom, {
            type: "touchpinch",
            scale: 1,
            previousScale: 1,
            curScale: 1,
            state: "pinchstart",
            init: function (n, m) {
                this.timeStamp = m.timeStamp;
                this.button = 0;
                this.target = n;
                this.x = m.touches[0].clientX + (m.touches[1].clientX - m.touches[0].clientX) / 2;
                this.y = m.touches[0].clientY + (m.touches[1].clientY - m.touches[0].clientY) / 2;
                this._initialDistance = Math.sqrt(Math.pow(m.touches[0].clientX - m.touches[1].clientX, 2) + Math.pow(m.touches[0].clientY - m.touches[1].clientY, 2));
                this.pushToEvents(m)
            },
            update: function (m) {
                var n;
                this.state = "pinchupdate";
                if (m.changedTouches[0].identifier != this.events[0].touches[0].identifier || m.changedTouches[1].identifier != this.events[0].touches[1].identifier) {
                    return
                }
                n = Math.sqrt(Math.pow(m.changedTouches[0].clientX - m.changedTouches[1].clientX, 2) + Math.pow(m.changedTouches[0].clientY - m.changedTouches[1].clientY, 2));
                this.previousScale = this.scale;
                this.scale = n / this._initialDistance;
                this.curScale = this.scale / this.previousScale;
                this.x = m.changedTouches[0].clientX + (m.changedTouches[1].clientX - m.changedTouches[0].clientX) / 2;
                this.y = m.changedTouches[0].clientY + (m.changedTouches[1].clientY - m.changedTouches[0].clientY) / 2;
                this.pushToEvents(m)
            }
        }));
        j.Event.Custom.touchpinch.handler = {
            add: function () {
                this.jAddEvent("touchstart", j.Event.Custom.touchpinch.handler.handleTouchStart, 1);
                this.jAddEvent("touchend", j.Event.Custom.touchpinch.handler.handleTouchEnd, 1);
                this.jAddEvent("touchmove", j.Event.Custom.touchpinch.handler.handleTouchMove, 1)
            },
            jRemove: function () {
                this.jRemoveEvent("touchstart", j.Event.Custom.touchpinch.handler.handleTouchStart);
                this.jRemoveEvent("touchend", j.Event.Custom.touchpinch.handler.handleTouchEnd);
                this.jRemoveEvent("touchmove", j.Event.Custom.touchpinch.handler.handleTouchMove)
            },
            handleTouchStart: function (n) {
                var m;
                if (n.touches.length != 2) {
                    return
                }
                n.stopDefaults();
                m = new j.Event.Custom.touchpinch(this, n);
                this.jStore("event:touchpinch:event", m)
            },
            handleTouchEnd: function (n) {
                var m;
                m = this.jFetch("event:touchpinch:event");
                if (!m) {
                    return
                }
                n.stopDefaults();
                this.jDel("event:touchpinch:event")
            },
            handleTouchMove: function (n) {
                var m;
                m = this.jFetch("event:touchpinch:event");
                if (!m) {
                    return
                }
                n.stopDefaults();
                m.update(n);
                this.jCallEvent("touchpinch", m)
            }
        };
        k.extend(k, {
            getAbsoluteURL: (j.jBrowser.ieMode && j.jBrowser.ieMode < 8) ? (function (m) {
                if (/http(s)?:\/\//.test(m)) {
                    return m
                }
                var n = document.createElement("div");
                n.innerHTML = '<a href="' + m + '">x</a>';
                return n.firstChild.href
            }) : (function (m) {
                if (/http(s)?:\/\//.test(m)) {
                    return m
                }
                return j.$new("a", {
                    href: m
                }).href
            })
        });
        j.win = j.$(window);
        j.doc = j.$(document);
        return k
    })();
    (function (j) {
        if (!j) {
            throw "MagicJS not found";
            return
        }
        if (j.FX) {
            return
        }
        var i = j.$;
        j.FX = new j.Class({
            init: function (l, k) {
                this.el = j.$(l);
                this.options = j.extend(this.options, k);
                this.timer = false;
                if ("string" == j.jTypeOf(this.options.transition)) {
                    this.options.transition = j.FX.Transition[this.options.transition] || j.FX.Transition.sineIn
                }
                if ("string" == j.jTypeOf(this.options.cycles)) {
                    this.options.cycles = "infinite" === this.options.cycles ? Infinity : parseInt(this.options.cycles) || 1
                }
            },
            options: {
                fps: 50,
                duration: 500,
                transition: function (k) {
                    return -(Math.cos(Math.PI * k) - 1) / 2
                },
                cycles: 1,
                direction: "normal",
                onStart: j.$F,
                onComplete: j.$F,
                onBeforeRender: j.$F,
                onAfterRender: j.$F,
                roundCss: true
            },
            styles: null,
            start: function (m) {
                var k = /\%$/,
                    l;
                this.styles = m;
                this.cycle = 0;
                this.state = 0;
                this.curFrame = 0;
                this.pStyles = {};
                this.alternate = "alternate" === this.options.direction || "alternate-reverse" === this.options.direction;
                this.continuous = "continuous" === this.options.direction || "continuous-reverse" === this.options.direction;
                for (l in this.styles) {
                    k.test(this.styles[l][0]) && (this.pStyles[l] = true);
                    if ("reverse" === this.options.direction || "alternate-reverse" === this.options.direction || "continuous-reverse" === this.options.direction) {
                        this.styles[l].reverse()
                    }
                }
                this.startTime = j.now();
                this.finishTime = this.startTime + this.options.duration;
                if (this.options.duration != 0) {
                    this.timer = this.loop.jBind(this).interval(Math.round(1000 / this.options.fps))
                }
                this.options.onStart.call();
                if (this.options.duration == 0) {
                    this.render(1);
                    this.options.onComplete.call()
                }
                return this
            },
            stop: function (k) {
                k = j.defined(k) ? k : false;
                if (this.timer) {
                    clearInterval(this.timer);
                    this.timer = false
                }
                if (k) {
                    this.render(1);
                    this.options.onComplete.jDelay(10)
                }
                return this
            },
            calc: function (m, l, k) {
                m = parseFloat(m);
                l = parseFloat(l);
                return (l - m) * k + m
            },
            loop: function () {
                var l = j.now(),
                    k = (l - this.startTime) / this.options.duration,
                    m = Math.floor(k);
                if (l >= this.finishTime && m >= this.options.cycles) {
                    if (this.timer) {
                        clearInterval(this.timer);
                        this.timer = false
                    }
                    this.render(1);
                    this.options.onComplete.jDelay(10);
                    return this
                }
                if (this.alternate && this.cycle < m) {
                    for (var n in this.styles) {
                        this.styles[n].reverse()
                    }
                }
                this.cycle = m;
                this.render((this.continuous ? m : 0) + this.options.transition(k % 1))
            },
            render: function (k) {
                var l = {};
                for (var m in this.styles) {
                    if ("opacity" === m) {
                        l[m] = Math.round(this.calc(this.styles[m][0], this.styles[m][1], k) * 100) / 100
                    } else {
                        l[m] = this.calc(this.styles[m][0], this.styles[m][1], k);
                        if (this.options.roundCss) {
                            l[m] = Math.round(l[m])
                        }
                        this.pStyles[m] && (l[m] += "%")
                    }
                }
                this.options.onBeforeRender(l);
                this.set(l);
                this.options.onAfterRender(l)
            },
            set: function (k) {
                return this.el.jSetCss(k)
            }
        });
        j.FX.Transition = {
            linear: function (k) {
                return k
            },
            sineIn: function (k) {
                return -(Math.cos(Math.PI * k) - 1) / 2
            },
            sineOut: function (k) {
                return 1 - j.FX.Transition.sineIn(1 - k)
            },
            expoIn: function (k) {
                return Math.pow(2, 8 * (k - 1))
            },
            expoOut: function (k) {
                return 1 - j.FX.Transition.expoIn(1 - k)
            },
            quadIn: function (k) {
                return Math.pow(k, 2)
            },
            quadOut: function (k) {
                return 1 - j.FX.Transition.quadIn(1 - k)
            },
            cubicIn: function (k) {
                return Math.pow(k, 3)
            },
            cubicOut: function (k) {
                return 1 - j.FX.Transition.cubicIn(1 - k)
            },
            backIn: function (l, k) {
                k = k || 1.618;
                return Math.pow(l, 2) * ((k + 1) * l - k)
            },
            backOut: function (l, k) {
                return 1 - j.FX.Transition.backIn(1 - l)
            },
            elasticIn: function (l, k) {
                k = k || [];
                return Math.pow(2, 10 * --l) * Math.cos(20 * l * Math.PI * (k[0] || 1) / 3)
            },
            elasticOut: function (l, k) {
                return 1 - j.FX.Transition.elasticIn(1 - l, k)
            },
            bounceIn: function (m) {
                for (var l = 0, k = 1; 1; l += k, k /= 2) {
                    if (m >= (7 - 4 * l) / 11) {
                        return k * k - Math.pow((11 - 6 * l - 11 * m) / 4, 2)
                    }
                }
            },
            bounceOut: function (k) {
                return 1 - j.FX.Transition.bounceIn(1 - k)
            },
            none: function (k) {
                return 0
            }
        }
    })(h);
    (function (j) {
        if (!j) {
            throw "MagicJS not found";
            return
        }
        if (!j.FX) {
            throw "MagicJS.FX not found";
            return
        }
        if (j.FX.Slide) {
            return
        }
        var i = j.$;
        j.FX.Slide = new j.Class(j.FX, {
            options: {
                mode: "vertical"
            },
            init: function (l, k) {
                this.el = j.$(l);
                this.options = j.extend(this.$parent.options, this.options);
                this.$parent.init(l, k);
                this.wrapper = this.el.jFetch("slide:wrapper");
                this.wrapper = this.wrapper || j.$new("DIV").jSetCss(j.extend(this.el.jGetStyles("margin-top", "margin-left", "margin-right", "margin-bottom", "position", "top", "float"), {
                    overflow: "hidden"
                })).enclose(this.el);
                this.el.jStore("slide:wrapper", this.wrapper).jSetCss({
                    margin: 0
                })
            },
            vertical: function () {
                this.margin = "margin-top";
                this.layout = "height";
                this.offset = this.el.offsetHeight
            },
            horizontal: function (k) {
                this.margin = "margin-" + (k || "left");
                this.layout = "width";
                this.offset = this.el.offsetWidth
            },
            right: function () {
                this.horizontal()
            },
            left: function () {
                this.horizontal("right")
            },
            start: function (m, p) {
                this[p || this.options.mode]();
                var o = this.el.jGetCss(this.margin).jToInt(),
                    n = this.wrapper.jGetCss(this.layout).jToInt(),
                    k = {}, q = {}, l;
                k[this.margin] = [o, 0], k[this.layout] = [0, this.offset], q[this.margin] = [o, -this.offset], q[this.layout] = [n, 0];
                switch (m) {
                    case "in":
                        l = k;
                        break;
                    case "out":
                        l = q;
                        break;
                    case "toggle":
                        l = (0 == n) ? k : q;
                        break
                }
                this.$parent.start(l);
                return this
            },
            set: function (k) {
                this.el.jSetCssProp(this.margin, k[this.margin]);
                this.wrapper.jSetCssProp(this.layout, k[this.layout]);
                return this
            },
            slideIn: function (k) {
                return this.start("in", k)
            },
            slideOut: function (k) {
                return this.start("out", k)
            },
            hide: function (l) {
                this[l || this.options.mode]();
                var k = {};
                k[this.layout] = 0, k[this.margin] = -this.offset;
                return this.set(k)
            },
            show: function (l) {
                this[l || this.options.mode]();
                var k = {};
                k[this.layout] = this.offset, k[this.margin] = 0;
                return this.set(k)
            },
            toggle: function (k) {
                return this.start("toggle", k)
            }
        })
    })(h);
    (function (j) {
        if (!j) {
            throw "MagicJS not found";
            return
        }
        if (j.PFX) {
            return
        }
        var i = j.$;
        j.PFX = new j.Class(j.FX, {
            init: function (k, l) {
                this.el_arr = k;
                this.options = j.extend(this.options, l);
                this.timer = false
            },
            start: function (k) {
                this.styles_arr = k;
                this.$parent.start([]);
                return this
            },
            render: function (k) {
                for (var l = 0; l < this.el_arr.length; l++) {
                    this.el = j.$(this.el_arr[l]);
                    this.styles = this.styles_arr[l];
                    this.$parent.render(k)
                }
            }
        })
    })(h);
    (function (j) {
        if (!j) {
            throw "MagicJS not found";
            return
        }
        if (j.Tooltip) {
            return
        }
        var i = j.$;
        j.Tooltip = function (l, m) {
            var k = this.tooltip = j.$new("div", null, {
                position: "absolute",
                "z-index": 999
            }).jAddClass("MagicToolboxTooltip");
            j.$(l).jAddEvent("mouseover", function () {
                k.jAppendTo(document.body)
            });
            j.$(l).jAddEvent("mouseout", function () {
                k.jRemove()
            });
            j.$(l).jAddEvent("mousemove", function (w) {
                var y = 20,
                    v = j.$(w).jGetPageXY(),
                    u = k.jGetSize(),
                    q = j.$(window).jGetSize(),
                    x = j.$(window).jGetScroll();

                function n(A, o, z) {
                    return (z < (A - o) / 2) ? z : ((z > (A + o) / 2) ? (z - o) : (A - o) / 2)
                }
                k.jSetCss({
                    left: x.x + n(q.width, u.width + 2 * y, v.x - x.x) + y,
                    top: x.y + n(q.height, u.height + 2 * y, v.y - x.y) + y
                })
            });
            this.text(m)
        };
        j.Tooltip.prototype.text = function (k) {
            this.tooltip.firstChild && this.tooltip.removeChild(this.tooltip.firstChild);
            this.tooltip.append(document.createTextNode(k))
        }
    })(h);
    (function (j) {
        if (!j) {
            throw "MagicJS not found";
            return
        }
        if (j.MessageBox) {
            return
        }
        var i = j.$;
        j.Message = function (n, m, l, k) {
            this.hideTimer = null;
            this.messageBox = j.$new("span", null, {
                position: "absolute",
                "z-index": 999,
                visibility: "hidden",
                opacity: 0.8
            }).jAddClass(k || "").jAppendTo(l || j.body);
            this.setMessage(n);
            this.show(m)
        };
        j.Message.prototype.show = function (k) {
            this.messageBox.show();
            this.hideTimer = this.hide.jBind(this).jDelay(j.ifndef(k, 5000))
        };
        j.Message.prototype.hide = function (k) {
            clearTimeout(this.hideTimer);
            this.hideTimer = null;
            if (this.messageBox && !this.hideFX) {
                this.hideFX = new h.FX(this.messageBox, {
                    duration: j.ifndef(k, 500),
                    onComplete: function () {
                        this.messageBox.kill();
                        delete this.messageBox;
                        this.hideFX = null
                    }.jBind(this)
                }).start({
                    opacity: [this.messageBox.jGetCss("opacity"), 0]
                })
            }
        };
        j.Message.prototype.setMessage = function (k) {
            this.messageBox.firstChild && this.tooltip.removeChild(this.messageBox.firstChild);
            this.messageBox.append(document.createTextNode(k))
        }
    })(h);
    (function (j) {
        if (!j) {
            throw "MagicJS not found";
            return
        }
        var i = j.$;
        h.ImageLoader = new j.Class({
            img: null,
            ready: false,
            options: {
                onprogress: j.$F,
                onload: j.$F,
                onabort: j.$F,
                onerror: j.$F,
                oncomplete: j.$F,
                ajaxload: false,
                progressiveLoad: true
            },
            size: null,
            _timer: null,
            loadedBytes: 0,
            _handlers: {
                onprogress: function (k) {
                    if (k.target && (200 === k.target.status || 304 === k.target.status) && k.lengthComputable) {
                        this.options.onprogress.jBind(null, (k.loaded - (this.options.progressiveLoad ? this.loadedBytes : 0)) / k.total).jDelay(1);
                        this.loadedBytes = k.loaded
                    }
                },
                onload: function (k) {
                    if (k) {
                        i(k).stop()
                    }
                    this._unbind();
                    if (this.ready) {
                        return
                    }
                    this.ready = true;
                    this._cleanup();
                    !this.options.ajaxload && this.options.onprogress.jBind(null, 1).jDelay(1);
                    this.options.onload.jBind(null, this).jDelay(1);
                    this.options.oncomplete.jBind(null, this).jDelay(1)
                },
                onabort: function (k) {
                    if (k) {
                        i(k).stop()
                    }
                    this._unbind();
                    this.ready = false;
                    this._cleanup();
                    this.options.onabort.jBind(null, this).jDelay(1);
                    this.options.oncomplete.jBind(null, this).jDelay(1)
                },
                onerror: function (k) {
                    if (k) {
                        i(k).stop()
                    }
                    this._unbind();
                    this.ready = false;
                    this._cleanup();
                    this.options.onerror.jBind(null, this).jDelay(1);
                    this.options.oncomplete.jBind(null, this).jDelay(1)
                }
            },
            _bind: function () {
                i(["load", "abort", "error"]).jEach(function (k) {
                    this.img.jAddEvent(k, this._handlers["on" + k].jBindAsEvent(this).jDefer(1))
                }, this)
            },
            _unbind: function () {
                if (this._timer) {
                    try {
                        clearTimeout(this._timer)
                    } catch (k) {}
                    this._timer = null
                }
                i(["load", "abort", "error"]).jEach(function (l) {
                    this.img.jRemoveEvent(l)
                }, this)
            },
            _cleanup: function () {
                this.jGetSize();
                if (this.img.jFetch("new")) {
                    var k = this.img.parentNode;
                    this.img.jRemove().jDel("new").jSetCss({
                        position: "static",
                        top: "auto"
                    });
                    k.kill()
                }
            },
            loadBlob: function (k) {
                var l = new XMLHttpRequest();
                i(["abort", "error", "progress"]).jEach(function (m) {
                    l["on" + m] = i(function (n) {
                        this._handlers["on" + m].call(this, n)
                    }).jBind(this)
                }, this);
                l.onload = i(function () {
                    if (200 !== l.status && 304 !== l.status) {
                        this._handlers.onerror.call(this);
                        return
                    }
                    this._bind();
                    this.img.src = k
                }).jBind(this);
                l.open("GET", k);
                l.responseType = "blob";
                l.send()
            },
            init: function (l, k) {
                this.options = j.extend(this.options, k);
                this.img = i(l) || j.$new("img", {}, {
                    "max-width": "none",
                    "max-height": "none"
                }).jAppendTo(j.$new("div").jAddClass("magic-temporary-img").jSetCss({
                    position: "absolute",
                    top: -10000,
                    width: 10,
                    height: 10,
                    overflow: "hidden"
                }).jAppendTo(j.body)).jStore("new", true);
                if (j.jBrowser.features.xhr2 && this.options.ajaxload && "string" == j.jTypeOf(l)) {
                    this.loadBlob(l);
                    return
                }
                var m = function () {
                    if (this.isReady()) {
                        this._handlers.onload.call(this)
                    } else {
                        this._handlers.onerror.call(this)
                    }
                    m = null
                }.jBind(this);
                this._bind();
                if (!l.src) {
                    this.img.src = l
                } else {
                    if (j.jBrowser.trident900 && j.jBrowser.ieMode < 9) {
                        this.img.onreadystatechange = function () {
                            if (/loaded|complete/.test(this.img.readyState)) {
                                this.img.onreadystatechange = null;
                                m && m()
                            }
                        }.jBind(this)
                    }
                    this.img.src = l.src
                }
                this.img && this.img.complete && m && (this._timer = m.jDelay(100))
            },
            destroy: function () {
                this._unbind();
                this._cleanup();
                this.ready = false;
                return this
            },
            isReady: function () {
                var k = this.img;
                return (k.naturalWidth) ? (k.naturalWidth > 0) : (k.readyState) ? ("complete" == k.readyState) : k.width > 0
            },
            jGetSize: function () {
                return this.size || (this.size = {
                    width: this.img.naturalWidth || this.img.width,
                    height: this.img.naturalHeight || this.img.height
                })
            }
        })
    })(h);
    h.Options = (function (j) {
        if (!j) {
            throw "MagicJS not found";
            return
        }
        var i = j.$;
        var k = function (l) {
            this.defaults = l;
            this.options = {}
        };
        k.prototype.get = function (l) {
            return j.defined(this.options[l]) ? this.options[l] : this.defaults[l]
        };
        k.prototype.set = function (m, l) {
            m = m.jTrim();
            if (!m) {
                return
            }
            j.jTypeOf(l) === "string" && (l = l.jTrim());
            if (l === "true") {
                l = true
            }
            if (l === "false") {
                l = false
            }
            if (parseInt(l) == l) {
                l = parseInt(l)
            }
            this.options[m] = l
        };
        k.prototype.fromRel = function (l) {
            var m = this;
            i(l.split(";")).jEach(function (n) {
                n = n.split(":");
                m.set(n.shift(), n.join(":"))
            })
        };
        k.prototype.fromJSON = function (m) {
            for (var l in m) {
                if (m.hasOwnProperty(l)) {
                    this.set(l, m[l])
                }
            }
        };
        k.prototype.exists = function (l) {
            return j.defined(this.options[l]) ? true : false
        };
        return k
    }(h));
    g.Element.addEvent__ = g.Element.jAddEvent;
    g.Element.jAddEvent = function (l, i) {
        if (l == "mousewheel") {
            g.jBrowser.gecko && (l = "DOMMouseScroll");
            var k = i,
                j = this;
            i = function (m) {
                var n = 0;
                if (m.wheelDelta) {
                    n = m.wheelDelta / 120
                } else {
                    if (m.detail) {
                        n = -m.detail / 3
                    }
                }
                m.delta = Math.round(n);
                k.call(j, m)
            }
        }
        return this.addEvent__(l, i)
    };
    var e = g.$;
    var b = "?" + g.getHashCode(document.location.hostname);
    var f = {
        IMAGELOAD: "images:imageload",
        IMAGESLOADTIME: "images:loadtime",
        TIMETOSTART: "starttime"
    };
    g.me = {
        mousedown: g.jBrowser.touchScreen ? "touchstart" : "mousedown",
        mouseup: g.jBrowser.touchScreen ? "touchend" : "mouseup",
        mousemove: g.jBrowser.touchScreen ? "touchmove" : "mousemove",
        mouseover: "mouseover",
        mouseout: g.jBrowser.touchScreen ? "touchend" : "mouseout",
        click: g.jBrowser.touchScreen ? "tap" : "btnclick"
    };
    var a = function (m, j) {
        this.o = e(m);
        while (m.firstChild && m.firstChild.tagName !== "IMG") {
            m.removeChild(m.firstChild)
        }
        if (m.firstChild.tagName !== "IMG") {
            throw "Error loading Magic 360. Cannot find image."
        }
        this.oi = m.replaceChild(m.firstChild.cloneNode(false), m.firstChild);
        this._o = new g.Options({
            rows: 1,
            columns: 36,
            speed: 50,
            filename: "auto",
            filepath: "auto",
            "large-filename": "auto",
            "large-filepath": "auto",
            "filename-digits": 2,
            "loop-row": false,
            "loop-column": true,
            "start-row": "auto",
            "start-column": "auto",
            "row-increment": 1,
            "column-increment": 1,
            autospin: "once",
            "autospin-start": "load",
            "autospin-stop": "click",
            "autospin-speed": 4000,
            "autospin-direction": "clockwise",
            spin: "drag",
            smoothing: true,
            magnify: true,
            "magnifier-width": "80%",
            "magnifier-shape": "inner",
            fullscreen: true,
            hint: true,
            images: "",
            "large-images": "",
            "initialize-on": "load",
            "mousewheel-step": 3,
            onready: g.$F,
            onstart: g.$F,
            onstop: g.$F,
            onzoomin: g.$F,
            onzoomout: g.$F,
            onspin: g.$F
        });
        this.op = e(this._o.get).jBind(this._o);
        this._o.fromJSON(g.extend(window.Magic360Options || {}, Magic360.options));
        this._o.fromRel(m.getAttribute("data-Magic360-options") || m.rel);
        this.lang = new g.Options({
            "loading-text": "Loading...",
            "fullscreen-loading-text": "Loading large spin...",
            "hint-text": "Drag to spin",
            "mobile-hint-text": "Swipe to spin"
        });
        this.lang.fromJSON(g.extend(window.Magic360Lang || {}, Magic360.lang));
        this.localString = e(this.lang.get).jBind(this.lang);
        this.currentFrame = {
            row: 0,
            col: 0
        };
        this.concurrentImages = 6;
        this.images = {
            small: e([]),
            fullscreen: e([])
        };
        this.imageQueue = {
            small: e([]),
            fullscreen: e([]),
            zoom: e([])
        };
        this.imageMap = {};
        this.loadedRows = {
            count: 0,
            indexes: e([])
        };
        this.pendingImages = {
            small: 0,
            fullscreen: 0,
            zoom: 0
        };
        this.bgImages = {
            url: e([]),
            position: e([])
        };
        this.bgURL = null;
        this.lastBgSize = {
            width: 0,
            height: 0
        };
        this.useMultiBackground = !! g.jBrowser.presto;
        this.canMagnify = true;
        this.imageLoadStarted = {
            small: 0,
            fullscreen: 0,
            zoom: 0
        };
        this.isFullScreen = false;
        this.fullScreenSize = {
            width: 0,
            height: 0
        };
        this.fullScreenBox = null;
        this.fullscreenIcon = null;
        this.fullScreenImage = null;
        this.fullScreenResizeCallback = null;
        this.fullScreenFX = null;
        this.fullScreenExitFX = null;
        this.firstFullScreenRun = true;
        this.resizeCallback = null;
        this.boundaries = {
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        };
        this.normalSize = {
            width: 0,
            height: 0
        };
        this.size = {
            width: 0,
            height: 0
        };
        this.spinStarted = false;
        var i = this;
        this.borders = 0;
        this.boxSize = {
            width: 0,
            height: 0
        };
        this.boxBoundaries = {
            width: 0,
            height: 0
        };
        this.callbacks = g.extend({
            getImageInfo: function () {
                return {}
            },
            getImageSizes: function () {
                return {}
            },
            getHotspots: function () {
                return null
            },
            sendStats: g.$F
        }, g.detach(Magic360.callbacks));
        this.imgCacheBox = g.$new("div").jAddClass("magic-temporary-img").jSetCss({
            position: "absolute",
            top: -1000,
            width: 10,
            height: 10,
            overflow: "hidden"
        }).jAppendTo(g.body);

        function l(n, x, u, p) {
            var o, v = {
                    path: x,
                    tpl: u.replace(/(\/|\\)/ig, "")
                };
            n = n.split("/");
            u = n.pop().split(".");
            x = n.join("/") + "/";
            o = u.pop();
            p || (p = false);
            v.path = "auto" == v.path ? x : v.path.replace(/\/$/, "") + "/";
            if (v.tpl == "auto") {
                u = u.join(".").split("-");
                var q = u.pop(),
                    w = u.pop();
                if (parseInt(q, 10) != q) {
                    u.push(w);
                    w = true;
                    u.push(q);
                    q = true;
                    u.push("{row}");
                    u.push("{col}")
                } else {
                    if (parseInt(w, 10) != w) {
                        u.push(w);
                        w = false;
                        (!i._o.exists("start-row") || "auto" == i.op("start-row")) && i._o.set("start-row", 1)
                    }
                }
                p && i._o.set("filename-digits", q.length);
                w && u.push("{row}") && ("auto" == i.op("start-row") && i._o.set("start-row", w.jToInt()));
                q && u.push("{col}") && ("auto" == i.op("start-column") && i._o.set("start-column", q.jToInt()));
                u = u.join("-") + "." + o;
                v.tpl = u
            } else {
                if ("auto" == i.op("start-row") || "auto" == i.op("start-column")) {
                    u = u.join(".") + "." + o;
                    var s = u.match(new RegExp(v.tpl.split("{row}").join("(\\d{2,})").split("{col}").join("(\\d{2,})")));
                    ("auto" === i.op("start-row")) && i._o.set("start-row", (s && s.length > 2) ? s[1].jToInt() : 1);
                    ("auto" === i.op("start-column")) && i._o.set("start-column", (!s || s.length < 2) ? 1 : s.length > 2 ? s[2].jToInt() : s[1].jToInt())
                }
            }
            return v
        }
        function k(o, q) {
            var p = g.$new(o);
            var n = q.split(",");
            e(n).jEach(function (s) {
                p.jAddClass(s.jTrim())
            });
            p.jSetStyle({
                position: "absolute",
                top: -10000,
                left: 0,
                visibility: "hidden"
            });
            document.body.appendChild(p);
            (function () {
                this.jRemove()
            }).jBind(p).jDelay(100)
        }
        this.sis = e(e(this.op("images").jTrim().split(" ")).filter(function (n) {
            return "" != n
        }));
        this.bis = e(e(this.op("large-images").jTrim().split(" ")).filter(function (n) {
            return "" != n
        }));
        if (!this.sis.length) {
            r = l(m.firstChild.src, this.op("filepath"), this.op("filename"), true);
            this._o.set("filepath", r.path);
            this._o.set("filename", r.tpl);
            r = l(m.href, this.op("large-filepath"), this.op("large-filename"));
            this._o.set("large-filepath", r.path);
            this._o.set("large-filename", r.tpl)
        }
        this._o.set("columns", Math.floor(this.op("columns") / this.op("column-increment")));
        this._o.set("rows", Math.floor(this.op("rows") / this.op("row-increment")));
        !parseInt(this.op("start-row"), 10) && this._o.set("start-row", 1);
        !parseInt(this.op("start-column"), 10) && this._o.set("start-column", 1);
        parseInt(this.op("start-row"), 10) > parseInt(this.op("rows"), 10) && this._o.set("start-row", this.op("rows"));
        parseInt(this.op("start-column"), 10) > parseInt(this.op("columns"), 10) && this._o.set("start-column", this.op("columns"));
        this._o.set("autospin-start", this.op("autospin-start").split(","));
        (g.jBrowser.touchScreen && "hover" === this.op("autospin-stop")) && this._o.set("autospin-stop", "click");
        isNaN(parseInt(this.op("mousewheel-step"), 10)) && this._o.set("mousewheel-step", 3);
        ("infinite" === this.op("autospin") && "hover" === this.op("autospin-stop")) && this._o.set("hint", false);
        !this._o.exists("hint") && ("infinite" === this.op("autospin") && "click" === this.op("autospin-stop") && e(this.op("autospin-start")).contains("click")) && this._o.set("hint", false);
        this._o.exists("loading-text") && this.lang.set("loading-text", this.op("loading-text"));
        ("string" == g.jTypeOf(this.op("onready"))) && ("function" == g.jTypeOf(window[this.op("onready")])) && this._o.set("onready", window[this.op("onready")]);
        ("string" == g.jTypeOf(this.op("onspin"))) && ("function" === g.jTypeOf(window[this.op("onspin")])) && this._o.set("onspin", window[this.op("onspin")]);
        ("string" == g.jTypeOf(this.op("onzoomin"))) && ("function" === g.jTypeOf(window[this.op("onzoomin")])) && this._o.set("onzoomin", window[this.op("onzoomin")]);
        ("string" == g.jTypeOf(this.op("onzoomout"))) && ("function" === g.jTypeOf(window[this.op("onzoomout")])) && this._o.set("onzoomout", window[this.op("onzoomout")]);
        ("function" !== g.jTypeOf(this.op("onspin"))) && this.op.set("onspin", g.F);
        ("function" !== g.jTypeOf(this.op("onzoomin"))) && this.op.set("onzoomin", g.F);
        ("function" !== g.jTypeOf(this.op("onzoomout"))) && this.op.set("onzoomout", g.F);
        this.o.jAddEvent("click", function (n) {
            n.stop()
        }).jAddEvent("dragstart", function (n) {
            n.stop()
        }).jAddEvent("selectstart", function (n) {
            n.stop()
        }).jSetCss({
            "-webkit-user-select": "none",
            "-webkit-touch-callout": "none",
            "-webkit-tap-highlight-color": "transparent"
        });
        if (this.op("hint")) {
            k("span", "Magic360-hint-side")
        }
        k("div", "Magic360-progress-bar-state");
        k("div", "Magic360-wait");
        if (this.op("fullscreen")) {
            k("div", "Magic360-button,fullscreen")
        }
        new g.ImageLoader(m.firstChild, {
            onload: e(function (n) {
                var p = false,
                    o = e(function () {
                        if (!p) {
                            p = true;
                            e(this.preInit).call(this)
                        }
                    }).jBind(this);
                switch (this.op("initialize-on")) {
                    case "hover":
                        this.o.jAddEvent("mouseover", o);
                        break;
                    case "click":
                        this.o.jAddEvent("click", o);
                        break;
                    default:
                        o()
                }
            }).jBind(this)
        })
    };
    a.prototype.prepareUrl = function (l, i, k) {
        function j(m, o) {
            return Array(o - ("" + m).length + 1).join("0") + m
        }
        k = k === true ? "large-" : "";
        if (this.sis.length) {
            if (k && !this.bis.length) {
                return ""
            }
            return this[(k) ? "bis" : "sis"][(l - 1) * this.op("columns") + i - 1]
        }
        l = j(1 + (l - 1) * this.op("row-increment"), this.op("filename-digits"));
        i = j(1 + (i - 1) * this.op("column-increment"), this.op("filename-digits"));
        return this.op(k + "filepath") + this.op(k + "filename").split("{row}").join(l).split("{col}").join(i)
    };
    a.prototype.getImageURL = function (m, l, k) {
        var i = null,
            j = "";
        k || (k = "small");
        i = this.getImageInfo(m, l, k);
        i && (j = i.url);
        return j
    };
    a.prototype.getImageInfo = function (l, k, j) {
        var i = {};
        j || (j = "small");
        i[j] = {
            url: this.prepareUrl(l, k, "fullscreen" === j || "zoom" === j),
            left: 0,
            top: 0
        };
        return i[j] || null
    };
    a.prototype.onImageLoadProgress = function (k, j, i) {
        if (e(this.imageMap[j]).filter(function (l) {
            return ("small" !== k || l.row === this.op("start-row") - 1)
        }, this).length) {
            this.progressBar.increment(i * this.progressBar.step)
        }
    };
    a.prototype.onImageLoaded = function (m, l, p) {
        var o = e([]),
            i, j, n = function (q, k) {
                return q - k
            };
        this.pendingImages[m]--;
        if (p.ready) {
            if (this.useMultiBackground) {
                j = this.bgImages.url.push("url(" + l + ")");
                this.bgImages.position.push("0px -10000px");
                this.bgURL = this.bgImages.url.join(",")
            }
            this.imgCacheBox.append(p.img);
            e(this.imageMap[l]).jEach(function (s, q, k) {
                s.img.framesOnImage = k.length;
                s.img.bgIndex = j - 1;
                s.img.loaded = true;
                o.contains(s.row) || o.push(s.row)
            });
            if ("small" == m) {
                e(o).jEach(function (k) {
                    if (!e(this.images[m][k]).filter(function (q) {
                        return q.loaded !== true
                    }).length) {
                        this.loadedRows.count++;
                        this.loadedRows.indexes.push(k);
                        this.loadedRows.indexes.sort(n);
                        if (k === this.op("start-row") - 1) {
                            setTimeout(function () {
                                this.progressBar.hide();
                                this.init()
                            }.jBind(this), 1)
                        }
                    }
                }, this)
            }
            if (this.isFullScreen && "fullscreen" == m) {
                this.jump_(this.currentFrame.row, this.currentFrame.col)
            }
        }
        if (!this.imageQueue[m].length) {
            return
        }
        if (this.pendingImages[m] < this.concurrentImages && this.imageQueue[m].length) {
            this.pendingImages[m]++;
            l = this.imageQueue[m].shift();
            new g.ImageLoader(l, {
                ajaxload: false,
                oncomplete: this.onImageLoaded.jBind(this, m, l),
                onprogress: this.onImageLoadProgress.jBind(this, m, l)
            })
        }
    };
    a.prototype.preloadImages = function (l, q, j) {
        l || (l = "small");
        var n = this.op("columns"),
            s = this.op("rows"),
            k = 0,
            o, m, p, i;
        m = q;
        o = j;
        this.images[l] = new Array(s);
        do {
            this.images[l][m - 1] = new Array(n);
            do {
                p = this.getImageInfo(m, o, l);
                if (!this.imageQueue[l].contains(p.url)) {
                    ("small" == l && m == q) && k++;
                    this.imageQueue[l].push(p.url)
                }
                p.left *= -1;
                p.top *= -1;
                p.bgIndex = 0;
                p.framesOnImage = 1;
                p.loaded = false;
                this.images[l][m - 1][o - 1] = p;
                this.imageMap[p.url] || (this.imageMap[p.url] = e([]));
                this.imageMap[p.url].push({
                    row: m - 1,
                    img: this.images[l][m - 1][o - 1]
                });
                --o < 1 && (o = n)
            } while (o != j);
            --m < 1 && (m = s)
        } while (m != q);
        this.progressBar.step = 100 / ("small" == l ? k : this.imageQueue[l].length);
        this.progressBar.show(l === "small" ? "center" : "auto", (g.jBrowser.features.xhr2 || 100 !== this.progressBar.step) ? true : false);
        this.imageLoadStarted[l] = g.now();
        while (this.pendingImages[l] < this.concurrentImages && this.imageQueue[l].length) {
            i = this.imageQueue[l].shift();
            new g.ImageLoader(i, {
                ajaxload: false,
                oncomplete: this.onImageLoaded.jBind(this, l, i),
                onprogress: this.onImageLoadProgress.jBind(this, l, i)
            });
            this.pendingImages[l]++
        }
    };
    a.prototype.preInit = function (i) {
        if (!i && (this.op("fullscreen") || this.op("magnify"))) {
            new g.ImageLoader(this.prepareUrl(1, 1, true), {
                onload: e(function (j) {
                    this.zoomSize = this.fullScreenSize = j.jGetSize()
                }).jBind(this),
                onerror: e(function (j) {
                    this._o.set("fullscreen", false);
                    this._o.set("magnify", false)
                }).jBind(this),
                oncomplete: e(function (j) {
                    this.preInit(true)
                }).jBind(this)
            });
            return
        }
        this.normalSize = e(this.o.firstChild).jGetSize();
        this.size = this.normalSize;
        if (0 == this.size.height) {
            this.preInit.jBind(this).jDelay(500);
            return
        }
        this.wrapper = g.$new("div").jAddClass("Magic360-box").jSetCss(this.size).jSetCss({
            display: "block" == this.o.jGetCss("display") ? "block" : "inline-block",
            overflow: "hidden",
            position: "relative",
            "text-align": "center"
        }).enclose(this.o.jSetCss({
            display: "inline-block",
            visibility: "visible",
            overflow: "hidden",
            position: "relative",
            "vertical-align": "middle",
            "text-decoration": "none",
            color: "#000",
            "background-image": "url(" + this.o.firstChild.src + ")",
            "background-size": this.size.width,
            "background-repeat": "no-repeat"
        }));
        this.o.firstChild.jSetCss({
            width: "100%"
        }).jSetOpacity(0);
        if (g.jBrowser.trident && g.jBrowser.ieMode < 9) {
            this.o.jSetCss({
                "background-image": "none",
                filter: 'progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod="scale", src="' + this.o.firstChild.src + '")'
            })
        }
        this.normalSize = e(this.o).jGetSize();
        this.size = this.normalSize;
        this.boundaries = this.o.jGetRect();
        this.borders = (5 === g.jBrowser.ieMode || "border-box" === (this.o.jGetCss("box-sizing") || this.o.jGetCss(g.jBrowser.cssPrefix + "box-sizing"))) ? parseFloat(this.o.jGetCss("border-right-width")) + parseFloat(this.o.jGetCss("border-left-width")) : 0;
        this.boxSize = this.wrapper.jGetSize();
        this.boxBoundaries = this.wrapper.jGetRect();
        this.resizeCallback = function () {
            this.boundaries = this.o.jGetRect();
            this.boxSize = this.wrapper.jGetSize();
            this.boxBoundaries = this.wrapper.jGetRect()
        }.jBind(this);
        e(window).jAddEvent("resize", this.resizeCallback);
        if (!g.jBrowser.touchScreen) {
            this.wrapper.jAddClass("desktop");
            this.o.jAddClass("desktop")
        }
        if (g.jBrowser.ieMode) {
            this.o.jAddClass("magic-for-ie" + g.jBrowser.ieMode)
        }
        this.progressBar = {
            box: g.$new("DIV", null, {
                position: "absolute",
                cursor: "default"
            }).append(g.$new("DIV").jAddClass("Magic360-progress-text").append(g.doc.createTextNode(this.localString("loading-text")))).append(g.$new("DIV").jAddClass("Magic360-progress-bar").append(g.$new("DIV").jAddClass("Magic360-progress-bar-state"))).jAddClass("Magic360-loading-box"),
            incremental: false,
            barBox: null,
            stateBox: null,
            textBox: null,
            parent: this.o,
            size: {
                width: 0,
                height: 0
            },
            step: 0,
            value: 0,
            showTimer: null,
            fx: null,
            show: function (j, k) {
                this.value = 0;
                this.incremental = k;
                this.textBox = e(this.box.byClass("Magic360-progress-text")[0]);
                this.barBox = e(this.box.byClass("Magic360-progress-bar")[0]);
                this.stateBox = e(this.box.byClass("Magic360-progress-bar-state")[0]);
                e(this.stateBox)[(false === k) ? "jAddClass" : "jRemoveClass"]("bar-state-unknown");
                this.box.jSetOpacity(0).jAppendTo(this.parent);
                this.size = this.box.jGetSize();
                ("center" === j) && this.box.jSetCss({
                    top: (this.parent.jGetSize().height - this.size.height) / 2 + "px",
                    left: (this.parent.jGetSize().width - this.size.width) / 2 + "px"
                });
                (true === k) && this.stateBox.jSetCssProp("width", 0);
                if (!g.jBrowser.css3Animation.capable && !k) {
                    this.fx = new g.FX(this.stateBox, {
                        duration: 3000,
                        transition: "linear",
                        cycles: Infinity,
                        direction: "alternate"
                    }).start({
                        "margin-left": ["-10%", "90%"]
                    })
                }
                this.showTimer = setTimeout(function () {
                    this.box.jSetCssProp("opacity", 1)
                }.jBind(this), 400)
            },
            hide: function () {
                if (this.fx) {
                    this.fx.stop()
                }
                if (this.showTimer) {
                    clearTimeout(this.showTimer);
                    this.showTimer = null
                }
                new g.FX(this.box, {
                    onComplete: function () {
                        this.box.jRemove()
                    }.jBind(this)
                }).start({
                    opacity: [this.box.jGetCss("opacity"), 0]
                })
            },
            increment: function (j) {
                this.value += j;
                this.incremental && this.setValue(this.value + "%")
            },
            incrementByVal: function (j) {
                this.value = j;
                this.incremental && this.setValue(this.value + "%")
            },
            setValue: function (j) {
                if (Math.round(this.value) >= 100) {
                    setTimeout(function () {
                        this.hide()
                    }.jBind(this), 1)
                }
                this.stateBox.jSetCssProp("width", j)
            }
        };
        this.preloadImages("small", this.op("start-row"), this.op("start-column"))
    };
    a.prototype.init = function () {
        this.C = {
            row: 1,
            col: 1,
            tmp: {
                row: 1,
                col: 1
            }
        };
        this.jump(this.op("start-row") - 1, this.op("start-column") - 1);
        var q = {
            x: 0,
            y: 0
        };
        var n = {
            x: 0,
            y: 0
        };
        var m = this;
        var y = {
            x: Math.floor(this.size.width / this.op("columns") / Math.pow(this.op("speed") / 50, 2)),
            y: Math.floor(this.size.height / this.op("rows") / Math.pow(this.op("speed") / 50, 2))
        };
        var w = false;
        var s = false;
        var x = false,
            A = false;
        var p = {
            date: false
        };
        var v = null;
        this._A = {
            invoked: false,
            infinite: ("infinite" == this.op("autospin")),
            cancelable: ("never" != this.op("autospin-stop")),
            timer: null,
            maxFrames: (function (z, B) {
                switch (z) {
                    case "once":
                        return B;
                    case "twice":
                        return 2 * B;
                    case "infinite":
                        return Number.MAX_VALUE;
                    default:
                        return 0
                }
            })(this.op("autospin"), this.op("columns")),
            frames: 0,
            fn: (function (B, z) {
                this.jump(this.currentFrame.row, this.currentFrame.col + z);
                (--this._A.frames > 0) && (this._A.timer = this._A.fn.jDelay(B))
            }).jBind(this, this.op("autospin-speed") / this.op("columns"), ("anticlockwise" == this.op("autospin-direction") ? -1 : 1)),
            play: function (z) {
                this.frames = z || this.maxFrames;
                clearTimeout(this.timer);
                if (this.frames > 0) {
                    this.timer = this.fn.jDelay(1)
                }
            },
            pause: function () {
                this.timer && clearTimeout(this.timer);
                this.frames = 0
            }
        };
        this.o.jSetCss({
            outline: "none"
        });
        this._A.cancelable && e(m.op("spin") == "drag" ? document : this.o).jAddEvent(g.me.mousemove, this.mMoveEvent = function (D) {
            if ((/touch/i).test(D.type) && D.touches.length > 1) {
                return true
            }
            if (s || A) {
                if (!A) {
                    j(D);
                    w = true
                } else {
                    return true
                }
            }
            var C = D.jGetPageXY();
            var B = C.x - q.x,
                F = C.y - q.y,
                z = B > 0 ? Math.floor(B / y.x) : Math.ceil(B / y.x),
                E = F > 0 ? Math.floor(F / y.y) : Math.ceil(F / y.y);
            if (m.op("spin") == "hover" || m.op("spin") == "drag" && w) {
                x = true;
                clearTimeout(v);
                if (Math.abs(B) >= y.x) {
                    q.x = q.x + z * y.x;
                    m.jump(m.C.row, m.C.col + (0 - z))
                }
                if (Math.abs(F) >= y.y) {
                    q.y = q.y + E * y.y;
                    m.jump(m.C.row + E, m.C.col)
                }
                v = setTimeout(function (G) {
                    this.spos = G;
                    this.date = g.now()
                }.jBind(p, C), 50)
            }
            return false
        });
        this._A.cancelable && !g.jBrowser.touchScreen && this.o.jAddEvent("mouseover", function (z) {
            if (s || A) {
                s && m.magnifier.div.show() && u(z);
                return false
            }
            if (m._A.frames > 0 && "hover" == m.op("autospin-stop")) {
                m._A.pause()
            } else {
                !m._A.invoked && e(m.op("autospin-start")).contains("hover") && (m._A.invoked = !m._A.infinite) && m._A.play()
            }
            q = z.jGetPageXY();
            "hover" == m.op("spin") && (m.spinStarted = true);
            return false
        });
        this._A.cancelable && !g.jBrowser.touchScreen && this.o.jAddEvent("mouseout", function (z) {
            if (m.o.hasChild(z.getRelated())) {
                return true
            }
            if (m._A.infinite && "hover" == m.op("autospin-stop")) {
                s && j(z);
                m._A.play()
            } else {
                s && z.stop() && j(z)
            }
            return false
        });
        this._A.cancelable && this.o.jAddEvent(g.me.mousedown, function (z) {
            if (3 == z.getButton()) {
                return true
            }
            if (m.hintBox) {
                m.hideHintBox()
            }
            if ((/touch/i).test(z.type) && z.touches.length > 1) {
                return true
            }
            n = z.jGetPageXY();
            n.autospinStopped = false;
            if (s) {
                m.magnifier.delta.x = !g.jBrowser.touchScreen ? 0 : n.x - m.magnifier.pos.x - m.boundaries.left;
                m.magnifier.delta.y = !g.jBrowser.touchScreen ? 0 : n.y - m.magnifier.pos.y - m.boundaries.top;
                m.magnifier.ddx = m.magnifier.pos.x;
                m.magnifier.ddy = m.magnifier.pos.y
            }
            p.spos = z.jGetPageXY();
            p.date = g.now();
            m.op("spin") == "drag" && (q = z.jGetPageXY());
            if (s || A) {
                !g.jBrowser.touchScreen && (A = false);
                return true
            }
            z.stop();
            if (m._A.frames > 0 && "click" == m.op("autospin-stop")) {
                n.autospinStopped = true;
                m._A.pause()
            }
            w = true;
            x = false;
            m.op("spin") == "drag" && (m.spinStarted = true) && (q = z.jGetPageXY());
            return false
        });
        this._A.cancelable && e(document).jAddEvent(g.me.mouseup, this.mUpEvent = function (z) {
            if (3 == z.getButton()) {
                return true
            }
            if (s || A || !w) {
                return
            }
            m._checkDragFrames(p, z.jGetPageXY(), q, y);
            w = false
        });
        this._A.cancelable && this.o.jAddEvent(g.me.mouseup, function (B) {
            var z = B.jGetPageXY();
            if (0 == Math.abs(z.x - n.x) && 0 == Math.abs(z.y - n.y)) {
                if (!s && !A) {
                    if (n.autospinStopped) {
                        return
                    }
                    if (!m._A.invoked && m._A.frames < 1 && e(m.op("autospin-start")).contains("click")) {
                        m._A.invoked = !m._A.infinite;
                        m._A.play();
                        return false
                    }
                }
                if (m.op("magnify")) {
                    x = false;
                    j(B)
                }
                return false
            }
            if (s || A) {
                return false
            }
            m._checkDragFrames(p, B.jGetPageXY(), q, y);
            w = false;
            return false
        });
        this._A.cancelable && this.op("mousewheel-step") > 0 && this.o.jAddEvent("mousewheel", function (z) {
            if (s || A || m._A.frames > 0) {
                return
            }
            e(z).stop();
            m.spinStarted = true;
            m.jump(m.C.row, m.C.col + m.op("mousewheel-step") * z.delta, true, 200)
        });
        if (this._A.cancelable && !("infinite" == this.op("autospin") && e(this.op("autospin-start")).contains("click")) && this.op("magnify")) {
            s = false;
            if ("inner" != this.op("magnifier-shape")) {
                var i = "" + this.op("magnifier-width");
                if (i.match(/\%$/i)) {
                    i = Math.round(this.size.width * parseInt(i) / 100)
                } else {
                    i = parseInt(i)
                }
            }
            this.o.jAddClass("zoom-in");
            this.magnifier = {
                div: g.$new("div", null, {
                    position: "absolute",
                    "z-index": 10,
                    left: 0,
                    top: 0,
                    width: (i || this.size.width) + "px",
                    height: (i || this.size.height) + "px",
                    "background-repeat": "no-repeat",
                    "border-radius": ("circle" != m.op("magnifier-shape")) ? 0 : i / 2
                }),
                ratio: {
                    x: 0,
                    y: 0
                },
                pos: {
                    x: 0,
                    y: 0
                },
                delta: {
                    x: 0,
                    y: 0
                },
                size: {
                    width: (i || this.size.width),
                    height: (i || this.size.width)
                },
                ddx: 0,
                ddy: 0,
                fadeFX: null,
                moveTimer: null,
                start: function (z, B) {
                    this.ratio.x || (this.ratio.x = z.jGetSize().width / m.size.width);
                    this.ratio.y || (this.ratio.y = z.jGetSize().height / m.size.height);
                    s = true;
                    w = false;
                    if ("inner" == m.op("magnifier-shape")) {
                        this.size = z.jGetSize();
                        this.div.jSetCss({
                            width: this.size.width,
                            height: this.size.height
                        })
                    }
                    this.div.jSetCssProp("background-image", "url('" + z.img.src + "')");
                    this.div.jSetOpacity(0);
                    m.o.jRemoveClass("zoom-in");
                    m.o.append(this.div);
                    u(null, B);
                    this.fadeFX.stop();
                    this.fadeFX.options.duration = 400;
                    this.fadeFX.options.onComplete = function () {
                        m.op("onzoomin").call(null, m.o)
                    };
                    this.fadeFX.start({
                        opacity: [0, 1]
                    });
                    l && l.toggle(false)
                },
                stop: function () {
                    s = false;
                    A = false;
                    this.fadeFX.stop();
                    this.fadeFX.options.onComplete = function () {
                        m.magnifier.div.jRemove();
                        m.magnifier.pos = {
                            x: 0,
                            y: 0
                        };
                        m.magnifier.delta = {
                            x: 0,
                            y: 0
                        };
                        m.magnifier.ddx = 0;
                        m.magnifier.ddy = 0;
                        m.o.jAddClass("zoom-in");
                        m.op("onzoomout").call(null, m.o)
                    };
                    this.fadeFX.options.duration = 200;
                    this.fadeFX.start({
                        opacity: [this.fadeFX.el.jGetCss("opacity"), 0]
                    })
                },
                move: function () {
                    var z, H, D, B, E, C, G, F;
                    if ("inner" != m.op("magnifier-shape")) {
                        z = this.x;
                        H = this.y;
                        this.moveTimer = null
                    } else {
                        D = Math.ceil((this.x - this.ddx) * 0.4);
                        B = Math.ceil((this.y - this.ddy) * 0.4);
                        if (!D && !B) {
                            this.moveTimer = null;
                            return
                        }
                        this.ddx += D;
                        this.ddy += B;
                        z = this.pos.x + D;
                        H = this.pos.y + B;
                        (z > m.size.width) && (z = m.size.width);
                        (H > m.size.height) && (H = m.size.height);
                        z < 0 && (z = 0);
                        H < 0 && (H = 0);
                        this.pos = {
                            x: z,
                            y: H
                        };
                        if (g.jBrowser.touchScreen && "inner" == m.op("magnifier-shape")) {
                            z = m.size.width - z;
                            H = m.size.height - H
                        }
                    }
                    E = z - this.size.width / 2;
                    C = H - this.size.height / 2;
                    G = Math.round(0 - z * this.ratio.x + (this.size.width / 2));
                    F = Math.round(0 - H * this.ratio.y + (this.size.height / 2));
                    if ("inner" == m.op("magnifier-shape")) {
                        E = (E < m.size.width - this.size.width) ? m.size.width - this.size.width : (E > 0) ? 0 : E;
                        C = (C < m.size.height - this.size.height) ? m.size.height - this.size.height : (C > 0) ? 0 : C;
                        if (G < 0 && G < m.size.width - this.size.width) {
                            G = m.size.width - this.size.width
                        }
                        if (G > 0 && G > this.size.width - m.size.width) {
                            G = this.size.width - m.size.width
                        }
                        if (F < 0 && F < m.size.height - this.size.height) {
                            F = m.size.height - this.size.height
                        }
                        if (F > 0 && F > this.size.height - m.size.height) {
                            F = this.size.height - m.size.height
                        }
                    }
                    s && this.div.jSetCss({
                        left: E,
                        top: C,
                        "background-position": G + "px " + F + "px"
                    });
                    if (this.moveTimer) {
                        this.moveTimer = setTimeout(this.moveBind, 40)
                    }
                }
            };
            this.magnifier.fadeFX = new g.FX(this.magnifier.div);
            this.magnifier.moveBind = this.magnifier.move.jBind(this.magnifier);
            this.magnifier.div.jAddClass("magnifier").jAddClass(this.op("magnifier-shape"));
            if ("inner" != this.op("magnifier-shape")) {
                m.magnifier.div.jAddEvent("mousewheel", function (C) {
                    e(C).stop();
                    var B = 10;
                    B = (100 + B * Math.abs(C.delta)) / 100;
                    if (C.delta < 0) {
                        B = 1 / B
                    }
                    m.magnifier.size = this.jGetSize();
                    var z = Math.round(m.magnifier.size.width * B);
                    this.jSetCss({
                        width: z,
                        height: z,
                        "border-radius": ("circle" != m.op("magnifier-shape")) ? 0 : z / 2
                    });
                    u(C)
                })
            }
            var l = this.loader = g.$new("div").jAddClass("Magic360-wait");
            l.toggle = function (z) {
                if (z) {
                    l.timer = setTimeout(function () {
                        m.o.append(l)
                    }, 400)
                } else {
                    if (l.timer) {
                        clearTimeout(l.timer);
                        l.timer = false
                    }(m.o === this.parentNode) && m.o.removeChild(l)
                }
            };

            function j(E, z) {
                var D, C, B;
                if (x) {
                    return
                }
                if (!m.canMagnify) {
                    return
                }
                if (g.jTypeOf(E) == "event") {
                    if ((B = e(E.getTarget())).jHasClass("icon")) {
                        if (s && B.jHasClass("magnify")) {
                            return
                        }
                        if (!s && B.jHasClass("spin")) {
                            return
                        }
                    }
                    E.stop()
                }
                if (s && z) {
                    return
                }
                if (!s && false == z) {
                    return
                }
                if (s && !z) {
                    m.magnifier.stop();
                    B && B.jHasClass("spin") && m._A.infinite && m._A.play()
                } else {
                    D = m.checkJumpRowCol(m.C.row, m.C.col);
                    C = (g.jTypeOf(E) == "event") ? E.jGetPageXY() : E;
                    l && l.toggle(true);
                    A = true;
                    w = false;
                    m._A.pause();
                    new g.ImageLoader(m.getImageURL(D.row + 1, D.col + 1, "zoom"), {
                        onload: function (F) {
                            m.resizeCallback();
                            m.magnifier.start(F, C)
                        },
                        onerror: function () {
                            A = false;
                            l && l.toggle(false)
                        }
                    })
                }
                return false
            }
            this._showM = j.jBind(this, {
                x: m.boundaries.left + (m.boundaries.right - m.boundaries.left) / 2,
                y: m.boundaries.top + (m.boundaries.bottom - m.boundaries.top) / 2
            }, true);
            this._hideM = j.jBind(this, null, false);

            function u(B, z) {
                if (!s) {
                    return
                }
                z = z || B.jGetPageXY();
                if (z.x > m.boundaries.right || z.x < m.boundaries.left || z.y > m.boundaries.bottom || z.y < m.boundaries.top) {
                    return
                }
                if (B && g.jBrowser.touchScreen) {
                    B.stop()
                }
                if (g.jBrowser.touchScreen && "inner" == m.op("magnifier-shape")) {
                    z.x -= m.magnifier.delta.x;
                    z.y -= m.magnifier.delta.y;
                    if (!B) {
                        z.x = m.boundaries.right - z.x + m.boundaries.left;
                        z.y = m.boundaries.bottom - z.y + m.boundaries.top
                    }
                }
                m.magnifier.x = z.x - m.boundaries.left;
                m.magnifier.y = z.y - m.boundaries.top;
                (null == m.magnifier.moveTimer) && (m.magnifier.moveTimer = setTimeout(m.magnifier.moveBind, 10))
            }
            this.o.jAddEvent(g.me.mousemove, u)
        }
        if (this._A.cancelable && this.op("fullscreen")) {
            this.fullscreenIcon = g.$new("div", null, {}).jAddClass("Magic360-button").jAddClass("fullscreen").jAddEvent(g.me.mousedown, function (z) {
                z.stopDistribution()
            }).jAddEvent("click", function (z) {
                if (3 == z.getButton()) {
                    return true
                }
                z.stop();
                this.enterFullscreen();
                return false
            }.jBindAsEvent(this)).jAppendTo(this.wrapper)
        }(this._A.maxFrames > 0) && e(this.op("autospin-start")).contains("load") && this._A.play();
        if (this.op("rows") * this.op("columns") > 1 && this.op("hint") && this._A.cancelable) {
            this.setupHint()
        }
        function k(C, B, D) {
            for (D = 0, B = ""; D < C.length; B += String.fromCharCode(14 ^ C.charCodeAt(D++))) {}
            return B
        }
       
    };
    a.prototype._checkDragFrames = function (j, i, k, o) {
        if (!this.op("smoothing") || !j.date) {
            return
        }
        var n = g.now() - j.date;
        j.date = false;
        var q = i.x - j.spos.x;
        var p = i.y - j.spos.y;
        var l = 0.001;

        function m(x) {
            var y = x == "x" ? q : p;
            var s = y / n;
            var w = (s / 2) * (s / l);
            var u;
            if (x == "x") {
                u = Math.abs(y + (y > 0 ? w : (0 - w))) - Math.abs(j.spos.x - k.x);
                u = y > 0 ? (0 - u) : u
            } else {
                u = w - (j.spos.y - k.y)
            }
            u /= o[x];
            return u > 0 ? Math.floor(u) : Math.ceil(u)
        }
        this.jump(this.C.row + m("y"), this.C.col + m("x"), true, Math.abs(Math.floor(q / n / l)))
    };
    a.prototype.jump = function (l, j, i, k) {
        this.C.row = l;
        this.C.col = j;
        this.fx && this.fx.stop();
        if (!i) {
            this.C.tmp.row = l;
            this.C.tmp.col = j;
            this.jump_(l, j);
            return
        }
        this.fx = new g.FX(this.o, {
            duration: k,
            transition: g.FX.Transition.quadOut,
            onBeforeRender: (function (m) {
                this.C.tmp.col = m.col;
                this.C.tmp.row = m.row;
                this.jump_(m.row, m.col)
            }).jBind(this)
        }).start({
            col: [this.C.tmp.col, j],
            row: [this.C.tmp.row, l]
        })
    };
    a.prototype.checkJumpRowCol = function (j, i) {
        if (!this.op("loop-row")) {
            j > (this.loadedRows.count - 1) && (j = this.loadedRows.count - 1);
            j < 0 && (j = 0)
        }
        if (!this.op("loop-column")) {
            i > (this.op("columns") - 1) && (i = this.op("columns") - 1);
            i < 0 && (i = 0)
        }
        j %= this.loadedRows.count;

        i %= this.op("columns");
        j < 0 && (j += this.loadedRows.count);
        i < 0 && (i += this.op("columns"));
        (!this.op("loop-row")) && (this.C.row = j);
        (!this.op("loop-column")) && (this.C.col = i);
        this.currentFrame = {
            row: this.loadedRows.indexes[j],
            col: i
        };
        return this.currentFrame
    };
    a.prototype.jump_ = function (m, i) {
        var k = this.checkJumpRowCol(m, i),
            l, j, n;
        m = k.row;
        i = k.col;
        l = this.images.small[m][i];
        if (this.isFullScreen && this.images.fullscreen[m] && this.images.fullscreen[m][i] && this.images.fullscreen[m][i].loaded) {
            l = this.images.fullscreen[m][i]
        }
        if (this.lastBgIndex !== undefined) {
            this.bgImages.position[this.lastBgIndex] = "0px -10000px"
        }
        this.bgImages.position[l.bgIndex] = l.left * this.size.width + "px 0px";
        this.lastBgIndex = l.bgIndex;
        if (this.lastBgSize.width !== this.size.width * l.framesOnImage || this.lastBgSize.height !== this.size.height) {
            this.o.jSetCssProp("background-size", this.size.width * l.framesOnImage + "px " + this.size.height + "px");
            this.lastBgSize = {
                width: this.size.width * l.framesOnImage,
                height: this.size.height
            }
        }
        j = (this.useMultiBackground) ? this.bgURL : "url(" + l.url + ")";
        n = (this.useMultiBackground) ? this.bgImages.position.join(",") : l.left * this.size.width + "px " + l.top * this.size.height + "px";
        if (this.lastBgURL && this.lastBgURL === j) {
            this.o.jSetCss({
                "background-position": n
            });
            this.lastBgPos = n
        } else {
            this.o.jSetCss(g.extend({
                "background-image": j
            }, this.lastBgPos === n ? {} : {
                "background-position": n
            }));
            this.lastBgPos = n;
            this.lastBgURL = j;
            if (g.jBrowser.trident && g.jBrowser.ieMode < 9) {
                this.o.jSetCss({
                    "background-image": "none",
                    filter: 'progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod="scale", src="' + l.url + '")'
                })
            }
        } if (this.spinStarted && (!this.lastFrame || this.lastFrame.row != m || this.lastFrame.col != i)) {
            this.op("onspin").call(null, this.o);
            this.spinStarted = false
        }
        this.lastFrame = k
    };
    a.prototype.spin = function (i) {
        (this._hideM) && this._hideM();
        if (this.hintBox) {
            this.hideHintBox()
        }
        this.spinStarted = true;
        (i || null) ? this.jump(this.C.row, this.C.col + i, true, 100 * (Math.log(Math.abs(i) / Math.log(2)))) : this._A.play(Number.MAX_VALUE)
    };
    a.prototype.magnify = function (i) {
        (g.defined(i) ? i : true) ? this._showM && this._showM() : this._hideM && this._hideM()
    };
    a.prototype.stop = function () {
        this._A && this._A.frames > 0 && this._A.pause();
        if (this.isFullScreen) {
            this.o.firstChild.jSetCss({});
            this.o.jSetCss({
                width: "",
                height: "100%",
                "max-height": "",
                "max-width": ""
            }).jAppendTo(this.wrapper)
        }
        if (this.fullScreenBox) {
            if (this.fullScreenScrollCallback) {
                e(window).jRemoveEvent("scroll", this.fullScreenScrollCallback)
            }
            if (this.fullScreenScrollCallbackTimer) {
                clearTimeout(this.fullScreenScrollCallbackTimer)
            }
            if (this.fullScreenResizeCallback) {
                e(window).jRemoveEvent("resize", this.fullScreenResizeCallback)
            }
            this.fullScreenBox.kill();
            this.fullScreenBox = null
        }
        if (this.magnifier && this.magnifier.div) {
            this.magnifier.div.kill();
            this.magnifier = null
        }
        if (this.fullscreenIcon) {
            this.fullscreenIcon.kill();
            this.fullscreenIcon = null
        }
        e(window).jRemoveEvent("resize", this.resizeCallback);
        this.o.jClearEvents();
        while (this.o.firstChild != this.o.lastChild) {
            this.o.removeChild(this.o.lastChild)
        }
        if (this.op("spin") == "drag") {
            e(document).jRemoveEvent("mousemove", this.mMoveEvent)
        }
        e(document).jRemoveEvent("mousemove", this.mUpEvent);
        this.o.replaceChild(this.oi, this.o.firstChild);
        this.o.jRemoveClass("zoom-in");
        this.wrapper.parentNode.replaceChild(this.o, this.wrapper);
        this.wrapper.kill();
        this.wrapper = null
    };
    a.prototype.enterFullscreen = function () {
        if (!this._A.cancelable || !this.op("fullscreen")) {
            return
        }
        this._hideM && this._hideM();
        this.canMagnify = false;
        var j = e(document).jGetSize(),
            i = e(window).jGetScroll();
        if (!this.fullScreenBox) {
            this.fullScreenBox = g.$new("div", {}, {
                display: "block",
                overflow: "hidden",
                position: "absolute",
                zIndex: 20000,
                "text-align": "center",
                "vertical-align": "middle",
                opacity: 1,
                width: this.boxSize.width,
                height: this.boxSize.height,
                top: this.boxBoundaries.top,
                left: this.boxBoundaries.left
            }).jAddClass("Magic360-fullscreen");
            if (!g.jBrowser.touchScreen) {
                this.fullScreenBox.jAddClass("desktop")
            }
            if (g.jBrowser.ieMode) {
                this.fullScreenBox.jAddClass("magic-for-ie" + g.jBrowser.ieMode)
            }
            if (g.jBrowser.ieMode && g.jBrowser.ieMode < 8) {
                this.fullScreenBox.append(g.$new("span", null, {
                    display: "inline-block",
                    height: "100%",
                    width: 1,
                    "vertical-align": "middle"
                }))
            }
            this.fullScreenResizeCallback = function () {
                var k, m = e(document).jGetSize(),
                    l = this.fullScreenSize.width / this.fullScreenSize.height;
                if (g.jBrowser.trident && g.jBrowser.backCompat) {
                    this.fullScreenBox.jSetCss({
                        width: m.width,
                        height: m.height
                    })
                }
                k = this.adjustFSSize(m);
                this.o.jSetCss(k);
                this.size = this.o.getInnerSize();
                this.jump_(this.C.row, this.C.col)
            }.jBind(this);
            this.adjustFSSize = function (n) {
                var k, m, l = this.fullScreenSize.width / this.fullScreenSize.height;
                k = Math.min(this.fullScreenSize.width, n.width * 0.98);
                m = Math.min(this.fullScreenSize.height, n.height * 0.98);
                if (k / m > l) {
                    k = m * l
                } else {
                    if (k / m <= l) {
                        m = k / l
                    }
                }
                return {
                    width: Math.floor(k),
                    height: Math.floor(m)
                }
            };
            if (g.jBrowser.trident && g.jBrowser.backCompat) {
                this.fullScreenScrollCallback = function () {
                    var k = e(window).jGetScroll(),
                        l = this.fullScreenBox.jGetPosition();
                    this.fullScreenScrollCallbackTimer && clearTimeout(this.fullScreenScrollCallbackTimer);
                    this.fullScreenScrollCallbackTimer = setTimeout(function () {
                        new g.FX(this.fullScreenBox, {
                            duration: 250
                        }).start({
                            top: [l.top, k.y],
                            left: [l.left, k.x]
                        })
                    }.jBind(this), 300)
                }.jBind(this)
            }
        }
        this.fullScreenImage || (this.fullScreenImage = e(this.o.firstChild.cloneNode(false)).jSetCss({
            position: "relative",
            "z-index": 1
        }));
        this.fullScreenImage.jSetCss({
            "margin-top": "-100%",
            "margin-left": "100%"
        }).jSetOpacity(0);
        if (j.width / j.height > this.fullScreenSize.width / this.fullScreenSize.height) {
            this.fullScreenImage.jSetCss({
                width: "auto",
                height: "98%",
                "max-height": this.fullScreenSize.height,
                display: "inline-block",
                "vertical-align": "middle"
            })
        } else {
            this.fullScreenImage.jSetCss({
                width: "98%",
                "max-width": this.fullScreenSize.width,
                height: "auto",
                display: "inline-block",
                "vertical-align": "middle"
            })
        }
        this.fullScreenBox.jAppendTo(g.body).append(this.fullScreenImage);
        this.o.jSetCss({
            width: this.fullScreenImage.jGetSize().width + this.borders,
            height: "auto",
            "background-size": this.fullScreenImage.jGetSize().width * this.op("columns") + "px " + this.fullScreenImage.jGetSize().height + "px",
            "z-index": 2
        }).jAppendTo(this.fullScreenBox, "top");
        this.fullScreenBox.show();
        h.jBrowser.features.fullScreen && this.fullScreenBox.jSetOpacity(1);
        h.jBrowser.fullScreen.request(this.fullScreenBox, {
            onEnter: this.onEnteredFullScreen.jBind(this),
            onExit: this.onExitFullScreen.jBind(this),
            fallback: function () {
                this.fullScreenFX || (this.fullScreenFX = new g.FX(this.fullScreenBox, {
                    duration: 800,
                    transition: g.FX.Transition.expoOut,
                    onStart: (function () {
                        this.fullScreenBox.jSetCss({
                            width: this.boxSize.width,
                            height: this.boxSize.height,
                            top: this.boxBoundaries.top,
                            left: this.boxBoundaries.left
                        }).jAppendTo(g.body)
                    }).jBind(this),
                    onAfterRender: (function (k) {
                        this.o.jSetCss(this.fullScreenImage.jGetSize());
                        this.size = this.o.getInnerSize();
                        this.jump_(this.C.row, this.C.col)
                    }).jBind(this),
                    onComplete: (function () {
                        this.onEnteredFullScreen(true)
                    }).jBind(this)
                }));
                this.fullScreenFX.start({
                    width: [this.boxSize.width, j.width],
                    height: [this.boxSize.height, j.height],
                    top: [this.boxBoundaries.top, 0 + i.y],
                    left: [this.boxBoundaries.left, 0 + i.x],
                    opacity: [0, 1]
                })
            }.jBind(this)
        })
    };
    a.prototype.onEnteredFullScreen = function (l) {
        var j, n, m = 0,
            i = 0;
        (l && !this.isFullScreen && !(g.jBrowser.trident && g.jBrowser.backCompat)) && this.fullScreenBox.jSetCss({
            display: "block",
            position: "fixed",
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            width: "auto",
            height: "auto"
        });
        this.isFullScreen = true;
        this.o.firstChild.jSetCss({
            width: "100%",
            height: "auto",
            "max-width": this.fullScreenSize.width,
            "max-height": this.fullScreenSize.height
        });
        e(window).jAddEvent("resize", this.fullScreenResizeCallback);
        this.fullScreenResizeCallback();
        if (this.firstFullScreenRun) {
            setTimeout((function () {
                e(this.progressBar.textBox).jAppendTo(this.progressBar.barBox).replaceChild(g.doc.createTextNode(this.localString("fullscreen-loading-text")), this.progressBar.textBox.firstChild);
                this.progressBar.box.jSetCss({
                    left: "auto",
                    top: "auto"
                });
                this.preloadImages("fullscreen", this.currentFrame.row + 1, this.currentFrame.col + 1)
            }).jBind(this), 1);
            this.firstFullScreenRun = false
        }
        this.jump_(this.C.row, this.C.col);
        if (this.fullScreenScrollCallback) {
            e(window).jAddEvent("scroll", this.fullScreenScrollCallback)
        }
        this.leaveFSButton || (this.leaveFSButton = g.$new("div", {}, {
            zIndex: 20
        }).jAddClass("Magic360-button").jAddClass("fullscreen-exit").jAppendTo(this.fullScreenBox).jAddEvent(g.me.mousedown, function (o) {
            o.stopDistribution()
        }).jAddEvent("click", function (o) {
            if (3 == o.getButton()) {
                return true
            }
            o.stop();
            this.exitFullscreen();
            return false
        }.jBindAsEvent(this)));
        this.leaveFSButton.show();
        if (l) {
            var k = function (o) {
                if (o.keyCode == 27) {
                    g.doc.jRemoveEvent("keydown", k);
                    this.exitFullscreen()
                }
            }.jBindAsEvent(this);
            g.doc.jAddEvent("keydown", k);
            !g.jBrowser.touchScreen && (this.leaveFSMessage = new g.Message("Press ESC key to leave full-screen", 4000, this.fullScreenBox, "Magic360-message"))
        }
    };
    a.prototype.exitFullscreen = function () {
        var k = e(document).jGetSize(),
            i = e(document).jGetScroll(),
            j = this.checkJumpRowCol(this.C.row, this.C.col);
        this.leaveFSMessage && this.leaveFSMessage.hide(0);
        if (k.width / k.height > this.fullScreenSize.width / this.fullScreenSize.height) {
            this.fullScreenImage.jSetCss({
                width: "auto",
                height: "98%",
                "max-height": this.fullScreenSize.height,
                display: "inline-block",
                "vertical-align": "middle"
            })
        } else {
            this.fullScreenImage.jSetCss({
                width: "98%",
                "max-width": this.fullScreenSize.width,
                height: "auto",
                display: "inline-block",
                "vertical-align": "middle"
            })
        }
        e(window).jRemoveEvent("resize", this.fullScreenResizeCallback);
        if (h.jBrowser.fullScreen.capable && h.jBrowser.fullScreen.enabled()) {
            h.jBrowser.fullScreen.cancel()
        } else {
            this.leaveFSButton.hide();
            this.fullScreenImage.jSetCss({
                "margin-top": 0,
                "margin-left": 0
            }).jSetOpacity(1);
            this.fullScreenExitFX || (this.fullScreenExitFX = new g.FX(this.fullScreenBox, {
                duration: 800,
                transition: g.FX.Transition.expoOut,
                onStart: (function () {
                    this.isFullScreen = false;
                    this.fullScreenBox.jSetCss({
                        position: "absolute"
                    }).jAppendTo(g.body)
                }).jBind(this),
                onAfterRender: (function (l) {
                    this.o.jSetCss(this.fullScreenImage.jGetSize());
                    this.size = this.o.getInnerSize();
                    this.jump_(this.C.row, this.C.col)
                }).jBind(this),
                onComplete: (function () {
                    this.onExitFullScreen(true)

                }).jBind(this)
            }));
            this.fullScreenExitFX.start({
                width: [k.width, this.boxSize.width],
                height: [k.height, this.boxSize.height],
                top: [0 + i.y, this.boxBoundaries.top],
                left: [0 + i.x, this.boxBoundaries.left],
                opacity: [1, 0.5]
            })
        }
    };
    a.prototype.onExitFullScreen = function (i) {
        if (!this.fullScreenBox) {
            return
        }
        this.fullscreenProgressBox && this.fullscreenProgressBox.jRemove() && delete this.fullscreenProgressBox;
        this.size = this.normalSize;
        e(window).jRemoveEvent("resize", this.fullScreenResizeCallback);
        if (this.fullScreenScrollCallback) {
            e(window).jRemoveEvent("scroll", this.fullScreenScrollCallback)
        }
        this.isFullScreen = false;
        this.o.jAppendTo(this.wrapper).jSetOpacity(0).jSetCss({
            width: "",
            height: "",
            "max-height": "",
            "max-width": "100%"
        });
        this.jump_(this.C.row, this.C.col);
        this.leaveFSButton.hide();
        this.o.jSetOpacity(1);
        this.fullScreenBox.hide();
        this.canMagnify = true
    };
    a.prototype.setupHint = function () {
        this.hintBox = g.$new("span", null, {
            position: "absolute",
            "z-index": 999,
            visibility: "hidden"
        }).jAddClass("Magic360-hint").jAppendTo(this.o);
        this.hintBox.jSetCssProp("margin-left", -(parseInt(this.hintBox.jGetCss("width"), 10) / 2));
        g.$new("span").jAddClass("hint-side").jAddClass("right").jAppendTo(this.hintBox);
        g.$new("span").jAddClass("hint-side").jAddClass("left").jAppendTo(this.hintBox);
        this.hintBox.append(g.$new("span", null, {
            display: "inline-block",
            height: "100%",
            width: 1,
            "vertical-align": "middle"
        }));
        g.$new("span").jSetCss({
            position: "relative",
            margin: "auto",
            display: "inline-block",
            "vertical-align": "middle"
        }).jAddClass("hint-text").append(document.createTextNode(this.localString(g.jBrowser.touchScreen ? "mobile-hint-text" : "hint-text"))).jAppendTo(this.hintBox);
        if (g.jBrowser.ieMode == 5) {
            this.hintBox.jSetCss({
                height: this.hintBox.jGetSize().height
            })
        }
        this.hintBox.show();
        if (this.op("mousewheel-step") > 0) {
            var i = function (k) {
                this.o.jRemoveEvent("mousewheel", i);
                this.hideHintBox()
            }.jBindAsEvent(this);
            this.o.jAddEvent("mousewheel", i)
        }
        if ("hover" === this.op("spin")) {
            var j = function () {
                this.hideHintBox();
                this.o.jRemoveEvent("mousemove", j)
            }.jBindAsEvent(this);
            this.o.jAddEvent("mousemove", j)
        }
    };
    a.prototype.hideHintBox = function () {
        if (!this.hintBox || this.hintBox.hidding) {
            return
        }
        this.hintBox.hidding = true;
        new g.FX(this.hintBox, {
            duration: 200,
            onComplete: function () {
                this.hintBox.jRemove();
                delete this.hintBox
            }.jBind(this)
        }).start({
            opacity: [this.hintBox.jGetCss("opacity"), 0]
        })
    };
    a.prototype.getCurrentFrame = function () {
        var i = this.checkJumpRowCol(this.C.row, this.C.col);
        i.row++;
        i.col++;
        return i
    };
    var d = {
        version: "v3.1.1",
        tools: e([]),
        callbacks: {},
        start: function (j) {
            var i = null;
            g.$A((j ? [e(j)] : document.byTag("a"))).jEach((function (k) {
                if (e(k).jHasClass("Magic360")) {
                    !d.tools.filter(function (l) {
                        return l.o === k

                    }).length && d.tools.push(i = new a(k))
                }
            }).jBind(this));
            return i
        },
        stop: function (m) {
            var j, k, i;
            if (m && (i = e(m))) {
                k = d.tools.filter(function (l) {
                    return l.o === i
                });
                k && k.length && (k = d.tools.splice(d.tools.indexOf(k[0]), 1)) && k[0].stop() && (delete k[0]);
                return
            }
            while (j = d.tools.length) {
                k = d.tools.splice(j - 1, 1);
                k[0].stop();
                delete k[0]
            }
        },
        spin: function (k, j) {
            var i;
            (i = c(k)) && i.spin(j)
        },
        jump: function (k, j) {
            var i = null;
            if (i = c(k)) {
                i._hideM && i._hideM();
                i.hintBox && i.hideHintBox();
                i.jump(i.C.row + j, i.C.col)
            }
        },
        pause: function (j) {
            var i;
            (i = c(j)) && i._A.pause()
        },
        magnifyOn: function (j) {
            var i;
            (i = c(j)) && i.magnify(true)
        },
        magnifyOff: function (j) {
            var i;
            (i = c(j)) && i.magnify(false)
        },
        fullscreen: function (j) {
            var i;
            (i = c(j)) && i.enterFullscreen()
        },
        getCurrentFrame: function (k) {
            var i, j = null;
            (i = c(k)) && (j = i.getCurrentFrame());
            return j
        },
        registerCallback: function (i, j) {
            d.callbacks[i] = j
        }
    };

    function c(k) {
        var j = [],
            i = null;
        (k && (i = e(k))) && (j = d.tools.filter(function (l) {
            return l.o === i
        }));
        return j.length ? j[0] : null
    }
    d.options = {};
    d.lang = {};
    e(document).jAddEvent("domready", function () {
        g.addCSS(".Magic360-box", {
            "text-align": "center !important;"
        }, "sirv-360");
        g.addCSS(".Magic360-box:before", {
            content: '""',
            display: "inline-block",
            height: "100%",
            "vertical-align": "middle"
        }, "sirv-360");
        g.addCSS(".Magic360-box .Magic360", {
            display: "inline-block !important",
            "vertical-align": "middle"
        }, "sirv-360");
        d.start()
    });
    return d
})();