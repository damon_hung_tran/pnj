jQuery(document).ready(function(){
	/* Hover for Products */
	jQuery('.grid .item').mouseenter (function(){
		jQuery(this).addClass('item-hover');
	});
	jQuery('.grid .item').mouseleave (function(){
		jQuery(this).removeClass('item-hover');
	});
	
	/* Show popup Newsletter */
	jQuery("footer .newsletter a").fancybox({
		overlayColor	: '#003064',
		autoDimensions	: false,
		autoSize		: false,
		fitToView		: false,
		width			: 440,
		height			: 80
	});
	
	/* Show popup Update_function */
	
	jQuery("footer .affiliate a").fancybox({
		overlayColor 	: '#003064',
		fitToView		: false,
        autoSize		: false,
        autoDimensions	: false,
		width			: 600,
		height			: 460 
	});
	
	jQuery("#cbo_category").selectbox({});
	
	var _web_links_status = false;
	jQuery('.web_links .web_link_contain').hide();
	
	jQuery('.web_links span').click(function(){
		if (_web_links_status) {
			_web_links_status = false;
		} else {
			_web_links_status = true;
		}
		show_hide_weblink();
	});
	jQuery('.web_links .web_link_contain a').click(function(){
		_web_links_status = false;
		show_hide_weblink();
	});
	jQuery('.web_links .web_link_contain').mouseleave(function(){
		_web_links_status = false;
		show_hide_weblink();
	});
	
	function show_hide_weblink() {
		if (_web_links_status) {
			jQuery('.web_links .web_link_contain').stop().slideDown(100);
		} else {
			jQuery('.web_links .web_link_contain').stop().slideUp(100);
		}
	}
    
    // Click quickview bottom
    jQuery('.btn-qview').click(function(){
        jQuery('#md_quickview_handler').click();
        return false;
    }); 
});