jQuery(document).ready(function() {
	jQuery('#commitment li').mouseenter(function(){
		jQuery(this).addClass('hover');
	});
	jQuery('#commitment li').mouseleave(function(){
		jQuery(this).removeClass('hover');
	});
	
	jQuery('.header_middle_adv .item').mouseenter(function(){
		jQuery(this).find('div.more a').addClass('active', 500);
	});
	jQuery('.header_middle_adv .item').mouseleave(function(){
		jQuery(this).find('div.more a').removeClass('active', 500);
	});
	
});
