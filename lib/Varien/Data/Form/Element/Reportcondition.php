<?php

class Varien_Data_Form_Element_Reportcondition extends Varien_Data_Form_Element_Abstract
{
    protected $extdata = null;
    public function __construct($attributes = array()){
        parent::__construct($attributes);
        $this->extdata = $attributes;
    }

    public function createConditionBlock(){
        if($this->extdata["condition_block"] == null){
            throw new Exception("Condition block is NULL");
        }

        $block = Mage::app()->getLayout()->createBlock($this->extdata["condition_block"],$this->getId()."condition",$this->extdata);
        return $block;
    }
    public function getElementHtml(){
        return $this->createConditionBlock()->toHtml();
    }
}
