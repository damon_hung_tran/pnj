//payment function starts
var Payment = Class.create();
Payment.prototype = {
	initialize: function(reviewUrl,paymentUrl){
	
	    this.reviewUrl = reviewUrl;
	    this.paymentUrl = paymentUrl;
	
	},
	 //function triggers when payment methods change
    switchMethod: function(method){
    	if (this.currentMethod && $('payment_form_'+this.currentMethod)) {
            this.changeVisible(this.currentMethod, true);
        }
        if ($('payment_form_'+method)){
                this.changeVisible(method, false);
            $('payment_form_'+method).fire('payment-method:switched', {
                method_code : method
            });
        } else {
        //Event fix for payment methods without form like "Check / Money order"
        //document.body.fire('payment-method:switched', {method_code : method});
        }
        this.currentMethod = method;
    },

    changeVisible: function(method, mode) {
        var block = 'payment_form_' + method;
        [block + '_before', block, block + '_after'].each(function(el) {
            element = $(el);;
            if (element) {
                element.style.display = (mode) ? 'none' : '';
                element.select('input', 'select', 'textarea').each(function(field) {
                    field.disabled = mode;
                });
            }
        });
    }
}