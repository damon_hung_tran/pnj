if (!Itoris) {
    var Itoris = {};
}

Itoris.GroupedPromotions = Class.create({
    initialize : function(qtyProducts, qtyCurrentProduct) {
        this.rulesInCheckoutCart();
        this.deleteTitle();
        this.changeQtyFromOtherProducts(qtyProducts);
        for (var i = 0; i < $$('.itoris_groupedproductpromotions_config_add_to_cart').length; i++) {
            Event.observe($$('.itoris_groupedproductpromotions_config_add_to_cart')[i], 'click', this.changeQtyFromCurrentProduct.bind(this, qtyCurrentProduct, $$('.itoris_groupedproductpromotions_config_add_to_cart')[i]));
        }
    },
    deleteTitle : function() {
        if ($$('.box-itoris-product-promotions').length > 1) {
            for (var i = 1; i < $$('.box-itoris-product-promotions').length; i++) {
                if ($$('.box-itoris-product-promotions')[i].select('h2')[0]) {
                    $$('.box-itoris-product-promotions')[i].select('h2')[0].remove();
                }
                if (!$$('.box-itoris-product-promotions')[i].select('form').length) {
                    $$('.box-itoris-product-promotions')[i].hide();
                }
            }
        }
        if (!$$('.box-itoris-product-promotions')[0].select('form').length) {
            $$('.box-itoris-product-promotions')[0].hide();
        }
    },
    rulesInCheckoutCart : function() {
        if ($$('.checkout-cart-index')[0] && $$('.checkout-cart-index')[0].select('.cart')[0] && $$('.box-itoris-product-promotions')[0] && $$('.cart-collaterals')[0]) {
            $$('.checkout-cart-index')[0].select('.cart')[0].insertBefore($$('.box-itoris-product-promotions')[0], $$('.cart-collaterals')[0]);
        }
    },
    changeQtyFromOtherProducts : function(qtyProducts) {
        if (qtyProducts) {
            if ($('super-product-table')) {
                var input = $('super-product-table').select('.input-text.qty');
                for (var i = 0; i < input.length; i++) {
                    var matches = input[i].name.match(/[[0-9]+]/);
                    if (matches.length) {
                        var productId = matches[0].substr(1, matches[0].length - 2);
                    }
                    if (productId && qtyProducts[productId]) {
                        input[i].value = qtyProducts[productId];
                    }
                }
            }
        }
    },
    changeQtyFromCurrentProduct : function(qtyCurrentProduct, buttonConf) {
        if (buttonConf.up('form')) {
            var part = buttonConf.up('form').id.split('_');
            var ruleId = part[part.length - 1];
            var configForUpdate = qtyCurrentProduct[ruleId];
            if ($('super-product-table')) {
                var input = $('super-product-table').select('.input-text.qty');
                for (var i = 0; i < input.length; i++) {
                    var matches = input[i].name.match(/[[0-9]+]/);
                    if (matches.length) {
                        var productId = matches[0].substr(1, matches[0].length - 2);
                    }
                    if (productId && configForUpdate[productId]) {
                        input[i].value = configForUpdate[productId];
                    }
                }
                if (typeof itorisGroupedProduct != 'undefined') {
                    var inputTextQty = $('super-product-table').select('.input-text.qty');
                    for (var i = 0; i < inputTextQty.length; i++) {
                        var acenter = 0;
                        for (var j = 0; j < $('super-product-table').select('.a-center').length; j++) {
                            if ($('super-product-table').select('.a-center')[j] == inputTextQty[i].up('td')) {
                                acenter = j;
                                break;
                            }
                        }
                        itorisGroupedProduct.displayOption(i, acenter);
                    }
                }
                Effect.ScrollTo($('super-product-table'));
            }
        }
    }
});